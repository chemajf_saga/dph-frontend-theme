<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates" %>
<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.diputacion.huelva.frontend.messages">

	<div class="contentblock" id="${param.contentId}">
		<div class="contentblock-texto row">
			<%--Si la imagen va arriba, izquierda o derecha se pinta al principio. Solo si va a abajo se pinta primero el texto --%>
			<c:if test="${contentPrincipal.value.Media.exists && (contentPrincipal.value.Media.value.ImageMain.exists || contentPrincipal.value.Media.value.VideoMain.exists || contentPrincipal.value.Media.value.Ubicacion.exists || contentPrincipal.value.Media.value.FreeMedia.exists || contentPrincipal.value.Media.value.MediaMultiple.exists)}">
				<%-- Cargamos variables necesarias para calcular ancho y posicion segun el tipo de media--%>
				<c:if test="${contentPrincipal.value.Media.value.ImageMain.exists }">
					<c:set var="position">${contentPrincipal.value.Media.value.ImageMain.value.Position }</c:set>
					<c:set var="colwidth">${contentPrincipal.value.Media.value.ImageMain.value.Width }</c:set>
					<c:set var="width">col-sm-${contentPrincipal.value.Media.value.ImageMain.value.Width }</c:set>
					<c:set var="element">image</c:set>
				</c:if>
				<c:if test="${contentPrincipal.value.Media.value.VideoMain.exists }">
					<c:set var="position">${contentPrincipal.value.Media.value.VideoMain.value.Position }</c:set>
					<c:set var="colwidth">${contentPrincipal.value.Media.value.VideoMain.value.Width }</c:set>
					<c:set var="width">col-sm-${contentPrincipal.value.Media.value.VideoMain.value.Width }</c:set>
					<c:set var="element">video</c:set>
				</c:if>
				<c:if test="${contentPrincipal.value.Media.value.Ubicacion.exists }">
					<c:set var="position">${contentPrincipal.value.Media.value.Ubicacion.value.Position }</c:set>
					<c:set var="colwidth">${contentPrincipal.value.Media.value.Ubicacion.value.Width }</c:set>
					<c:set var="width">col-sm-${contentPrincipal.value.Media.value.Ubicacion.value.Width }</c:set>
					<c:set var="element">ubicacion</c:set>
				</c:if>	
				<c:if test="${contentPrincipal.value.Media.value.FreeMedia.exists }">
					<c:set var="position">${contentPrincipal.value.Media.value.FreeMedia.value.Position }</c:set>
					<c:set var="colwidth">${contentPrincipal.value.Media.value.FreeMedia.value.Width }</c:set>
					<c:set var="width">col-sm-${contentPrincipal.value.Media.value.FreeMedia.value.Width }</c:set>
					<c:set var="element">freemedia</c:set>
				</c:if>
				<c:if test="${contentPrincipal.value.Media.value.MediaMultiple.exists }">
					<c:set var="position">${contentPrincipal.value.Media.value.MediaMultiple.value.Position }</c:set>
					<c:set var="colwidth">${contentPrincipal.value.Media.value.MediaMultiple.value.Width }</c:set>
					<c:set var="width">col-sm-${contentPrincipal.value.Media.value.MediaMultiple.value.Width }</c:set>
					<c:set var="element">mediamultiple</c:set>
				</c:if>

				<c:if test="${width == null || width == '' }">
					<c:set var="width">col-sm-6</c:set>
				</c:if>
				
				<c:if test="${position == null || position == '' }">
					<c:set var="position">left</c:set>
				</c:if>
				
				<%--******* CENTERTOP ********--%>
				<c:if test="${position == 'top' || position == 'centertop' }">
					<%-- Para poder centrar el media, tenemos que ver cuando offset hay que añadirle --%>
					<c:choose>
						<c:when test="${colwidth == '12'}"><c:set var="offset"></c:set></c:when>
						<c:when test="${colwidth == '10'}"><c:set var="offset">col-sm-offset-1</c:set></c:when>
						<c:when test="${colwidth == '8'}"><c:set var="offset">col-sm-offset-2</c:set></c:when>
						<c:when test="${colwidth == '6'}"><c:set var="offset">col-sm-offset-3</c:set></c:when>
						<c:when test="${colwidth == '4'}"><c:set var="offset">col-sm-offset-4</c:set></c:when>
						<c:when test="${colwidth == '3'}"><c:set var="offset">col-sm-offset-4</c:set></c:when>
						<c:when test="${colwidth == '2'}"><c:set var="offset">col-sm-offset-5</c:set></c:when>
					</c:choose>
					<c:if test="${element == 'video' }">				
						<div class="video clearfix">
							<div class="media-object <c:out value='${width} ${offset} ' />">
								<div class="wrapper-video">
								${contentPrincipal.value.Media.value.VideoMain.value.Code}
								</div>
							</div>
						</div>
					</c:if>
					<c:if test="${element == 'image' }">
						<div class="image clearfix">
							<div class="wrapper" ${contentPrincipal.value.Media.value.ImageMain.rdfa.Image}>
								<c:set var="cssClass"><c:out value='media-object ${width} ${offset} ' /></c:set>
								<c:if test="${!contentPrincipal.value.Media.value.ImageMain.value.Link.exists}">
									<sg:image imagePath="${contentPrincipal.value.Media.value.ImageMain.value.Image}" footer="${contentPrincipal.value.Media.value.ImageMain.value.Footer}"
										  cssClass="${cssClass}" showBig="${contentPrincipal.value.Media.value.ImageMain.value.ShowBig.toBoolean}"/>
								</c:if>
								<c:if test="${contentPrincipal.value.Media.value.ImageMain.value.Link.exists}">
									<sg:image imagePath="${contentPrincipal.value.Media.value.ImageMain.value.Image}"
										  	footer="${contentPrincipal.value.Media.value.ImageMain.value.Footer}"
											cssClass="${cssClass}"
										  	showBig="${contentPrincipal.value.Media.value.ImageMain.value.ShowBig.toBoolean}"
											thumbnails="${contentPrincipal.value.Media.value.ImageMain.value.Thumbnail.toBoolean}"
											urllink="${contentPrincipal.value.Media.value.ImageMain.value.Link.value.Href}"
											titlelink="${contentPrincipal.value.Media.value.ImageMain.value.Link.value.Title}"
											targetlink="${contentPrincipal.value.Media.value.ImageMain.value.Link.value.Target}"/>
								</c:if>
							</div>
						</div>
					</c:if>
					<c:if test="${element == 'ubicacion' }">
						<c:set var="elem" value="${contentPrincipal.value.Media.value.Ubicacion}" scope="request"></c:set>
						<div class="ubicacion clearfix">
							<div class="wrapper media-object <c:out value='${width} ${offset} ' />">
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-ubicacion.jsp:ea391db3-df65-11e4-bcf9-01e4df46f753)">									
								</cms:include>
							</div>
						</div>
					</c:if>	
					<c:if test="${element == 'freemedia' }">
						<div class="freemedia clearfix">
							<div class="wrapper media-object <c:out value='${width} ${offset} ' />" ${contentPrincipal.value.Media.value.FreeMedia.rdfa.Content}>
									${contentPrincipal.value.Media.value.FreeMedia.value.Content}
							</div>
						</div>
					</c:if>
					<c:if test="${element == 'mediamultiple' }">
						<c:set var="mediaMultipleElements" value="${contentPrincipal.value.Media.value.MediaMultiple.subValueList['MediaMultipleElements']}" scope="request"></c:set>
						<div class="mediamultiple clearfix">
							<div class="wrapper media-object <c:out value='${width} ${offset} ' />">
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-mediamultiple.jsp:aeecc00e-b84a-11e5-9d19-7fb253176922)">
									<cms:param name="contentId">${param.contentId}</cms:param>
								</cms:include>
							</div>
						</div>
					</c:if>
				</c:if>		
				
				<%--******* LEFT ********--%>
				<c:if test="${position == 'left' or position == 'leftall'}">
					<c:if test="${element == 'video' }">
						<div class="media-object video ${width } left">
							<div class="wrapper-video">
							${contentPrincipal.value.Media.value.VideoMain.value.Code}
							</div>
						</div>
					</c:if>
					<c:if test="${element == 'image' }">
						<div class="media-object image ${width } left" ${contentPrincipal.value.Media.value.ImageMain.rdfa.Image}>
							<c:if test="${!contentPrincipal.value.Media.value.ImageMain.value.Link.exists}">
								<sg:image imagePath="${contentPrincipal.value.Media.value.ImageMain.value.Image}"
										  footer="${contentPrincipal.value.Media.value.ImageMain.value.Footer}"
										  thumbnails="${contentPrincipal.value.Media.value.ImageMain.value.Thumbnail.toBoolean}"
										   showBig="${contentPrincipal.value.Media.value.ImageMain.value.ShowBig.toBoolean}"/>
							</c:if>
							<c:if test="${contentPrincipal.value.Media.value.ImageMain.value.Link.exists}">
								<sg:image imagePath="${contentPrincipal.value.Media.value.ImageMain.value.Image}" footer="${contentPrincipal.value.Media.value.ImageMain.value.Footer}"
										  showBig="${contentPrincipal.value.Media.value.ImageMain.value.ShowBig.toBoolean}"
										  thumbnails="${contentPrincipal.value.Media.value.ImageMain.value.Thumbnail.toBoolean}"
										  urllink="${contentPrincipal.value.Media.value.ImageMain.value.Link.value.Href}"
										  titlelink="${contentPrincipal.value.Media.value.ImageMain.value.Link.value.Title}"
										  targetlink="${contentPrincipal.value.Media.value.ImageMain.value.Link.value.Target}"/>
							</c:if>

						</div>
					</c:if>
					<c:if test="${element == 'ubicacion' }">
						<c:set var="elem" value="${contentPrincipal.value.Media.value.Ubicacion}" scope="request"></c:set>
							<div class="media-object ubicacion ${width} left">
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-ubicacion.jsp:ea391db3-df65-11e4-bcf9-01e4df46f753)">									
								</cms:include>
							</div>
					</c:if>		
					<c:if test="${element == 'freemedia' }">
						<div class="media-object freemedia ${width } left" ${contentPrincipal.value.Media.value.FreeMedia.rdfa.Content}>
							${contentPrincipal.value.Media.value.FreeMedia.value.Content}
						</div>
					</c:if>
					<c:if test="${element == 'mediamultiple' }">
						<c:set var="mediaMultipleElements" value="${contentPrincipal.value.Media.value.MediaMultiple.subValueList['MediaMultipleElements']}" scope="request"></c:set>
						<div class="media-object mediamultiple ${width } left">
							<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-mediamultiple.jsp:aeecc00e-b84a-11e5-9d19-7fb253176922)">
								<cms:param name="contentId">${param.contentId}</cms:param>
							</cms:include>
						</div>
					</c:if>
				</c:if>			
				
				<%--******* RIGHT ********--%>
				<c:if test="${position == 'right' or position == 'rightall'}">
					<c:if test="${element == 'video' }">
						<div class="media-object video ${width } pull-right">
							<div class="wrapper-video">
							${contentPrincipal.value.Media.value.VideoMain.value.Code}
							</div>
						</div>
					</c:if>
					<c:if test="${element == 'image' }">
						<div class="media-object image ${width } pull-right" ${contentPrincipal.value.Media.value.ImageMain.rdfa.Image}>
							<c:if test="${!contentPrincipal.value.Media.value.ImageMain.value.Link.exists}">
								<sg:image imagePath="${contentPrincipal.value.Media.value.ImageMain.value.Image}"
										  footer="${contentPrincipal.value.Media.value.ImageMain.value.Footer}"
										  thumbnails="${contentPrincipal.value.Media.value.ImageMain.value.Thumbnail.toBoolean}"
										  showBig="${contentPrincipal.value.Media.value.ImageMain.value.ShowBig.toBoolean}"/>
							</c:if>
							<c:if test="${contentPrincipal.value.Media.value.ImageMain.value.Link.exists}">
								<sg:image imagePath="${contentPrincipal.value.Media.value.ImageMain.value.Image}"
										  footer="${contentPrincipal.value.Media.value.ImageMain.value.Footer}"
										  showBig="${contentPrincipal.value.Media.value.ImageMain.value.ShowBig.toBoolean}"
										  thumbnails="${contentPrincipal.value.Media.value.ImageMain.value.Thumbnail.toBoolean}"
										  urllink="${contentPrincipal.value.Media.value.ImageMain.value.Link.value.Href}"
										  titlelink="${contentPrincipal.value.Media.value.ImageMain.value.Link.value.Title}"
										  targetlink="${contentPrincipal.value.Media.value.ImageMain.value.Link.value.Target}"/>
							</c:if>
						</div>
					</c:if>
					<c:if test="${element == 'ubicacion' }">
						<c:set var="elem" value="${contentPrincipal.value.Media.value.Ubicacion}" scope="request"></c:set>
							<div class="media-object ubicacion ${width} pull-right">
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-ubicacion.jsp:ea391db3-df65-11e4-bcf9-01e4df46f753)">									
								</cms:include>
							</div>
					</c:if>	
					<c:if test="${element == 'freemedia' }">
						<div class="media-object freemedia ${width } pull-right" ${contentPrincipal.value.Media.value.FreeMedia.rdfa.Content}>
							${contentPrincipal.value.Media.value.FreeMedia.value.Content}
						</div>
					</c:if>
					<c:if test="${element == 'mediamultiple' }">
						<c:set var="mediaMultipleElements" value="${contentPrincipal.value.Media.value.MediaMultiple.subValueList['MediaMultipleElements']}" scope="request"></c:set>
						<div class="media-object mediamultiple ${width }  pull-right">
							<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-mediamultiple.jsp:aeecc00e-b84a-11e5-9d19-7fb253176922)">
								<cms:param name="contentId">${param.contentId}</cms:param>
							</cms:include>
						</div>
					</c:if>
				</c:if>			
				
				<%--******* TEXTO ********--%>

				<c:if test="${position == 'leftall' or position == 'rightall'}">
					<c:set var="column">media-body</c:set>
				</c:if>
				<div class="container-fluid ${column}">
					<cms:include file="%(link.strong:/system/modules/com.saga.diputacion.huelva.frontend/elements/e-ficha-evento.jsp:15a033f4-0a72-11e8-abc7-63f9a58dae09)" >
						<c:if test="${not empty param.program }">
							<cms:param name="program">${param.program}</cms:param>
						</c:if>
						<c:if test="${not empty param.tematica}">
							<cms:param name="tematica">${param.tematica}</cms:param>
						</c:if>
						<cms:param name="searchpage">${param.searchpage}</cms:param>
						<cms:param name="themesearchfieldname">${param.themesearchfieldname}</cms:param>
						<cms:param name="typesearchfieldname">${param.typesearchfieldname}</cms:param>
					</cms:include>
				</div>
				
				<%--******* CENTERBOTTOM ********--%>
				<c:if test="${position == 'bottom' || position == 'centerbottom'}">
					<c:choose>
						<c:when test="${colwidth == '12'}"><c:set var="offset"></c:set></c:when>
						<c:when test="${colwidth == '10'}"><c:set var="offset">col-md-offset-1</c:set></c:when>
						<c:when test="${colwidth == '8'}"><c:set var="offset">col-md-offset-2</c:set></c:when>
						<c:when test="${colwidth == '6'}"><c:set var="offset">col-md-offset-3</c:set></c:when>
						<c:when test="${colwidth == '4'}"><c:set var="offset">col-md-offset-4</c:set></c:when>
						<c:when test="${colwidth == '3'}"><c:set var="offset">col-md-offset-4</c:set></c:when>
						<c:when test="${colwidth == '2'}"><c:set var="offset">col-md-offset-5</c:set></c:when>
					</c:choose>
					<c:if test="${element == 'video' }">				
						<div class="video clearfix">
							<div class="media-object <c:out value='${width} ${offset} ' />">
								<div class="wrapper-video">
								${contentPrincipal.value.Media.value.VideoMain.value.Code}
								</div>
							</div>
						</div>
					</c:if>
					<c:if test="${element == 'image' }">
						<div class="image clearfix">
							<div class="wrapper" ${contentPrincipal.value.Media.value.ImageMain.rdfa.Image}>
								<c:set var="cssClass"><c:out value='media-object ${width} ${offset} ' /></c:set>
								<c:if test="${!contentPrincipal.value.Media.value.ImageMain.value.Link.exists}">
									<sg:image imagePath="${contentPrincipal.value.Media.value.ImageMain.value.Image}"
											  footer="${contentPrincipal.value.Media.value.ImageMain.value.Footer}"
											  thumbnails="${contentPrincipal.value.Media.value.ImageMain.value.Thumbnail.toBoolean}"
											  cssClass="${cssClass}"
											  showBig="${contentPrincipal.value.Media.value.ImageMain.value.ShowBig.toBoolean}"/>
								</c:if>
								<c:if test="${contentPrincipal.value.Media.value.ImageMain.value.Link.exists}">
									<sg:image imagePath="${contentPrincipal.value.Media.value.ImageMain.value.Image}"
											  footer="${contentPrincipal.value.Media.value.ImageMain.value.Footer}"
											  cssClass="${cssClass}"
											  showBig="${contentPrincipal.value.Media.value.ImageMain.value.ShowBig.toBoolean}"
											  thumbnails="${contentPrincipal.value.Media.value.ImageMain.value.Thumbnail.toBoolean}"
											  urllink="${contentPrincipal.value.Media.value.ImageMain.value.Link.value.Href}"
											  titlelink="${contentPrincipal.value.Media.value.ImageMain.value.Link.value.Title}"
											  targetlink="${contentPrincipal.value.Media.value.ImageMain.value.Link.value.Target}"/>
								</c:if>

							</div>
						</div>
					</c:if>
					<c:if test="${element == 'ubicacion' }">
						<c:set var="elem" value="${contentPrincipal.value.Media.value.Ubicacion}" scope="request"></c:set>
						<div class="ubicacion clearfix">
							<div class="wrapper media-object <c:out value='${width} ${offset} ' />">
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-ubicacion.jsp:ea391db3-df65-11e4-bcf9-01e4df46f753)">									
									<cms:param name="contentPrincipalCount">${param.contentPrincipalCount}</cms:param>
								</cms:include>
							</div>
						</div>
					</c:if>	
					<c:if test="${element == 'freemedia' }">
						<div class="freemedia clearfix">
							<div class="wrapper media-object <c:out value='${width} ${offset} ' />" ${contentPrincipal.value.Media.value.FreeMedia.rdfa.Content}>
								${contentPrincipal.value.Media.value.FreeMedia.value.Content}
							</div>
						</div>
					</c:if>
					<c:if test="${element == 'mediamultiple' }">
						<c:set var="mediaMultipleElements" value="${contentPrincipal.value.Media.value.MediaMultiple.subValueList['MediaMultipleElements']}" scope="request"></c:set>
						<div class="mediamultiple clearfix">
							<div class="wrapper media-object <c:out value='${width} ${offset} ' />">
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-mediamultiple.jsp:aeecc00e-b84a-11e5-9d19-7fb253176922)">
									<cms:param name="contentId">${param.contentId}</cms:param>
								</cms:include>
							</div>
						</div>
					</c:if>
				</c:if>		
			</c:if>
			
		<%-- ******* SIN ELEMENTO PRINCIPAL ******** --%>
			<c:if test="${!contentPrincipal.value.Media.exists || (!contentPrincipal.value.Media.value.ImageMain.exists && !contentPrincipal.value.Media.value.VideoMain.exists && !contentPrincipal.value.Media.value.Ubicacion.exists && !contentPrincipal.value.Media.value.FreeMedia.exists && !contentPrincipal.value.Media.value.MediaMultiple.exists)}">
				<div class="container-fluid">
					<cms:include file="%(link.strong:/system/modules/com.saga.diputacion.huelva.frontend/elements/e-ficha-evento.jsp:15a033f4-0a72-11e8-abc7-63f9a58dae09)" >
						<c:if test="${not empty param.program }">
							<cms:param name="program">${param.program}</cms:param>
						</c:if>
						<c:if test="${not empty param.tematica}">
							<cms:param name="tematica">${param.tematica}</cms:param>
						</c:if>
						<cms:param name="searchpage">${param.searchpage}</cms:param>
						<cms:param name="themesearchfieldname">${param.themesearchfieldname}</cms:param>
						<cms:param name="typesearchfieldname">${param.typesearchfieldname}</cms:param>
					</cms:include>
				</div>
			</c:if>
			
		</div>		
	</div>	
</cms:bundle>