<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.diputacion.huelva.frontend.messages">
				<c:if test="${mediablock.value.ImageMain.exists }">
					<c:set var="element">image</c:set>
					<c:set var="position">${mediablock.value.ImageMain.value.Position }</c:set>
					<c:set var="image">${mediablock.value.ImageMain.value.Image}</c:set>
					<c:set var="width">${mediablock.value.ImageMain.value.Width}</c:set>
					<c:set var="imagefooter">${mediablock.value.ImageMain.value.Footer}</c:set>
					<c:set var="positionInterrogacion" value="${fn:indexOf(image,'?' )}"/>
					<c:set var="urlImagen" value="${fn:substring(image,0 , positionInterrogacion)}"/>
					<c:set var="imageEnd" value="${fn:indexOf(image,',cx' )}"/>
					<c:set var="urlSize" value="${fn:substring(image,positionInterrogacion , imageEnd)}"/>
					<c:set var="sizeorigin"><cms:property name="image.size" file="${urlImagen}"/></c:set>

					<%-- Imagen Sin Cropping--%>
					<c:if test="${imageEnd - positionInterrogacion == 0}">
						<c:set var="positionComa" value="${fn:indexOf(sizeorigin,',' )}"/>
						<c:set var="widthImage" value="${fn:substring(sizeorigin,2, positionComa )}"/>
					</c:if>
					<%-- Imagen Con Cropping--%>
					<c:if test="${imageEnd - positionInterrogacion != 0}">
						<c:set var="positionComa" value="${fn:indexOf(urlSize,',' )}"/>
						<c:set var="widthImage" value="${fn:substring(urlSize,positionComa + 3,imageEnd )}"/>
					</c:if>

					<fmt:parseNumber var="widthImage" value="${widthImage}" />

					<c:if test="${widthImage > 720 }">
						<c:set var="widthimg" value="720"/>
					</c:if>
				</c:if>

				<c:if test="${mediablock.exists && mediablock.value.Freemediablock.exists }">
					<c:set var="element">freemedia</c:set>
					<c:set var="position">${mediablock.value.Freemediablock.value.Position }</c:set>
					<c:set var="width">${mediablock.value.Freemediablock.value.Width }</c:set>
				</c:if>

				<c:if test="${mediablock.value.Ubicacion.exists }">
					<c:set var="element">ubicacion</c:set>
					<c:set var="position">${mediablock.value.Ubicacion.value.Position }</c:set>
					<c:set var="width">${mediablock.value.Ubicacion.value.Width }</c:set>
				</c:if>

				<c:if test="${mediablock.value.MediaMultiple.exists }">
					<c:set var="element">mediamultiple</c:set>
					<c:set var="position">${mediablock.value.MediaMultiple.value.Position }</c:set>
					<c:set var="width">${mediablock.value.MediaMultiple.value.Width }</c:set>
				</c:if>

				<c:set var="cssclass" value="media-object-print col-xs-${width}" />

					<c:if test="${element == 'image' }">
						<div class="${cssclass}">
							<div class="wrapper">
								<cms:img src="${image}" width="${widthimg}" cssclass="img-responsive"/>
								<a href="<cms:link>${image}</cms:link>" class="btn-download-img" target="_blank"><fmt:message key="label.download.image" /></a>
								<c:if test="${not empty imagefooter}">
									<p class="image-footer"><small>${imagefooter}</small></p>
								</c:if>
							</div>
						</div>
					</c:if>

					<c:if test="${element == 'freemedia' }">
						<div class="${cssclass}">
								${mediablock.value.Freemediablock.value.Content}
						</div>
					</c:if>

					<c:if test="${element == 'ubicacion' }">
						<div class="${cssclass}">
							<div class="ubicacion">
								<c:set var="elem" value="${mediablock.value.Ubicacion}" scope="request"></c:set>
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-ubicacion.jsp:ea391db3-df65-11e4-bcf9-01e4df46f753)"></cms:include>
							</div>
						</div>
					</c:if>

					<c:if test="${element == 'mediamultiple' }">
						<c:set var="mediaMultipleElements" value="${mediablock.value.MediaMultiple.subValueList['MediaMultipleElements']}" scope="request"></c:set>
						<div class="${cssclass }">
							<div class="mediamultiple">
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-mediamultiple-pdf.jsp:f17f0a66-cb13-11e5-aebf-7fb253176922)">
									<cms:param name="contentBlockCount">${param.contentBlockCount}</cms:param>
								</cms:include>
							</div>
						</div>
					</c:if>
</cms:bundle>