<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.diputacion.huelva.frontend.messages">

<dl class="ficha dl-vertical">

	<c:if test="${not empty param.program }">
		<dt><fmt:message key="ficha.evento.programa"/></dt>
		<dd>
			<%--====== Comprobamos si existe la propiedad Title especifica para el locale actual (Title_es, Title_en...) para la categoria.
    		Si es asi usamos esa propiedad. Si no existe, usamos la propiedad Title =============================--%>
			<c:set var="programtitle"><cms:property name="Title_${locale}" file="${param.program}"/></c:set>
			<c:if test="${empty programtitle}">
				<c:set var="programtitle"><cms:property name="Title" file="${param.program}"/></c:set>
			</c:if>
			<a class="hastooltip" title="<fmt:message key='key.element.ficha.event.view.program' />" href="<cms:link>${param.program }</cms:link>">${programtitle}</a>
		</dd>
	</c:if>
	<c:if test="${not empty param.tematica }">
		<c:set var="searchpage">${param.searchpage}</c:set>
		<c:set var="themesearchfieldname">${param.themesearchfieldname}</c:set>
		<c:set var="typesearchfieldname">${param.typesearchfieldname}</c:set>
		<dt><fmt:message key="ficha.evento.tematica"/></dt>
		<dd>
			<c:set var="tematicas" value="${fn:split(param.tematica, ',')}" />
			<c:set var="counter" value="0" />
			<c:forEach items="${tematicas }" var="tematica" varStatus="status">
				<%--====== Comprobamos si existe la propiedad Title especifica para el locale actual (Title_es, Title_en...) para la categoria.
                Si es asi usamos esa propiedad. Si no existe, usamos la propiedad Title =============================--%>
				<c:set var="tematicatitle"><cms:property name="Title_${locale}" file="${tematica}"/></c:set>
				<c:if test="${empty tematicatitle}">
					<c:set var="tematicatitle"><cms:property name="Title" file="${tematica}"/></c:set>
				</c:if>
				<c:set var="tematicashort" value="${fn:replace(tematica, '/.categories/','')}" />
				<c:set var="tematicashortencoded"><%= java.net.URLEncoder.encode(pageContext.getAttribute("tematicashort").toString() , "UTF-8") %></c:set>
				<c:if test="${status.count != 1}">&nbsp;|&nbsp;</c:if>
				<a class="hastooltip" href="<cms:link>${searchpage}?${themesearchfieldname}=${tematicashortencoded }&${typesearchfieldname}=dphevento</cms:link>" title="<fmt:message key='key.element.ficha.event.view.all.same.tematica' />">${tematicatitle}</a>
			</c:forEach>
		</dd>
	</c:if>
	<c:if test="${fichaevento.value.Description.isSet }">
		<dt><fmt:message key="ficha.evento.description"/></dt>
		<dd>${fichaevento.value.Description}</dd>
	</c:if>
	<c:if test="${fichaevento.value.Remarks.isSet }">
		<dt><fmt:message key="ficha.evento.remarks"/></dt>
		<dd>${fichaevento.value.Remarks}</dd>
	</c:if>
 	<c:if test="${fichaevento.value.Lugar.isSet }">
		<dt><fmt:message key="ficha.evento.lugar"/></dt>
		<dd>${fichaevento.value.Lugar}</dd>
	</c:if>
 	<c:if test="${fichaevento.value.Direccion.isSet}">
		<dt><fmt:message key="ficha.evento.direccion"/></dt>
		<dd>${fichaevento.value.Direccion }</dd>
	</c:if>
	<c:if test="${fichaevento.value.Precio.isSet }">
		<dt><fmt:message key="ficha.evento.precio"/></dt>
		<dd>${fichaevento.value.Precio }</dd>
	</c:if>
	<c:if test="${fichaevento.value.Organizador.isSet  }">
		<dt><fmt:message key="ficha.evento.organizador"/></dt>
		<dd>${fichaevento.value.Organizador }</dd>
	</c:if>
	<c:if test="${fichaevento.value.Contacto.exists and (fichaevento.value.Contacto.value.Telefono.isSet or fichaevento.value.Contacto.value.Email.isSet )}">
		<dt><fmt:message key="ficha.evento.contacto"/></dt>
		<dd>
			<c:if test="${fichaevento.value.Contacto.value.Telefono.isSet}">
				<strong><fmt:message key="ficha.evento.telefono"/></strong> ${fichaevento.value.Contacto.value.Telefono }
			</c:if>
			<c:if test="${fichaevento.value.Contacto.value.Telefono.isSet and fichaevento.value.Contacto.value.Email.isSet }">
				<br>
			</c:if>
			<c:if test="${fichaevento.value.Contacto.value.Email.isSet }">
				<strong><fmt:message key="ficha.evento.email"/></strong> ${fichaevento.value.Contacto.value.Email }
			</c:if>
		</dd>
	</c:if>
	<c:if test="${fichaevento.value.Web.exists and fichaevento.value.Web.value.Href.isSet }">
		<dt><fmt:message key="ficha.evento.enlace"/></dt>
		<dd>
				<c:if test="${fn:startsWith(href, 'http')==false}">
					<c:set var="href"><cms:link>${fichaevento.value.Web.value.Href}</cms:link></c:set>
				</c:if>

				<a href="${fichaevento.value.Web.value.Href}" target="${fichaevento.value.Web.value.Target}" <c:if test="${fichaevento.value.Web.value.Follow !=null && fichaevento.value.Web.value.Follow=='false' }">rel="nofollow"</c:if> title="${fichaevento.value.Web.value.Title}">${fichaevento.value.Web.value.Title}</a>
		</dd>
	</c:if>
	<dt> <fmt:message key="ficha.evento.inidate"/></dt>
	<dd>
		<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-time.jsp:ea2c737f-df65-11e4-bcf9-01e4df46f753)">
			<cms:param name="date">${fichaevento.value.FechaInicio }</cms:param>
			<cms:param name="showtime">false</cms:param>
			<cms:param name="shortdate">true</cms:param>
		</cms:include>
		<c:if test='${fichaevento.value.ShowInicioTime != "false" }'>
			&nbsp;|&nbsp;
			<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-time.jsp:ea2c737f-df65-11e4-bcf9-01e4df46f753)">
				<cms:param name="date">${fichaevento.value.FechaInicio }</cms:param>
				<cms:param name="showtime">true</cms:param>
				<cms:param name="onlytime">true</cms:param>
			</cms:include>
		</c:if>
	</dd>
	<c:if test="${fichaevento.value.FechaFin.isSet }">
		<dt><fmt:message key="ficha.evento.findate"/></dt>
		<dd>
			<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-time.jsp:ea2c737f-df65-11e4-bcf9-01e4df46f753)">
				<cms:param name="date">${fichaevento.value.FechaFin }</cms:param>
				<cms:param name="showtime">false</cms:param>
				<cms:param name="shortdate">true</cms:param>
			</cms:include>
			<c:if test='${fichaevento.value.ShowFinTime != "false" }'>
				&nbsp;|&nbsp;
				<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-time.jsp:ea2c737f-df65-11e4-bcf9-01e4df46f753)">
					<cms:param name="date">${fichaevento.value.FechaFin }</cms:param>
					<cms:param name="showtime">true</cms:param>
					<cms:param name="onlytime">true</cms:param>
				</cms:include>
			</c:if>
		</dd>
	</c:if>
	<c:if test="${fichaevento.value.MasInfo.exists }">
		<c:forEach var="masinfo" items="${fichaevento.valueList.MasInfo}">
			<dt>${masinfo.value.Key }</dt>
			<dd>${masinfo.value.Value }</dd>
		</c:forEach>
	</c:if>
</dl>
</cms:bundle>