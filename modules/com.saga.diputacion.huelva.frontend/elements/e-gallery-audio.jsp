<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.diputacion.huelva.frontend.messages">
	<c:if test="${not empty param.image}">
		<cms:img src="${param.image}" cssclass="img-responsive" width="768" height="400" scaleType="2" />
	</c:if>
	<audio class="center-block w100" controls>
		<c:if test="${not empty param.audiofilemp3}">
			<source src="<cms:link>${param.audiofilemp3}</cms:link>" type="audio/mpeg">
		</c:if>
		<c:if test="${not empty param.audiofilwav}">
			<source src="<cms:link>${param.audiofilwav}</cms:link>" type="audio/wav">
		</c:if>
		<c:if test="${not empty param.audiofileogg}">
			<source src="<cms:link>${param.audiofileogg}</cms:link>" type="audio/ogg">
		</c:if>
		<fmt:message key='key.messages.audio.not.supported'/>
	</audio>
</cms:bundle>