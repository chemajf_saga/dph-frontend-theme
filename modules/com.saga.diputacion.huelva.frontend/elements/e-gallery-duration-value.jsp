<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.diputacion.huelva.frontend.messages">
		<c:set var="hours">00</c:set>
		<c:set var="minutes">00</c:set>
		<c:set var="seconds">00</c:set>
		<c:if test="${not empty param.hours}">
			<fmt:parseNumber var="hours" integerOnly="true" value="${param.hours}" />
			<fmt:formatNumber var="hours" minIntegerDigits="2" maxFractionDigits="0" value="${hours}" />
		</c:if>
		<c:if test="${not empty param.minutes}">
			<fmt:parseNumber var="minutes" integerOnly="true" value="${param.minutes}" />
			<fmt:formatNumber var="minutes" minIntegerDigits="2" maxFractionDigits="0" value="${minutes}" />
		</c:if>
		<c:if test="${not empty param.seconds}">
			<fmt:parseNumber var="seconds" integerOnly="true" value="${param.seconds}" />
			<fmt:formatNumber var="seconds" minIntegerDigits="2" maxFractionDigits="0" value="${seconds}" />
		</c:if>
		<c:set var="durationmsg" scope="request">
			<fmt:message key="key.formatter.duration" />
		</c:set>
		<c:set var="duration" scope="request">
			${hours}:${minutes}:${seconds}
		</c:set>
</cms:bundle>