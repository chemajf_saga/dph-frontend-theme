<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.diputacion.huelva.frontend.messages">
	<c:if test="${param.mediatype == 'audio'}">
		<div class="gallery-media gallery-media-audio">
			<cms:img cssclass="img-responsive" src="${param.mediaposter}" width="768"/>
			<audio class="center-block w100" controls>
				<source src="${param.mediasource}" type="${param.mediasourcetype}" />
			</audio>
		</div>
	</c:if>
	<c:if test="${param.mediatype == 'videourl'}">
		<div class="gallery-media gallery-media-video gallery-media-video-url">
			<div class="embed-responsive embed-responsive-16by9">
				<iframe title="${param.mediatitle}" src="${param.mediasource}" frameborder="0" allowfullscreen></iframe>
			</div>
		</div>
	</c:if>
	<c:if test="${param.mediatype == 'video'}">
		<div class="gallery-media gallery-media-video gallery-media-video-html5">
			<div class="embed-responsive embed-responsive-16by9">
				<video poster="${param.mediaposter}" controls="true" preload="auto">
					<source src="${param.mediasource}" type="${param.mediasourcetype}" />
				</video>
			</div>
		</div>
	</c:if>
	<h2 class="h3">${param.mediatitle}</h2>
	<c:if test="${param.mediaduration != null}">
		<div class="duration">
			<strong><fmt:message key="key.formatter.duration" />&nbsp;</strong>${param.mediaduration}
		</div>
	</c:if>
	<c:if test="${param.mediafecha != null}">
		<div class="duration">
			<strong><fmt:message key="key.formatter.date" />&nbsp;</strong>${param.mediafecha}
		</div>
	</c:if>
	<c:if test="${param.mediadescription != null}">
		<div class="description mt-10">
			${param.mediadescription}
		</div>
	</c:if>
</cms:bundle>