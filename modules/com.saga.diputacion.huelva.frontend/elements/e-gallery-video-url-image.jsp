<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.diputacion.huelva.frontend.messages">

	<c:set var="url">${param.url}</c:set>
	<c:if test="${(fn:contains(url,'youtube.com' )) or (fn:contains(url,'youtu.be' ))}">
		<c:set var="streaming">youtube</c:set>
		<c:if test="${fn:contains(url,'youtu.be' )}">
			<c:set var="shorturl">true</c:set>
		</c:if>
	</c:if>
	<c:if test="${fn:contains(url,'vimeo.com' )}">
		<c:set var="streaming">vimeo</c:set>
	</c:if>
	<c:if test="${(fn:contains(url,'dailymotion.com' )) or (fn:contains(url,'dai.ly' ))}">
		<c:set var="streaming">dailymotion</c:set>
		<c:if test="${fn:contains(url,'dai.ly' )}">
			<c:set var="shorturl">true</c:set>
		</c:if>
	</c:if>
			<%-- YOUTUBE ===============================================================================--%>
		<c:if test="${streaming == 'youtube'}">
			<c:if test="${empty shorturl}">
				<c:set var="idvideo">${fn:substringAfter(url,'?v=')}</c:set>
			</c:if>
			<c:if test="${not empty shorturl}">
				<c:set var="idvideo">${fn:substringAfter(url,'youtu.be/')}</c:set>
			</c:if>
			<c:set var="thumbvideo" scope="request">http://img.youtube.com/vi/${idvideo}/0.jpg</c:set>
		</c:if>
			<%-- VIMEO ===============================================================================--%>
		<c:if test="${streaming == 'vimeo'}">
			<c:set var="idvideo">${fn:substringAfter(url,'vimeo.com/')}</c:set>
			<c:set var="thumbvideo" scope="request">http://i.vimeocdn.com/video/${idvideo}</c:set>
		</c:if>
			<%-- DAILYMOTION ===============================================================================--%>
		<c:if test="${streaming == 'dailymotion'}">
			<c:if test="${empty shorturl}">
				<c:set var="beginidvideo">${fn:indexOf(url,'/video/' ) + '/video/'.length()}</c:set>
				<c:set var="endidvideo">${fn:indexOf(url,'_' )}</c:set>
				<c:set var="idvideo">${fn:substring(url, beginidvideo ,endidvideo )}</c:set>
			</c:if>
			<c:if test="${not empty shorturl}">
				<c:set var="idvideo">${fn:substringAfter(url,"http://dai.ly/" )}</c:set>
			</c:if>
			<c:set var="thumbvideo" scope="request">http://www.dailymotion.com/thumbnail/video/${idvideo}</c:set>
		</c:if>
		<img src="${thumbvideo}" width="480" class="img-responsive" alt="${param.title}" />
</cms:bundle>
