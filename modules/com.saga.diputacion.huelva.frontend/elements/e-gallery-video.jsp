<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.diputacion.huelva.frontend.messages">
	<div class="embed-responsive embed-responsive-16by9">
		<video <c:if test="${not empty param.image}"> poster="<cms:link>${param.image}</cms:link>"</c:if> controls="true" preload="auto">
			<c:if test="${not empty param.source}">
				<source src="<cms:link>${param.source}</cms:link>" type="video/mp4" />
			</c:if>
			<fmt:message key="key.videohtml5.not.supported" />
		</video>
	</div>
</cms:bundle>