<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" import="java.text.DecimalFormat, org.opencms.jsp.CmsJspActionElement,
        org.opencms.search.solr.*, org.opencms.file.*, org.opencms.main.*,org.opencms.ade.detailpage.CmsDetailPageUtil"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates" %>
<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.core.messages">

	<%
		CmsJspActionElement cms = new CmsJspActionElement(pageContext, request, response);
	%>


	<c:set var="galleryid" value="${param.galleryId}" />
	<c:set var="galleryFolder" value="${param.galleryFolder}" />
	<c:set var="galleryImages" value="${param.galleryImages}" />
	<c:set var="galleryPageElements" value="${param.galleryPageElements}" />
	<c:set var="galleryResourcesForRow" value="${param.galleryResourcesForRow}" />
	<c:set var="galleryOriginSize" value="${param.galleryOriginSize}" />

	<div class="gallery image-gallery">
		<div id="${galleryid}">
			<c:set var="rrows">${galleryResourcesForRow}</c:set>
			<c:choose>
				<c:when test="${galleryResourcesForRow == '2'}"><c:set var="width">col-xs-6 col-md-6</c:set></c:when>
				<c:when test="${galleryResourcesForRow == '3'}"><c:set var="width">col-xs-6 col-md-4</c:set></c:when>
				<c:when test="${galleryResourcesForRow == '4'}"><c:set var="width">col-xs-6 col-md-3</c:set></c:when>
				<c:when test="${galleryResourcesForRow == '6'}"><c:set var="width">col-xs-6 col-md-2</c:set></c:when>
			</c:choose>
			<c:if test="${galleryOriginSize == 'true'}">
				<c:set var="imageWidth">800</c:set>
				<c:set var="imageHeight"></c:set>
			</c:if>
			<c:if test="${galleryOriginSize == 'false'}">
				<c:set var="imageWidth">400</c:set>
				<c:set var="imageHeight">300</c:set>
			</c:if>

			<%-- Mostramos primero las imagenes independientes --%>
			<c:if test="${not empty galleryImages and empty galleryFolder}">
				<c:if test="${fn:indexOf(galleryImages, '[') != -1}">
					<c:set var="galleryImages" value="${fn:replace(galleryImages,']' ,'')}" />
					<c:set var="galleryImages" value="${fn:replace(galleryImages,'[' ,'')}" />
					<c:set var="galleryImages" value="${fn:split(galleryImages,',')}" />
				</c:if>
				<%-- Cada fila de la galeria es una row-fluid --%>
				<div class="row">
				<c:forEach var="image" items="${galleryImages}" varStatus="status">
					<sg:image imagePath="${image}" cssClass="${width}" thumbnails="true" width="${imageWidth}" height="${imageHeight}"/>

					<%-- Si coincide con el numero de elementos por fila generamos un nuevo row-fluid --%>

					<c:if test="${status.count % rrows == 0 }">
						</div>
						<div class="row">
					</c:if>
				</c:forEach>
				</div>
			</c:if>
				<%-- Mostramos segundo el directorio de imagenes --%>
			<c:if test="${not empty galleryFolder}">

				<%-- Definimos las variables de nuestro entorno --%>
				<c:set var="cmsObject" value="${cms.vfs.cmsObject }" scope="request"/>
				<c:set var="imagefolder" value="${galleryFolder}" />
				<c:set var="query">&fq=type:image&fq=parent-folders:"${cms.vfs.requestContext.siteRoot}${imagefolder}"&fq=con_locales:*&sort=path asc</c:set>
				<c:set var="pageImages" value="${galleryPageElements}" />

				<%-- GESTIONAMOS LA PAGINACION CUANDO ES PAGINA DE DETALLE --%>
				<c:set var="urlDetail" value="" />
				<c:if test="${cms.detailRequest}">
					<%-- obtenemos el urlname del recurso si existe para añadirlo en el enlace generado para las paginas --%>
					<c:set var="urlDetail" value="${cms.detailContentId}" />
					<c:set var="idDetailResource" value="${cms.detailContent}" />
					<%-- obtenemos el urlname del recurso si existe para añadirlo en el enlace generado para las paginas --%>
					<%
						try {
							CmsObject cmso = (CmsObject)request.getAttribute("cmsObject");
							CmsResource resource = (CmsResource)pageContext.getAttribute("idDetailResource");
							String bestUrlName = CmsDetailPageUtil.getBestUrlName(cmso, resource.getStructureId());
							pageContext.setAttribute("urlName", bestUrlName);
						} catch (Exception e) {
							pageContext.setAttribute("urlName", null);
						}

					%>
					<c:if test="${not empty urlName}">
						<c:set var="urlDetail" value="${urlName}" />
					</c:if>
				</c:if>


				<c:set var="uri" value="${cms.requestContext.uri}" />
				<c:set var="indexPos" value="${fn:indexOf(uri, 'index.html')}" />
				<c:if test="${indexPos > 0}">
					<c:set var="uri" value="${fn:substring(uri, 0, indexPos)}" />
				</c:if>

				<%
					//Obtenemos el numero de imagenes por pagina
					String pageImagesString = ""+pageContext.getAttribute("pageImages");
					int pageImages = 30;
					try{
						pageImages = Integer.parseInt(pageImagesString);
						if (pageImages == 0){
							pageImages = 1;
						}
					}catch(Exception ex){}
					pageContext.setAttribute("pageImages", pageImages);

					//Obtenemos la pagina actual
					String id = ""+pageContext.getAttribute("galleryid");
					String currentPageString = request.getParameter("page"+id);

					int currentPage = 1;
					if(currentPageString!=null){
						try{
							currentPage = Integer.parseInt(currentPageString);
						}catch(Exception ex){}
					}
					pageContext.setAttribute("currentPage", currentPage);

					//Obtenemos si mostramos la paginacion

					pageContext.setAttribute("showPagination", true);

					//Obtenemos el numero de paginas a mostrar en la paginacion
					int navPages = 4;
					pageContext.setAttribute("navigationPages", 4);

					//Obtenemos si mostramos el informe de paginacion

					pageContext.setAttribute("showInforme", false);

					//Calculamos el primer elemento a mostrar en la pagina
					int firstResource = (currentPage-1)*pageImages;
					pageContext.setAttribute("firstResource", firstResource);

					//Obtenemos el cmsObject
					CmsObject cmsObject = (CmsObject)request.getAttribute("cmsObject");

					//Obtenemos el indice
					String indice = cmsObject.readPropertyObject(cmsObject.getRequestContext().getUri(), "search.index", true).getValue();
					if(indice == null || indice.length()==0)
					{
						if(cms.getRequestContext().getCurrentProject().isOnlineProject())
							indice = "Solr Online";
						else
							indice = "Solr Offline";
					}

					//Realizamos la consulta a solr y obtenemos el total de elementos
					String query = ""+pageContext.getAttribute("query");
					CmsSolrResultList results = OpenCms.getSearchManager().getIndexSolr(indice).search(cmsObject, query+"&rows=0");

					int totalResources = (int)results.getNumFound();
					pageContext.setAttribute("totalResources", totalResources);

					//Obtener firstPageShow, lastPageShow
					int firstPageShow = currentPage;
					int lastPageShow = currentPage;
					int lastPage = (totalResources%pageImages) == 0 ? (totalResources/pageImages) : (totalResources/pageImages + 1);
					pageContext.setAttribute("lastPage", lastPage);
					while (navPages > 0 && firstPageShow - 1 > 0){
						firstPageShow--;
						navPages--;
					}
					while (navPages > 0 && lastPageShow + 1 <= lastPage){
						lastPageShow++;
						navPages--;
					}
					if (lastPageShow == currentPage && lastPage > lastPageShow){
						lastPageShow++;
						firstPageShow++;
					}
					pageContext.setAttribute("firstPageShow", firstPageShow);
					pageContext.setAttribute("lastPageShow", lastPageShow);
					int contador = 0;
					String rrowsString = ""+pageContext.getAttribute("rrows");
					int rrows = 4;
					try{
						rrows = Integer.parseInt(rrowsString);
						if (rrows == 0){
							rrows = 1;
						}
					}catch(Exception ex){}

				%>
				<div class="row">
					<c:set var="collectorParam">${query }&rows=${pageImages }&start=${firstResource }</c:set>
					<cms:resourceload collector="byContext" param="${collectorParam}">
						<cms:contentinfo var="info" />

						<c:if test="${info.resultSize == 0}">
							<div class="alert alert-warning">
								<fmt:message key="label.textcontent.filesgallery.nofile"/>
							</div>
						</c:if>
						<c:if test="${info.resultSize > 0}">
							<cms:resourceaccess var="res" />
							<c:set var="imagePath">${res.filename}</c:set>

							<%
								if(contador % rrows == 0){
									out.println("</div><div class=\"row\">");
								}
								contador ++;
							%>

							<sg:image imagePath="${imagePath}" cssClass="${width}" thumbnails="true"
									  width="${imageWidth}" height="${imageHeight}"/>

						</c:if>
					</cms:resourceload>
				</div> <%-- Cierra el ultimo row --%>
			</c:if>
		</div>


				<%-- Release 1.5.6.1 (12/01/2018) - Movemos la paginación al final del código en lugar de tenerla dos veces, una por cada modo de visualización. Accesibilidad - Cargamos el título del recurso en los enlaces de la galería de imágenes --%>
				<!-- Paginacion -->
				<c:if test="${showPagination && navigationPages > 0 && totalResources > pageImages}">
					<div class="text-center">
						<c:set var="resourcetitle"><cms:property name="Title" file="${cms.element.sitePath}" /></c:set>
						<ul class="pagination">
							<c:if test="${currentPage == '1' }">
								<li><a class="firstpagelink pagelink" href="#${galleryid}"><span class="sr-only">${resourcetitle} - <fmt:message key="key.pagination.imagegallery" />: <fmt:message key="key.pagination.first.page" /></span><span class="fa fa-angle-double-left" aria-hidden="true"></span></a></li>
								<li><a class="previouspagelink pagelink" href="#${galleryid}"><span class="sr-only">${resourcetitle} - <fmt:message key="key.pagination.imagegallery" />: <fmt:message key="key.pagination.previous.page" /></span><span class="fa fa-angle-left" aria-hidden="true"></span></a></li>
							</c:if>
							<c:if test="${currentPage != '1' }">
								<li><a class="firstpagelink pagelink" href="<cms:link>${uri}${urlDetail}?page${galleryid}=1#${galleryid}</cms:link>" data-numpag="1"><span class="sr-only">${resourcetitle} - <fmt:message key="key.pagination.imagegallery" />: <fmt:message key="key.pagination.first.page" /></span><span class="fa fa-angle-double-left" aria-hidden="true"></span></a></li>
								<li><a class="previouspagelink pagelink" href="<cms:link>${uri}${urlDetail}?page${galleryid}=${currentPage-1 }#${galleryid}</cms:link>" data-numpag="${currentPage-1 }"><span class="sr-only">${resourcetitle} - <fmt:message key="key.pagination.imagegallery" />: <fmt:message key="key.pagination.previous.page" /></span><span class="fa fa-angle-left" aria-hidden="true"></span></a></li>
							</c:if>
							<c:forEach var="i" begin="${firstPageShow}" end="${lastPageShow}">
								<c:choose>
									<c:when test="${i == currentPage}">
										<li class="active"><a class="currentpagelink" href="<cms:link>${uri}${urlDetail}?page${galleryid}=${i}#${galleryid}</cms:link>" data-numpag="${i}"><span class="sr-only">${resourcetitle} - <fmt:message key="key.pagination.imagegallery" />: <fmt:message key="key.pagination.current.page" /></span><c:out value="${i}"/></a></li>
									</c:when>
									<c:otherwise>
										<li><a class="pagelink" href="<cms:link>${uri}${urlDetail}?page${galleryid}=${i}#${galleryid}</cms:link>" data-numpag="${i}"><span class="sr-only">${resourcetitle} - <fmt:message key="key.pagination.imagegallery" />: <fmt:message key="key.pagination.go.page" /></span><c:out value="${i}" /></a></li>
									</c:otherwise>
								</c:choose>
							</c:forEach>
							<c:if test="${currentPage == lastPage }">
								<li><a class="nextpagelink pagelink" href="#${galleryid}" ><span class="sr-only">${resourcetitle} - <fmt:message key="key.pagination.imagegallery" />: <fmt:message key="key.pagination.next.page" /></span><span class="fa fa-angle-right" aria-hidden="true"></span></a></li>
								<li><a class="lastpagelink pagelink" href="#${galleryid}" ><span class="sr-only">${resourcetitle} - <fmt:message key="key.pagination.imagegallery" />: <fmt:message key="key.pagination.last.page" /></span><span class="fa fa-angle-double-right" aria-hidden="true"></span></a></li>
							</c:if>
							<c:if test="${currentPage != lastPage }">
								<li><a class="nextpagelink pagelink" href="<cms:link>${uri}${urlDetail}?page${galleryid}=${currentPage+1 }#${galleryid}</cms:link>" data-numpag="${currentPage+1 }"><span class="sr-only">${resourcetitle} - <fmt:message key="key.pagination.imagegallery" />: <fmt:message key="key.pagination.next.page" /></span><span class="fa fa-angle-right" aria-hidden="true"></span></a></li>
								<li><a class="lastpagelink pagelink" href="<cms:link>${uri}${urlDetail}?page${galleryid}=${lastPage }#${galleryid}</cms:link>" data-numpag="${data-numpages }"><span class="sr-only">${resourcetitle} - <fmt:message key="key.pagination.imagegallery" />: <fmt:message key="key.pagination.last.page" /></span><span class="fa fa-angle-double-right" aria-hidden="true"></span></a></li>
							</c:if>
						</ul>
					</div>
				</c:if>
	</div>
</cms:bundle>