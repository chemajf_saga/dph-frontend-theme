<%@page buffer="none" session="false" import="org.opencms.jsp.CmsJspActionElement,org.opencms.file.*,org.opencms.main.*,java.text.DecimalFormat" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.diputacion.huelva.frontend.messages">
	<div class="related-item col-xxs-12 col-xs-6 col-sm-4 col-md-3">
		<c:set var="link">${param.relateditem}</c:set>
		<c:set var="resourcetype"></c:set>
		<c:if test="${not fn:startsWith(link, 'http://')}">
			<c:set var="cmsResource" value="${cms:getCmsObject(pageContext).readResource(link)}"/>
			<%
				CmsResource cmsResource = (CmsResource)pageContext.getAttribute("cmsResource");
				String resourcetype = OpenCms.getResourceManager().getResourceType(cmsResource).getTypeName();
				pageContext.setAttribute("resourcetype", resourcetype);
			%>
		</c:if>
		<c:choose>
			<%-- Si enlaza con un recurso de DPH usamos su formatter de display //////////////////// --%>
			<c:when test="${resourcetype == 'dphnoticia' or resourcetype == 'dphcontenido' or resourcetype == 'dphevento' or resourcetype == 'dphgaleria' or resourcetype == 'dphprograma' or resourcetype == 'dphenlace'}">
				<cms:display value="${link}">
					<cms:displayFormatter type="${resourcetype}" path="/system/modules/com.saga.diputacion.huelva.frontend/formatters/dphresource-display-formatter.xml" />
				</cms:display>
			</c:when>
			<%-- Si no enlaza con un recurso de los anteriores creamos un enlace normal /////////////////////////// --%>
			<c:otherwise>
				<%-- Definimos las variables que gestionan la imagen --%>
				<jsp:useBean id="random" class="java.util.Random" scope="application"/>
				<c:set var="defaultimgint">${random.nextInt(4)+1}</c:set>

				<c:if test="${defaultimgint > 0 and defaultimgint <= 1 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg}">
					<c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg}</c:set>
				</c:if>
				<c:if test="${defaultimgint > 1 and defaultimgint <= 2 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg2}">
					<c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg2}</c:set>
				</c:if>
				<c:if test="${defaultimgint > 2 and defaultimgint <= 3 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg3}">
					<c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg3}</c:set>
				</c:if>
				<c:if test="${defaultimgint > 3 and defaultimgint <= 4 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg4}">
					<c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg4}</c:set>
				</c:if>
				<c:if test="${defaultimgint > 4 and defaultimgint <= 5 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg5}">
					<c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg5}</c:set>
				</c:if>
				<c:set var="titlelink">${link}</c:set>
				<c:if test="${not fn:startsWith(link, 'http://')}">
					<c:set var="titlelink">
						<cms:property name="Title_${cms.locale}" file="${link}"/>
					</c:set>
					<c:if test="${empty titlelink}">
						<c:set var="titlelink">
							<cms:property name="Title" file="${link}"/>
						</c:set>
					</c:if>
				</c:if>
				<c:set var="iconlinkclass">pe-7s-link fs20</c:set>
				<c:set var="linktarget">_self</c:set>
				<c:if test="${resourcetype == 'binary' or resourcetype == 'image' or fn:contains(link, 'http://')}">
					<c:set var="linktarget">_blank</c:set>
				</c:if>
				<c:if test="${resourcetype == 'binary'}">
					<c:set var="iconlinkclass">pe-7s-download fs20</c:set>
				</c:if>
				<c:if test="${resourcetype == 'image'}">
					<c:set var="iconlinkclass">pe-7s-photo fs20</c:set>
					<c:set var="imagePath">${link}</c:set>
				</c:if>
				<c:set var="titlesize">h5</c:set>
				<c:set var="marginClass">mb-30</c:set>
				<c:set var="equalheight">equalheight-three</c:set>
				<c:set var="bgimagealign">center</c:set>
				<c:set var="height">height: 200px;</c:set>
				<c:set var="bgattributes">style="${height}background-image:url('<cms:link>${imagePath}</cms:link>');background-size:cover;background-position: center ${bgimagealign};background-repeat: no-repeat;"</c:set>
				<div class="parent dph-noticia-box sg-microcontent sg-microcontent-panel sg-microcontent-overlay box mb-30 <c:out value=' ${marginClass} ${equalheight}'/>"${spacer}${bgattributes}>
					<div class="element complete-mode with-media with-link">
						<span class="${iconlinkclass}" aria-hidden="true" style="position: absolute;right: 15px;top:15px;color: #fff"></span>
						<a class="no-icon" href="<cms:link>${link}</cms:link>" target="${linktarget}" style="position: absolute;left: 0;top: 0;bottom: 0;right: 0;width: 100%;height: 100%;z-index: 1">
							<span class="sr-only">${titlelink}</span>
						</a>
						<div class="text-overlay">
							<div class="wrapper">
								<c:if test="${not empty titlelink}">
									<div class="title no-margin no-padding ${titlesize}">
										${titlelink}
									</div>
								</c:if>
							</div>
						</div>
					</div>
				</div>
			</c:otherwise>
		</c:choose>
	</div>
	<c:set var="target" scope="request" value="" />
</cms:bundle>
