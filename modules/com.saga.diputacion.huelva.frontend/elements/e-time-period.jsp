<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<fmt:setLocale value="${cms.locale}" />
<c:if test="${not empty param.inidate }">
	<c:if test="${param.showtime==true }">
		<time datetime="<fmt:formatDate value="${cms:convertDate(param.date)}" pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ" />">
			<fmt:formatDate value="${cms:convertDate(param.date)}" dateStyle="SHORT" timeStyle="SHORT" type="both" />
		</time>
	</c:if>
	<c:if test="${empty param.showtime || param.showtime==false }">
		<c:if test="${not empty param.findate }">
			<time datetime="<fmt:formatDate value="${cms:convertDate(param.date)}" pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ" />">
				<c:set var="mesini"><fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="MMMM" type="date" /></c:set>
				<c:set var="mesfin"><fmt:formatDate value="${cms:convertDate(param.findate)}" pattern="MMMM" type="date" /></c:set>
				<c:set var="diaini"><fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="dd" type="date" /></c:set>
				<c:set var="diafin"><fmt:formatDate value="${cms:convertDate(param.findate)}" pattern="dd" type="date" /></c:set>
				<c:choose>
					<c:when test="${mesini == mesfin }">
						<c:if test="${diaini != diafin }">
							<fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="'Del' dd' '" type="date" />
							<fmt:formatDate value="${cms:convertDate(param.findate)}" pattern="'al' dd 'de' MMMM 'de' yyyy" type="date" />
						</c:if>
						<c:if test="${diaini == diafin }">							
							<fmt:formatDate value="${cms:convertDate(param.inidate)}" dateStyle="FULL" type="date" />
						</c:if>						
					</c:when>
					<c:when test="${mesini != mesfin }">
						<fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="'Del' dd 'de' MMMM' '" type="date" />
						<fmt:formatDate value="${cms:convertDate(param.findate)}" pattern="'al' dd 'de' MMMM 'de' yyyy" type="date" />
					</c:when>				
				</c:choose>				
			</time>
		</c:if>
		<c:if test="${empty param.findate }">
			<time datetime="<fmt:formatDate value="${cms:convertDate(param.date)}" pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ" />">
				<fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="EEEE',' dd 'de' MMMM 'de' yyyy" type="date" />
			</time>
		</c:if>			
	</c:if>
</c:if>
