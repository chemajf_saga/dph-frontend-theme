<%@page trimDirectiveWhitespaces="true" buffer="none" session="false" taglibs="c,cms,fmt,fn" %>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.diputacion.huelva.frontend.messages">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<cms:formatter var="content">

<%-- Accedemos al .themeconfig de esta forma porque no nos llega en el request del template del Core --%>
<c:set var="filename">${content.filename}</c:set>
<cms:contentload collector="singleFile" param="${param.themeconfigpath}">
    <cms:contentaccess var="themeconfig"/>
</cms:contentload>

<head>
    <title>${content.value.Title}</title>
    <link rel="stylesheet" href="<cms:link>/system/modules/com.saga.sagasuite.core/resources/css/bootstrap.min.css</cms:link>" type="text/css" />
    <link rel="stylesheet" href="<cms:link>/system/modules/com.saga.sagasuite.core/resources/css/generated-pdf.css</cms:link>" type="text/css" />
<%-- si se define una custom css en el themeconfig para los pdfs se incluye aqui --%>
    <c:if test="${themeconfig.value.CustomCssPdf.isSet}">
        <link rel="stylesheet" href="<cms:link>${themeconfig.value.CustomCssPdf}</cms:link>" type="text/css" />
    </c:if>
	<%-- Si se edita el themeconfig para que haya cabecera y/o pie se añade un estilo que hace mas grande el margen de la pagina por arriba para que el contenido no pise la cabecera y por abajo para el pie --%>
    <c:if test="${themeconfig.value.LogoEntity.isSet or themeconfig.value.AdditionalInfo.isSet}">
        <style type="text/css">
            @page  {
                margin-top: 14%;
            }
        </style>
    </c:if>
    <c:if test="${themeconfig.value.TextoPiePdf.isSet}">
        <style type="text/css">
            @page  {
                margin-bottom: 14%;
            }
        </style>
    </c:if>
	<%-- si se define una css inline en el themeconfig para los pdfs se incluye aqui --%>
	<c:if test="${themeconfig.value.CssInline.isSet}">
	    <style type="text/css">
			${themeconfig.value.CssInline}
        </style>
	</c:if>
</head>
<body>

<%-- cabecera --%>
<c:choose>
    <c:when test="${themeconfig.value.LogoCabaceraPdf.isSet or themeconfig.value.TextoCabeceraPdf.isSet}">
        <div id="header">
            <div class="header-content">
                <c:if test="${themeconfig.value.LogoCabaceraPdf.isSet}">
                    <div class="header-img">
                        <cms:img src="${themeconfig.value.LogoCabaceraPdf}" scaleQuality="100"/>
                    </div>
                </c:if>
                <c:if test="${themeconfig.value.TextoCabeceraPdf.isSet}">
                    <div class="additional-info">
                        ${themeconfig.value.TextoCabeceraPdf}
                    </div>
                </c:if>
            </div>
        </div>
    </c:when>
    <c:when test="${themeconfig.value.LogoEntity.isSet or themeconfig.value.AdditionalInfo.isSet}">
        <div id="header">
            <div class="header-content">
                <c:if test="${themeconfig.value.LogoEntity.isSet}">
                    <div class="header-img">
                        <cms:img src="${themeconfig.value.LogoEntity}" scaleQuality="100"/>
                    </div>
                </c:if>
                <c:if test="${themeconfig.value.AdditionalInfo.isSet}">
                    <div class="additional-info">
                        ${themeconfig.value.AdditionalInfo}
                    </div>
                </c:if>
            </div>
        </div>
    </c:when>
</c:choose>

	<%-- footer --%>
	<div id="footer">
		<c:if test="${themeconfig.value.TextoPiePdf.isSet}">
			<div class="footer-content">
				${themeconfig.value.TextoPiePdf}
			</div>					
		</c:if>
		<div class="pagination">
			<span id="pagenumber"></span>
		</div>
	</div>

<div class="articulo parent element dph-evento">

    <div class="wrapper">

        <!-- Cabecera del articulo -->

        <div class="time">
            <cms:include file="%(link.strong:/system/modules/com.saga.diputacion.huelva.frontend/elements/e-time-period.jsp:15920321-0a72-11e8-abc7-63f9a58dae09)">
                <cms:param name="inidate">${content.value.FichaEvento.value.FechaInicio }</cms:param>
                <c:if test="${content.value.FichaEvento.value.FechaFin.isSet }">
                    <cms:param name="findate">${content.value.FichaEvento.value.FechaFin }</cms:param>
                </c:if>
                <cms:param name="showtime">false</cms:param>
            </cms:include>
        </div>
        <div class="headline">
            <h1 class="title">
                ${content.value.Title}
            </h1>
            <c:if test="${content.value.SubTitle.isSet }">
                <h2 class="subtitle">${content.value.SubTitle}</h2>
            </c:if>
        </div>
        <div class="contentblock">
            <div class="contentblock-texto row">
                <c:if test="${content.value.Media.exists}">
                    <c:set var="mediablock" value="${content.value.Media}" scope="request"></c:set>
                    <cms:include file="%(link.strong:/system/modules/com.saga.diputacion.huelva.frontend/elements/e-evento-media-pdf.jsp:15ad0537-0a72-11e8-abc7-63f9a58dae09)" />
                </c:if>
                <div class="container-fluid">
                    <div class="ficha ficha-evento">
                        <h4><fmt:message key="ficha.evento.titulo"/></h4>
                        <c:if test="${content.value.FichaEvento.value.Municipio.isSet }">
                            <dl class="place dl-vertical">
                                <dt><fmt:message key="ficha.evento.municipio"/></dt>
                                <dd>${content.value.FichaEvento.value.Municipio}</dd>
                            </dl>
                        </c:if>
                        <c:if test="${content.value.FichaEvento.value.Lugar.isSet }">
                            <dl class="place dl-vertical">
                                <dt><fmt:message key="ficha.evento.lugar"/></dt>
                                <dd>${content.value.FichaEvento.value.Lugar}</dd>
                            </dl>
                        </c:if>
                        <c:if test="${content.value.FichaEvento.value.Direccion.isSet}">
                            <dl class="address dl-vertical">
                                <dt><fmt:message key="ficha.evento.direccion"/></dt>
                                <dd>${content.value.FichaEvento.value.Direccion }</dd>
                            </dl>
                        </c:if>
                        <c:if test="${content.value.FichaEvento.value.Precio.isSet }">
                            <dl class="price dl-vertical">
                                <dt><fmt:message key="ficha.evento.precio"/></dt>
                                <dd>${content.value.FichaEvento.value.Precio }</dd>
                            </dl>
                        </c:if>
                        <c:if test="${content.value.FichaEvento.value.Organizador.isSet  }">
                            <dl class="organize dl-vertical">
                                <dt><fmt:message key="ficha.evento.organizador"/></dt>
                                <dd>${content.value.FichaEvento.value.Organizador }</dd>
                            </dl>
                        </c:if>
                        <c:if test="${content.value.FichaEvento.value.Contacto.exists and (content.value.FichaEvento.value.Contacto.value.Telefono.isSet or content.value.FichaEvento.value.Contacto.value.Email.isSet )}">
                            <dl class="contacto dl-vertical">
                                <dt><fmt:message key="ficha.evento.contacto"/></dt>
                                <dd>
                                    <strong><fmt:message key="ficha.evento.telefono"/></strong> ${content.value.FichaEvento.value.Contacto.value.Telefono }<br/>
                                    <strong><fmt:message key="ficha.evento.email"/></strong> ${content.value.FichaEvento.value.Contacto.value.Email }
                                </dd>
                            </dl>
                        </c:if>
                        <c:if test="${content.value.FichaEvento.value.Web.exists and content.value.FichaEvento.value.Web.value.Href.isSet }">
                            <dl class="enlace dl-vertical">
                                <dt><fmt:message key="ficha.evento.enlace"/></dt>
                                <dd>
                                    <c:if test="${fn:startsWith(href, 'http')==false}">
                                        <c:set var="href"><cms:link>${content.value.FichaEvento.value.Web.value.Href}</cms:link></c:set>
                                    </c:if>

                                    <a href="${content.value.FichaEvento.value.Web.value.Href}" target="${content.value.FichaEvento.value.Web.value.Target}" <c:if test="${content.value.FichaEvento.value.Web.value.Follow !=null && content.value.FichaEvento.value.Web.value.Follow=='false' }">rel="nofollow"</c:if> title="${content.value.FichaEvento.value.Web.value.Title}">${content.value.FichaEvento.value.Web.value.Title}</a>
                                </dd>
                            </dl>
                        </c:if>
                        <dl class="inidate dl-vertical">
                            <dt> <fmt:message key="ficha.evento.inidate"/></dt>
                            <dd>
                                <cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-time.jsp:ea2c737f-df65-11e4-bcf9-01e4df46f753)">
                                    <cms:param name="date">${content.value.FichaEvento.value.FechaInicio }</cms:param>
                                    <cms:param name="showtime">false</cms:param>
                                    <cms:param name="shortdate">true</cms:param>
                                </cms:include>
                                <c:if test='${content.value.FichaEvento.value.ShowInicioTime != "false" }'>
                                    &nbsp;|&nbsp;
                                    <cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-time.jsp:ea2c737f-df65-11e4-bcf9-01e4df46f753)">
                                        <cms:param name="date">${content.value.FichaEvento.value.FechaInicio }</cms:param>
                                        <cms:param name="showtime">true</cms:param>
                                        <cms:param name="onlytime">true</cms:param>
                                    </cms:include>
                                </c:if>
                            </dd>
                        </dl>
                        <c:if test="${content.value.FichaEvento.value.FechaFin.isSet }">
                            <dl class="findate dl-vertical">
                                <dt><fmt:message key="ficha.evento.findate"/></dt>
                                <dd>
                                    <cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-time.jsp:ea2c737f-df65-11e4-bcf9-01e4df46f753)">
                                        <cms:param name="date">${content.value.FichaEvento.value.FechaFin }</cms:param>
                                        <cms:param name="showtime">false</cms:param>
                                        <cms:param name="shortdate">true</cms:param>
                                    </cms:include>
                                    <c:if test='${content.value.FichaEvento.value.ShowFinTime != "false" }'>
                                        &nbsp;|&nbsp;
                                        <cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-time.jsp:ea2c737f-df65-11e4-bcf9-01e4df46f753)">
                                            <cms:param name="date">${content.value.FichaEvento.value.FechaFin }</cms:param>
                                            <cms:param name="showtime">true</cms:param>
                                            <cms:param name="onlytime">true</cms:param>
                                        </cms:include>
                                    </c:if>
                                </dd>
                            </dl>
                        </c:if>
                        <c:if test="${content.value.FichaEvento.value.MasInfo.exists }">
                            <dl class="masinfo dl-vertical">
                                <dt>${content.value.FichaEvento.value.MasInfo.value.Key }</dt>
                                <dd>${content.value.FichaEvento.value.MasInfo.value.Value }</dd>
                            </dl>
                        </c:if>
                    </div>
                    <c:if test="${content.value.Text.isSet}">
                        ${content.value.Text}
                    </c:if>
                </div>
            </div>
        </div>
        <c:if test="${content.value.Content.exists}">

            <c:forEach var="elem" items="${content.valueList.Content}" varStatus="status">
                <c:set var="contentblock" value="${elem}" scope="request"></c:set>
                <cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-default-contentblock-pdf.jsp:74df911a-b5fd-11e5-8e51-7fb253176922)">
                    <cms:param name="count">${status.count}</cms:param>
                    <cms:param name="tabs">false</cms:param>
                </cms:include>
            </c:forEach>

        </c:if> <%-- Fin cierre comprobacion de seccion --%>

                <!-- Pie del articulo -->
                <c:if test="${content.value.ShowFooter == 'true' }">
                    <div class="posted">
                        <span class="fa fa-calendar" aria-hidden="true"></span>&nbsp;&nbsp;
                        <cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-time.jsp:ea2c737f-df65-11e4-bcf9-01e4df46f753)">
                            <cms:param name="date">${content.value.Date }</cms:param>
                            <cms:param name="showtime">false</cms:param>
                        </cms:include>
                    </div>
                    <c:if test="${content.value.Author.exists }">
                        <div class="autor"><span class="fa fa-user" aria-hidden="true"></span>&nbsp;&nbsp;${content.value.Author.value.Author }</div>
                    </c:if>
                    <c:if test="${content.value.Source.isSet }">
                        <div class="fuente"><span class="fa fa-book" aria-hidden="true"></span>&nbsp;&nbsp;${content.value.Source}</div>
                    </c:if>
                </c:if>
            </div>
        </div>

</body>


</cms:formatter>
</html>
</cms:bundle>