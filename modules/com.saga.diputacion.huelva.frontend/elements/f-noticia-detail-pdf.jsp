<%@page trimDirectiveWhitespaces="true" buffer="none" session="false" taglibs="c,cms,fmt,fn" %>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates" %>
<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.core.messages">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>
<cms:formatter var="content">


<%-- Accedemos al .themeconfig de esta forma porque no nos llega en el request del template del Core --%>
<c:set var="filename">${content.filename}</c:set>
<%--<c:set var="themeConfigPath"><cms:link>${param.themeconfigpath}</cms:link></c:set>--%>
<cms:contentload collector="singleFile" param="${param.themeconfigpath}">
    <cms:contentaccess var="themeconfig"/>
</cms:contentload>

<head>
    <title>${content.value.Title}</title>
    <link rel="stylesheet" href="<cms:link>/system/modules/com.saga.sagasuite.core/resources/css/bootstrap.min.css</cms:link>" type="text/css" />
    <link rel="stylesheet" href="<cms:link>/system/modules/com.saga.sagasuite.core/resources/css/generated-pdf.css</cms:link>" type="text/css" />
    <%-- Si se edita el themeconfig para que haya cabecera se añade un estilo que hace mas grande el margen de la pagina por arriba para que el contenido no pise la cebcera --%>
	<%-- si se define una custom css en el themeconfig para los pdfs la incluye aqui --%>
    <c:if test="${themeconfig.value.CustomCssPdf.isSet}">
        <link rel="stylesheet" href="<cms:link>${themeconfig.value.CustomCssPdf}</cms:link>" type="text/css" />
    </c:if>
    <c:if test="${themeconfig.value.LogoEntity.isSet or themeconfig.value.AdditionalInfo.isSet}">
        <style type="text/css">
            @page  {
                margin-top: 14%;
            }
        </style>
    </c:if>
    <c:if test="${themeconfig.value.TextoPiePdf.isSet}">
        <style type="text/css">
            @page  {
                margin-bottom: 14%;
            }
        </style>
    </c:if>
	<c:if test="${themeconfig.value.CssInline.isSet}">
	    <style type="text/css">
			${themeconfig.value.CssInline}
        </style>
	</c:if>
</head>
<body>

<%-- cabecera --%>
<c:choose>
    <c:when test="${themeconfig.value.LogoCabaceraPdf.isSet or themeconfig.value.TextoCabeceraPdf.isSet}">
        <div id="header">
            <div class="header-content">
                <c:if test="${themeconfig.value.LogoCabaceraPdf.isSet}">
                    <div class="header-img">
                        <cms:img src="${themeconfig.value.LogoCabaceraPdf}" scaleQuality="100"/>
                    </div>
                </c:if>
                <c:if test="${themeconfig.value.TextoCabeceraPdf.isSet}">
                    <div class="additional-info">
                        ${themeconfig.value.TextoCabeceraPdf}
                    </div>
                </c:if>
            </div>
        </div>
    </c:when>
    <c:when test="${themeconfig.value.LogoEntity.isSet or themeconfig.value.AdditionalInfo.isSet}">
        <div id="header">
            <div class="header-content">
                <c:if test="${themeconfig.value.LogoEntity.isSet}">
                    <div class="header-img">
                        <cms:img src="${themeconfig.value.LogoEntity}" scaleQuality="100"/>
                    </div>
                </c:if>
                <c:if test="${themeconfig.value.AdditionalInfo.isSet}">
                    <div class="additional-info">
                        ${themeconfig.value.AdditionalInfo}
                    </div>
                </c:if>
            </div>
        </div>
    </c:when>
</c:choose>

	<%-- footer --%>
	<div id="footer">
		<c:if test="${themeconfig.value.TextoPiePdf.isSet}">
			<div class="footer-content">
				${themeconfig.value.TextoPiePdf}
			</div>					
		</c:if>
		<div class="pagination">
			<span id="pagenumber"></span>
		</div>
	</div>
<div class="articulo parent element dph-noticia">
    <div class="wrapper">

            <!-- Cabecera del articulo -->
        <div class="headline">
			<span class="time">
				<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-time.jsp:ea2c737f-df65-11e4-bcf9-01e4df46f753)">
                    <cms:param name="date">${content.value.Date }</cms:param>
                    <cms:param name="showtime">false</cms:param>
                </cms:include>
			</span>
                <h1 class="title">
                    ${content.value.Title}
                </h1>
                <c:if test="${content.value.SubTitle.isSet }">
                    <h2 class="subtitle">${content.value.SubTitle}</h2>
                </c:if>

        </div>
        <c:if test="${content.value.Entradilla.isSet }">
            <div class="entradilla">
                ${content.value.Entradilla }
            </div>
        </c:if>

                <c:if test="${content.value.Content.exists}">

                    <c:forEach var="elem" items="${content.valueList.Content}" varStatus="status">
                        <c:set var="contentblock" value="${elem}" scope="request"></c:set>
                        <cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-default-contentblock-pdf.jsp:74df911a-b5fd-11e5-8e51-7fb253176922)">
                            <cms:param name="count">${status.count}</cms:param>
                        </cms:include>
                    </c:forEach>

                </c:if>

                <!-- Pie del articulo -->
                <c:if test="${content.value.ShowFooter == 'true' }">
                    <div class="posted">
                        <span class="fa fa-calendar" aria-hidden="true"></span>&nbsp;&nbsp;
                        <cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-time.jsp:ea2c737f-df65-11e4-bcf9-01e4df46f753)">
                            <cms:param name="date">${content.value.Date }</cms:param>
                            <cms:param name="showtime">false</cms:param>
                        </cms:include>
                    </div>
                    <c:if test="${content.value.Author.exists }">
                        <div class="autor"><span class="fa fa-user" aria-hidden="true"></span>&nbsp;&nbsp;${content.value.Author.value.Author }</div>
                    </c:if>
                    <c:if test="${content.value.Source.isSet }">
                        <div class="fuente"><span class="fa fa-book" aria-hidden="true"></span>&nbsp;&nbsp;${content.value.Source}</div>
                    </c:if>
                </c:if>
            </div>
        </div>
</body>

</cms:formatter>

</html>
</cms:bundle>