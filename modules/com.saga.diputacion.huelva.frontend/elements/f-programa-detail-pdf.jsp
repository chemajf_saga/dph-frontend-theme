<%@page trimDirectiveWhitespaces="true" buffer="none" session="false" taglibs="c,cms,fmt,fn"%>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates"%>
<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.diputacion.huelva.frontend.messages">
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
	<html>
<cms:formatter var="content">
	<%-- Accedemos al .themeconfig de esta forma porque no nos llega en el request del template del Core --%>
	<c:set var="filename">${content.filename}</c:set>
	<%--<c:set var="themeConfigPath"><cms:link>${param.themeconfigpath}</cms:link></c:set>--%>
	<cms:contentload collector="singleFile" param="${param.themeconfigpath}">
		<cms:contentaccess var="themeconfig" />
	</cms:contentload>
	<head>
<title>${content.value.Title}</title>
<link rel="stylesheet"
	href="<cms:link>/system/modules/com.saga.sagasuite.core/resources/css/bootstrap.min.css</cms:link>" type="text/css" />
<link rel="stylesheet"
	href="<cms:link>/system/modules/com.saga.sagasuite.core/resources/css/generated-pdf.css</cms:link>" type="text/css" />
<%-- Si se edita el themeconfig para que haya cabecera se añade un estilo que hace mas grande el margen de la pagina por arriba para que el contenido no pise la cebcera --%>
<%-- si se define una custom css en el themeconfig para los pdfs la incluye aqui --%>
<c:if test="${themeconfig.value.CustomCssPdf.isSet}">
	<link rel="stylesheet" href="<cms:link>${themeconfig.value.CustomCssPdf}</cms:link>" type="text/css" />
</c:if>
<c:if test="${themeconfig.value.LogoEntity.isSet or themeconfig.value.AdditionalInfo.isSet}">
	<style type="text/css">
@page {
	margin-top: 14%;
}
</style>
</c:if>
<c:if test="${themeconfig.value.TextoPiePdf.isSet}">
	<style type="text/css">
@page {
	margin-bottom: 14%;
}
</style>
</c:if>
<c:if test="${themeconfig.value.CssInline.isSet}">
	<style type="text/css">
${
themeconfig
.value
.CssInline
}
</style>
</c:if>
	</head>
	<body>
		<%-- cabecera --%>
		<c:choose>
			<c:when test="${themeconfig.value.LogoCabaceraPdf.isSet or themeconfig.value.TextoCabeceraPdf.isSet}">
				<div id="header">
					<div class="header-content">
						<c:if test="${themeconfig.value.LogoCabaceraPdf.isSet}">
							<div class="header-img">
								<cms:img src="${themeconfig.value.LogoCabaceraPdf}" scaleQuality="100" />
							</div>
						</c:if>
						<c:if test="${themeconfig.value.TextoCabeceraPdf.isSet}">
							<div class="additional-info">${themeconfig.value.TextoCabeceraPdf}</div>
						</c:if>
					</div>
				</div>
			</c:when>
			<c:when test="${themeconfig.value.LogoEntity.isSet or themeconfig.value.AdditionalInfo.isSet}">
				<div id="header">
					<div class="header-content">
						<c:if test="${themeconfig.value.LogoEntity.isSet}">
							<div class="header-img">
								<cms:img src="${themeconfig.value.LogoEntity}" scaleQuality="100" />
							</div>
						</c:if>
						<c:if test="${themeconfig.value.AdditionalInfo.isSet}">
							<div class="additional-info">${themeconfig.value.AdditionalInfo}</div>
						</c:if>
					</div>
				</div>
			</c:when>
		</c:choose>
		<%-- footer --%>
		<div id="footer">
			<c:if test="${themeconfig.value.TextoPiePdf.isSet}">
				<div class="footer-content">${themeconfig.value.TextoPiePdf}</div>
			</c:if>
			<div class="pagination">
				<span id="pagenumber"></span>
			</div>
		</div>
		<div class="articulo parent element dph-programa">
			<div class="wrapper">
				<!-- Cabecera del articulo -->
				<div class="headline">
					<h1 class="title">${content.value.Title}</h1>
					<c:if test="${content.value.SubTitle.isSet }">
						<h2 class="subtitle">${content.value.SubTitle}</h2>
					</c:if>
					<c:if test="${content.value.Municipio.isSet }">
						<div class="h5 upper municipio" style="text-transform: uppercase">
							<c:set var="categorias" value="${fn:split(content.value.Municipio, ',')}" />
							<c:set var="counter" value="0" />
							<c:forEach items="${categorias }" var="category" varStatus="status">
								<c:set var="categoryhasmunicipio" value="${fn:indexOf(category, '/municipios/')}" />
								<c:if test="${categoryhasmunicipio != -1}">
									<%--====== Comprobamos si existe la propiedad Title especifica para el locale actual (Title_es, Title_en...) para la categoria.
                                Si es asi usamos esa propiedad. Si no existe, usamos la propiedad Title =============================--%>
									<c:set var="categorytitle">
										<cms:property name="Title_${locale}" file="${category}" />
									</c:set>
									<c:if test="${empty categoryPropertyTitle}">
										<c:set var="categorytitle">
											<cms:property name="Title" file="${category}" />
										</c:set>
									</c:if>
                                ${categorytitle}
                            </c:if>
							</c:forEach>
						</div>
					</c:if>
				</div>
				<div class="contentblock">
					<div class="contentblock-texto row">
						<%-- Imagen --%>
						<c:if test="${content.value.Image.isSet}">
							<c:set var="imagePath">${content.value.Image}</c:set>
							<c:set var="imgWidth">720</c:set>
							<c:set var="imgHeight">140</c:set>
							<div class="media-object-print container-fluid" style="margin-bottom: 20px">
								<div class="wrapper">
									<cms:img src="${imagePath}" width="720" height="140" cssclass="img-responsive" maxWidth="720" scaleType="2" />
									<a href="<cms:link>${imagePath}</cms:link>" class="btn-download-img"><fmt:message
											key="label.download.image" /></a>
								</div>
							</div>
						</c:if>
						<%-- Texto y actividades--%>
						<div class="container-fluid">
							<c:if test="${content.value.Text.isSet}">
								<div class="text-block">${content.value.Text}</div>
							</c:if>
							<%-- actividades --%>
							<c:set var="programRootPath">${content.file.rootPath}</c:set>
							<c:set var="rootPath">${cms.requestContext.siteRoot}/</c:set>
							<c:set var="resourceType">dphevento</c:set>
							<c:set var="solrRows">100</c:set>
							<c:set var="solrQuery">fq=parent-folders:"${rootPath}"&fq=type:${resourceType}&xmlprograma_${cms.locale}_s:"${programRootPath}"&rows=${solrRows}&sort=xmldate_${cms.locale}_dt desc</c:set>
							<cms:contentload collector="byContext" param='${solrQuery}' preload="true">
								<cms:contentinfo var="info" />
								<c:choose>
									<c:when test="${info.resultSize > 0}">
										<div class="gallery link-gallery" style="margin-top: 20px">
											<div class="h4 mb-20">
												<fmt:message key="key.formatter.program.activities" />
											</div>
											<ul class="list-unstyled no-margin">
												<c:set var="now" value="<%=new java.util.Date().getTime()%>" />
												<cms:contentload>
													<cms:contentaccess var="elem" />
													<fmt:parseNumber var="eventDate" value="${elem.value.FichaEvento.value.FechaInicio}" integerOnly="true" />
													<c:if test="${elem.value.FichaEvento.value.FechaFin.isSet}">
														<fmt:parseNumber var="eventDate" value="${elem.value.FichaEvento.value.FechaFin}" integerOnly="true" />
													</c:if>
													<c:set var="eventactiveclass"></c:set>
													<c:if test="${now > eventDate}">
														<c:set var="eventactiveclass">event-held</c:set>
														<c:set var="eventheld" scope="request">true</c:set>
													</c:if>
													<li class="media program-event-item ${eventactiveclass}">
														<div class="media-body-print">
															<div class="date">
																<strong><fmt:formatDate dateStyle="LONG"
																		value="${cms:convertDate(elem.value.FichaEvento.value.FechaInicio)}" type="date" /></strong>
															</div>
															<div class="media-heading h5">
																<a href="<cms:link>${elem.filename }</cms:link>"> ${elem.value.Title } </a>
															</div>
														</div>
													</li>
												</cms:contentload>
											</ul>
										</div>
									</c:when>
									<c:otherwise></c:otherwise>
								</c:choose>
							</cms:contentload>
							<c:set var="eventheld" scope="request" value="" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</cms:formatter>
	</html>
</cms:bundle>