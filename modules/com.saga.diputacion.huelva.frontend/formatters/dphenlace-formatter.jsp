<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.microcontent.messages">
<cms:formatter var="content" val="value" rdfa="rdfa">

	<%--===== settings =================================================--%>

	<c:if test="${cms.element.setting.marginbottom.value != '0'}">
		<c:set var="marginClass">margin-bottom-${cms.element.setting.marginbottom.value}</c:set>
	</c:if>
	<c:if test="${not empty cms.element.settings.classmainbox }">
		<c:set var="classmainbox">${cms.element.settings.classmainbox}</c:set>
	</c:if>
	<c:if test="${cms.element.settings.borderedbox == 'true' }">
		<c:set var="borderedbox">bordered-box</c:set>
	</c:if>

	<c:choose>
		<c:when test="${cms.element.inMemoryOnly}">
			<header>
				<h1 class="title">
					Edite este recurso!!
				</h1>
			</header>
		</c:when>
		<c:otherwise>

			<div class="element parent box dph-banner<c:out value=' ${marginClass} ${classmainbox} ${value.CssClass} ${borderedbox}' />">
				<%--===== si hay enlace =================================================--%>
				<c:if test="${value.LinkConfig.exists}">
					<c:set var="bannerlinktitle"></c:set>
					<c:set var="bannerlinkurl"></c:set>
					<c:set var="bannerlinktarget"></c:set>
					<c:set var="bannerlinkfollow"></c:set>
					<c:set var="space" value=" " />
					<c:set var="bannerlink" value="${value.LinkConfig}" />
					<c:if test="${bannerlink.value.Title.isSet}">
						<c:set var="bannerlinktitle">${fn:replace(bannerlink.value.Title,'\"','\'')}</c:set>
					</c:if>
					<c:if test="${bannerlink.value.Href.isSet}">
						<c:set var="bannerlinkurl"><cms:link>${bannerlink.value.Href}</cms:link></c:set>
						<c:if test="${fn:startsWith(bannerlinkurl, 'http:')}">
							<c:set var="bannerlinkurl">${bannerlink.value.Href}</c:set>
						</c:if>
					</c:if>
					<c:if test="${bannerlink.value.Target == '_blank'}">
						<c:set var="bannerlinktarget">target="_blank"</c:set>
					</c:if>
					<c:if test="${bannerlink.value.Follow == 'false'}">
						<c:set var="bannerlinkfollow">follow="nofollow"</c:set>
					</c:if>
					<a href="${bannerlinkurl}" class="wrapper-image" <c:if test="${not empty bannerlinktitle}">title="${bannerlinktitle}"</c:if>${space}${bannerlinktarget}${space}${bannerlinkfollow}>
				</c:if>
						<%--===== si no se edita la imagen se carga una por defecto que avisa que no se ha seleccionado imagen y si se edita se usa la del campo  =================================================--%>
						<c:set var="bannerimage"></c:set>
						<c:set var="bannerimagealt"></c:set>
						<c:set var="bannerimagewidth"></c:set>
						<c:set var="bannerimageheight"></c:set>
						<c:if test="${value.Image.isSet}">
							<c:set var="bannerimage" value="${value.Image}" />
						</c:if>
						<c:if test="${!value.Image.isSet}">
							<c:set var="bannerimage">/system/modules/com.saga.sagasuite.microcontent/resources/img/no-img-default.png</c:set>
						</c:if>
						<c:set var="locale">${cms.locale}</c:set>
						<%--====== Comprobamos si existe la propiedad Title especifica para el locale actual (Title_es, Title_en...).
						Si es asi usamos esa propiedad. Si no existe, usamos la propiedad Title =============================--%>
						<c:set var="bannerimagealt"><cms:property name="Title_${locale}" file="${bannerimage}"/></c:set>
						<c:if test="${empty bannerimagealt}">
							<c:set var="bannerimagealt"><cms:property name="Title" file="${bannerimage}"/></c:set>
						</c:if>
						<c:if test="${value.ImageWidth.isSet}">
							<c:set var="bannerimagewidth">${value.ImageWidth}</c:set>
						</c:if>
						<c:if test="${value.ImageHeight.isSet}">
							<c:set var="bannerimageheight">${value.ImageHeight}</c:set>
						</c:if>
						<cms:img src="${bannerimage}" alt="${bannerimagealt}" cssclass="img-responsive" width="${bannerimagewidth}" height="${bannerimageheight}" scaleType="2"/>
				<%--===== si hay enlace se cierra la etiqueta =================================================--%>
				<c:if test="${value.LinkConfig.exists}">
					</a>
				</c:if>
			</div>

		</c:otherwise>
	</c:choose>
</cms:formatter>
</cms:bundle>