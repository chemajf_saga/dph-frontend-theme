<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.diputacion.huelva.frontend.messages">
<cms:formatter var="content" val="value" rdfa="rdfa">
	<c:set var="locale">${cms.locale}</c:set>
	<article>
		<div class="media <c:out value='${value.CssClass}' />">
			<div class="media-left media-middle">
				<c:set var="date">${value.FichaEvento.value.FechaInicio }</c:set>
				<div class="media-object hastooltip" title="<fmt:formatDate dateStyle='FULL' value='${cms:convertDate(date)}' type='date' />">
					<time class="sr-only" datetime="<fmt:formatDate value="${cms:convertDate(date)}" pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ" />">
						<fmt:formatDate dateStyle="FULL" value="${cms:convertDate(date)}" type="date" />
					</time>
					<c:set var="dateday">
						<fmt:formatDate value="${cms:convertDate(date)}" pattern="EEE" />
					</c:set>
					<c:set var="datedaynumber">
						<fmt:formatDate value="${cms:convertDate(date)}" pattern="dd"/>
					</c:set>
					<c:set var="datemonth">
						<fmt:formatDate value="${cms:convertDate(date)}" pattern="MMM"/>
					</c:set>
					<div class="day upper">${dateday}</div>
					<div class="daynumber">${datedaynumber}</div>
					<div class="month upper">${datemonth}</div>
				</div>
			</div>
			<a class="media-body media-middle" href="<cms:link>${content.filename}</cms:link>">
				<span class="media-heading">
					${value.Title}
				</span>
				<c:if test="${eventheld == 'true'}">
					<abbr class="event-held-msg hastooltip pull-right inline-b v-align-m" title="<fmt:message key='key.formatter.event.activity.held' />">
						<span class="fa fa-calendar-check-o fs20" aria-hidden="true"></span>
						<span class="sr-only"><fmt:message key="key.formatter.event.held" /></span>
					</abbr>
				</c:if>
			</a>
		</div>
	</article>
</cms:formatter>
</cms:bundle>