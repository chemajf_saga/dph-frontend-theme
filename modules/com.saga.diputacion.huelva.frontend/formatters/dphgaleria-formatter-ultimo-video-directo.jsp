<%@ page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="org.opencms.jsp.util.CmsJspContentAccessValueWrapper"%>
<%@ page import="com.saga.diputacion.huelva.frontend.ObjectOrder"%>
<%@ page import="java.util.Collections"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates"%>
<%@ taglib prefix="sgcore" tagdir="/WEB-INF/tags/core/templates"%>
<%
// Realizamos una consulta a base de datos para ver si tenemos vídeo en directo
List<Map<String, String>> res = new LinkedList<Map<String, String>>();
String sql = 
"SELECT rp.reproductor as rp, rp.html as html, rt.descripcion as des, rt.titulo as tit, fi.fichero as imagen_fichero, fi.descripcion as imagen_descripcion " +
"FROM retransmision rt " +
"LEFT OUTER JOIN reproductor rp " +
"ON rt.reproductor = rp.reproductor " +
"LEFT OUTER JOIN fichero fi " +
"ON rt.imagen = fi.idfichero " +
"WHERE rt.publicado = 'S' OR " +
"(rt.public_auto = 'S' AND rt.fecha = CURRENT_DATE AND rt.hora_inicio <= CURRENT_TIME AND rt.hora_fin >= CURRENT_TIME)";

Connection conexion = null;
Statement sentencia = null;
ResultSet rs = null;

try {
    Class.forName("org.postgresql.Driver");
    //System.out.println("Puente JDBC-ODBC cargado (dphgaleria-formatter-ultimo-video-directo.jsp)!");
} catch (Exception ex) {
    //System.out.println("ERROR (dphgaleria-formatter-ultimo-video-directo.jsp): No se pudo cargar el puente JDBC-ODBC.");
    ex.printStackTrace();
    return;
}

try {
    // DESA conexion = DriverManager.getConnection("jdbc:postgresql://192.168.131.30:5432/portalweb", "ocmsdph", "ocmsdph");
    /*PROD*/conexion = DriverManager.getConnection("jdbc:postgresql://192.168.136.33:5432/portalweb", "ocmsdph", "ocmsdph");
    //System.out.println("Conexion realizada (dphgaleria-formatter-ultimo-video-directo.jsp)");
} catch (Exception er) {
    //System.out.println("ERROR obteniendo conexion (dphgaleria-formatter-ultimo-video-directo.jsp): " + er);
    er.printStackTrace();
    return;
}

try {
    sentencia = conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
    //System.out.println("Sentencia creada (dphgaleria-formatter-ultimo-video-directo.jsp)");
} catch (Exception er) {
    //System.out.println("ERROR obteniendo sentencia (dphgaleria-formatter-ultimo-video-directo.jsp): " + er);
    er.printStackTrace();
    return;
}

int i = 0;
try {
    //System.out.println("Realizamos la consulta (dphgaleria-formatter-ultimo-video-directo.jsp): " + sql);
    rs = sentencia.executeQuery(sql);
    while (rs.next()) {
        Map<String, String> aux = new HashMap<String, String>();
        aux.put("rp", rs.getString("rp"));
        aux.put("html", rs.getString("html"));
        aux.put("des", rs.getString("des"));
        aux.put("tit", rs.getString("tit"));
        aux.put("imagen_fichero", rs.getString("imagen_fichero"));
        aux.put("imagen_descripcion", rs.getString("imagen_descripcion"));

        res.add(i, aux);
        i++;
    }
} catch (Exception er) {
    //System.out.println("ERROR realizando consulta (dphgaleria-formatter-ultimo-video-directo.jsp): " + er);
    er.printStackTrace();
    return;
}

%>
<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.diputacion.huelva.frontend.messages">
	<cms:formatter var="content" val="value" rdfa="rdfa">
		<c:choose>
			<c:when test="${cms.element.inMemoryOnly}">
				<h1 class="title">Edite este recurso!!</h1>
			</c:when>
			<c:otherwise>
			
			<%-- Cargamos la variable con el id especifico del recurso para su uso posterior--%>
			<c:set var="idresource" value="${content.id}"></c:set>
			<c:set var="id">${fn:substringBefore(idresource, '-')}</c:set>
			<c:set var="locale">${cms.locale}</c:set>
			<c:if test="${not empty cms.element.settings.classmainbox }">
				<c:set var="classmainbox">${cms.element.settings.classmainbox}</c:set>
			</c:if>
			<c:if test="${cms.element.setting.marginbottom.value != '0'}">
				<c:set var="marginClass">margin-bottom-${cms.element.setting.marginbottom.value}</c:set>
			</c:if>
			<%-- Definimos el tipo de etiqueta html para nuestro encabezado --%>
			<c:set var="titletag">h1</c:set>
			<c:set var="subtitletag">h2</c:set>
			<c:set var="titletagelement">h2</c:set>
			<c:set var="titletagelementsize">h5</c:set>
			<c:if test="${not empty cms.element.settings.titletag }">
				<c:set var="titletag">${cms.element.settings.titletag }</c:set>
				<c:if test="${titletag == 'h1'}">
					<c:set var="subtitletag">h2</c:set>
				</c:if>
				<c:if test="${titletag == 'h2'}">
					<c:set var="subtitletag">h3</c:set>
				</c:if>
				<c:if test="${titletag == 'h3'}">
					<c:set var="subtitletag">h4</c:set>
				</c:if>
				<c:if test="${titletag == 'h4'}">
					<c:set var="subtitletag">h5</c:set>
				</c:if>
				<c:if test="${titletag == 'h5'}">
					<c:set var="subtitletag">h6</c:set>
				</c:if>
				<c:if test="${titletag == 'div'}">
					<c:set var="subtitletag">div</c:set>
				</c:if>
			</c:if>
			
<%
			if (res.size() > 0) {
			    //Si tenemos resultado, tenemos vídeo en directo
			    System.out.println(" ~ Tenemos vídeo en directo ~ ");
			    Map<String, String> m = res.get(0);
%>
				<div class="articulo parent element dph-galeria<c:out value=' ${classmainbox} ${marginClass}' />" id="${id}">
					<div class="wrapper <c:out value='${value.CssClass}' />">
						<header class="headline">
							<${titletag} class="title" ${rdfa.Title}>Emisión en Directo</${titletag}>
						</header>
					</div>
				</div>
				
				<div class="galleryblock">
					<div class="row">
						<div class="featured-item-block mb-40 col-xxs-${cms.element.settings.widthsett}">
							<div aria-label="Elemento multimedia destacado o reproduciéndose" aria-live="assertive" aria-atomic="true" aria-relevant="all" aria-busy="true">
								<div class="gallery-media gallery-media-video gallery-media-video-url">
									<div class="gallery-media-wrapper">
										<div class="embed-responsive embed-responsive-16by9">
											<%=m.get("html")%>
										</div>
									</div>
									<h2 class="h3"><%=m.get("tit")%></h2>
									<div class="description mt-10"><%=m.get("des")%></div>
								</div>
							</div>
						</div>
					</div>
				</div>
<%
			} else {
%>
				<%-- si se ha editado el subtitulo lo mandamos en el request para que se tenga en cuenta en la seccion o en el bloque de texto por si se edita el titulo de seccion o de bloque --%>
				<c:if test="${value.SubTitle.isSet }">
					<c:set var="subtitletag">${subtitletag}</c:set>
					<c:if test="${subtitletag == 'h2'}">
						<c:set var="titletagelement">h3</c:set>
					</c:if>
					<c:if test="${subtitletag == 'h3'}">
						<c:set var="titletagelement">h4</c:set>
					</c:if>
					<c:if test="${subtitletag == 'h4'}">
						<c:set var="titletagelement">h5</c:set>
					</c:if>
					<c:if test="${subtitletag == 'h5' or subtitletag == 'div'}">
						<c:set var="titletagelement">div</c:set>
					</c:if>
				</c:if>
				<c:if test="${not empty cms.element.settings.titletagelement }">
					<c:set var="titletagelement">${cms.element.settings.titletagelement}</c:set>
				</c:if>
				<c:if test="${not empty cms.element.settings.titletagelementsize }">
					<c:set var="titletagelementsize">${cms.element.settings.titletagelementsize}</c:set>
				</c:if>
				<%-- Establecemos la imagen por defecto definida en el .themeconfig por si no viene imagen en el recurso --%>
				<c:if test="${not empty themeConfiguration.CustomFieldKeyValue.defaultimg}">
					<c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg}</c:set>
					<c:if test="${count > 1 and count %2 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg2}">
						<c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg2}</c:set>
					</c:if>
					<c:if test="${count > 1 and count %3 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg3}">
						<c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg3}</c:set>
					</c:if>
					<c:if test="${count > 1 and count %4 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg4}">
						<c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg4}</c:set>
					</c:if>
					<c:if test="${count > 1 and count %5 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg5}">
						<c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg5}</c:set>
					</c:if>
				</c:if>
				<%-- guardamos en una variable la ruta del themeconfig que le corresponde al recurso en funcion de la rama donde se este pintando. despues lo mandamos como parametro en el href del enlace para generar el pdf --%>
				<c:set var="themeconfigpath">${cms.vfs.context.siteRoot}${themeconfigpath}<sg:resourcePathLookBack filename=".themeconfig" /></c:set>
				<div class="articulo parent element dph-galeria<c:out value=' ${classmainbox} ${marginClass}' />" id="${id}">
					<div class="wrapper <c:out value='${value.CssClass}' />">
						<c:if test="${not cms.element.settings.hidetitle}">
							<!-- Cabecera del articulo -->
							<header class="headline">
								<div class="time">
									<cms:include
										file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-time.jsp:ea2c737f-df65-11e4-bcf9-01e4df46f753)">
										<cms:param name="date">${value.Date}</cms:param>
										<cms:param name="showtime">false</cms:param>
									</cms:include>
								</div>
								<${titletag} class="title" ${rdfa.Title}>${value.Title}</${titletag}>
								<c:if test="${value.SubTitle.isSet }"> <${subtitletag} class="subtitle" ${rdfa.SubTitle}>${value.SubTitle}</${subtitletag}></c:if>
								<c:if test="${value.Municipio.isSet or (value.Tags.exists and value.Tags.isSet)}">
									<c:set var="searchpage"></c:set>
									<c:set var="searchfieldname"></c:set>
									<%-- Primero buscamos la propiedad y luego los campos customizados del .themeconfig
									para la página del buscador que recibirá los tags por param y
									el atributo 'name' del campo del buscador que los cargará --%>
									<c:set var="searchpage">
										<cms:property name="sagasuite.articles.search.page" file="search" />
									</c:set>
									<c:if test="${not empty themeConfiguration.CustomFieldKeyValue.UrlSearchParamPage}">
										<c:set var="searchpage">${themeConfiguration.CustomFieldKeyValue.UrlSearchParamPage}</c:set>
									</c:if>
									<c:set var="tagsearchfieldname">
										<cms:property name="sagasuite.articles.search.tag.field.name" file="search" />
									</c:set>
									<c:if test="${not empty themeConfiguration.CustomFieldKeyValue.TagSearchFieldName}">
										<c:set var="tagsearchfieldname">${themeConfiguration.CustomFieldKeyValue.TagSearchFieldName}</c:set>
									</c:if>
									<c:set var="citysearchfieldname">
										<cms:property name="sagasuite.articles.search.city.field.name" file="search" />
									</c:set>
									<c:if test="${not empty themeConfiguration.CustomFieldKeyValue.CitySearchFieldName}">
										<c:set var="citysearchfieldname">${themeConfiguration.CustomFieldKeyValue.CitySearchFieldName}</c:set>
									</c:if>
									<c:set var="typesearchfieldname">
										<cms:property name="sagasuite.articles.search.type.field.name" file="search" />
									</c:set>
									<c:if test="${not empty themeConfiguration.CustomFieldKeyValue.TypeSearchFieldName}">
										<c:set var="typesearchfieldname">${themeConfiguration.CustomFieldKeyValue.TypeSearchFieldName}</c:set>
									</c:if>
									<c:if test="${empty searchpage }">
										<c:set var="searchpage">/busqueda/</c:set>
									</c:if>
									<c:if test="${empty tagsearchfieldname }">
										<c:set var="tagsearchfieldname">tagfield</c:set>
									</c:if>
									<c:if test="${empty citysearchfieldname }">
										<c:set var="citysearchfieldname">cityfield</c:set>
									</c:if>
									<c:if test="${empty typesearchfieldname }">
										<c:set var="typesearchfieldname">typefield</c:set>
									</c:if>
								</c:if>
								<c:if test="${value.Municipio.isSet }">
									<div class="h5 upper municipio">
										<c:set var="categorias" value="${fn:split(value.Municipio, ',')}" />
										<c:set var="counter" value="0" />
										<c:forEach items="${categorias }" var="category" varStatus="status">
											<c:set var="categoryhasmunicipio" value="${fn:indexOf(category, '/municipios/')}" />
											<c:if test="${categoryhasmunicipio != -1}">
												<%-- Comprobamos si existe la propiedad Title especifica para el locale actual (Title_es, Title_en...) para la categoria.
												Si es asi usamos esa propiedad. Si no existe, usamos la propiedad Title --%>
												<c:set var="categorytitle">
													<cms:property name="Title_${locale}" file="${category}" />
												</c:set>
												<c:if test="${empty categoryPropertyTitle}">
													<c:set var="categorytitle">
														<cms:property name="Title" file="${category}" />
													</c:set>
												</c:if>
												<c:set var="categoryshort" value="${fn:replace(category, '/.categories/','')}" />
												<c:set var="categoryshortencoded"><%=java.net.URLEncoder.encode(pageContext.getAttribute("categoryshort").toString(), "UTF-8")%></c:set>
												<a class="hastooltip"
													href="<cms:link>${searchpage}?${citysearchfieldname}=${categoryshortencoded }&${typesearchfieldname}=dphgaleria</cms:link>"
													title="<fmt:message key='key.formatter.new.view.all.same.city' />"><span class="pe-7s-map-marker mr-10"
													aria-hidden="true"></span>${categorytitle}</a>
											</c:if>
										</c:forEach>
									</div>
								</c:if>
								<c:if test="${value.Tags.exists and value.Tags.isSet}">
									<div class="info info-cat info-tag mt-30">
										<ul class="list-inline no-margin">
											<c:forEach items="${content.valueList.Tags }" var="elemtag" varStatus="tagstatus">
												<%--Para que el enlace se codifique de manera que no haya errores si el tag contiene caracteres especiales como por ejemplo "+"--%>
												<c:set var="elemtagencoded"><%=java.net.URLEncoder.encode(pageContext.getAttribute("elemtag").toString(), "UTF-8")%></c:set>
												<li>
													<a class="hastooltip" title="<fmt:message key='key.formatter.new.view.all.same.tag' />"
													href="<cms:link>${searchpage}?${tagsearchfieldname}=${elemtagencoded }&${typesearchfieldname}=dphgaleria</cms:link>">
													<span class="fa fa-hashtag" aria-hidden="true"></span><strong>${elemtag}</strong></a>
												</li>
											</c:forEach>
										</ul>
									</div>
								</c:if>
							</header>
						</c:if>
						<%-- <c:if test="${value.Text.isSet}">
							<div class="contentblock">
								<div class="contentblock-texto" ${rdfa.Text}>${value.Text}</div>
							</div>
						</c:if> --%>
						<c:if test="${value.Content.exists}">
							<c:forEach var="elem" items="${content.valueList.Content}" varStatus="status">
								<c:set var="contentblock" value="${elem}" scope="request"></c:set>
								<cms:include
									file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-default-contentblock.jsp:e9a36d37-df65-11e4-bcf9-01e4df46f753)">
									<cms:param name="contentId">${id}-contentblock-${status.count}</cms:param>
								</cms:include>
							</c:forEach>
						</c:if>
						<%-- Fin cierre comprobacion de seccion --%>
						<%-- galería de imágenes automática  --%>
						<c:if
							test="${value.GalleryImage.exists and (value.GalleryImage.value.ImageGalleryFolder.isSet or value.GalleryImage.value.Image.isSet)}">
							<cms:include
								file="%(link.strong:/system/modules/com.saga.diputacion.huelva.frontend/elements/e-imagegallery.jsp:c8d7fe5a-5369-11e8-a09e-63f9a58dae09)">
								<cms:param name="galleryId">imagegallery-${id}</cms:param>
								<cms:param name="galleryResourcesForRow" value="${value.GalleryImage.value.ResourcesForRow}" />
								<cms:param name="galleryPageElements" value="${value.GalleryImage.value.PageElements}" />
								<cms:param name="galleryOriginSize" value="${value.GalleryImage.value.OriginSize}" />
								<c:if test="${value.GalleryImage.value.ImageGalleryFolder.isSet}">
									<cms:param name="galleryFolder" value="${value.GalleryImage.value.ImageGalleryFolder}" />
								</c:if>
								<c:if test="${value.GalleryImage.value.Image.isSet}">
									<cms:param name="galleryImages" value="${value.GalleryImage.valueList.Image}" />
								</c:if>
							</cms:include>
						</c:if>
						<%-- elementos multimedia --%>
						<c:if test="${value.GalleryMediaElement.exists}">
							<div class="galleryblock">
								<div class="row">
									<%-- PRIMER ITEM --%>
									<div class="featured-item-block mb-40 col-xxs-${cms.element.settings.widthsett}">
										<div class="dph-galeria-loading" style="visibility: hidden">
											<span class="fa fa-spinner fa-pulse fa-3x fa-fw" aria-hidden="true"></span> <span class="sr-only">cargando...</span>
										</div>
										<div aria-label="Elemento multimedia destacado o reproduciéndose"
											aria-live="assertive" aria-atomic="true" aria-relevant="all" aria-busy="true">
											<c:set var="eltos" value="${content.valueList.GalleryMediaElement}" />
<%
										    List<ObjectOrder> listaOrdenada = new ArrayList<ObjectOrder>();
					                        int j = 0;
					                        List<CmsJspContentAccessValueWrapper> elementos =
					                                        (ArrayList<CmsJspContentAccessValueWrapper>) pageContext.getAttribute("eltos");
					                        for (CmsJspContentAccessValueWrapper elemento : elementos) {
					                            long dateL = Long.parseLong(elemento.getValue().get("Date").toString());
					                            ObjectOrder auxOrder = new ObjectOrder(new Date(dateL), elemento);
					                            listaOrdenada.add(j, auxOrder);
					                            j++;
					                        }
					                        Collections.reverse(listaOrdenada);
					                        pageContext.setAttribute("elem", (CmsJspContentAccessValueWrapper) (listaOrdenada.get(0).getObject()) );
%>
											<%-- <c:forEach var="elem" items="${content.valueList.GalleryMediaElement}" varStatus="status" begin="0" end="0"> --%>
												<%-- AUDIO ////////////////////////////////// --%>
												<c:if
													test="${elem.value.GalleryMediaFile.value.Audio.exists and elem.value.GalleryMediaFile.value.Audio.value.Source.exists and (elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileMp3.isSet or elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileWav.isSet or elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileOgg.isSet)}">
													<c:set var="datamediatype">audio</c:set>
													<div class="gallery-media gallery-media-audio">
														<div class="gallery-media-wrapper">
															<cms:include
																file="%(link.strong:/system/modules/com.saga.diputacion.huelva.frontend/elements/e-gallery-audio.jsp:f5a09907-0fda-11e8-966e-63f9a58dae09)">
																<c:if test="${not empty image}">
																	<cms:param name="image">${image}</cms:param>
																</c:if>
																<c:choose>
																	<c:when test="${elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileMp3.isSet}">
																		<cms:param name="audiofilemp3">${elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileMp3}</cms:param>
																	</c:when>
																	<c:when test="${elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileWav.isSet}">
																		<cms:param name="audiofilewav">${elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileWav}</cms:param>
																	</c:when>
																	<c:when test="${elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileOgg.isSet}">
																		<cms:param name="audiofileogg">${elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileOgg}</cms:param>
																	</c:when>
																</c:choose>
															</cms:include>
														</div>
													</div>
												</c:if>
												<%-- VIDEO ////////////////////////////////////////////////// --%>
												<c:if
													test="${elem.value.GalleryMediaFile.value.Video.exists and
														(elem.value.GalleryMediaFile.value.Video.value.FileMp4.isSet
															or elem.value.GalleryMediaFile.value.Video.value.FileWebm.isSet
															or elem.value.GalleryMediaFile.value.Video.value.FileOgg.isSet)}">
													<c:set var="datamediatype">video</c:set>
													<div class="gallery-media gallery-media-video">
														<div class="gallery-media-wrapper">
															<cms:include
																file="%(link.strong:/system/modules/com.saga.diputacion.huelva.frontend/elements/e-gallery-video.jsp:a7f29d3c-0ff1-11e8-966e-63f9a58dae09)">
																<c:if test="${not empty image and !elem.value.GalleryMediaFile.value.Video.value.ImageCover.isSet}">
																	<cms:param name="image">${image}</cms:param>
																</c:if>
																<c:if test="${elem.value.GalleryMediaFile.value.Video.value.ImageCover.isSet}">
																	<cms:param name="image">${elem.value.GalleryMediaFile.value.Video.value.ImageCover}</cms:param>
																</c:if>
																<c:if test="${elem.value.GalleryMediaFile.value.Video.value.FileMp4.isSet}">
																	<cms:param name="source">${elem.value.GalleryMediaFile.value.Video.value.FileMp4}</cms:param>
																</c:if>
															</cms:include>
														</div>
													</div>
												</c:if>
												<%-- VIDEO EMBED URL////////////////////////////////////////////////// --%>
												<c:if
													test="${elem.value.GalleryMediaFile.value.VideoUrl.exists and elem.value.GalleryMediaFile.value.VideoUrl.value.Url.isSet}">
													<c:set var="datamediatype">video</c:set>
													<div class="gallery-media gallery-media-video gallery-media-video-url">
														<div class="gallery-media-wrapper">
															<cms:include
																file="%(link.strong:/system/modules/com.saga.diputacion.huelva.frontend/elements/e-gallery-video-url.jsp:9ea0ff9c-0ff7-11e8-966e-63f9a58dae09)">
																<cms:param name="url">${elem.value.GalleryMediaFile.value.VideoUrl.value.Url}</cms:param>
															</cms:include>
														</div>
													</div>
												</c:if>
												<%-- IMAGEN ////////////////////////////////////////////////// --%>
												<c:if
													test="${elem.value.GalleryMediaFile.value.Image.exists and elem.value.GalleryMediaFile.value.Image.value.Image.isSet}">
													<c:set var="datamediatype">imagen</c:set>
													<div class="gallery-media gallery-media-img">
														<div class="gallery-media-wrapper">
															<c:set var="image">${elem.value.GalleryMediaFile.value.Image.value.Image}</c:set>
															<c:set var="imageTitle">
																<cms:property name="Title_${cms.locale}" file="${image}" />
															</c:set>
															<c:if test="${empty imageTitle}">
																<c:set var="imageTitle">
																	<cms:property name="Title" file="${image}" />
																</c:set>
															</c:if>
															<cms:img src="${image}" cssclass="img-responsive" scaleType="2" width="480" height="360"
																alt="${imageTitle}" />
														</div>
													</div>
													<a href="<cms:link>${image}</cms:link>" data-title="${imageTitle}"
														title="<fmt:message key='label.formatter.expand.image' /> '${imageTitle}'" class="disp-block zoom-filter">
														<span class="sr-only"><fmt:message key='label.formatter.expand.image' /></span>
													</a>
												</c:if>
												<c:if test="${elem.value.Title.isSet}">
													<${titletagelement} class="h3">${elem.value.Title}</${titletagelement}>
												</c:if>
												<c:if test="${!elem.value.Title.isSet and datamediatype == 'imagen'}">
													<${titletagelement} class="h3">${imageTitle}</${titletagelement}>
												</c:if>
												<c:if
													test="${datamediatype != 'imagen' and (
														(elem.value.GalleryMediaFile.value.VideoUrl.exists and elem.value.GalleryMediaFile.value.VideoUrl.value.Duration.exists)
														 or (elem.value.GalleryMediaFile.value.Video.exists and elem.value.GalleryMediaFile.value.Video.value.Duration.exists)
														 or (elem.value.GalleryMediaFile.value.Audio.exists and elem.value.GalleryMediaFile.value.Audio.value.Duration.exists)
														)}">
													<c:if
														test="${elem.value.GalleryMediaFile.value.VideoUrl.exists and elem.value.GalleryMediaFile.value.VideoUrl.value.Duration.exists}">
														<c:set var="hours">${elem.value.GalleryMediaFile.value.VideoUrl.value.Duration.value.Hours}</c:set>
														<c:set var="minutes">${elem.value.GalleryMediaFile.value.VideoUrl.value.Duration.value.Minutes}</c:set>
														<c:set var="seconds">${elem.value.GalleryMediaFile.value.VideoUrl.value.Duration.value.Seconds}</c:set>
													</c:if>
													<c:if
														test="${elem.value.GalleryMediaFile.value.Video.exists and elem.value.GalleryMediaFile.value.Video.value.Duration.exists}">
														<c:set var="hours">${elem.value.GalleryMediaFile.value.Video.value.Duration.value.Hours}</c:set>
														<c:set var="minutes">${elem.value.GalleryMediaFile.value.Video.value.Duration.value.Minutes}</c:set>
														<c:set var="seconds">${elem.value.GalleryMediaFile.value.Video.value.Duration.value.Seconds}</c:set>
													</c:if>
													<c:if
														test="${elem.value.GalleryMediaFile.value.Audio.exists and elem.value.GalleryMediaFile.value.Audio.value.Duration.exists}">
														<c:set var="hours">${elem.value.GalleryMediaFile.value.Audio.value.Duration.value.Hours}</c:set>
														<c:set var="minutes">${elem.value.GalleryMediaFile.value.Audio.value.Duration.value.Minutes}</c:set>
														<c:set var="seconds">${elem.value.GalleryMediaFile.value.Audio.value.Duration.value.Seconds}</c:set>
													</c:if>
													<cms:include
														file="%(link.strong:/system/modules/com.saga.diputacion.huelva.frontend/elements/e-gallery-duration.jsp:0ed0f75e-10b9-11e8-966e-63f9a58dae09)">
														<cms:param name="hours">${hours}</cms:param>
														<cms:param name="minutes">${minutes}</cms:param>
														<cms:param name="seconds">${seconds}</cms:param>
													</cms:include>
													<c:if test="${datamediatype != 'imagen' and (
														(elem.value.GalleryMediaFile.value.VideoUrl.exists and elem.value.Date.exists)
														 or (elem.value.GalleryMediaFile.value.Video.exists and elem.value.Date.exists)
														 or (elem.value.GalleryMediaFile.value.Audio.exists and elem.value.Date.exists)
														)}">
														<div class="duration">
															<c:set var="fecha" value="${elem.value.Date}"/>
															<%pageContext.setAttribute("fechaFormateada", (new SimpleDateFormat("dd'/'MM'/'yyyy")).format(new Date(Long.parseLong( pageContext.getAttribute("fecha").toString() ))));%>
															<strong><fmt:message key="key.formatter.date" /> </strong><c:out value="${fechaFormateada}"/>
														</div>
													</c:if>
												</c:if>
												<c:if test="${elem.value.Description.isSet}">
													<div class="description mt-10">${elem.value.Description}</div>
												</c:if>
											<%-- </c:forEach> --%>
										</div>
									</div>
								</div>
								<%-- FIN ROW --%>
							</div>
							<%-- FIN GALLERY BLOCK --%>
						</c:if>
						<%-- Fin cierre comprobacion de seccion --%>
					</div>
					<!-- Fin de wrapper -->
				</div>
				<c:set var="titletag"></c:set>
				<c:set var="subtitletag"></c:set>
				<c:set var="titlesection"></c:set>
				<c:set var="titleblock"></c:set>
				<c:set var="titlegallery"></c:set>
				<c:set var="thumbvideo" value="" scope="request" />
				<c:set var="urlsourcevideo" value="" scope="request" />
				<c:set var="duration" value="" scope="request" />
				<c:set var="durationmsg" value="" scope="request" />
				<c:set var="gallery" value="" scope="request" />
<%
			}
%>
			</c:otherwise>
		</c:choose>
	</cms:formatter>
</cms:bundle>