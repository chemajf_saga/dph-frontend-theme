<%@page import="org.apache.commons.lang.StringUtils"%>
<%@ page buffer="none" session="false" trimDirectiveWhitespaces="true" import="java.net.URLEncoder" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Date"%>
<%@ page import="org.opencms.jsp.util.CmsJspContentAccessValueWrapper"%>
<%@ page import="com.saga.diputacion.huelva.frontend.ObjectOrder"%>
<%@ page import="java.util.Collections"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="org.opencms.flex.CmsFlexController" %>
<%@ page import="org.opencms.file.CmsObject" %>
<%@ page import="org.opencms.file.CmsResource" %>
<%@ page import="org.opencms.loader.CmsImageScaler" %>
<%@ page import="org.opencms.jsp.CmsJspTagImage" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates" %>
<%@ taglib prefix="sgcore" tagdir="/WEB-INF/tags/core/templates" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.diputacion.huelva.frontend.messages">
<cms:formatter var="content" val="value" rdfa="rdfa">

<c:choose>
    <c:when test="${cms.element.inMemoryOnly}">
                <h1 class="title">
                    Edite este recurso!!
                </h1>
    </c:when>
<c:otherwise>
<%-- Cargamos la variable con el id especifico del recurso para su uso posterior--%>
<c:set var="idresource" value="${content.id}"></c:set>
<c:set var="id">${fn:substringBefore(idresource, '-')}</c:set>

<c:set var="locale">${cms.locale}</c:set>

<c:if test="${not empty cms.element.settings.classmainbox }">
    <c:set var="classmainbox">${cms.element.settings.classmainbox}</c:set>
</c:if>

    <c:if test="${cms.element.setting.marginbottom.value != '0'}">
        <c:set var="marginClass">margin-bottom-${cms.element.setting.marginbottom.value}</c:set>
    </c:if>

    <%-- Definimos el tipo de etiqueta html para nuestro encabezado --%>

    <c:set var="titletag">h1</c:set>
    <c:set var="subtitletag">h2</c:set>
    <c:set var="titletagelement">h2</c:set>
    <c:set var="titletagelementsize">h5</c:set>

    <c:if test="${not empty cms.element.settings.titletag }">
        <c:set var="titletag">${cms.element.settings.titletag }</c:set>
        <c:if test="${titletag == 'h1'}">
            <c:set var="subtitletag">h2</c:set>
        </c:if>
        <c:if test="${titletag == 'h2'}">
            <c:set var="subtitletag">h3</c:set>
        </c:if>
        <c:if test="${titletag == 'h3'}">
            <c:set var="subtitletag">h4</c:set>
        </c:if>
        <c:if test="${titletag == 'h4'}">
            <c:set var="subtitletag">h5</c:set>
        </c:if>
        <c:if test="${titletag == 'h5'}">
            <c:set var="subtitletag">h6</c:set>
        </c:if>
        <c:if test="${titletag == 'div'}">
            <c:set var="subtitletag">div</c:set>
        </c:if>
    </c:if>
    <%-- si se ha editado el subtitulo lo mandamos en el request para que se tenga en cuenta en la seccion o en el bloque de texto por si se edita el titulo de seccion o de bloque --%>
    <c:if test="${value.SubTitle.isSet }">
        <c:set var="subtitletag">${subtitletag}</c:set>
        <c:if test="${subtitletag == 'h2'}">
            <c:set var="titletagelement">h3</c:set>
        </c:if>
        <c:if test="${subtitletag == 'h3'}">
            <c:set var="titletagelement">h4</c:set>
        </c:if>
        <c:if test="${subtitletag == 'h4'}">
            <c:set var="titletagelement">h5</c:set>
        </c:if>
        <c:if test="${subtitletag == 'h5' or subtitletag == 'div'}">
            <c:set var="titletagelement">div</c:set>
        </c:if>
    </c:if>
    <c:if test="${not empty cms.element.settings.titletagelement }">
        <c:set var="titletagelement">${cms.element.settings.titletagelement}</c:set>
    </c:if>
    <c:if test="${not empty cms.element.settings.titletagelementsize }">
        <c:set var="titletagelementsize">${cms.element.settings.titletagelementsize}</c:set>
    </c:if>
    <%-- Establecemos la imagen por defecto definida en el .themeconfig por si no viene imagen en el recurso --%>
    <c:if test="${not empty themeConfiguration.CustomFieldKeyValue.defaultimg}">
        <c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg}</c:set>
        <c:if test="${count > 1 and count %2 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg2}">
            <c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg2}</c:set>
        </c:if>
        <c:if test="${count > 1 and count %3 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg3}">
            <c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg3}</c:set>
        </c:if>
        <c:if test="${count > 1 and count %4 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg4}">
            <c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg4}</c:set>
        </c:if>
        <c:if test="${count > 1 and count %5 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg5}">
            <c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg5}</c:set>
        </c:if>
    </c:if>

<%-- guardamos en una variable la ruta del themeconfig que le corresponde al recurso en funcion de la rama donde se este pintando. despues lo mandamos como parametro en el href del enlace para generar el pdf --%>
<c:set var="themeconfigpath">${cms.vfs.context.siteRoot}${themeconfigpath}<sg:resourcePathLookBack filename=".themeconfig"/></c:set>

<div class="articulo parent element dph-galeria<c:out value=' ${classmainbox} ${marginClass}' />" id="${id}">

<div class="wrapper <c:out value='${value.CssClass}' />">

<c:if test="${not cms.element.settings.hidetitle}">
    <!-- Cabecera del articulo -->
    <header class="headline">
		<c:if test="${not cms.element.settings.hidedate}">
            <div class="time">
                <cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-time.jsp:ea2c737f-df65-11e4-bcf9-01e4df46f753)">
                    <cms:param name="date">${value.Date }</cms:param>
                    <cms:param name="showtime">false</cms:param>
                </cms:include>
            </div>
		</c:if>
            <${titletag} class="title" ${rdfa.Title}>${value.Title}</${titletag}>
            <c:if test="${value.SubTitle.isSet }">
                    <${subtitletag} class="subtitle" ${rdfa.SubTitle}>${value.SubTitle}</${subtitletag}>
            </c:if>
            <c:if test="${value.Municipio.isSet or (value.Tags.exists and value.Tags.isSet)}">
                <c:set var="searchpage"></c:set>
                <c:set var="searchfieldname"></c:set>
                <%-- Primero buscamos la propiedad y luego los campos customizados del .themeconfig para la página del buscador que recibirá los tags por param
                y el atributo 'name' del campo del buscador que los cargará --%>
                <c:set var="searchpage"><cms:property name="sagasuite.articles.search.page" file="search"/></c:set>
                <c:if test="${not empty themeConfiguration.CustomFieldKeyValue.UrlSearchParamPage}">
                    <c:set var="searchpage">${themeConfiguration.CustomFieldKeyValue.UrlSearchParamPage}</c:set>
                </c:if>
                <c:set var="tagsearchfieldname"><cms:property name="sagasuite.articles.search.tag.field.name" file="search"/></c:set>
                <c:if test="${not empty themeConfiguration.CustomFieldKeyValue.TagSearchFieldName}">
                    <c:set var="tagsearchfieldname">${themeConfiguration.CustomFieldKeyValue.TagSearchFieldName}</c:set>
                </c:if>
                <c:set var="citysearchfieldname"><cms:property name="sagasuite.articles.search.city.field.name" file="search"/></c:set>
                <c:if test="${not empty themeConfiguration.CustomFieldKeyValue.CitySearchFieldName}">
                    <c:set var="citysearchfieldname">${themeConfiguration.CustomFieldKeyValue.CitySearchFieldName}</c:set>
                </c:if>
                <c:set var="typesearchfieldname"><cms:property name="sagasuite.articles.search.type.field.name" file="search"/></c:set>
                <c:if test="${not empty themeConfiguration.CustomFieldKeyValue.TypeSearchFieldName}">
                    <c:set var="typesearchfieldname">${themeConfiguration.CustomFieldKeyValue.TypeSearchFieldName}</c:set>
                </c:if>

                <c:if test="${empty searchpage }">
                    <c:set var="searchpage">/busqueda/</c:set>
                </c:if>
                <c:if test="${empty tagsearchfieldname }">
                    <c:set var="tagsearchfieldname">tagfield</c:set>
                </c:if>
                <c:if test="${empty citysearchfieldname }">
                    <c:set var="citysearchfieldname">cityfield</c:set>
                </c:if>
                <c:if test="${empty typesearchfieldname }">
                    <c:set var="typesearchfieldname">typefield</c:set>
                </c:if>
            </c:if>
            <c:if test="${value.Municipio.isSet }">
                <div class="h5 upper municipio">
                    <c:set var="categorias" value="${fn:split(value.Municipio, ',')}" />
                    <c:set var="counter" value="0" />
                    <c:forEach items="${categorias }" var="category" varStatus="status">
                        <c:set var="categoryhasmunicipio" value="${fn:indexOf(category, '/municipios/')}" />
                        <c:if  test="${categoryhasmunicipio != -1}">
                            <%--====== Comprobamos si existe la propiedad Title especifica para el locale actual (Title_es, Title_en...) para la categoria.
                            Si es asi usamos esa propiedad. Si no existe, usamos la propiedad Title =============================--%>
                            <c:set var="categorytitle"><cms:property name="Title_${locale}" file="${category}"/></c:set>
                            <c:if test="${empty categoryPropertyTitle}">
                                <c:set var="categorytitle"><cms:property name="Title" file="${category}"/></c:set>
                            </c:if>
                            <c:set var="categoryshort" value="${fn:replace(category, '/.categories/','')}" />
                            <c:set var="categoryshortencoded"><%= java.net.URLEncoder.encode(pageContext.getAttribute("categoryshort").toString() , "UTF-8") %></c:set>
                            <a class="hastooltip" href="<cms:link>${searchpage}?${citysearchfieldname}=${categoryshortencoded }&${typesearchfieldname}=dphgaleria</cms:link>" title="<fmt:message key='key.formatter.new.view.all.same.city' />"><span class="pe-7s-map-marker mr-10" aria-hidden="true"></span>${categorytitle}</a>
                        </c:if>
                    </c:forEach>
                </div>
            </c:if>
            <c:if test="${value.Tags.exists and value.Tags.isSet}">
                <div class="info info-cat info-tag mt-30">
                    <ul class="list-inline no-margin">
                        <c:forEach items="${content.valueList.Tags }" var="elemtag" varStatus="tagstatus">
                            <%--Para que el enlace se codifique de manera que no haya errores si el tag contiene caracteres especiales como por ejemplo "+"--%>
                            <c:set var="elemtagencoded"><%= java.net.URLEncoder.encode(pageContext.getAttribute("elemtag").toString() , "UTF-8") %></c:set>
                            <li>
                                <a class="hastooltip" title="<fmt:message key='key.formatter.new.view.all.same.tag' />" href="<cms:link>${searchpage}?${tagsearchfieldname}=${elemtagencoded }&${typesearchfieldname}=dphgaleria</cms:link>"><span class="fa fa-hashtag" aria-hidden="true"></span><strong>${elemtag }</strong></a>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
            </c:if>
    </header>
</c:if>
    <c:if test="${value.Text.isSet}">
        <div class="contentblock">
            <div class="contentblock-texto" ${rdfa.Text}>
                ${value.Text}
            </div>
        </div>
    </c:if>
    <c:if test="${value.Content.exists}">
        <c:forEach var="elem" items="${content.valueList.Content}" varStatus="status">
            <c:set var="contentblock" value="${elem}" scope="request"></c:set>
            <cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-default-contentblock.jsp:e9a36d37-df65-11e4-bcf9-01e4df46f753)">
                <cms:param name="contentId">${id}-contentblock-${status.count}</cms:param>
            </cms:include>
        </c:forEach>
    </c:if> <%-- Fin cierre comprobacion de seccion --%>

    <%-- galería de imágenes automática --%>
    <c:if test="${value.GalleryImage.exists and (value.GalleryImage.value.ImageGalleryFolder.isSet or value.GalleryImage.value.Image.isSet)}">
        <cms:include file="%(link.strong:/system/modules/com.saga.diputacion.huelva.frontend/elements/e-imagegallery.jsp:c8d7fe5a-5369-11e8-a09e-63f9a58dae09)">
            <cms:param name="galleryId">imagegallery-${id}</cms:param>
            <cms:param name="galleryResourcesForRow" value="${value.GalleryImage.value.ResourcesForRow}" />
            <cms:param name="galleryPageElements" value="${value.GalleryImage.value.PageElements}" />
            <cms:param name="galleryOriginSize" value="${value.GalleryImage.value.OriginSize}" />
            <c:if test="${value.GalleryImage.value.ImageGalleryFolder.isSet}">
                <cms:param name="galleryFolder" value="${value.GalleryImage.value.ImageGalleryFolder}" />
            </c:if>
            <c:if test="${value.GalleryImage.value.Image.isSet}">
                <cms:param name="galleryImages" value="${value.GalleryImage.valueList.Image}" />
            </c:if>
        </cms:include>
    </c:if>

    <%-- elementos multimedia --%>
<c:if test="${value.GalleryMediaElement.exists}">
    <c:set var="showList">true</c:set>
    <c:forEach var="elem" items="${content.valueList.GalleryMediaElement}" varStatus="status">
        <c:set var="totalGalleryElements">${status.count}</c:set>
    </c:forEach>
    <c:if test="${totalGalleryElements > 1}">
        <c:set var="featuredClass">col-sm-5 col-xxs-12</c:set>
    </c:if>
    <c:if test="${totalGalleryElements == 1}">
        <c:set var="featuredClass">col-xxs-12</c:set>
        <c:set var="showList">false</c:set>
    </c:if>
    <div class="galleryblock">
        <div class="row">
            <%-- ITEM DESTACADO Y CARGADOR DE DETALLE DE LISTA DE ITEMS --%>
            <div class="featured-item-block mb-40 ${featuredClass}">
                <div class="dph-galeria-loading" style="visibility: hidden">
                    <span class="fa fa-spinner fa-pulse fa-3x fa-fw" aria-hidden="true"></span>
                    <span class="sr-only">cargando...</span>
                </div>
                <c:set var="eltos" value="${content.valueList.GalleryMediaElement}" />
<%
                //Ordenamos los elementos de la galería
                List<ObjectOrder> listaOrdenada = new ArrayList<ObjectOrder>();
                int i = 0;
                //Ordenamos los elementos
                List<CmsJspContentAccessValueWrapper> elementos =
                                (ArrayList<CmsJspContentAccessValueWrapper>) pageContext.getAttribute("eltos");
                for (CmsJspContentAccessValueWrapper elemento : elementos) {
                    String eltoStr = elemento.getValue().get("Date").toString();
	                    long dateL = 0L;
                    if(StringUtils.isNotEmpty(eltoStr)){
	                    dateL = Long.parseLong(eltoStr);
                    }
                    ObjectOrder auxOrder = new ObjectOrder(new Date(dateL), elemento);
                    listaOrdenada.add(i, auxOrder);
                    i++;
                }
                Collections.reverse(listaOrdenada);
                pageContext.setAttribute("listaOrdenada", listaOrdenada);
                pageContext.setAttribute("elem", (CmsJspContentAccessValueWrapper) (listaOrdenada.get(0).getObject()) );
%>
                <div class="gallery-featured-item" aria-label="Elemento multimedia destacado o reproduciéndose" aria-live="assertive" aria-atomic="true" aria-relevant="all" aria-busy="true">
                    
                        <%-- AUDIO ////////////////////////////////// --%>
                        <c:if test="${elem.value.GalleryMediaFile.value.Audio.exists and elem.value.GalleryMediaFile.value.Audio.value.Source.exists and (elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileMp3.isSet or elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileWav.isSet or elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileOgg.isSet)}">
                            <c:set var="datamediatype">audio</c:set>
                            <div class="gallery-media gallery-media-audio">
                                <div class="gallery-media-wrapper">
                                    <cms:include file="%(link.strong:/system/modules/com.saga.diputacion.huelva.frontend/elements/e-gallery-audio.jsp:f5a09907-0fda-11e8-966e-63f9a58dae09)">
                                        <c:if test="${not empty image}">
                                            <cms:param name="image">${image}</cms:param>
                                        </c:if>
                                        <c:choose>
                                            <c:when test="${elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileMp3.isSet}">
                                                <cms:param name="audiofilemp3">${elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileMp3}</cms:param>
                                            </c:when>
                                            <c:when test="${elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileWav.isSet}">
                                                <cms:param name="audiofilewav">${elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileWav}</cms:param>
                                            </c:when>
                                            <c:when test="${elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileOgg.isSet}">
                                                <cms:param name="audiofileogg">${elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileOgg}</cms:param>
                                            </c:when>
                                        </c:choose>
                                    </cms:include>
                                </div>
                            </div>
                        </c:if>
                        <%-- VIDEO ////////////////////////////////////////////////// --%>
                        <c:if test="${elem.value.GalleryMediaFile.value.Video.exists and
                                        (elem.value.GalleryMediaFile.value.Video.value.FileMp4.isSet
                                            or elem.value.GalleryMediaFile.value.Video.value.FileWebm.isSet
                                            or elem.value.GalleryMediaFile.value.Video.value.FileOgg.isSet)}">
                            <c:set var="datamediatype">video</c:set>
                            <div class="gallery-media gallery-media-video">
                                <div class="gallery-media-wrapper">
                                    <cms:include file="%(link.strong:/system/modules/com.saga.diputacion.huelva.frontend/elements/e-gallery-video.jsp:a7f29d3c-0ff1-11e8-966e-63f9a58dae09)">
                                        <c:if test="${not empty image and !elem.value.GalleryMediaFile.value.Video.value.ImageCover.isSet}">
                                            <cms:param name="image">${image}</cms:param>
                                        </c:if>
                                        <c:if test="${elem.value.GalleryMediaFile.value.Video.value.ImageCover.isSet}">
                                            <cms:param name="image">${elem.value.GalleryMediaFile.value.Video.value.ImageCover}</cms:param>
                                        </c:if>
                                        <c:if test="${elem.value.GalleryMediaFile.value.Video.value.FileMp4.isSet}">
                                            <cms:param name="source">${elem.value.GalleryMediaFile.value.Video.value.FileMp4}</cms:param>
                                        </c:if>
                                    </cms:include>
                                </div>
                            </div>
                        </c:if>
                        <%-- VIDEO EMBED URL////////////////////////////////////////////////// --%>
                        <c:if test="${elem.value.GalleryMediaFile.value.VideoUrl.exists and elem.value.GalleryMediaFile.value.VideoUrl.value.Url.isSet}">
                            <c:set var="datamediatype">video</c:set>
                            <div class="gallery-media gallery-media-video gallery-media-video-url">
                                <div class="gallery-media-wrapper">
                                    <cms:include file="%(link.strong:/system/modules/com.saga.diputacion.huelva.frontend/elements/e-gallery-video-url.jsp:9ea0ff9c-0ff7-11e8-966e-63f9a58dae09)">
                                        <cms:param name="url">${elem.value.GalleryMediaFile.value.VideoUrl.value.Url}</cms:param>
                                    </cms:include>
                                </div>
                            </div>
                        </c:if>
                            <%-- IMAGEN ////////////////////////////////////////////////// --%>
                        <c:if test="${elem.value.GalleryMediaFile.value.Image.exists and elem.value.GalleryMediaFile.value.Image.value.Image.isSet}">
                            <c:set var="datamediatype">imagen</c:set>
                            <div class="gallery-media gallery-media-img">
                                <div class="gallery-media-wrapper">
                                    <c:set var="image">${elem.value.GalleryMediaFile.value.Image.value.Image}</c:set>
                                    <c:set var="imageTitle"><cms:property name="Title_${cms.locale}" file="${image}"/></c:set>
                                    <c:if test="${empty imageTitle}">
                                        <c:set var="imageTitle"><cms:property name="Title" file="${image}"/></c:set>
                                    </c:if>
                                    <cms:img src="${image}" cssclass="img-responsive" scaleType="2" width="480"  height="360" alt="${imageTitle}"/>
                                </div>
                            </div>
                            <a href="<cms:link>${image}</cms:link>" data-title="${imageTitle}" title="<fmt:message key='label.formatter.expand.image' /> '${imageTitle}'" class="disp-block zoom-filter">
                                <span class="sr-only"><fmt:message key='label.formatter.expand.image' /></span>
                            </a>
                        </c:if>
                        <c:if test="${elem.value.Title.isSet}">
                            <${titletagelement} class="h3">${elem.value.Title}</${titletagelement}>
                        </c:if>
                        <c:if test="${!elem.value.Title.isSet and datamediatype == 'imagen'}">
                            <${titletagelement} class="h3">${imageTitle}</${titletagelement}>
                        </c:if>
                        <c:if test="${datamediatype != 'imagen' and (
                        (elem.value.GalleryMediaFile.value.VideoUrl.exists and elem.value.GalleryMediaFile.value.VideoUrl.value.Duration.exists)
                         or (elem.value.GalleryMediaFile.value.Video.exists and elem.value.GalleryMediaFile.value.Video.value.Duration.exists)
                         or (elem.value.GalleryMediaFile.value.Audio.exists and elem.value.GalleryMediaFile.value.Audio.value.Duration.exists)
                        )}">
                            <c:if test="${elem.value.GalleryMediaFile.value.VideoUrl.exists and elem.value.GalleryMediaFile.value.VideoUrl.value.Duration.exists}">
                                <c:set var="hours">${elem.value.GalleryMediaFile.value.VideoUrl.value.Duration.value.Hours}</c:set>
                                <c:set var="minutes">${elem.value.GalleryMediaFile.value.VideoUrl.value.Duration.value.Minutes}</c:set>
                                <c:set var="seconds">${elem.value.GalleryMediaFile.value.VideoUrl.value.Duration.value.Seconds}</c:set>
                            </c:if>
                            <c:if test="${elem.value.GalleryMediaFile.value.Video.exists and elem.value.GalleryMediaFile.value.Video.value.Duration.exists}">
                                <c:set var="hours">${elem.value.GalleryMediaFile.value.Video.value.Duration.value.Hours}</c:set>
                                <c:set var="minutes">${elem.value.GalleryMediaFile.value.Video.value.Duration.value.Minutes}</c:set>
                                <c:set var="seconds">${elem.value.GalleryMediaFile.value.Video.value.Duration.value.Seconds}</c:set>
                            </c:if>
                            <c:if test="${elem.value.GalleryMediaFile.value.Audio.exists and elem.value.GalleryMediaFile.value.Audio.value.Duration.exists}">
                                <c:set var="hours">${elem.value.GalleryMediaFile.value.Audio.value.Duration.value.Hours}</c:set>
                                <c:set var="minutes">${elem.value.GalleryMediaFile.value.Audio.value.Duration.value.Minutes}</c:set>
                                <c:set var="seconds">${elem.value.GalleryMediaFile.value.Audio.value.Duration.value.Seconds}</c:set>
                            </c:if>
                            <cms:include file="%(link.strong:/system/modules/com.saga.diputacion.huelva.frontend/elements/e-gallery-duration.jsp:0ed0f75e-10b9-11e8-966e-63f9a58dae09)">
                                <cms:param name="hours">${hours}</cms:param>
                                <cms:param name="minutes">${minutes}</cms:param>
                                <cms:param name="seconds">${seconds}</cms:param>
                            </cms:include>
                        </c:if>
                        <c:if test="${datamediatype != 'imagen' and (
                        (elem.value.GalleryMediaFile.value.VideoUrl.exists and elem.value.Date.exists)
                         or (elem.value.GalleryMediaFile.value.Video.exists and elem.value.Date.exists)
                         or (elem.value.GalleryMediaFile.value.Audio.exists and elem.value.Date.exists)
                        )}">
							<c:set var="fecha" value="${elem.value.Date}"/>
<%
							if(StringUtils.isNotBlank(pageContext.getAttribute("fecha").toString())) {
    							pageContext.setAttribute("fechaFormateada", (new SimpleDateFormat("dd'/'MM'/'yyyy")).format(new Date(Long.parseLong(pageContext.getAttribute("fecha").toString()))));
%>
	                            <div class="duration">
									<strong><fmt:message key="key.formatter.date" /> </strong><c:out value="${fechaFormateada}"/>
	                            </div>
<%
							}
%>
                        </c:if>
                        <c:if test="${elem.value.Description.isSet}">
                            <div class="description mt-10">
                                ${elem.value.Description}
                            </div>
                        </c:if>
                    
                </div>
            </div>
            <%-- LISTA DE ITEMS --%>
            <c:if test="${showList == 'true'}">
            <div class="list-items col-sm-7 col-xxs-12">
                <div class="row">
                    <c:forEach var="objOrder" items="${listaOrdenada}" varStatus="status">
                        <c:set var="elem" value="${objOrder.object}" />
                        <%-- generamos el id --%>
                        <c:if test="${elem.value.Title.isSet}">
                            <c:set var="galleryItemId"><sgcore:generated-id label="${elem.value.Title}"/></c:set>
                        </c:if>
                        <c:if test="${!elem.value.Title.isSet and datamediatype == 'imagen'}">
                            <c:set var="galleryItemId"><sgcore:generated-id label="${imageTitle}"/></c:set>
                        </c:if>
                        <%-- Establecemos la imagen por defecto definida en el .themeconfig por si no viene imagen en el recurso --%>
                        <c:if test="${not empty themeConfiguration.CustomFieldKeyValue.defaultimg}">
                            <c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg}</c:set>
                            <c:if test="${status.count > 1 and status.count %2 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg2}">
                                <c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg2}</c:set>
                            </c:if>
                            <c:if test="${status.count > 1 and status.count %3 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg3}">
                                <c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg3}</c:set>
                            </c:if>
                            <c:if test="${status.count > 1 and status.count %4 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg4}">
                                <c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg4}</c:set>
                            </c:if>
                            <c:if test="${status.count > 1 and status.count %5 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg5}">
                                <c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg5}</c:set>
                            </c:if>
                        </c:if>
                        <div class="gallery-item col-md-4 col-xs-6 col-xxs-12">
                            <div class="gallery-item-wrapper">
                                <div class="gallery-item-wrapper-media">
                                    <%-- AUDIO /////////////////////////////////////////// --%>
                                    <c:if test="${elem.value.GalleryMediaFile.value.Audio.exists and elem.value.GalleryMediaFile.value.Audio.value.Source.exists and (elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileMp3.isSet or elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileWav.isSet or elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileOgg.isSet)}">
                                        <c:set var="datamediatype">audio</c:set>
                                        <c:set var="datamedia">audio</c:set>
                                        <c:choose>
                                            <c:when test="${elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileMp3.isSet}">
                                                <c:set var="datamediasource"><cms:link>${elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileMp3}</cms:link></c:set>
                                                <c:set var="datamediasourcetype">audio/mpeg</c:set>
                                            </c:when>
                                            <c:when test="${elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileWav.isSet}">
                                                <c:set var="datamediasource"><cms:link>${elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileWav}</cms:link></c:set>
                                                <c:set var="datamediasourcetype">audio/wav</c:set>
                                            </c:when>
                                            <c:when test="${elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileOgg.isSet}">
                                                <c:set var="datamediasource"><cms:link>${elem.value.GalleryMediaFile.value.Audio.value.Source.value.FileOgg}</cms:link></c:set>
                                                <c:set var="datamediasourcetype">audio/ogg</c:set>
                                            </c:when>
                                        </c:choose>
                                        <div class="gallery-media gallery-media-audio">
                                            <div class="gallery-media-wrapper">
                                                <c:if test="${not empty image}">
                                                    <cms:img src="${image}" cssclass="img-responsive" width="480" height="360" scaleType="2" />
                                                </c:if>
                                            </div>
                                        </div>
                                    </c:if>
                                    <%-- VÍDEO HTML5 /////////////////////////////////////////// --%>
                                    <c:if test="${elem.value.GalleryMediaFile.value.Video.exists and
                                                    (elem.value.GalleryMediaFile.value.Video.value.FileMp4.isSet
                                                        or elem.value.GalleryMediaFile.value.Video.value.FileWebm.isSet
                                                        or elem.value.GalleryMediaFile.value.Video.value.FileOgg.isSet)}">
                                        <c:set var="datamediatype">video</c:set>
                                        <c:if test="${elem.value.GalleryMediaFile.value.Video.value.ImageCover.isSet}">
                                            <c:set var="image">${elem.value.GalleryMediaFile.value.Video.value.ImageCover}</c:set>
                                        </c:if>
                                        <c:if test="${elem.value.GalleryMediaFile.value.Video.value.FileMp4.isSet}">
                                            <c:set var="datamediasource"><cms:link>${elem.value.GalleryMediaFile.value.Video.value.FileMp4}</cms:link></c:set>
                                            <c:set var="datamediasourcetype">video/mp4</c:set>
                                        </c:if>
                                        <div class="gallery-media gallery-media-video">
                                            <div class="gallery-media-wrapper">
                                                <c:if test="${not empty image}">
                                                    <cms:img src="${image}" cssclass="img-responsive" width="480" height="360" scaleType="2" />
                                                </c:if>
                                            </div>
                                        </div>
                                    </c:if>
                                    <%-- VIDEO EMBED URL////////////////////////////////////////////////// --%>
                                    <c:if test="${elem.value.GalleryMediaFile.value.VideoUrl.exists and elem.value.GalleryMediaFile.value.VideoUrl.value.Url.isSet}">
                                        <c:set var="datamediatype">videourl</c:set>
                                        <div class="gallery-media gallery-media-video gallery-media-video-url">
                                            <div class="gallery-media-wrapper">
                                                <cms:include file="%(link.strong:/system/modules/com.saga.diputacion.huelva.frontend/elements/e-gallery-video-url-image.jsp:48cd091e-0ff9-11e8-966e-63f9a58dae09)">
                                                    <cms:param name="url">${elem.value.GalleryMediaFile.value.VideoUrl.value.Url}</cms:param>
                                                    <cms:param name="title">${elem.value.GalleryMediaFile.value.Title}</cms:param>
                                                </cms:include>
                                            </div>
                                        </div>
                                        <cms:include file="%(link.strong:/system/modules/com.saga.diputacion.huelva.frontend/elements/e-gallery-video-url-idvideo.jsp:8f551a8d-0ffa-11e8-966e-63f9a58dae09)">
                                            <cms:param name="url">${elem.value.GalleryMediaFile.value.VideoUrl.value.Url}</cms:param>
                                        </cms:include>
                                        <c:set var="datamediasource">${urlsourcevideo}</c:set>
                                        <c:set var="datamediasourcetype">video/url</c:set>
                                    </c:if>
                                    <%-- IMAGEN ////////////////////////////////////////////////// --%>
                                    <c:if test="${elem.value.GalleryMediaFile.value.Image.exists and elem.value.GalleryMediaFile.value.Image.value.Image.isSet}">
                                        <c:set var="datamediatype">imagen</c:set>
                                        <div class="gallery-media gallery-media-img">
                                            <div class="gallery-media-wrapper">
                                                <c:set var="image">${elem.value.GalleryMediaFile.value.Image.value.Image}</c:set>
                                                <c:set var="imageTitle"><cms:property name="Title_${cms.locale}" file="${image}"/></c:set>
                                                <c:if test="${empty imageTitle}">
                                                    <c:set var="imageTitle"><cms:property name="Title" file="${image}"/></c:set>
                                                </c:if>
                                                <cms:img src="${image}" cssclass="img-responsive" scaleType="2" width="480"  height="360" alt="${imageTitle}"/>
                                            </div>
                                        </div>
                                        <a href="<cms:link>${image}</cms:link>" data-title="${imageTitle}" title="<fmt:message key='label.formatter.expand.image' /> '${imageTitle}'" class="disp-block zoom-filter">
                                            <span class="sr-only"><fmt:message key='label.formatter.expand.image' /></span>
                                        </a>
                                    </c:if>
                                    <c:if test="${datamediatype != 'imagen' and (
                                        (elem.value.GalleryMediaFile.value.VideoUrl.exists and elem.value.GalleryMediaFile.value.VideoUrl.value.Duration.exists)
                                         or (elem.value.GalleryMediaFile.value.Video.exists and elem.value.GalleryMediaFile.value.Video.value.Duration.exists)
                                         or (elem.value.GalleryMediaFile.value.Audio.exists and elem.value.GalleryMediaFile.value.Audio.value.Duration.exists)
                                        )}">
                                            <c:if test="${elem.value.GalleryMediaFile.value.VideoUrl.exists and elem.value.GalleryMediaFile.value.VideoUrl.value.Duration.exists}">
                                                <c:set var="hours">${elem.value.GalleryMediaFile.value.VideoUrl.value.Duration.value.Hours}</c:set>
                                                <c:set var="minutes">${elem.value.GalleryMediaFile.value.VideoUrl.value.Duration.value.Minutes}</c:set>
                                                <c:set var="seconds">${elem.value.GalleryMediaFile.value.VideoUrl.value.Duration.value.Seconds}</c:set>
                                            </c:if>
                                            <c:if test="${elem.value.GalleryMediaFile.value.Video.exists and elem.value.GalleryMediaFile.value.Video.value.Duration.exists}">
                                                <c:set var="hours">${elem.value.GalleryMediaFile.value.Video.value.Duration.value.Hours}</c:set>
                                                <c:set var="minutes">${elem.value.GalleryMediaFile.value.Video.value.Duration.value.Minutes}</c:set>
                                                <c:set var="seconds">${elem.value.GalleryMediaFile.value.Video.value.Duration.value.Seconds}</c:set>
                                            </c:if>
                                            <c:if test="${elem.value.GalleryMediaFile.value.Audio.exists and elem.value.GalleryMediaFile.value.Audio.value.Duration.exists}">
                                                <c:set var="hours">${elem.value.GalleryMediaFile.value.Audio.value.Duration.value.Hours}</c:set>
                                                <c:set var="minutes">${elem.value.GalleryMediaFile.value.Audio.value.Duration.value.Minutes}</c:set>
                                                <c:set var="seconds">${elem.value.GalleryMediaFile.value.Audio.value.Duration.value.Seconds}</c:set>
                                            </c:if>
                                            <cms:include file="%(link.strong:/system/modules/com.saga.diputacion.huelva.frontend/elements/e-gallery-duration-value.jsp:edcd8d89-10bb-11e8-966e-63f9a58dae09)">
                                                <cms:param name="hours">${hours}</cms:param>
                                                <cms:param name="minutes">${minutes}</cms:param>
                                                <cms:param name="seconds">${seconds}</cms:param>
                                            </cms:include>
                                        </c:if>

                                        <c:set var="fecha" value="${elem.value.Date}"/>
<%
										if(StringUtils.isNotBlank(pageContext.getAttribute("fecha").toString())) {
											    pageContext.setAttribute("fechaFormateada", (new SimpleDateFormat("dd'/'MM'/'yyyy")).format(new Date(Long.parseLong(pageContext.getAttribute("fecha").toString()))));
%>
	                                        <div class="duration">
												<strong><fmt:message key="key.formatter.date" /> </strong><c:out value="${fechaFormateada}"/>
	                                        </div>
<%
										}
%>
                                    <%-- imagen de caratula --%>
                                    <c:if test="${not empty image}">
                                        <c:set var="imagepath">${image}</c:set>
                                    </c:if>
                                    <%-- description --%>
                                    <c:if test="${elem.value.Description.isSet}">
                                        <c:set var="description">${elem.value.Description}</c:set>
                                    </c:if>
                                    <c:if test="${datamediatype != 'imagen'}">
                                        <a class="data-media-loader data-media-loader-overlayer"
                                            data-mediaid="${galleryItemId}"
                                            data-loading="dph-galeria-loading"
                                            data-targetcontainer="gallery-featured-item"
                                            data-url="<cms:link>/system/modules/com.saga.diputacion.huelva.frontend/elements/e-gallery-load-media.jsp</cms:link>"
                                            href="#${galleryItemId}"
                                            data-mediaduration="${duration}"
                                            data-mediasource="${datamediasource}"
                                            data-mediasourcetype="${datamediasourcetype}"
                                            data-mediatitle="${elem.value.Title}"
                                            data-mediadescription="${description}"
                                            data-mediatype="${datamediatype}"
                                            data-mediaposter="<cms:link>${imagepath}</cms:link>"
                                            data-mediafecha="${fechaFormateada}"
                                            title="<fmt:message key='key.formatter.program.load.content' />">
                                        <span class="sr-only"><fmt:message key="key.formatter.program.load.content" />${elem.value.Title}</span></a>
                                    </c:if>
                                </div>
                                <c:if test="${elem.value.Title.isSet}">
                                    <${titletagelement} class="${titletagelementsize}">
                                        <c:if test="${datamediatype != 'imagen'}">
                                            <a class="data-media-loader"
                                            data-loading="dph-galeria-loading"
                                            data-mediaid="${galleryItemId}"
                                            data-targetcontainer="gallery-featured-item"
                                            data-url="<cms:link>/system/modules/com.saga.diputacion.huelva.frontend/elements/e-gallery-load-media.jsp</cms:link>"
                                            href="#${galleryItemId}"
                                            data-mediaduration="${duration}"
                                            data-mediasource="${datamediasource}"
                                            data-mediasourcetype="${datamediasourcetype}"
                                            data-mediatitle="${elem.value.Title}"
                                            data-mediadescription="${description}"
                                            data-mediatype="${datamediatype}"
                                            data-mediafecha="${fechaFormateada}"
                                            data-mediaposter="<cms:link>${imagepath}</cms:link>">${elem.value.Title}</a>
                                        </c:if>
                                        <c:if test="${datamediatype == 'imagen'}">
                                            ${elem.value.Title}
                                        </c:if>
                                    </${titletagelement}>
                                </c:if>
                                <c:if test="${!elem.value.Title.isSet and datamediatype == 'imagen'}">
                                    <${titletagelement} class="${titletagelementsize}">${imageTitle}</${titletagelement}>
                                </c:if>
                            </div>
                        </div>
                        <c:if test="${status.count > 1 and status.count %3 == 0}">
                            <div class="clearfix col-md-12 visible-md visible-lg" aria-hidden="true"></div>
                        </c:if>
                        <c:if test="${status.count > 1 and status.count %2 == 0}">
                            <div class="clearfix col-xs-12 visible-xs visible-sm" aria-hidden="true"></div>
                        </c:if>
                    </c:forEach>
                </div><%-- FIN ROW LISTA DE ITEMS --%>
            </div><%-- FIN LISTA DE ITEMS --%>
            </c:if>
        </div><%-- FIN ROW --%>
    </div><%-- FIN GALLERY BLOCK --%>

</c:if> <%-- Fin cierre comprobacion de seccion --%>
    <%-- CONTENIDO RELACIONADO --%>
    <c:choose>
        <c:when test="${value.RelatedContent.isSet}">
            <%-- LISTA BASADA EN EL CAMPO DE CONTENIDO RELACIONADO DEL RECURSO --%>
            <div class="h3 mt-50"><fmt:message key="key.formatter.dph.related.content" /></div>
            <div class="related-content">
                <div class="row">
                    <c:forEach var="related" items="${content.valueList.RelatedContent}" varStatus="status">
                        <%--<c:set var="relateditem" value="${related}" scope="request" />--%>
                        <cms:include file="%(link.strong:/system/modules/com.saga.diputacion.huelva.frontend/elements/e-related-content.jsp:65eabc2f-3ccb-11e8-b79c-63f9a58dae09)">
                            <cms:param name="relateditem">${related}</cms:param>
                            <cms:param name="count">${status.count}</cms:param>
                        </cms:include>
                    </c:forEach>
                </div>
            </div>
        </c:when>
        <c:otherwise>
            <%-- LISTA AUTOMÁTICA DE CONTENIDO RELACIONADO --%>
            <c:set var="querytype">type:(dphnoticia OR dphevento OR dphcontenido OR dphgaleria OR dphprograma OR dphenlace)</c:set>
            <c:set var="queryrows">&rows=4</c:set>
            <c:set var="queryorder">&sort:score</c:set>
            <c:set var="queryid">&fq=id:(NOT "${idresource}")</c:set>
            <c:set var="resourcetitle">${value.Title}</c:set>
            <c:set var="categoryText"></c:set>
            <c:if test="${value.Tematica.isSet}">
                <c:set var="categoryText">
                    <c:if test="${value.Tematica.isSet}">
                        <c:set var="categories" value="${fn:split(value.Tematica, ',')}" />
                        <c:set var="spacer" value=" " />
                        <c:forEach var="category" items="${categories}" varStatus="catstatus">
                            <c:set var="categorysolr">${fn:substringAfter(category, '/.categories/tematicas/')}</c:set>
                            <c:set var="categorysolr">${fn:replace(categorysolr, '-',' ')}</c:set>
                            <c:set var="categorysolr">${fn:replace(categorysolr, '/','')}</c:set>
                            <c:if test="${catstatus.first}">
                                ${categorysolr}
                            </c:if>
                            <c:if test="${!catstatus.first}">
                                ${spacer}${categorysolr}
                            </c:if>
                        </c:forEach>
                    </c:if>
                </c:set>
            </c:if>
            <c:set var="titletext">${fn:replace(value.Title,':' ,' ' )}</c:set>
            <c:set var="querytext">
                &q=(xmltitle_${cms.locale}_t:${titletext}^100 OR ${cms.locale}_excerpt:${titletext}<c:if test="${not empty categoryText}"> OR categoryTitle_${cms.locale}_t:${categoryText}</c:if>)
            </c:set>
            <c:set var="relatedContentQuery">
                fq=${querytype}${querytext}${queryid}${queryrows}${queryorder}
            </c:set>
            <%--<strong>QUERY: ${relatedContentQuery}</strong>--%>
            <cms:contentload collector="byContext" param="${relatedContentQuery}" preload="true">
                <cms:contentinfo var="info" />
                <c:if test="${info.resultSize > 0}">
                    <div class="h3 mt-50"><fmt:message key="key.formatter.dph.related.content" /></div>
                    <div class="related-content">
                        <div class="row">
                            <cms:contentload>
                                <cms:contentaccess var="content" />
                                <cms:include file="%(link.strong:/system/modules/com.saga.diputacion.huelva.frontend/elements/e-related-content.jsp:65eabc2f-3ccb-11e8-b79c-63f9a58dae09)">
                                    <cms:param name="relateditem">${content.filename}</cms:param>
                                    <cms:param name="count">${status.count}</cms:param>
                                </cms:include>
                            </cms:contentload>
                        </div>
                    </div>
                </c:if>
            </cms:contentload>
        </c:otherwise>
    </c:choose>
</div> <!-- Fin de wrapper -->
    <script>
        // Evento onclick para el botón cargar contenido multimedia en la caja destacada
        $(document).ready(function () {
            $('.data-media-loader').on('click', execute)
            var hashToLoad = document.location.hash;
            var idToLoad = hashToLoad.replace('#','');
            if($('[data-mediaid="'+idToLoad+'"]').length) {
                $('[data-mediaid="'+idToLoad+'"]').click();
            }else{}
        });

        // ejecuta carga por AJAX
        function execute(e){
            e.preventDefault();
            try {
                var $btn = $(e.target);
                var ctxt = {btn: $btn}
                load(ctxt)

                loading(ctxt);

                if($(window).width() < 768){
                    $('html, body').animate({scrollTop: $('.' + ctxt.targetcontainer).offset().top}, 1000);
                }
                
                var params = {};
                params.mediafecha = ctxt.mediafecha;
                params.mediasourcetype = ctxt.mediasourcetype;
                params.mediatype = ctxt.mediatype;
                params.mediaduration = ctxt.mediaduration;
                params.mediasource = ctxt.mediasource;
                params.mediatitle = ctxt.mediatitle;
                params.mediadescription = ctxt.mediadescription;
                params.mediaposter = ctxt.mediaposter;
                params.targetcontainer = ctxt.targetcontainer;
                params.mediaid = ctxt.mediaid;
                
                console.log('media-id: '+ctxt.mediaid);
                
                if(history.pushState) {
                    history.pushState(null, null, '#'+ctxt.mediaid);
                } else {
                    location.hash = "#" + ctxt.mediaid;
                }
                $.post(ctxt.url, params).done(function (data) {
                        if (data.trim() === "") {
                            $('.' + ctxt.targetcontainer).html('No hay nada que mostrar').unbind('click');
                        } else {
                            $('.' + ctxt.targetcontainer).html(data);
                        }
                        console.debug('PARRIBA!!')
                        $('html,body').animate({
                            scrollTop: $("header").offset().top
                        }, 500);
                    })
                    .fail(function (err) {
                        console.error("ERROR execute", ctxt, err);
                    })
                    .always(function () {
                        update(ctxt);
                    })
            } catch (err) {
                console.error(err);
            }


        }

        function load(ctxt) {
            var $btn = ctxt.btn;
            var datas = $btn.data();
            ctxt.mediafecha = datas.mediafecha;
            ctxt.mediasourcetype = datas.mediasourcetype;
            ctxt.mediatype = datas.mediatype;
            ctxt.mediaduration = datas.mediaduration;
            ctxt.mediasource = datas.mediasource;
            ctxt.mediatitle = datas.mediatitle;
            ctxt.mediadescription = datas.mediadescription;
            ctxt.mediaposter = datas.mediaposter;
            ctxt.targetcontainer = datas.targetcontainer;
            ctxt.mediaid = datas.mediaid;
            ctxt.url = datas.url;
            ctxt.loading = datas.loading;
        }

        function loading(ctxt) {
            $('.' + ctxt.targetcontainer).css('visibility','hidden');
            $('.' + ctxt.loading).css('visibility','visible');
        }

        function update(ctxt) {
            var btn = $('.' + ctxt.class);
            $('.' + ctxt.loading).css('visibility','hidden');
            $('.' + ctxt.targetcontainer).css('visibility','visible');
        }
    </script>

</div>
    <c:set var="titletag"></c:set>
    <c:set var="subtitletag"></c:set>
    <c:set var="titlesection"></c:set>
    <c:set var="titleblock"></c:set>
    <c:set var="titlegallery"></c:set>
    <c:set var="thumbvideo" value="" scope="request" />
    <c:set var="urlsourcevideo" value="" scope="request" />
    <c:set var="duration" value="" scope="request" />
    <c:set var="durationmsg" value="" scope="request" />
    <c:set var="gallery" value="" scope="request" />
</c:otherwise>
</c:choose>

</cms:formatter>
</cms:bundle>