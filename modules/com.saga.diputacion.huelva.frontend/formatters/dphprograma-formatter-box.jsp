<%@ page import="java.awt.*" %>
<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.diputacion.huelva.frontend.messages">

	<cms:formatter var="content" val="value" rdfa="rdfa">
		<c:choose>
			<c:when test="${cms.element.inMemoryOnly}">
				<h1 class="title">
					Por favor, edite este recurso.
				</h1>
			</c:when>
			<c:otherwise>

				<c:if test="${cms.element.setting.marginbottom.value != '0'}">
					<c:set var="marginClass">margin-bottom-${cms.element.setting.marginbottom.value}</c:set>
				</c:if>

				<c:if test="${not empty cms.element.settings.classmainbox }">
					<c:set var="classmainbox">${cms.element.settings.classmainbox}</c:set>
				</c:if>

				<c:if test="${not empty cms.element.settings.fontsize }">
					<c:set var="fontsize">${cms.element.settings.fontsize}</c:set>
				</c:if>

				<c:if test="${not empty cms.element.settings.wellbox }">
					<c:set var="wellbox">well ${cms.element.settings.wellbox}</c:set>
				</c:if>

				<c:set var="hidedate">false</c:set>
				<c:if test="${cms.element.settings.hidedate == 'true' }">
					<c:set var="hidedate">true</c:set>
				</c:if>

				<c:if test="${not empty cms.element.settings.equalheight}">
					<c:set var="equalheight">${cms.element.settings.equalheight}</c:set>
				</c:if>

				<c:if test="${not empty cms.element.settings.bgimagealign}">
					<c:set var="bgimagealign">${cms.element.settings.bgimagealign}</c:set>
				</c:if>

				<%-- Definimos el tipo de etiqueta html para nuestro encabezado --%>

				<c:set var="titletag">h2</c:set>
				<c:set var="titlesize">h4</c:set>

				<c:if test="${not empty cms.element.settings.titletag }">
					<c:set var="titletag">${cms.element.settings.titletag }</c:set>
				</c:if>

				<%-- definimos el tamaño del encabezado --%>

				<c:if test="${not empty cms.element.settings.titlesize }">
					<c:set var="titlesize">${cms.element.settings.titlesize }</c:set>
				</c:if>

				<c:if test="${not empty cms.element.settings.height }">
					<c:set var="height">height: ${cms.element.settings.height }px;</c:set>
				</c:if>

				<%-- Definimos las variables que gestionan la imagen --%>
				<jsp:useBean id="random" class="java.util.Random" scope="application"/>
				<c:set var="defaultimgint">${random.nextInt(4)+1}</c:set>


			<c:if test="${defaultimgint > 0 and defaultimgint <= 1 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg}">
				<c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg}</c:set>
			</c:if>
			<c:if test="${defaultimgint > 1 and defaultimgint <= 2 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg2}">
				<c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg2}</c:set>
			</c:if>
			<c:if test="${defaultimgint > 2 and defaultimgint <= 3 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg3}">
				<c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg3}</c:set>
			</c:if>
			<c:if test="${defaultimgint > 3 and defaultimgint <= 4 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg4}">
				<c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg4}</c:set>
			</c:if>
			<c:if test="${defaultimgint > 4 and defaultimgint <= 5 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg5}">
				<c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg5}</c:set>
			</c:if>


				<c:if test="${value.Image.isSet}">
					<c:set var="imagePath">
						${value.Image.isSet}
					</c:set>
				</c:if>

				<c:set var="bgattributes">style="${height}background-image:url('<cms:link>${imagePath}</cms:link>');background-size:cover;background-position: center ${bgimagealign};background-repeat: no-repeat;"</c:set>

				<article class="parent dph-programa-box sg-microcontent sg-microcontent-panel sg-microcontent-overlay box<c:out value=' ${marginClass} ${classmainbox} ${value.CssClass} ${fontsize} ${equalheight}'/>"${spacer}${bgattributes}>
					<div class="wrapper">
						<div class="element complete-mode with-media with-link">
							<a href="<cms:link>${content.filename}</cms:link>" title="<fmt:message key='key.list.element.go.to' />${value.Title}" style="position: absolute;left: 0;top: 0;bottom: 0;right: 0;width: 100%;height: 100%;z-index: 1">
								<span class="sr-only">${value.Title}</span>
							</a>
							<div class="text-overlay">
								<div class="wrapper">
									<c:if test="${value.Title.isSet}">
										<header>
											<${titletag} class="title ${titlesize}">
												${value.Title}
											</${titletag}>
										</header>
									</c:if>
								</div>
							</div>
						</div><%-- Fin de element --%>
					</div> <%-- Fin de wrapper --%>
				</article> <%-- Fin de article --%>

			</c:otherwise>
		</c:choose>
	</cms:formatter>
</cms:bundle>