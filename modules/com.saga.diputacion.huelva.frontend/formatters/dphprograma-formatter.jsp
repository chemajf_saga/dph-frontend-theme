<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" import="java.net.URLEncoder" %>
<%@ page import="org.opencms.main.*" %>
<%@ page import="org.apache.commons.logging.Log" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates" %>

<%!
	final static Log LOG = CmsLog.getLog("com.saga.diputacion.huelva.frontend.dphprograma-formatter.jsp");
%>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.diputacion.huelva.frontend.messages">
	<cms:formatter var="content" val="value" rdfa="rdfa">

		<c:choose>
			<c:when test="${cms.element.inMemoryOnly}">
				<h1 class="title">
					Edite este recurso!!
				</h1>
			</c:when>
			<c:otherwise>
				<%-- Cargamos la variable con el id especifico del recurso para su uso posterior--%>
				<c:set var="idresource" value="${content.id}"></c:set>
				<c:set var="id">${fn:substringBefore(idresource, '-')}</c:set>

				<c:set var="locale">${cms.locale}</c:set>

				<c:if test="${not empty cms.element.settings.classmainbox }">
					<c:set var="classmainbox">${cms.element.settings.classmainbox}</c:set>
				</c:if>
				<%-- Definimos el tipo de etiqueta html para nuestro encabezado --%>

				<c:set var="titletag">h1</c:set>
				<c:set var="subtitletag">h2</c:set>

				<c:if test="${not empty cms.element.settings.titletag }">
					<c:set var="titletag">${cms.element.settings.titletag }</c:set>
					<c:if test="${titletag == 'h1'}">
						<c:set var="subtitletag">h2</c:set>
					</c:if>
					<c:if test="${titletag == 'h2'}">
						<c:set var="subtitletag">h3</c:set>
					</c:if>
					<c:if test="${titletag == 'h3'}">
						<c:set var="subtitletag">h4</c:set>
					</c:if>
					<c:if test="${titletag == 'h4'}">
						<c:set var="subtitletag">h5</c:set>
					</c:if>
					<c:if test="${titletag == 'h5'}">
						<c:set var="subtitletag">h6</c:set>
					</c:if>
					<c:if test="${titletag == 'div'}">
						<c:set var="subtitletag">div</c:set>
					</c:if>
				</c:if>
				<%-- si se ha editado el subtitulo lo mandamos en el request para que se tenga en cuenta en la seccion o en el bloque de texto por si se edita el titulo de seccion o de bloque --%>
				<c:if test="${value.SubTitle.isSet }">
					<c:set var="subtitletag">${subtitletag}</c:set>
				</c:if>
				<%-- si no se ha editado el subtitulo lo mandamos vacio en el request --%>
				<c:if test="${!value.SubTitle.isSet }">
					<c:set var="subtitletag"></c:set>
				</c:if>

				<%-- guardamos en una variable la ruta del themeconfig que le corresponde al recurso en funcion de la rama donde se este pintando. despues lo mandamos como parametro en el href del enlace para generar el pdf --%>
				<c:set var="themeconfigpath">${cms.vfs.context.siteRoot}${themeconfigpath}<sg:resourcePathLookBack filename=".themeconfig"/></c:set>

				<article class="articulo parent element dph-programa<c:out value=' ${classmainbox}' />">

					<div class="wrapper <c:out value='${value.CssClass}' />">

					<c:if test="${not cms.element.settings.hidetitle}">
							<!-- Cabecera del articulo -->
						<header class="headline">
							<c:if test="${cms.element.settings.generatepdf == 'true' }">
								<a class="btn btn-default btn-sm btn-pdf hidden-xs hidden-xxs pull-right hastooltip" title="<fmt:message key="label.download.program.title.pdf" />" href="<cms:pdf format='%(link.weak:/system/modules/com.saga.diputacion.huelva.frontend/elements/f-programa-detail-pdf.jsp:b772581b-0bf4-11e8-abc7-63f9a58dae09)' content='${content.filename}' locale='${locale}'/>?themeconfigpath=${themeconfigpath}" target="pdf">
									<span class="fa fa-file-pdf-o" aria-hidden="true"></span>&nbsp;<span class="btn-pdf-text"><fmt:message key="label.download.pdf" /></span>
								</a>
							</c:if>
<%--							<div class="time">
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-time.jsp:ea2c737f-df65-11e4-bcf9-01e4df46f753)">
									<cms:param name="date">${value.Date }</cms:param>
									<cms:param name="showtime">false</cms:param>
								</cms:include>
							</div>--%>
							<${titletag} class="title" ${rdfa.Title}>${value.Title}</${titletag}>
							<c:if test="${value.SubTitle.isSet }">
								<${subtitletag} class="subtitle" ${rdfa.SubTitle}>${value.SubTitle}</${subtitletag}>
							</c:if>

							<c:if test="${cms.element.settings.generatepdf == 'true' or cms.element.settings.subscribe == 'true' }">
								<div class="visible-xxs visible-xs text-right margin-top-15">
									<c:if test="${cms.element.settings.generatepdf == 'true'}">
										<a class="btn btn-default btn-pdf btn-sm visible-xxs-inline-block visible-xs-inline-block hastooltip" title="<fmt:message key="label.download.program.title.pdf" />" href="<cms:pdf format='%(link.weak:/system/modules/com.saga.diputacion.huelva.frontend/elements/f-programa-detail-pdf.jsp:b772581b-0bf4-11e8-abc7-63f9a58dae09)' content='${content.filename}' locale='${locale}'/>?themeconfigpath=${themeconfigpath}" target="pdf">
											<span class="fa fa-file-pdf-o" aria-hidden="true"></span>&nbsp;<span class="btn-pdf-text"><fmt:message key="label.download.pdf" /></span>
										</a>
									</c:if>
								</div>
							</c:if>
							<c:if test="${value.Municipio.isSet or (value.Tags.exists and value.Tags.isSet)}">
								<c:set var="searchpage"></c:set>
								<c:set var="searchfieldname"></c:set>
									<%-- Primero buscamos la propiedad y luego los campos customizados del .themeconfig para la página del buscador que recibirá los tags por param
									y el atributo 'name' del campo del buscador que los cargará --%>
								<c:set var="searchpage"><cms:property name="sagasuite.articles.search.page" file="search"/></c:set>
								<c:if test="${not empty themeConfiguration.CustomFieldKeyValue.UrlSearchParamPage}">
									<c:set var="searchpage">${themeConfiguration.CustomFieldKeyValue.UrlSearchParamPage}</c:set>
								</c:if>
								<c:set var="tagsearchfieldname"><cms:property name="sagasuite.articles.search.tag.field.name" file="search"/></c:set>
								<c:if test="${not empty themeConfiguration.CustomFieldKeyValue.TagSearchFieldName}">
									<c:set var="tagsearchfieldname">${themeConfiguration.CustomFieldKeyValue.TagSearchFieldName}</c:set>
								</c:if>
								<c:set var="citysearchfieldname"><cms:property name="sagasuite.articles.search.city.field.name" file="search"/></c:set>
								<c:if test="${not empty themeConfiguration.CustomFieldKeyValue.CitySearchFieldName}">
									<c:set var="citysearchfieldname">${themeConfiguration.CustomFieldKeyValue.CitySearchFieldName}</c:set>
								</c:if>
								<c:set var="typesearchfieldname"><cms:property name="sagasuite.articles.search.type.field.name" file="search"/></c:set>
								<c:if test="${not empty themeConfiguration.CustomFieldKeyValue.TypeSearchFieldName}">
									<c:set var="typesearchfieldname">${themeConfiguration.CustomFieldKeyValue.TypeSearchFieldName}</c:set>
								</c:if>

								<c:if test="${empty searchpage }">
									<c:set var="searchpage">/busqueda/</c:set>
								</c:if>
								<c:if test="${empty tagsearchfieldname }">
									<c:set var="tagsearchfieldname">tagfield</c:set>
								</c:if>
								<c:if test="${empty citysearchfieldname }">
									<c:set var="citysearchfieldname">cityfield</c:set>
								</c:if>
								<c:if test="${empty typesearchfieldname }">
									<c:set var="typesearchfieldname">typefield</c:set>
								</c:if>
							</c:if>
							<c:if test="${value.Municipio.isSet }">
								<div class="h5 upper municipio">
									<c:set var="categorias" value="${fn:split(value.Municipio, ',')}" />
									<c:set var="counter" value="0" />
									<c:forEach items="${categorias }" var="category" varStatus="status">
										<c:set var="categoryhasmunicipio" value="${fn:indexOf(category, '/municipios/')}" />
										<c:if  test="${categoryhasmunicipio != -1}">
											<%--====== Comprobamos si existe la propiedad Title especifica para el locale actual (Title_es, Title_en...) para la categoria.
											Si es asi usamos esa propiedad. Si no existe, usamos la propiedad Title =============================--%>
											<c:set var="categorytitle"><cms:property name="Title_${locale}" file="${category}"/></c:set>
											<c:if test="${empty categoryPropertyTitle}">
												<c:set var="categorytitle"><cms:property name="Title" file="${category}"/></c:set>
											</c:if>
											<c:set var="categoryshort" value="${fn:replace(category, '/.categories/','')}" />
											<c:set var="categoryshortencoded"><%= java.net.URLEncoder.encode(pageContext.getAttribute("categoryshort").toString() , "UTF-8") %></c:set>
											<a class="hastooltip" href="<cms:link>${searchpage}?${citysearchfieldname}=${categoryshortencoded }&${typesearchfieldname}=dphprograma</cms:link>" title="<fmt:message key='key.formatter.program.view.all.same.city' />"><span class="pe-7s-map-marker mr-10" aria-hidden="true"></span>${categorytitle}</a>
										</c:if>
									</c:forEach>
								</div>
							</c:if>
							<c:if test="${value.Tags.exists and value.Tags.isSet}">
								<div class="info info-cat info-tag mt-30">
									<ul class="list-inline no-margin">
										<c:forEach items="${content.valueList.Tags }" var="elemtag" varStatus="tagstatus">
											<%--Para que el enlace se codifique de manera que no haya errores si el tag contiene caracteres especiales como por ejemplo "+"--%>
											<c:set var="elemtagencoded"><%= java.net.URLEncoder.encode(pageContext.getAttribute("elemtag").toString() , "UTF-8") %></c:set>
											<li>
												<a class="hastooltip" title="<fmt:message key='key.formatter.program.view.all.same.tag' />" href="<cms:link>${searchpage}?${tagsearchfieldname}=${elemtagencoded }&${typesearchfieldname}=dphprograma</cms:link>"><span class="fa fa-hashtag" aria-hidden="true"></span><strong>${elemtag }</strong></a>
											</li>
										</c:forEach>
									</ul>
								</div>
							</c:if>
						</header>
					</c:if>
					<c:if test="${cms.element.settings.hidetitle}">
						<c:if test="${cms.element.settings.generatepdf == 'true' }">
							<div class="clearfix margin-bottom-30">
								<a class="btn btn-default btn-pdf btn-sm pull-right hastooltip" title="<fmt:message key="label.download.program.title.pdf" />" href="<cms:pdf format='%(link.weak:/system/modules/com.saga.diputacion.huelva.frontend/elements/f-programa-detail-pdf.jsp:b772581b-0bf4-11e8-abc7-63f9a58dae09)' content='${content.filename}' locale='${cms.locale}'/>?themeconfigpath=${themeconfigpath}" target="pdf">
									<span class="fa fa-file-pdf-o" aria-hidden="true"></span>&nbsp;<fmt:message key="label.download.pdf" />
								</a>
							</div>
						</c:if>
					</c:if>
					<div class="contentblock">
						<div class="contentblock-texto row">
							<%-- Imagen --%>
							<c:if test="${value.Image.isSet}">
								<c:set var="imagePath">${value.Image}</c:set>
								<c:set var="spacer" value=" " />
								<c:set var="imageTitle">
									<cms:property name="Title_${cms.locale}" file="${imagePath}" />
								</c:set>
								<c:if test="${empty imageTitle}">
									<c:set var="imageTitle">
										<cms:property name="Title" file="${imagePath}" />
									</c:set>
								</c:if>
								<c:set var="imgblockwidth">12</c:set>
								<c:set var="imgWidth">1150</c:set>
								<c:set var="imgHeight">200</c:set>
								<c:if test="${value.Text.isSet}">
									<c:set var="imgblockwidth">4</c:set>
									<c:set var="imgWidth"></c:set>
									<c:set var="imgHeight"></c:set>
								</c:if>
								<div class="media-object image col-sm-${imgblockwidth}">
									<div class="saga-imagen" ${rdfa.Image}>
										<a href="<cms:link>${value.Image}</cms:link>" title="${imageTitle}" data-title="${imageTitle}" title="<fmt:message key='key.formatter.expand.image' />${spacer}${imageTitle}" class="wrapper-image zoom-filter ">
											<div class="overlay-zoom">
												<cms:img src="${imagePath}" alt="${imageTitle}" cssclass="img-responsive" width="${imgWidth}" height="${imgHeight}" maxWidth="1150" scaleType="2"/>
												<div class="zoom-icon">
													<span class="fa fa-expand" aria-hidden="true"></span>
												</div>
											</div>
										</a>
									</div>
								</div>
							</c:if>
							<%-- Texto y actividades --%>
							<div class="container-fluid media-body">
								<c:if test="${value.Description.isSet}">
									<div class="h4"><fmt:message key="key.formatter.program.description" /></div>
									<div class="text-block" ${rdfa.Description}>
										${value.Description}
									</div>
								</c:if>
								<c:if test="${value.Remarks.isSet}">
									<div class="h4"><fmt:message key="key.formatter.program.remarks" /></div>
									<div class="text-block mt-20" ${rdfa.Remarks}>
										${value.Remarks}
									</div>
								</c:if>
								<%-- EVENTOS DEL PROGRAMA - Hacemos una query a solr para ver los recursos evento con el campo de programa correspondiente
								Primero guardamos en una variable la ruta completa del programa para compararla con el campo indexado en Solr de los eventos
								con la ruta del programa al que apuntan
								--%>
								<c:set var="programRootPath">${content.file.rootPath}</c:set>
								<c:set var="rootPath">${cms.requestContext.siteRoot}/</c:set>
								<c:set var="resourceType">dphevento</c:set>
								<c:set var="solrRows">100</c:set>
								<c:set var="solrQuery">fq=parent-folders:"${rootPath}"&fq=type:${resourceType}&fq=xmlprograma_${cms.locale}_s:"${programRootPath}"&rows=${solrRows}&sort=xmldate_${cms.locale}_dt desc</c:set>
								<cms:contentload collector="byContext" param='${solrQuery}' preload="true" >
									<cms:contentinfo var="info" />
									<c:choose>
										<c:when test="${info.resultSize > 0}">
											<div class="list program-events-list mt-30">
												<div class="h4 mb-20"><fmt:message key="key.formatter.program.activities" /></div>
												<ul class="list-unstyled no-margin">
													<c:set var="now" value ="<%= new java.util.Date().getTime()%>" />
													<cms:contentload>
														<cms:contentaccess var="content" />
														<fmt:parseNumber var="eventDate" value="${content.value.FichaEvento.value.FechaInicio}" integerOnly="true" />
														<c:if test="${content.value.FichaEvento.value.FechaFin.isSet}">
															<fmt:parseNumber var="eventDate" value="${content.value.FichaEvento.value.FechaFin}" integerOnly="true" />
														</c:if>
														<c:set var="eventactiveclass"></c:set>
														<c:if test="${now > eventDate}">
															<c:set var="eventactiveclass">event-held</c:set>
															<c:set var="eventheld" scope="request">true</c:set>
														</c:if>
														<li class="program-event-item ${eventactiveclass}">
															<cms:display value="${content.filename}">
																<cms:displayFormatter type="dphevento" path="/system/modules/com.saga.diputacion.huelva.frontend/formatters/dphevento-formatter-display.xml" />
															</cms:display>
														</li>
													</cms:contentload>
												</ul>
											</div>
										</c:when>
										<c:otherwise></c:otherwise>
									</c:choose>
								</cms:contentload>
								<c:set var="eventheld" scope="request" value="" />
								</div>
							</div>
					</div>
					<c:if test="${value.Content.exists}">
						<c:forEach var="elem" items="${content.valueList.Content}" varStatus="status">
							<c:set var="contentblock" value="${elem}" scope="request"></c:set>
							<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-default-contentblock.jsp:e9a36d37-df65-11e4-bcf9-01e4df46f753)">
								<cms:param name="contentId">${id}-contentblock-${status.count}</cms:param>
							</cms:include>
						</c:forEach>
					</c:if> <%-- Fin cierre comprobacion de seccion --%>
					</div> <!-- Fin de wrapper -->
				</article>
			</c:otherwise>
		</c:choose>

	</cms:formatter>
</cms:bundle>