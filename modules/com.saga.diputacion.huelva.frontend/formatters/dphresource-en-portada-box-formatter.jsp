<%@ page import="java.awt.*" %>
<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.diputacion.huelva.frontend.messages">
	<cms:formatter var="content" val="value">
		<c:if test="${cms.element.setting.marginbottom.value != '0'}">
			<c:set var="marginClass">margin-bottom-${cms.element.setting.marginbottom.value}</c:set>
		</c:if>

		<c:if test="${not empty cms.element.settings.classmainbox }">
			<c:set var="classmainbox">${cms.element.settings.classmainbox}</c:set>
		</c:if>

		<c:if test="${not empty cms.element.settings.fontsize }">
			<c:set var="fontsize">${cms.element.settings.fontsize}</c:set>
		</c:if>

		<c:if test="${not empty cms.element.settings.wellbox }">
			<c:set var="wellbox">well ${cms.element.settings.wellbox}</c:set>
		</c:if>

		<c:set var="hidedate">false</c:set>
		<c:if test="${cms.element.settings.hidedate == 'true' }">
			<c:set var="hidedate">true</c:set>
		</c:if>

		<c:if test="${not empty cms.element.settings.equalheight}">
			<c:set var="equalheight">${cms.element.settings.equalheight}</c:set>
		</c:if>

		<c:if test="${not empty cms.element.settings.bgimagealign}">
			<c:set var="bgimagealign">${cms.element.settings.bgimagealign}</c:set>
		</c:if>

		<%-- Definimos el tipo de etiqueta html para nuestro encabezado --%>

		<c:set var="titletag">h2</c:set>
		<c:set var="titlesize">h4</c:set>

		<c:if test="${not empty cms.element.settings.titletag }">
			<c:set var="titletag">${cms.element.settings.titletag }</c:set>
		</c:if>

		<%-- definimos el tamaño del encabezado --%>

		<c:if test="${not empty cms.element.settings.titlesize }">
			<c:set var="titlesize">${cms.element.settings.titlesize }</c:set>
		</c:if>

		<c:if test="${not empty cms.element.settings.height }">
			<c:set var="height">min-height: ${cms.element.settings.height }px;</c:set>
		</c:if>

		<c:set var="htmltag">article</c:set>
		<c:set var="dphenlace"></c:set>
		<c:set var="link">${content.filename}</c:set>
		<c:set var="linktarget">_self</c:set>
		<c:set var="showtext">true</c:set>
		<c:set var="titleAttr"><fmt:message key="key.list.element.go.to" />'${value.Title}'</c:set>
		<%-- recurso dphenlace --%>
		<c:if test="${value.LinkConfig.exists and value.LinkConfig.value.Href.isSet}">
			<c:set var="dphenlace">true</c:set>
			<c:set var="link">${value.LinkConfig.value.Href}</c:set>
			<c:if test="${value.LinkConfig.value.Target == '_blank'}">
				<c:set var="linktarget">_blank</c:set>
			</c:if>
			<c:if test="${value.LinkConfig.value.Title.isSet}">
				<c:set var="titleAttr">${value.LinkConfig.value.Title}</c:set>
			</c:if>
			<c:set var="htmltag">div</c:set>
			<c:set var="showtext">false</c:set>
			<c:if test="${value.ListTitle.isSet}">
				<c:set var="showtext">true</c:set>
				<c:set var="title">${value.ListTitle}</c:set>
			</c:if>
		</c:if>
		<%-- Definimos las variables que gestionan la imagen --%>
		<jsp:useBean id="random" class="java.util.Random" scope="application"/>
		<c:set var="defaultimgint">${random.nextInt(4)+1}</c:set>

		<c:if test="${defaultimgint > 0 and defaultimgint <= 1 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg}">
			<c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg}</c:set>
		</c:if>
		<c:if test="${defaultimgint > 1 and defaultimgint <= 2 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg2}">
			<c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg2}</c:set>
		</c:if>
		<c:if test="${defaultimgint > 2 and defaultimgint <= 3 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg3}">
			<c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg3}</c:set>
		</c:if>
		<c:if test="${defaultimgint > 3 and defaultimgint <= 4 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg4}">
			<c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg4}</c:set>
		</c:if>
		<c:if test="${defaultimgint > 4 and defaultimgint <= 5 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg5}">
			<c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg5}</c:set>
		</c:if>


		<c:if test="${value.Content.exists
					and value.Content.value.Media.exists
					and value.Content.value.Media.value.MediaMultiple.exists
					and value.Content.value.Media.value.MediaMultiple.value.MediaMultipleElements.exists
					and value.Content.value.Media.value.MediaMultiple.value.MediaMultipleElements.value.ImageMain.exists
					and value.Content.value.Media.value.MediaMultiple.value.MediaMultipleElements.value.ImageMain.value.Image.isSet}">
			<c:set var="imagePath">
				${value.Content.value.Media.value.MediaMultiple.value.MediaMultipleElements.value.ImageMain.value.Image}
			</c:set>
		</c:if>
		<c:if test="${value.Content.exists
					and value.Content.value.Media.exists
					and value.Content.value.Media.value.ImageMain.exists
					and value.Content.value.Media.value.ImageMain.value.Image.isSet}">
			<c:set var="imagePath">
				${value.Content.value.Media.value.ImageMain.value.Image}
			</c:set>
		</c:if>
		<c:if test="${value.Media.exists
					and value.Media.value.ImageMain.exists
					and value.Media.value.ImageMain.value.Image.isSet}">
			<c:set var="imagePath">
				${value.Media.value.ImageMain.value.Image}
			</c:set>
		</c:if>
		<c:if test="${value.ListImage.isSet}">
			<c:set var="imagePath">
				${value.ListImage}
			</c:set>
		</c:if>
		<%-- recurso dphenlace --%>
		<c:if test="${value.Image.isSet}">
			<c:set var="imagePath">
				${value.Image}
			</c:set>
		</c:if>
		<c:set var="bgattributes">style="${height}height:1px;display:table;table-layout:fixed;width:100%;background-image:url('<cms:link>${imagePath}</cms:link>');background-size:cover;background-position: center ${bgimagealign};background-repeat: no-repeat;"</c:set>

		<${htmltag} class="parent dph-noticia-box sg-microcontent sg-microcontent-panel sg-microcontent-overlay box-featured-new masonry-box box<c:out value=' ${marginClass} ${equalheight}'/>"${spacer}${bgattributes}>
				<div class="element complete-mode with-media">
					<a class="overlay-link" target="${linktarget}" href="<cms:link>${link}</cms:link>" title="${titleAttr}">
						<span class="hover-overlay" aria-hidden="true"></span>
						<span class="sr-only">${value.Title}</span>
					</a>
					<c:if test="${showtext == 'true'}">
						<div class="text-overlay">
							<div class="wrapper">
								<header>
									<${titletag} class="title no-margin no-padding ${titlesize}">
										${value.Title}
									</${titletag}>
									<c:if test="${empty dphenlace and hidedate != 'true'}">
										<div class="info-date">
											<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-time.jsp:ea2c737f-df65-11e4-bcf9-01e4df46f753)">
												<cms:param name="date">${value.Date }</cms:param>
												<cms:param name="shortdate">true</cms:param>
												<cms:param name="showtime">false</cms:param>
											</cms:include>
										</div>
									</c:if>
								</header>
							</div>
						</div>
					</c:if>
				</div>
		</${htmltag}>
	</cms:formatter>
</cms:bundle>