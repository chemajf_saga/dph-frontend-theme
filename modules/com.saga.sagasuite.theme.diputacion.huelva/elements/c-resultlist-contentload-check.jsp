<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates" %>

  <cms:contentload collector="byQuery" param="${solrquery }" preload="false">
    <cms:contentinfo var="info"/>
    <c:if test="${info.resultSize > 0}">
      <c:set var="withResults" scope="request">true</c:set>
    </c:if>
    <c:if test="${info.resultSize == 0}">
      <c:set var="withResults" scope="request">false</c:set>
    </c:if>
  </cms:contentload>
