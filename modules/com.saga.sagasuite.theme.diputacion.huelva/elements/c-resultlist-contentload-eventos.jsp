<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">

  <c:set var="id" value="${value.Id }"/>
  <%
    String id = "" + pageContext.getAttribute("id");
    String p = (String) request.getParameter("p" + id);
    if (p == null)
      p = "1";
    request.setAttribute("currentPage", p);
  %>
<%-- recogemos los parametros de visualizacion en columnas y otros settings --%>

<c:set var="showdate">${param.showdate}</c:set>
<c:set var="formatdate">${param.formatdate}</c:set>
<c:set var="titletagelement">${param.titletagelement}</c:set>
<c:set var="titletagelementsize">${param.titletagelementsize}</c:set>
<c:set var="titleblockfeaturedevent">${param.titleblockfeaturedevent}</c:set>
<c:set var="titleblocknextevents">${param.titleblocknextevents}</c:set>


<%-- Es necesario anadir el rows al limite configurado para que la paginacion la haga el contentload y no solr --%>
<c:set var="solrqueryFeatured">${solrquery }&fq=category:"destacado/evento-destacado/"&rows=1</c:set>
<c:set var="solrquery">${solrquery }&fq=xmldate_es_dt:[NOW TO *]&rows=${value.ResultsSize }</c:set>

  <cms:contentload collector="byQuery" param="${solrquery }" preload="false">
  <cms:contentinfo var="info"/>
    <c:if test="${info.resultSize > 0}">
      <c:set var="withResults">true</c:set>
    </c:if>
    <c:if test="${info.resultSize == 0}">
      <c:set var="withResults">false</c:set>
    </c:if>
  </cms:contentload>
  <cms:contentload collector="byQuery" param="${solrqueryFeatured }" preload="false">
    <cms:contentinfo var="info"/>
    <c:if test="${info.resultSize > 0}">
      <c:set var="withResultsFeatured">true</c:set>
    </c:if>
    <c:if test="${info.resultSize == 0}">
      <c:set var="withResultsFeatured">false</c:set>
    </c:if>
  </cms:contentload>
  <c:if test="${withResultsFeatured == 'true'}">
    <c:set var="listClass">col-sm-6</c:set>
  </c:if>
<div class="row">
    <%-- Lista 1 evento destacado --%>
  <c:if test="${withResultsFeatured == 'true'}">
    <div class="col-xxs-12 ${listClass}">
      <div class="title h4">${titleblockfeaturedevent}</div>
      <ul class="list-unstyled main-ul">
        <cms:contentload collector="byQuery" param="${solrqueryFeatured }" preload="true">
          <cms:contentinfo var="info"/>
          <cms:contentload pageSize="1" pageIndex="${currentPage}" pageNavLength="${value.NavigationSize}" editable="false">
            <cms:contentinfo var="infocontent"/>
            <cms:contentaccess var="resource"/>
            <c:set var="resultSize" value="${info.resultSize}"/>
            <c:set var="numPages" value="${infocontent.pageCount}"/>
            <c:set var="resourceForPages" value="${infocontent.pageSize}"/>
            <c:set var="firstPage" value="${infocontent.pageNavStartIndex}"/>
            <c:set var="lastPage" value="${infocontent.pageNavEndIndex}"/>
            <c:set var="count">${infocontent.resultIndex}</c:set>
            <c:set var="empezarEn">${value.StartIn}</c:set>
            <c:if test="${!value.StartIn.isSet or count>empezarEn}">
              <c:set var="linkDetail"><cms:contentshow element="%(opencms.filename)"/></c:set>
              <c:set var="title"></c:set>
              <c:set var="titlelarge"></c:set>
              <c:forEach var="titleprev" items="${content.valueList.MappingTitle }">
                <c:if test="${empty title}">
                  <cms:contentcheck ifexists="${titleprev}" >
                    <c:set var="titleValue"><cms:contentshow element="${titleprev}"/></c:set>
                    <c:if test="${titleValue !=null and titleValue !=''}">
                      <c:set var="title">${titleValue}</c:set>
                      <c:set var="titlelarge">${titleValue}</c:set>
                    </c:if>
                  </cms:contentcheck>
                </c:if>
              </c:forEach>
              <c:set var="description"><cms:contentshow element="${value.MappingDescription }"/></c:set>
              <c:set var="date"><cms:contentshow element="${value.MappingDate }"/></c:set>
              <c:set var="imagePath"></c:set>
              <c:forEach var="image" items="${content.valueList.MappingImage }">
                <c:if test="${empty imagePath}">
                  <cms:contentcheck ifexists="${image}" >
                    <c:set var="imagePathValue"><cms:contentshow element="${image}"/></c:set>
                    <c:if test="${imagePathValue !=null and imagePathValue !='' and fn:indexOf(imagePathValue, '???')==-1}">
                      <c:set var="imagePath">${imagePathValue}</c:set>
                    </c:if>
                  </cms:contentcheck>
                </c:if>
              </c:forEach>
              <%-- Establecemos la imagen por defecto definida en el .themeconfig por si no viene imagen en el recurso --%>
              <c:if test="${empty imagePath and not empty themeConfiguration.CustomFieldKeyValue.defaultimg}">
                <c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg}</c:set>
                <c:if test="${count > 1 and count %2 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg2}">
                  <c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg2}</c:set>
                </c:if>
                <c:if test="${count > 1 and count %3 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg3}">
                  <c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg3}</c:set>
                </c:if>
                <c:if test="${count > 1 and count %4 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg4}">
                  <c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg4}</c:set>
                </c:if>
                <c:if test="${count > 1 and count %5 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg5}">
                  <c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg5}</c:set>
                </c:if>
              </c:if>
              <c:set var="showLinkToDetail" value="true"/> <%-- Por defecto siempre mostramos link --%>
              <c:if test="${value.ShowLinkToDetail.isSet and value.ShowLinkToDetail=='false'}">
                <c:set var="showLinkToDetail" value="false"/>
              </c:if>
              <c:if test="${value.ShowLinkToDetail.isSet and value.ShowLinkToDetail=='nodefined'}">
                <%-- En este caso tenemos que leer la propiedad del recurso para ver si se muestra o no --%>
                <c:set var="propertyShowDetail">${cms.vfs.property[resource.filename]['sagasuite.showdetail']}</c:set>
                <c:if test="${propertyShowDetail!=null and propertyShowDetail == 'false'}">
                  <c:set var="showLinkToDetail" value="false"/>
                </c:if>
              </c:if>

              <c:set var="published" value="true"/>
              <c:if test="${resource.file.state.state==1 or resource.file.state.state==2 }">
                <c:set var="published" value="false"/>
              </c:if>

              <c:if test="${value.DescriptionMaxCharacter.isSet }">
                <c:set var="descriptionMaxCharacter">${value.DescriptionMaxCharacter }</c:set>
                <%
                  String description = "" + pageContext.getAttribute("description");
                  Integer descriptionMaxCharacter = Integer.parseInt(pageContext.getAttribute("descriptionMaxCharacter") + "");
                  if (description.length() > descriptionMaxCharacter)
                    description = description.substring(0, descriptionMaxCharacter) + "...";
                  pageContext.setAttribute("description", description);
                %>
              </c:if>
              <c:if test="${value.TitleMaxCharacter.isSet }">
                <c:set var="titleMaxCharacter">${value.TitleMaxCharacter }</c:set>
                <%
                  String title = "" + pageContext.getAttribute("title");
                  Integer titleMaxCharacter = Integer.parseInt(pageContext.getAttribute("titleMaxCharacter") + "");
                  if (title.length() > titleMaxCharacter)
                    title = title.substring(0, titleMaxCharacter) + "...";
                  pageContext.setAttribute("title", title);
                %>
              </c:if>
              <%-- Comprobamos si el title y el titlelarge son diferentes, y si es asi cargamos el atributo title del enlace --%>
              <c:set var="titleEqualsToTitleLarge">true</c:set>
              <c:if test="${not empty titlelarge}">
                <c:set var="trimTitleLarge">${fn:trim(titlelarge)}</c:set>
              </c:if>
              <c:if test="${not empty title}">
                <c:set var="trimTitle">${fn:trim(title)}</c:set>
              </c:if>
              <c:if test="${(not empty trimTitle) and (not empty trimTitleLarge)}">
                <c:set var="titleEqualsToTitleLarge">${trimTitle eq trimTitleLarge}</c:set>
              </c:if>
              <c:if test="${titleEqualsToTitleLarge == 'false'}">
                <c:set var="titleAttr">${titlelarge}</c:set>
              </c:if>

              <%-- Para definir si las imagenes de los elementos de listadoi van a tener el boton de ampliar --%>
              <c:set var="imageshowbig">false</c:set>
              <c:if test="${value.ImageElementShowBig.isSet and value.ImageElementShowBig == 'true' }">
                <c:set var="imageshowbig">true</c:set>
              </c:if>
              <%-- Definimos la variable del ancho de imagen a 480 por defecto y si el campo se edita se carga el campo --%>

              <c:if test="${value.ImageElementWidth.isSet }">
                <c:set var="imageWidth">${value.ImageElementWidth }</c:set>
              </c:if>
              <c:if test="${!value.ImageElementWidth.isSet }">
                <c:set var="imageWidth">480</c:set>
              </c:if>
              <c:if test="${value.ImageElementHeight.isSet }">
                <c:set var="imageHeight">${value.ImageElementHeight }</c:set>
              </c:if>

              <li class="item-thumbnail has-media media-to-top has-media-complete">
                <article class="media no-padding">
                  <c:if test="${imagePath!=null and imagePath!='' and fn:indexOf(imagePath, '???')==-1}">
                    <c:set var="imageTitle"><cms:property name="Title_${cms.locale}" file="${imagePath}"/></c:set>
                    <c:if test="${empty imageTitle}">
                      <c:set var="imageTitle"><cms:property name="Title" file="${imagePath}"/></c:set>
                    </c:if>
                  <div class="image media-object">
                    <div class="saga-imagen">
                      <a href="<cms:link>${linkDetail }</cms:link>" class="wrapper-image">
                        <cms:img src="${imagePath}" width="${imageWidth}" height="${imageHeight}" alt="${imageTitle}" scaleType="2" cssclass="img-responsive" />
                      </a>
                    </div>
                  </div>
                  </c:if>
                  <div class="media-body">
                    <div class="info">
                      <c:if test="${not empty date}">
					<span class="info-element date-element inline-b">
						<span class="pe-7s-date fs20 v-align-m mr-5" aria-hidden="true"></span>
						<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/e-time-period-item.jsp:ee1472e5-1185-11e8-a1f9-63f9a58dae09)">
                          <cms:param name="inidate">${date }</cms:param>
                          <c:if test="${resource.value.FichaEvento.value.ShowInicioTime == 'true' }">
                            <cms:param name="showinitime">true</cms:param>
                          </c:if>
                          <c:if test="${resource.value.FichaEvento.value.FechaFin.isSet }">
                            <cms:param name="findate">${resource.value.FichaEvento.value.FechaFin }</cms:param>
                          </c:if>
                          <c:if test="${resource.value.FichaEvento.value.ShowFinTime == 'true' }">
                            <cms:param name="showfintime">true</cms:param>
                          </c:if>
                        </cms:include>
					</span>
                      </c:if>
                      <c:if test="${resource.value.Municipio.isSet }">
					<span class="info-element municipio-element inline-b ml-15">
						<span class="pe-7s-map-marker fs20 v-align-m" aria-hidden="true"></span>
						<c:set var="categorytitle"><cms:property name="Title_${locale}" file="${resource.value.Municipio}"/></c:set>
						<c:if test="${empty categoryPropertyTitle}">
                          <c:set var="categorytitle"><cms:property name="Title" file="${resource.value.Municipio}"/></c:set>
                        </c:if>
						${categorytitle}
					</span>
                      </c:if>
                    </div>
                    <${titletagelement} class="media-heading ${titletagelementsize}">
                    <a href="<cms:link>${linkDetail }</cms:link>">${title }</a>
                  </${titletagelement}>
                  <c:if test="${not empty description}">
                  <div class="description">
                    <p>${description}</p>
                  </div>
                  </c:if>
              </div>
              </article>
              </li>
            </c:if>
          </cms:contentload>
        </cms:contentload>
        </ul>
    </div>
  </c:if>
  <%-- Lista próximos eventos--%>
<c:if test="${withResults == 'true'}">
  <div class="col-xxs-12 ${listClass}">
    <div class="title h4">${titleblocknextevents}</div>
    <ul class="list-unstyled main-ul">
      <cms:contentload collector="byQuery" param="${solrquery }" preload="true">
        <cms:contentinfo var="info"/>
          <cms:contentload pageSize="${value.PageSize}" pageIndex="${currentPage}" pageNavLength="${value.NavigationSize}" editable="false">
            <cms:contentinfo var="infocontent"/>
            <cms:contentaccess var="resource"/>
            <c:set var="resultSize" value="${info.resultSize}"/>
            <c:set var="numPages" value="${infocontent.pageCount}"/>
            <c:set var="pageSize" value="${infocontent.pageSize}"/>
            <c:set var="resourceForPages" value="${infocontent.pageSize}"/>
            <c:set var="firstPage" value="${infocontent.pageNavStartIndex}"/>
            <c:set var="lastPage" value="${infocontent.pageNavEndIndex}"/>
            <c:set var="count">${infocontent.resultIndex}</c:set>
            <c:set var="empezarEn">${value.StartIn}</c:set>
            <c:if test="${!value.StartIn.isSet or count>empezarEn}">
                <c:set var="linkDetail"><cms:contentshow element="%(opencms.filename)"/></c:set>
                <c:set var="title"></c:set>
                <c:set var="titlelarge"></c:set>
                <c:forEach var="titleprev" items="${content.valueList.MappingTitle }">
                  <c:if test="${empty title}">
                    <cms:contentcheck ifexists="${titleprev}" >
                      <c:set var="titleValue"><cms:contentshow element="${titleprev}"/></c:set>
                      <c:if test="${titleValue !=null and titleValue !=''}">
                        <c:set var="title">${titleValue}</c:set>
                        <c:set var="titlelarge">${titleValue}</c:set>
                      </c:if>
                    </cms:contentcheck>
                  </c:if>
                </c:forEach>
                <c:set var="description"><cms:contentshow element="${value.MappingDescription }"/></c:set>
                <c:set var="date"><cms:contentshow element="${value.MappingDate }"/></c:set>
                <c:set var="imagePath"></c:set>
                <c:forEach var="image" items="${content.valueList.MappingImage }">
                  <c:if test="${empty imagePath}">
                    <cms:contentcheck ifexists="${image}" >
                      <c:set var="imagePathValue"><cms:contentshow element="${image}"/></c:set>
                      <c:if test="${imagePathValue !=null and imagePathValue !='' and fn:indexOf(imagePathValue, '???')==-1}">
                        <c:set var="imagePath">${imagePathValue}</c:set>
                      </c:if>
                    </cms:contentcheck>
                  </c:if>
                </c:forEach>
                <%-- Establecemos la imagen por defecto definida en el .themeconfig por si no viene imagen en el recurso --%>
                <c:if test="${empty imagePath and not empty themeConfiguration.CustomFieldKeyValue.defaultimg}">
                  <c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg}</c:set>
                  <c:if test="${count > 1 and count %2 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg2}">
                    <c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg2}</c:set>
                  </c:if>
                  <c:if test="${count > 1 and count %3 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg3}">
                    <c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg3}</c:set>
                  </c:if>
                  <c:if test="${count > 1 and count %4 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg4}">
                    <c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg4}</c:set>
                  </c:if>
                  <c:if test="${count > 1 and count %5 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg5}">
                    <c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg5}</c:set>
                  </c:if>
                </c:if>
                <c:set var="showLinkToDetail" value="true"/> <%-- Por defecto siempre mostramos link --%>
                <c:if test="${value.ShowLinkToDetail.isSet and value.ShowLinkToDetail=='false'}">
                  <c:set var="showLinkToDetail" value="false"/>
                </c:if>
                <c:if test="${value.ShowLinkToDetail.isSet and value.ShowLinkToDetail=='nodefined'}">
                  <%-- En este caso tenemos que leer la propiedad del recurso para ver si se muestra o no --%>
                  <c:set var="propertyShowDetail">${cms.vfs.property[resource.filename]['sagasuite.showdetail']}</c:set>
                  <c:if test="${propertyShowDetail!=null and propertyShowDetail == 'false'}">
                    <c:set var="showLinkToDetail" value="false"/>
                  </c:if>
                </c:if>

                <c:set var="published" value="true"/>
                <c:if test="${resource.file.state.state==1 or resource.file.state.state==2 }">
                  <c:set var="published" value="false"/>
                </c:if>

                <c:if test="${value.DescriptionMaxCharacter.isSet }">
                  <c:set var="descriptionMaxCharacter">${value.DescriptionMaxCharacter }</c:set>
                  <%
                    String description = "" + pageContext.getAttribute("description");
                    Integer descriptionMaxCharacter = Integer.parseInt(pageContext.getAttribute("descriptionMaxCharacter") + "");
                    if (description.length() > descriptionMaxCharacter)
                      description = description.substring(0, descriptionMaxCharacter) + "...";
                    pageContext.setAttribute("description", description);
                  %>
                </c:if>
                <c:if test="${value.TitleMaxCharacter.isSet }">
                  <c:set var="titleMaxCharacter">${value.TitleMaxCharacter }</c:set>
                  <%
                    String title = "" + pageContext.getAttribute("title");
                    Integer titleMaxCharacter = Integer.parseInt(pageContext.getAttribute("titleMaxCharacter") + "");
                    if (title.length() > titleMaxCharacter)
                      title = title.substring(0, titleMaxCharacter) + "...";
                    pageContext.setAttribute("title", title);
                  %>
                </c:if>
                <%-- Comprobamos si el title y el titlelarge son diferentes, y si es asi cargamos el atributo title del enlace --%>
                <c:set var="titleEqualsToTitleLarge">true</c:set>
                <c:if test="${not empty titlelarge}">
                  <c:set var="trimTitleLarge">${fn:trim(titlelarge)}</c:set>
                </c:if>
                <c:if test="${not empty title}">
                  <c:set var="trimTitle">${fn:trim(title)}</c:set>
                </c:if>
                <c:if test="${(not empty trimTitle) and (not empty trimTitleLarge)}">
                  <c:set var="titleEqualsToTitleLarge">${trimTitle eq trimTitleLarge}</c:set>
                </c:if>
                <c:if test="${titleEqualsToTitleLarge == 'false'}">
                  <c:set var="titleAttr">${titlelarge}</c:set>
                </c:if>

                <%-- Para definir si las imagenes de los elementos de listadoi van a tener el boton de ampliar --%>
                <c:set var="imageshowbig">false</c:set>
                <c:if test="${value.ImageElementShowBig.isSet and value.ImageElementShowBig == 'true' }">
                  <c:set var="imageshowbig">true</c:set>
                </c:if>
                <%-- Definimos la variable del ancho de imagen a 480 por defecto y si el campo se edita se carga el campo --%>

                <c:if test="${value.ImageElementWidth.isSet }">
                  <c:set var="imageWidth">${value.ImageElementWidth }</c:set>
                </c:if>
                <c:if test="${!value.ImageElementWidth.isSet }">
                  <c:set var="imageWidth">480</c:set>
                </c:if>
                <c:if test="${value.ImageElementHeight.isSet }">
                  <c:set var="imageHeight">${value.ImageElementHeight }</c:set>
                </c:if>

              <li class="item-featured-date">
                <div class="media media-wrapper media-wrapper-sm no-padding">
                  <div class="media-date media-date-sm media-left media-middle">
                    <c:set var="date">${resource.value.FichaEvento.value.FechaInicio }</c:set>
                    <div class="media-object tooltip-right" title="<fmt:formatDate dateStyle='FULL' value='${cms:convertDate(date)}' type='date' />">
                      <time class="sr-only" datetime="<fmt:formatDate value="${cms:convertDate(date)}" pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ" />">
                        <fmt:formatDate dateStyle="FULL" value="${cms:convertDate(date)}" type="date" />
                      </time>
                      <c:set var="dateday">
                        <fmt:formatDate value="${cms:convertDate(date)}" pattern="EEE" />
                      </c:set>
                      <c:set var="datedaynumber">
                        <fmt:formatDate value="${cms:convertDate(date)}" pattern="dd"/>
                      </c:set>
                      <c:set var="datemonth">
                        <fmt:formatDate value="${cms:convertDate(date)}" pattern="MMM"/>
                      </c:set>
                      <div class="day upper">${dateday}</div>
                      <div class="daynumber">${datedaynumber}</div>
                      <div class="month upper">${datemonth}</div>
                    </div>
                  </div>
                  
					<c:set var="imagePath">${imagePath}?__scale=h:480,w:480,t:2,c:transparent,q:100</c:set>
					<div class="media-img media-body media-middle col-xxs-12 col-xs-3"
						style="background-size: cover;background-position: center center;background-image: url('<cms:link>${imagePath}</cms:link>');float:none;">
						<span class="sr-only">${imageTitle}</span>
					</div>
                  
                  <a class="media-info media-body media-middle col-xxs-12 col-xs-9" style="float:left" href="<cms:link>${linkDetail}</cms:link>">
                    <c:if test="${resource.value.Municipio.isSet }">
                      <div class="info">
                        <c:set var="municipios" value="${fn:split(resource.value.Municipio, ',')}" />
                        <c:forEach items="${municipios }" var="municipio" varStatus="status">
                          <c:if test="${status.count != 1}">
                            <span class="ml-5 mr-5 v-align-m inline-b">|</span>
                          </c:if>
						<span class="municipio-element info-element inline-b">
							<c:if test="${status.count == 1}">
                              <span class="pe-7s-map-marker fs18 v-align-m" aria-hidden="true"></span>
                            </c:if>
							<c:set var="municipiotitle"><cms:property name="Title_${locale}" file="${municipio}"/></c:set>
							<c:if test="${empty municipiotitle}">
                              <c:set var="municipiotitle"><cms:property name="Title" file="${municipio}"/></c:set>
                            </c:if>
							<span class="v-align-m">${municipiotitle}</span>
						</span>
                        </c:forEach>
                      </div>
                    </c:if>
                <span class="media-heading h5">
                    ${title}
                </span>
                    <c:if test="${not empty description}">
                      <span class="disp-block mt-5 description">${description}</span>
                    </c:if>
                  </a>
                </div>
              </li>
            </c:if>
          </cms:contentload>
      </cms:contentload>
      </ul>
        <%-- botón de ver más resultados --%>
      <c:if test="${not empty param.listlinkhref and param.showbuttonlist == 'true'}">
        <c:set var="linkhref">${param.listlinkhref}</c:set>
        <c:set var="linktarget">_self</c:set>
        <c:if test="${not empty param.listlinktarget}">
          <c:set var="linktarget">${param.listlinktarget}</c:set>
        </c:if>
        <c:set var="linktitle"><fmt:message key="key.element.read.more" /></c:set>
        <c:if test="${not empty param.listlinktitle}">
          <c:set var="linktitle">${param.listlinktitle}</c:set>
        </c:if>
        <c:set var="buttonlistposition">${param.buttonlistposition}</c:set>
        <c:set var="buttonclass">btn btn-lg btn-xlg btn-specific-main btn-bordered</c:set>
        <c:if test="${not empty param.buttonlistclass}">
          <c:set var="buttonclass">${param.buttonlistclass}</c:set>
        </c:if>
        <%-- comprobamos si existen más resultados de los definidos a mostrar en el listado y si es asi cargamos el boton de ver todos --%>
        <fmt:parseNumber value="${resultSize}" parseLocale="${cms.locale}" integerOnly="true" var="resultSize"/>
        <fmt:parseNumber value="${value.PageSize}" parseLocale="${cms.locale}" integerOnly="true" var="pageSize"/>
        <c:if test="${resultSize > 0 and resultSize > pageSize }">
          <div class="mt-30 text-${buttonlistposition}">
            <a class="${buttonclass}" target="${linktarget}" href="<cms:link>${linkhref}</cms:link>">
              <span class="fa fa-plus mr-10" aria-hidden="true"></span>${linktitle}
            </a>
          </div>
        </c:if>
      </c:if>
    </div>
  </c:if>
  </div><%-- fin de row--%>
</cms:bundle>