<%@page buffer="none" session="false" import="org.opencms.jsp.CmsJspActionElement,org.opencms.file.*,org.opencms.main.*" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">

<c:set var="id" value="${value.Id }"/>
<%
  String id = "" + pageContext.getAttribute("id");
  String p = (String) request.getParameter("p" + id);
  if (p == null)
    p = "1";
  request.setAttribute("currentPage", p);
%>

<%-- Es necesario anadir el rows al limite configurado para que la paginacion la haga el contentload y no solr --%>
<c:set var="solrquery">${solrquery }&rows=${value.ResultsSize }</c:set>
<c:if test="${value.CreatePath.isSet and value.Pattern.isSet }">
  <c:set var="createPath">|createPath=${value.CreatePath }${value.Pattern }</c:set>
</c:if>

<%-- Si se lee la property de category avisamos en offline--%>
<c:if test="${content.value.readPropertyCategory.exists && content.value.readPropertyCategory=='true'}">
  <c:set var="categoryProperty"><cms:property name="category" file="search"/></c:set>
  <c:if test="${not empty categoryProperty}">
    <c:if test="${!cms.isOnlineProject}">
      <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
        <h5 class="no-margin"><strong>Mensaje de edición</strong></h5>
        Este listado lee de la propiedad de "categoría para listado" siendo su valor:
        <strong> ${categoryProperty}</strong>
      </div>
    </c:if>
  </c:if>
</c:if>

<ul class="list-unstyled main-ul grid-masonry">
  <li class="grid-sizer grid-item-width-1"></li>
  <cms:contentload collector="byQuery" param="${solrquery }${createPath }" preload="true">
    <cms:contentinfo var="info"/>

    <c:if test="${((value.ShowButtonNewResource.isSet and value.ShowButtonNewResource=='true' and info.resultSize == 0) or
			  (value.ShowButtonNewResourceAlways.isSet and value.ShowButtonNewResourceAlways=='true'))
		  	  and not cms.requestContext.currentProject.onlineProject}">
      <div class="solrlistbuttonnew" style="font-size: 24px; line-height: 1.5em;" id="divbuttonnew-${value.Id }">
        <button type="button" title="Crea un nuevo recurso del tipo configurado en el listado" class="btn btn-success"
                id="buttonnew-${value.Id }"><i class="fa fa-plus"></i>&nbsp;&nbsp;Nuevo recurso
        </button>
        <p class="alert alert-warning" style="padding: 8px;line-height: 1.1;font-size:13px;margin-top: 10px;">Si despu&eacute;s
          de hacer click en el bot&oacute;n, el listado no carga el nuevo recurso, pulse F5 para actualizar</p>
      </div>
      <div style="display:none;" id="resultnew-${value.Id }"></div>
      <%-- Para poder pasar por parametro el caracter % tenemos que cambiarlo a %25 --%>
      <c:set var="urlpath">${value.CreatePath}${value.Pattern}</c:set>
      <c:set var="urlpath" value="${fn:replace(urlpath, '%', '%25')}"/>
      <c:set
          var="urlparams">?path=${urlpath}&resourceType=${value.ResourceType}&titleField=${value.MappingTitle}</c:set>
      <c:if test="${value.Category.isSet }">
        <c:set var="urlparams">${urlparams }&category=${value.Category}</c:set>
      </c:if>


      <script>
        $(document).ready(function () {
          $("#buttonnew-${value.Id }").click(function () {
            $("#divbuttonnew-${value.Id }").html("<div class='alert alert-info'><i class='fa fa-spinner fa-spin'>&nbsp;</i>&nbsp;&nbsp;Creando ...<div>");
            $.ajax({
              url: "<cms:link>/system/modules/com.saga.sagasuite.solrlist/elements/ajax_newresource.jsp${urlparams}</cms:link>",
              dataType: "json",
              success: function (result) {
                if (result.codeerror == 'false') {

                  setTimeout(function () {
                    M = 100;
                    N = 10000;
                    num = Math.floor(M + (1 + N - M) * Math.random())
                    window.location.href = "<cms:link>${cms.requestContext.uri}?nocache=" + num + "</cms:link>#solrlist-${value.Id}";
                  }, 5000);

                } else {
                  $("#resultnew-${value.Id }").show();
                  $("#divbuttonnew-${value.Id }").hide();
                  $("#resultnew-${value.Id }").html('<p class="alert alert-danger">Error:' + result.msg + '</p>');
                }
              },
              error: function (result, textStatus, errorThrown) {
                $("#resultnew-${value.Id }").show();
                $("#divbuttonnew-${value.Id }").hide();
                $("#resultnew-${value.Id }").html('<p class="alert alert-danger">Error:' + textStatus + ' | ' + errorThrown + '</p>');
              }
            });
          });
        });
      </script>
    </c:if>

    <c:if test="${info.resultSize > 0}">
      <cms:contentload pageSize="${value.PageSize}" pageIndex="${currentPage}" pageNavLength="${value.NavigationSize}"
                       editable="${value.Editable }">
        <cms:contentinfo var="infocontent"/>
        <cms:contentaccess var="resource"/>

        <c:set var="resultSize" value="${info.resultSize}"/>
        <c:set var="numPages" value="${infocontent.pageCount}"/>
        <c:set var="resourceForPages" value="${infocontent.pageSize}"/>
        <c:set var="firstPage" value="${infocontent.pageNavStartIndex}"/>
        <c:set var="lastPage" value="${infocontent.pageNavEndIndex}"/>
        <c:set var="count">${infocontent.resultIndex}</c:set>
        <c:set var="empezarEn">${value.StartIn}</c:set>
        <c:if test="${!value.StartIn.isSet or count>empezarEn}">

            <c:set var="linkDetail"><cms:contentshow element="%(opencms.filename)"/></c:set>
			  <c:set var="title"></c:set>
			  <c:set var="titlelarge"></c:set>
			  <c:forEach var="titleprev" items="${content.valueList.MappingTitle }">
				  <c:if test="${empty title}">
					  <cms:contentcheck ifexists="${titleprev}" >
						  <c:set var="titleValue"><cms:contentshow element="${titleprev}"/></c:set>
						  <c:if test="${titleValue !=null and titleValue !=''}">
							  <c:set var="title">${titleValue}</c:set>
							  <c:set var="titlelarge">${titleValue}</c:set>
						  </c:if>
					  </cms:contentcheck>
				  </c:if>
			  </c:forEach>
            <c:set var="description"><cms:contentshow element="${value.MappingDescription }"/></c:set>
            <c:set var="date"><cms:contentshow element="${value.MappingDate }"/></c:set>
            <c:set var="imagePath"></c:set>
			  <c:forEach var="image" items="${content.valueList.MappingImage }">
				  <c:if test="${empty imagePath}">
					  <cms:contentcheck ifexists="${image}" >
						  <c:set var="imagePathValue"><cms:contentshow element="${image}"/></c:set>
						  <c:if test="${imagePathValue !=null and imagePathValue !='' and fn:indexOf(imagePathValue, '???')==-1}">
							  <c:set var="imagePath">${imagePathValue}</c:set>
						  </c:if>
					  </cms:contentcheck>
				  </c:if>
			  </c:forEach>
            <%-- Establecemos la imagen por defecto definida en el .themeconfig por si no viene imagen en el recurso --%>
            <c:if test="${empty imagePath and not empty themeConfiguration.CustomFieldKeyValue.defaultimg}">
              <c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg}</c:set>
              <c:if test="${count > 1 and count %2 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg2}">
                <c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg2}</c:set>
              </c:if>
              <c:if test="${count > 1 and count %3 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg3}">
                <c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg3}</c:set>
              </c:if>
              <c:if test="${count > 1 and count %4 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg4}">
                <c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg4}</c:set>
              </c:if>
              <c:if test="${count > 1 and count %5 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg5}">
                <c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg5}</c:set>
              </c:if>
            </c:if>

            <c:set var="showLinkToDetail" value="true"/> <%-- Por defecto siempre mostramos link --%>
            <c:if test="${value.ShowLinkToDetail.isSet and value.ShowLinkToDetail=='false'}">
              <c:set var="showLinkToDetail" value="false"/>
            </c:if>
            <c:if test="${value.ShowLinkToDetail.isSet and value.ShowLinkToDetail=='nodefined'}">
              <%-- En este caso tenemos que leer la propiedad del recurso para ver si se muestra o no --%>
              <c:set var="propertyShowDetail">${cms.vfs.property[resource.filename]['sagasuite.showdetail']}</c:set>
              <c:if test="${propertyShowDetail!=null and propertyShowDetail == 'false'}">
                <c:set var="showLinkToDetail" value="false"/>
              </c:if>
            </c:if>

            <c:set var="published" value="true"/>
            <c:if test="${resource.file.state.state==1 or resource.file.state.state==2 }">
              <c:set var="published" value="false"/>
            </c:if>

            <c:if test="${value.DescriptionMaxCharacter.isSet }">
              <c:set var="descriptionMaxCharacter">${value.DescriptionMaxCharacter }</c:set>
              <%
                String description = "" + pageContext.getAttribute("description");
                Integer descriptionMaxCharacter = Integer.parseInt(pageContext.getAttribute("descriptionMaxCharacter") + "");
                if (description.length() > descriptionMaxCharacter)
                  description = description.substring(0, descriptionMaxCharacter) + "...";
                pageContext.setAttribute("description", description);
              %>
            </c:if>
            <c:if test="${value.TitleMaxCharacter.isSet }">
              <c:set var="titleMaxCharacter">${value.TitleMaxCharacter }</c:set>
              <%
                String title = "" + pageContext.getAttribute("title");
                Integer titleMaxCharacter = Integer.parseInt(pageContext.getAttribute("titleMaxCharacter") + "");
                if (title.length() > titleMaxCharacter)
                  title = title.substring(0, titleMaxCharacter) + "...";
                pageContext.setAttribute("title", title);
              %>
            </c:if>
			<%-- Comprobamos si el title y el titlelarge son diferentes, y si es asi cargamos el atributo title del enlace --%>
				<c:set var="titleEqualsToTitleLarge">true</c:set>
				<c:if test="${not empty titlelarge}">
					<c:set var="trimTitleLarge">${fn:trim(titlelarge)}</c:set>
				</c:if>
				<c:if test="${not empty title}">
					<c:set var="trimTitle">${fn:trim(title)}</c:set>
				</c:if>
				<c:if test="${(not empty trimTitle) and (not empty trimTitleLarge)}">
					<c:set var="titleEqualsToTitleLarge">${trimTitle eq trimTitleLarge}</c:set>
				</c:if>
				<c:if test="${titleEqualsToTitleLarge == 'false'}">
					<c:set var="titleAttr">${titlelarge}</c:set>
				</c:if>
				
            <%-- Para definir si las imagenes de los elementos de listadoi van a tener el boton de ampliar --%>
            <c:set var="imageshowbig">false</c:set>
            <c:if test="${value.ImageElementShowBig.isSet and value.ImageElementShowBig == 'true' }">
              <c:set var="imageshowbig">true</c:set>
            </c:if>
            <%-- Definimos la variable del ancho de imagen a 480 por defecto y si el campo se edita se carga el campo --%>

            <c:if test="${value.ImageElementWidth.isSet }">
              <c:set var="imageWidth">${value.ImageElementWidth }</c:set>
            </c:if>
            <c:if test="${!value.ImageElementWidth.isSet }">
              <c:set var="imageWidth">480</c:set>
            </c:if>
            <c:if test="${value.ImageElementHeight.isSet }">
              <c:set var="imageHeight">${value.ImageElementHeight }</c:set>
            </c:if>

            <c:if
                test="${value.ImageBlockWidth.isSet and imagePath !=null and imagePath !='' and fn:indexOf(imagePath, '???')==-1}">
              <c:set var="imageBlockWidth" value="${value.ImageBlockWidth }"/>
              <fmt:parseNumber var="imageBlockWidthnumber" integerOnly="true" type="number" value="${imageBlockWidth}"/>
              <c:if test="${value.ImageBlockWidth == '12' or imageBlockPosition == 'top'}">
                <c:set var="textBlockWidth" value="12"/>
              </c:if>
              <c:if test="${value.ImageBlockWidth != '12' and imageBlockPosition != 'top'}">
                <c:set var="textBlockWidth" value="${12-imageBlockWidthnumber}"/>
              </c:if>

            </c:if>
            <c:if
                test="${!value.ImageBlockWidth.isSet and imagePath !=null and imagePath !='' and fn:indexOf(imagePath, '???')==-1}">
              <c:if test="${imageBlockPosition == 'top' }">
                <c:set var="imageBlockWidth" value="12"/>
                <c:set var="textBlockWidth" value="12"/>
              </c:if>
              <c:if test="${imageBlockPosition != 'top' }">
                <c:set var="imageBlockWidth" value="3"/>
                <c:set var="textBlockWidth" value="9"/>
              </c:if>
            </c:if>

            <c:if test="${imagePath == null or imagePath =='' or fn:indexOf(imagePath, '???')!=-1}">
              <c:set var="textBlockWidth">12</c:set>
            </c:if>
          <c:set var="widthtesela">grid-item-width-1</c:set>
          <c:set var="heighttesela">grid-item-height-1</c:set>
          <c:if test="${resource.value.ListWidth.isSet}">
            <c:set var="widthtesela">grid-item-width-${resource.value.ListWidth}</c:set>
          </c:if>
          <c:if test="${resource.value.ListHeight.isSet}">
            <c:set var="heighttesela">grid-item-height-${resource.value.ListHeight}</c:set>
          </c:if>
          <%-- comprobamos el tipo de recurso para gestionar el renderizado de forma distinta si es un recurso dphenlace --%>
          <c:set var="htmltag">article</c:set>
          <c:set var="borderclass"></c:set>
          <c:set var="resourcetype"></c:set>
          <c:set var="linktarget">_self</c:set>
          <c:set var="showtext">true</c:set>
          <c:set var="titleAttr"><fmt:message key='key.list.element.go.to' />${titlelarge}</c:set>
          <c:set var="cmsResource" value="${cms:getCmsObject(pageContext).readResource(linkDetail)}"/>
          <%
            CmsResource cmsResource = (CmsResource)pageContext.getAttribute("cmsResource");
            String resourcetype = OpenCms.getResourceManager().getResourceType(cmsResource).getTypeName();
            pageContext.setAttribute("resourcetype", resourcetype);
          %>
          <c:if test="${resourcetype == 'dphenlace'}">
            <c:set var="htmltag">div</c:set>
            <c:set var="borderclass">element-bordered</c:set>
            <c:set var="showtext">false</c:set>
            <c:if test="${resource.value.ListTitle.isSet}">
              <c:set var="showtext">true</c:set>
              <c:set var="title">${resource.value.ListTitle}</c:set>
            </c:if>
            <c:if test="${resource.value.LinkConfig.exists and resource.value.LinkConfig.value.Href.isSet}">
              <c:set var="linkDetail">${resource.value.LinkConfig.value.Href}</c:set>
            </c:if>
            <c:if test="${resource.value.LinkConfig.exists and resource.value.LinkConfig.value.Target.isSet}">
              <c:set var="linktarget">${resource.value.LinkConfig.value.Target}</c:set>
            </c:if>
            <c:if test="${resource.value.LinkConfig.exists and resource.value.LinkConfig.value.Title.isSet}">
              <%--<c:set var="titleAttr">${resource.value.LinkConfig.value.Title}</c:set>--%>
              <c:set var="titleAttr">${fn:replace(resource.value.LinkConfig.value.Title,'\"','\'')}</c:set>
            </c:if>

          </c:if>

            <li class="element sg-microcontent sg-microcontent-panel sg-microcontent-overlay box ${borderclass} box-featured-new default-box mb-30 ${heighttesela} grid-masonry-item ${widthtesela}">
              <${htmltag} style="background-image:url('<cms:link>${imagePath}</cms:link>');background-size:cover;background-position: center center;background-repeat: no-repeat;">
                  <div class="element complete-mode with-media">
                    <a class="overlay-link" target="${linktarget}" href="<cms:link>${linkDetail }</cms:link>" title="${titleAttr}">
                      <span class="hover-overlay" aria-hidden="true"></span>
                      <span class="sr-only">${titlelarge}</span>
                    </a>
                    <c:if test="${showtext == 'true'}">
                      <div class="text-overlay">
                        <div class="wrapper">
                          <header>
                            <${titletagelement} class="title-element ${titletagelementsize}">
                              ${title}
                          </${titletagelement}>
                          <c:if test="${resourcetype != 'dphenlace' and showdate != 'false'}">
                            <div class="info-date">
                              <cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-time-new.jsp:d9451486-147d-11e7-8644-7fb253176922)">
                                <cms:param name="date">${date }</cms:param>
                                <cms:param name="formatdate">${formatdate }</cms:param>
                              </cms:include>
                            </div>
                          </c:if>
                          </header>
                        </div>
                      </div>
                    </c:if>
                  </div>
              </${htmltag}>
            </li>
        </c:if>
        <%--<c:if test="${not empty elementsbyrow}">
          <c:if test="${count>0 and resourceForPages!=2 and count%2==0 and elementsbyrow==2}">
            <li class="visible-xs col-xs-12 clearfix">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=3 and count%3==0 and elementsbyrow==3}">
            <li class="visible-xs col-xs-12 clearfix">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=4 and count%4==0 and elementsbyrow==4}">
            <li class="visible-xs col-xs-12 clearfix">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=6 and count%6==0 and elementsbyrow==6}">
            <li class="visible-xs col-xs-12 clearfix">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=12 and count%12==0 and elementsbyrow==12}">
            <li class="visible-xs col-xs-12 clearfix">
              <hr/>
            </li>
          </c:if>
        </c:if>
        <c:if test="${not empty elementsbyrowsm}">
          <c:if test="${count>0 and resourceForPages!=2 and count%2==0 and elementsbyrowsm==2}">
            <li class="visible-sm col-sm-12 clearfix">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=3 and count%3==0 and elementsbyrowsm==3}">
            <li class="visible-sm col-sm-12 clearfix">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=4 and count%4==0 and elementsbyrowsm==4}">
            <li class="visible-sm col-sm-12 clearfix">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=6 and count%6==0 and elementsbyrowsm==6}">
            <li class="visible-sm col-sm-12 clearfix">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=12 and count%12==0 and elementsbyrowsm==12}">
            <li class="visible-sm col-sm-12 clearfix">
              <hr/>
            </li>
          </c:if>
        </c:if>
        <c:if test="${not empty elementsbyrowmd}">
          <c:if test="${count>0 and resourceForPages!=2 and count%2==0 and elementsbyrowmd==2}">
            <li class="visible-md col-md-12 clearfix">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=3 and count%3==0 and elementsbyrowmd==3}">
            <li class="visible-md col-md-12 clearfix">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=4 and count%4==0 and elementsbyrowmd==4}">
            <li class="visible-md col-md-12 clearfix">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=6 and count%6==0 and elementsbyrowmd==6}">
            <li class="visible-md col-md-12 clearfix">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=12 and count%12==0 and elementsbyrowmd==12}">
            <li class="visible-md col-md-12 clearfix">
              <hr/>
            </li>
          </c:if>
        </c:if>
        <c:if test="${not empty elementsbyrowlg}">
          <c:if test="${count>0 and resourceForPages!=2 and count%2==0 and elementsbyrowlg==2}">
            <li class="visible-lg col-lg-12 clearfix">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=3 and count%3==0 and elementsbyrowlg==3}">
            <li class="visible-lg col-lg-12 clearfix">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=4 and count%4==0 and elementsbyrowlg==4}">
            <li class="visible-lg col-lg-12 clearfix">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=6 and count%6==0 and elementsbyrowlg==6}">
            <li class="visible-lg col-lg-12 clearfix">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=12 and count%12==0 and elementsbyrowlg==12}">
            <li class="visible-lg col-lg-12 clearfix">
              <hr/>
            </li>
          </c:if>
        </c:if>--%>

      </cms:contentload>
    </c:if>
    <c:if test="${info.resultSize == 0}">
      <%-- Si queremos borrar la caja cuando estemos en online --%>
      <c:if
          test="${value.HideBoxIfEmpty.isSet and value.HideBoxIfEmpty=='true' and cms.requestContext.currentProject.onlineProject}">
        <script>
          $(document).ready(function () {
            $("#solrlist-${value.Id}").remove();
          });
        </script>
      </c:if>
      <div class="alert alert-warning">
          ${value.LabelNoResult }
      </div>
    </c:if>
  </cms:contentload>
  <%-- comprobamos si existen más resultados de los definidos a mostrar en el listado y si es asi cargamos el boton de ver todos --%>
  <fmt:parseNumber value="${resultSize}" parseLocale="${cms.locale}" integerOnly="true" var="resultSize"/>
  <fmt:parseNumber value="${value.PageSize}" parseLocale="${cms.locale}" integerOnly="true" var="pageSize"/>
  <c:if test="${resultSize > 0 and resultSize > pageSize }">
    <%-- Si se lee la property de category definimos el botón que llama al buscador con la categoria --%>
    <c:if test="${content.value.readPropertyCategory.exists && content.value.readPropertyCategory=='true'}">
      <c:set var="pageurl">${themeConfiguration.CustomFieldKeyValue.UrlArticlesPage}</c:set>
      <c:if test="${empty pageurl}">
        <c:set var="pageurl">/articulos/</c:set>
      </c:if>
      <c:set var="linkquery"></c:set>
      <%--====== Comprobamos si existe la propiedad Title especifica para el locale actual (Title_es, Title_en...) para la categoria.
      Si es asi usamos esa propiedad. Si no existe, usamos la propiedad Title =============================--%>
      <c:set var="categoryProperty"><cms:property name="category" file="search"/></c:set>
      <c:if test="${not empty categoryProperty}">
        <c:set var="categoryshort" value="${fn:replace(categoryProperty, '/.categories/','')}" />
        <c:set var="categoryPropertyTitle">
          <cms:property name="Title_${locale}" file="${categoryProperty}"/>
        </c:set>
        <c:if test="${empty categoryPropertyTitle}">
          <c:set var="categoryPropertyTitle"><cms:property name="Title" file="${categoryProperty}"/></c:set>
        </c:if>
        <c:set var="linkquery">?buscadorthemesfield-1=&buscadorthemesfield-2_d1=&buscadorthemesfield-2_d2=&buscadorthemesfield-3=${categoryshort}&numfield=3&searchaction=search&searchPage=1&submit=Buscar</c:set>
        <div class="text-center margin-top-30">
          <a class="btn btn-specific-main pt-10 pb-10 pr-15 pl-15" href="<cms:link>${pageurl}${linkquery}</cms:link>">
            <span class="fa fa-plus mr-10"></span><fmt:message key="key.btn.view.all.articles.of" />${categoryPropertyTitle}
          </a>
        </div>
      </c:if>
      <c:if test="${empty categoryProperty}">
        <div class="text-center margin-top-30">
          <a class="btn btn-specific-main pt-10 pb-10 pr-15 pl-15" href="<cms:link>${pageurl}${linkquery}</cms:link>">
            <span class="fa fa-plus mr-10"></span><fmt:message key="key.btn.view.all.articles" />
          </a>
        </div>
      </c:if>
    </c:if>
  </c:if>
</ul>
</cms:bundle>