<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">

<c:set var="id" value="${value.Id }"/>
<%
  String id = "" + pageContext.getAttribute("id");
  String p = (String) request.getParameter("p" + id);
  if (p == null)
    p = "1";
  request.setAttribute("currentPage", p);
%>

<%-- recogemos los parametros de visualizacion en columnas y otros settings --%>

<c:set var="columns">${param.columns}</c:set>
<c:set var="width">${param.width}</c:set>
<c:set var="columnsnumberxxs">${param.columnsnumberxxs}</c:set>
<c:set var="columnsnumberxs">${param.columnsnumberxs}</c:set>
<c:set var="columnsnumbersm">${param.columnsnumbersm}</c:set>
<c:set var="columnsnumbermd">${param.columnsnumbermd}</c:set>
<c:set var="columnsnumberlg">${param.columnsnumberlg}</c:set>
<c:set var="titletagelement">${param.titletagelement}</c:set>
<c:set var="titletagelementsize">${param.titletagelementsize}</c:set>
<c:set var="imageBlockPosition">${param.imageBlockPosition}</c:set>
<c:set var="imageBlockWidth">${param.imageBlockWidth}</c:set>
<c:set var="showdate">${param.showdate}</c:set>
<c:set var="formatdate">${param.formatdate}</c:set>


<%-- Es necesario anadir el rows al limite configurado para que la paginacion la haga el contentload y no solr --%>
<c:set var="solrquery">${solrquery }&rows=${value.ResultsSize }</c:set>
<c:if test="${value.CreatePath.isSet and value.Pattern.isSet }">
  <c:set var="createPath">|createPath=${value.CreatePath }${value.Pattern }</c:set>
</c:if>

<%-- Si se lee la property de category avisamos en offline--%>
<c:if test="${content.value.readPropertyCategory.exists && content.value.readPropertyCategory=='true'}">
  <c:set var="categoryProperty"><cms:property name="category" file="search"/></c:set>
  <c:if test="${not empty categoryProperty}">
    <c:if test="${!cms.isOnlineProject}">
      <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <h5 class="no-margin"><strong>Mensaje de edición</strong></h5>
        Este listado lee de la propiedad de "categoría para listado" siendo su valor:
        <strong> ${categoryProperty}</strong>
      </div>
    </c:if>
  </c:if>
</c:if>

<ul class="list-unstyled main-ul ${columns}">

  <cms:contentload collector="byQuery" param="${solrquery }${createPath }" preload="true">
    <cms:contentinfo var="info"/>

    <c:if test="${info.resultSize > 0}">
      <cms:contentload pageSize="${value.PageSize}" pageIndex="${currentPage}" pageNavLength="${value.NavigationSize}"
                       editable="${value.Editable }">
        <cms:contentinfo var="infocontent"/>
        <cms:contentaccess var="resource" scope="request"/>

        <c:set var="resultSize" scope="request" value="${info.resultSize}"/>
        <c:set var="numPages" scope="request" value="${infocontent.pageCount}"/>
        <c:set var="pageSize" value="${infocontent.pageSize}"/>
        <c:set var="resourceForPages" scope="request" value="${infocontent.pageSize}"/>
        <c:set var="firstPage" scope="request" value="${infocontent.pageNavStartIndex}"/>
        <c:set var="lastPage" scope="request" value="${infocontent.pageNavEndIndex}"/>
        <c:set var="count" scope="request">${infocontent.resultIndex}</c:set>
        <c:set var="empezarEn">${value.StartIn}</c:set>
        <c:if test="${!value.StartIn.isSet or count>empezarEn}">

          <c:if test="${value.JspResultTemplate.isSet }">
            <c:set var="linkDetail" scope="request"><cms:contentshow element="%(opencms.filename)"/></c:set>
            <c:set var="title"></c:set>
            <c:set var="titlelarge"></c:set>
            <c:forEach var="titleprev" items="${content.valueList.MappingTitle }">
              <c:if test="${empty title}">
                <cms:contentcheck ifexists="${titleprev}" >
                  <c:set var="titleValue"><cms:contentshow element="${titleprev}"/></c:set>
                  <c:if test="${titleValue !=null and titleValue !=''}">
                    <c:set var="title" scope="request">${titleValue}</c:set>
                    <c:set var="titlelarge" scope="request">${titleValue}</c:set>
                  </c:if>
                </cms:contentcheck>
              </c:if>
            </c:forEach>
            <c:set var="description" scope="request"><cms:contentshow element="${value.MappingDescription }"/></c:set>
            <c:set var="date" scope="request"><cms:contentshow element="${value.MappingDate }"/></c:set>
            <c:set var="imagePath" scope="request"></c:set>
            <c:forEach var="image" items="${content.valueList.MappingImage }">
              <c:if test="${empty imagePath}">
                <cms:contentcheck ifexists="${image}" >
                  <c:set var="imagePathValue"><cms:contentshow element="${image}"/></c:set>
                  <c:if test="${imagePathValue !=null and imagePathValue !='' and fn:indexOf(imagePathValue, '???')==-1}">
                    <c:set var="imagePath" scope="request">${imagePathValue}</c:set>
                  </c:if>
                </cms:contentcheck>
              </c:if>
            </c:forEach>
            <%-- Establecemos la imagen por defecto definida en el .themeconfig por si no viene imagen en el recurso --%>
            <c:if test="${empty imagePath and not empty themeConfiguration.CustomFieldKeyValue.defaultimg}">
              <c:set var="imagePath" scope="request">${themeConfiguration.CustomFieldKeyValue.defaultimg}</c:set>
              <c:if test="${count > 1 and count %2 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg2}">
                <c:set var="imagePath" scope="request">${themeConfiguration.CustomFieldKeyValue.defaultimg2}</c:set>
              </c:if>
              <c:if test="${count > 1 and count %3 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg3}">
                <c:set var="imagePath" scope="request">${themeConfiguration.CustomFieldKeyValue.defaultimg3}</c:set>
              </c:if>
              <c:if test="${count > 1 and count %4 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg4}">
                <c:set var="imagePath" scope="request">${themeConfiguration.CustomFieldKeyValue.defaultimg4}</c:set>
              </c:if>
              <c:if test="${count > 1 and count %5 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg5}">
                <c:set var="imagePath" scope="request">${themeConfiguration.CustomFieldKeyValue.defaultimg5}</c:set>
              </c:if>
            </c:if>
            <%-- Para definir si las imagenes de los elementos de listadoi van a tener el boton de ampliar --%>
            <c:set var="imageshowbig" scope="request">false</c:set>
            <c:if test="${value.ImageElementShowBig.isSet and value.ImageElementShowBig == 'true' }">
              <c:set var="imageshowbig" scope="request">true</c:set>
            </c:if>
            <c:set var="published" scope="request" value="true"/>
            <c:if test="${resource.file.state.state==1 or resource.file.state.state==2 }">
              <c:set var="published" scope="request" value="false"/>
            </c:if>

            <c:if test="${value.DescriptionMaxCharacter.isSet }">
              <c:set var="descriptionMaxCharacter" scope="request">${value.DescriptionMaxCharacter }</c:set>
              <%
                String description = "" + request.getAttribute("description");
                Integer descriptionMaxCharacter = Integer.parseInt(request.getAttribute("descriptionMaxCharacter") + "");
                if (description.length() > descriptionMaxCharacter)
                  description = description.substring(0, descriptionMaxCharacter) + "...";
                request.setAttribute("description", description);
              %>
            </c:if>
            <c:if test="${value.TitleMaxCharacter.isSet }">
              <c:set var="titleMaxCharacter" scope="request">${value.TitleMaxCharacter }</c:set>
              <%
                String title = "" + request.getAttribute("title");
                Integer titleMaxCharacter = Integer.parseInt(request.getAttribute("titleMaxCharacter") + "");
                if (title.length() > titleMaxCharacter)
                  title = title.substring(0, titleMaxCharacter) + "...";
                request.setAttribute("title", title);
              %>
            </c:if>

            <%-- Comprobamos si el title y el titlelarge son diferentes, y si es asi cargamos el atributo title del enlace --%>
            <c:set var="titleEqualsToTitleLarge">true</c:set>
            <c:if test="${not empty titlelarge}">
              <c:set var="trimTitleLarge">${fn:trim(titlelarge)}</c:set>
            </c:if>
            <c:if test="${not empty title}">
              <c:set var="trimTitle">${fn:trim(title)}</c:set>
            </c:if>
            <c:if test="${(not empty trimTitle) and (not empty trimTitleLarge)}">
              <c:set var="titleEqualsToTitleLarge">${trimTitle eq trimTitleLarge}</c:set>
            </c:if>
            <c:if test="${titleEqualsToTitleLarge == 'false'}">
              <c:set var="titleAttr" scope="request">${titlelarge}</c:set>
            </c:if>

            <%-- Definimos la variable del ancho de imagen a 175 por defecto y si el campo se edita se carga el campo --%>


            <c:if test="${value.ImageElementWidth.isSet }">
              <c:set var="imageWidth" scope="request">${value.ImageElementWidth }</c:set>
            </c:if>
            <c:if test="${!value.ImageElementWidth.isSet }">
              <c:set var="imageWidth" scope="request">175</c:set>
            </c:if>
            <c:if test="${value.ImageElementHeight.isSet }">
              <c:set var="imageHeight" scope="request">${value.ImageElementHeight }</c:set>
            </c:if>
            <c:set var="showLinkToDetail" value="true" scope="request"/> <%-- Por defecto siempre mostramos link --%>
            <c:if test="${value.ShowLinkToDetail.isSet and value.ShowLinkToDetail=='false'}">
              <c:set var="showLinkToDetail" value="false" scope="request"/>
            </c:if>
            <c:if test="${value.ShowLinkToDetail.isSet and value.ShowLinkToDetail=='nodefined'}">
              <%-- En este caso tenemos que leer la propiedad del recurso para ver si se muestra o no --%>
              <c:set var="propertyShowDetail">${cms.vfs.property[resource.filename]['sagasuite.showdetail']}</c:set>
              <c:if test="${propertyShowDetail!=null and propertyShowDetail == 'false'}">
                <c:set var="showLinkToDetail" value="false" scope="request"/>
              </c:if>
            </c:if>

            <c:set var="published" scope="request" value="true"/>
            <c:if test="${resource.file.state.state==1 or resource.file.state.state==2 }">
              <c:set var="published" scope="request" value="false"/>
            </c:if>
            <c:set var="width" scope="request">${width}</c:set>
            <c:set var="titletagelement" scope="request">${titletagelement}</c:set>
            <c:set var="titletagelementsize" scope="request">${titletagelementsize}</c:set>
            <c:set var="imageBlockPosition" scope="request">${imageBlockPosition}</c:set>
            <c:set var="imageBlockWidth" scope="request">${imageBlockWidth}</c:set>
            <c:set var="showdate" scope="request">${showdate}</c:set>
            <c:set var="formatdate" scope="request">${formatdate}</c:set>
            <cms:include file="${value.JspResultTemplate}"/>

          </c:if>

          <c:if test="${!value.JspResultTemplate.isSet }">
            <c:set var="linkDetail"><cms:contentshow element="%(opencms.filename)"/></c:set>
            <c:set var="title"></c:set>
            <c:set var="titlelarge"></c:set>
            <c:forEach var="titleprev" items="${content.valueList.MappingTitle }">
              <c:if test="${empty title}">
                <cms:contentcheck ifexists="${titleprev}" >
                  <c:set var="titleValue"><cms:contentshow element="${titleprev}"/></c:set>
                  <c:if test="${titleValue !=null and titleValue !=''}">
                    <c:set var="title">${titleValue}</c:set>
                    <c:set var="titlelarge">${titleValue}</c:set>
                  </c:if>
                </cms:contentcheck>
              </c:if>
            </c:forEach>
            <c:set var="description"><cms:contentshow element="${value.MappingDescription }"/></c:set>
            <c:set var="date"><cms:contentshow element="${value.MappingDate }"/></c:set>
            <c:set var="imagePath"></c:set>
            <c:forEach var="image" items="${content.valueList.MappingImage }">
              <c:if test="${empty imagePath}">
                <cms:contentcheck ifexists="${image}" >
                  <c:set var="imagePathValue"><cms:contentshow element="${image}"/></c:set>
                  <c:if test="${imagePathValue !=null and imagePathValue !='' and fn:indexOf(imagePathValue, '???')==-1}">
                    <c:set var="imagePath">${imagePathValue}</c:set>
                  </c:if>
                </cms:contentcheck>
              </c:if>
            </c:forEach>
            <%-- Establecemos la imagen por defecto definida en el .themeconfig por si no viene imagen en el recurso --%>
            <c:if test="${empty imagePath and not empty themeConfiguration.CustomFieldKeyValue.defaultimg}">
              <c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg}</c:set>
              <c:if test="${count > 1 and count %2 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg2}">
                <c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg2}</c:set>
              </c:if>
              <c:if test="${count > 1 and count %3 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg3}">
                <c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg3}</c:set>
              </c:if>
              <c:if test="${count > 1 and count %4 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg4}">
                <c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg4}</c:set>
              </c:if>
              <c:if test="${count > 1 and count %5 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg5}">
                <c:set var="imagePath">${themeConfiguration.CustomFieldKeyValue.defaultimg5}</c:set>
              </c:if>
            </c:if>
            <c:set var="showLinkToDetail" value="true"/> <%-- Por defecto siempre mostramos link --%>
            <c:if test="${value.ShowLinkToDetail.isSet and value.ShowLinkToDetail=='false'}">
              <c:set var="showLinkToDetail" value="false"/>
            </c:if>
            <c:if test="${value.ShowLinkToDetail.isSet and value.ShowLinkToDetail=='nodefined'}">
              <%-- En este caso tenemos que leer la propiedad del recurso para ver si se muestra o no --%>
              <c:set var="propertyShowDetail">${cms.vfs.property[resource.filename]['sagasuite.showdetail']}</c:set>
              <c:if test="${propertyShowDetail!=null and propertyShowDetail == 'false'}">
                <c:set var="showLinkToDetail" value="false"/>
              </c:if>
            </c:if>

            <c:set var="published" value="true"/>
            <c:if test="${resource.file.state.state==1 or resource.file.state.state==2 }">
              <c:set var="published" value="false"/>
            </c:if>

            <c:if test="${value.DescriptionMaxCharacter.isSet }">
              <c:set var="descriptionMaxCharacter">${value.DescriptionMaxCharacter }</c:set>
              <%
                String description = "" + pageContext.getAttribute("description");
                Integer descriptionMaxCharacter = Integer.parseInt(pageContext.getAttribute("descriptionMaxCharacter") + "");
                if (description.length() > descriptionMaxCharacter)
                  description = description.substring(0, descriptionMaxCharacter) + "...";
                pageContext.setAttribute("description", description);
              %>
            </c:if>
            <c:if test="${value.TitleMaxCharacter.isSet }">
              <c:set var="titleMaxCharacter">${value.TitleMaxCharacter }</c:set>
              <%
                String title = "" + pageContext.getAttribute("title");
                Integer titleMaxCharacter = Integer.parseInt(pageContext.getAttribute("titleMaxCharacter") + "");
                if (title.length() > titleMaxCharacter)
                  title = title.substring(0, titleMaxCharacter) + "...";
                pageContext.setAttribute("title", title);
              %>
            </c:if>
            <%-- Comprobamos si el title y el titlelarge son diferentes, y si es asi cargamos el atributo title del enlace --%>
            <c:set var="titleEqualsToTitleLarge">true</c:set>
            <c:if test="${not empty titlelarge}">
              <c:set var="trimTitleLarge">${fn:trim(titlelarge)}</c:set>
            </c:if>
            <c:if test="${not empty title}">
              <c:set var="trimTitle">${fn:trim(title)}</c:set>
            </c:if>
            <c:if test="${(not empty trimTitle) and (not empty trimTitleLarge)}">
              <c:set var="titleEqualsToTitleLarge">${trimTitle eq trimTitleLarge}</c:set>
            </c:if>
            <c:if test="${titleEqualsToTitleLarge == 'false'}">
              <c:set var="titleAttr">${titlelarge}</c:set>
            </c:if>

            <%-- Para definir si las imagenes de los elementos de listadoi van a tener el boton de ampliar --%>
            <c:set var="imageshowbig">false</c:set>
            <c:if test="${value.ImageElementShowBig.isSet and value.ImageElementShowBig == 'true' }">
              <c:set var="imageshowbig">true</c:set>
            </c:if>
            <%-- Definimos la variable del ancho de imagen a 480 por defecto y si el campo se edita se carga el campo --%>

            <c:if test="${value.ImageElementWidth.isSet }">
              <c:set var="imageWidth">${value.ImageElementWidth }</c:set>
            </c:if>
            <c:if test="${!value.ImageElementWidth.isSet }">
              <c:set var="imageWidth">480</c:set>
            </c:if>
            <c:if test="${value.ImageElementHeight.isSet }">
              <c:set var="imageHeight">${value.ImageElementHeight }</c:set>
            </c:if>

            <c:if test="${empty imageBlockWidth}">
              <c:if test="${imageBlockPosition == 'top' }">
                <c:set var="imageBlockWidth" value="12"/>
              </c:if>
              <c:if test="${imageBlockPosition != 'top' }">
                <c:set var="imageBlockWidth" value="3"/>
              </c:if>
            </c:if>

            <c:if test="${value.ViewType == 'tit' }">
              <li class="simple only-title ${width} <c:if test='${count%2==0}'> par</c:if>">
                <article>
                  <div class="<c:if test='${empty titletagelementsize}'>h4 </c:if>title-element ${titletagelementsize}">
                    <c:if test="${showLinkToDetail}">
                      <a href="<cms:link>${linkDetail }</cms:link>" <c:if test="${not empty titleAttr}"> title='${titleAttr}'</c:if>">
                      <c:if test="${!cms.isOnlineProject and !published}">
                        <abbr title="Recurso poendiente de publicaci&oacute;n"><span class="fa fa-flag small"
                                                                                     aria-hidden="true"></span></abbr>
                      </c:if>
                      <c:out value="${title}"/>
                      </a>
                    </c:if>
                    <c:if test="${!showLinkToDetail}">
                      <c:if test="${!cms.isOnlineProject and !published}">
                        <abbr title="Recurso poendiente de publicaci&oacute;n"><span class="fa fa-flag small"
                                                                                     aria-hidden="true"></span></abbr>
                      </c:if>
                      <c:out value="${title}"/>
                    </c:if>
                  </div>
                </article>
              </li>
            </c:if>
            <c:if test="${value.ViewType == 'tit-dat' }">
              <li class="simple title-date ${width} <c:if test='${count%2==0}'> par</c:if>">
                <article>
                  <c:if test="${empty showdate or showdate == 'true'}">
										<span class="date-element">
											<span class="fa fa-calendar" aria-hidden="true"></span>
											<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-time-new.jsp:d9451486-147d-11e7-8644-7fb253176922)">
                                              <cms:param name="date">${date }</cms:param>
                                              <cms:param name="formatdate">${formatdate }</cms:param>
                                            </cms:include>
										</span>
                  </c:if>
                  <div class="<c:if test='${empty titletagelementsize}'>h4 </c:if>title-element ${titletagelementsize}">
                    <c:if test="${showLinkToDetail}">
                      <a href="<cms:link>${linkDetail }</cms:link>" <c:if test="${not empty titleAttr}"> title='${titleAttr}'</c:if>>
                        <c:if test="${!cms.isOnlineProject and !published}">
                          <abbr title="Recurso poendiente de publicaci&oacute;n"><span class="fa fa-flag small"
                                                                                       aria-hidden="true"></span></abbr>
                        </c:if>
                        <c:out value="${title}"/>
                      </a>
                    </c:if>
                    <c:if test="${!showLinkToDetail}">
                      <c:if test="${!cms.isOnlineProject and !published}">
                        <abbr title="Recurso poendiente de publicaci&oacute;n"><span class="fa fa-flag small"
                                                                                     aria-hidden="true"></span></abbr>
                      </c:if>
                      <c:out value="${title}"/>
                    </c:if>
                  </div>
                </article>
              </li>
            </c:if>
            <c:if test="${value.ViewType == 'tit-des' }">
              <li class="simple box default-box title-desc ${width} <c:if test='${count%2==0}'> par</c:if>">
                <article>
                  <header>
                    <${titletagelement} class="title ${titletagelementsize}">
                    <c:if test="${showLinkToDetail}">
                      <a href="<cms:link>${linkDetail }</cms:link>" <c:if test="${not empty titleAttr}"> title='${titleAttr}'</c:if>>
                        <c:if test="${!cms.isOnlineProject and !published}">
                          <abbr title="Recurso poendiente de publicaci&oacute;n"><span class="fa fa-flag small"
                                                                                       aria-hidden="true"></span></abbr>
                        </c:if>
                        <c:out value="${title}"/>
                      </a>
                    </c:if>
                    <c:if test="${!showLinkToDetail}">
                      <c:if test="${!cms.isOnlineProject and !published}">
                        <abbr title="Recurso poendiente de publicaci&oacute;n"><span class="fa fa-flag small"
                                                                                     aria-hidden="true"></span></abbr>
                      </c:if>
                      <c:out value="${title}"/>
                    </c:if>
                  </${titletagelement}>
                  </header>
                  <c:if test="${not empty description and description!=null and fn:indexOf(description, '???')==-1}">
                    <div class="description">
                      <p>${description}</p>
                    </div>
                  </c:if>
                  <c:if test="${value.ConfigButton.exists and showLinkToDetail}">
                    <div class="actions">
                      <cms:include file="%(link.weak:/system/modules/com.saga.sagasuite.solrlist/elements/c-boton.jsp:2ea80592-a44c-11e3-8e81-f18cf451b707)">
                        <cms:param name="tipoboton">${value.ConfigButton.value.CssClass}</cms:param>
                        <cms:param name="iconboton">${value.ConfigButton.value.Icon}</cms:param>
                        <cms:param name="sizeboton">${value.ConfigButton.value.Size}</cms:param>
                        <cms:param name="positionboton">${value.ConfigButton.value.Position}</cms:param>
                        <cms:param name="textoboton">${value.ConfigButton.value.TextLink}</cms:param>
                        <cms:param name="linkboton">${linkDetail}</cms:param>
                        <cms:param name="targetboton">_self</cms:param>
                        <cms:param name="titleboton">${titlelarge}</cms:param>
                        <cms:param name="followboton">true</cms:param>
                      </cms:include>
                    </div>
                  </c:if>
                </article>
              </li>
            </c:if>
            <c:if test="${value.ViewType == 'tit-dat-des' }">
              <li class="simple box default-box title-date-desc ${width} <c:if test='${count%2==0}'> par</c:if>">
                <article>
                  <c:if test="${empty showdate or showdate == 'true'}">
										<span class="date-element">
											<span class="fa fa-calendar" aria-hidden="true"></span>
											<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-time-new.jsp:d9451486-147d-11e7-8644-7fb253176922)">
                                              <cms:param name="date">${date }</cms:param>
                                              <cms:param name="formatdate">${formatdate }</cms:param>
                                            </cms:include>
										</span>
                  </c:if>
                  <header>
                    <${titletagelement} class="title ${titletagelementsize}">
                    <c:if test="${showLinkToDetail}">
                      <a href="<cms:link>${linkDetail }</cms:link>" <c:if test="${not empty titleAttr}"> title='${titleAttr}'</c:if>>
                        <c:if test="${!cms.isOnlineProject and !published}">
                          <abbr title="Recurso poendiente de publicaci&oacute;n"><span class="fa fa-flag small"
                                                                                       aria-hidden="true"></span></abbr>
                        </c:if>
                        <c:out value="${title}"/>
                      </a>
                    </c:if>
                    <c:if test="${!showLinkToDetail}">
                      <c:if test="${!cms.isOnlineProject and !published}">
                        <abbr title="Recurso poendiente de publicaci&oacute;n"><span class="fa fa-flag small"
                                                                                     aria-hidden="true"></span></abbr>
                      </c:if>
                      <c:out value="${title}"/>
                    </c:if>
                  </${titletagelement}>
                  </header>
                  <c:if test="${not empty description and description!=null and fn:indexOf(description, '???')==-1}">
                    <div class="description">
                      <p>${description}</p>
                    </div>
                  </c:if>
                  <c:if test="${value.ConfigButton.exists and showLinkToDetail}">
                    <div class="actions">
                      <cms:include file="%(link.weak:/system/modules/com.saga.sagasuite.solrlist/elements/c-boton.jsp:2ea80592-a44c-11e3-8e81-f18cf451b707)">
                        <cms:param name="tipoboton">${value.ConfigButton.value.CssClass}</cms:param>
                        <cms:param name="iconboton">${value.ConfigButton.value.Icon}</cms:param>
                        <cms:param name="sizeboton">${value.ConfigButton.value.Size}</cms:param>
                        <cms:param name="positionboton">${value.ConfigButton.value.Position}</cms:param>
                        <cms:param name="textoboton">${value.ConfigButton.value.TextLink}</cms:param>
                        <cms:param name="linkboton">${linkDetail}</cms:param>
                        <cms:param name="targetboton">_self</cms:param>
                        <cms:param name="titleboton">${titlelarge}</cms:param>
                        <cms:param name="followboton">true</cms:param>
                      </cms:include>
                    </div>
                  </c:if>
                </article>
              </li>
            </c:if>
            <c:if test="${value.ViewType == 'tit-img' }">
              <li class="has-media media-to-${imageBlockPosition} has-media-complete ${width} <c:if test='${count%2==0}'> par</c:if>">
                <article class="media">
                  <div class="row">
                    <c:if test="${imageBlockPosition == 'top' or imageBlockPosition == 'left'}">
                      <c:if test="${imagePath!=null and imagePath!='' and fn:indexOf(imagePath, '???')==-1}">
                        <c:if test="${imageBlockPosition == 'top'}">
                          <div class="clearfix">
                        </c:if>
                        <div
                                class="image media-object col-xs-${imageBlockWidth} col-xxs-12 <c:if test='${imageBlockPosition == "top"}'>center-block col-centered</c:if>">
                          <sg:image imagePath="${imagePath}" width="${imageWidth}"
                                    height="${value.ImageElementHeight.isSet?imageHeight:null}"
                                    gallery="gallery${value.Id }"
                                    showBig="${imageshowbig}"
                                    urllink="${showLinkToDetail?linkDetail:null }"
                                    titlelink="${showLinkToDetail?titlelarge:null }"/>
                        </div>
                        <c:if test="${imageBlockPosition == 'top'}">
                          </div>
                        </c:if>
                      </c:if>
                    </c:if>
                    <div class="media-body container-fluid">
                      <${titletagelement} class="title-element media-heading ${titletagelementsize}">
                      <c:if test="${showLinkToDetail}">
                        <a href="<cms:link>${linkDetail }</cms:link>" <c:if test="${not empty titleAttr}"> title='${titleAttr}'</c:if>>
                          <c:if test="${!cms.isOnlineProject and !published}">
                            <abbr title="Recurso poendiente de publicaci&oacute;n"><span class="fa fa-flag small"
                                                                                         aria-hidden="true"></span></abbr>
                          </c:if>
                          <c:out value="${title}"/>
                        </a>
                      </c:if>
                      <c:if test="${!showLinkToDetail}">
                        <c:if test="${!cms.isOnlineProject and !published}">
                          <abbr title="Recurso poendiente de publicaci&oacute;n"><span class="fa fa-flag small"
                                                                                       aria-hidden="true"></span></abbr>
                        </c:if>
                        <c:out value="${title}"/>
                      </c:if>
                    </${titletagelement}>
                    <c:if test="${value.ConfigButton.exists and showLinkToDetail}">
                      <div class="actions">
                        <cms:include
                                file="%(link.weak:/system/modules/com.saga.sagasuite.solrlist/elements/c-boton.jsp:2ea80592-a44c-11e3-8e81-f18cf451b707)">
                          <cms:param name="tipoboton">${value.ConfigButton.value.CssClass}</cms:param>
                          <cms:param name="iconboton">${value.ConfigButton.value.Icon}</cms:param>
                          <cms:param name="sizeboton">${value.ConfigButton.value.Size}</cms:param>
                          <cms:param name="positionboton">${value.ConfigButton.value.Position}</cms:param>
                          <cms:param name="textoboton">${value.ConfigButton.value.TextLink}</cms:param>
                          <cms:param name="linkboton">${linkDetail}</cms:param>
                          <cms:param name="targetboton">_self</cms:param>
                          <cms:param name="titleboton">${titlelarge}</cms:param>
                          <cms:param name="followboton">true</cms:param>
                        </cms:include>
                      </div>
                    </c:if>
                  </div>
                  <c:if test="${imageBlockPosition == 'right'}">
                    <c:if test="${imagePath!=null and imagePath!='' and fn:indexOf(imagePath, '???')==-1}">
                      <div class="image media-object col-xs-${imageBlockWidth} col-xxs-12">
                        <sg:image imagePath="${imagePath}" width="${imageWidth}"
                                  height="${value.ImageElementHeight.isSet?imageHeight:null}"
                                  showBig="${imageshowbig}"
                                  gallery="gallery${value.Id }"
                                  urllink="${showLinkToDetail?linkDetail:null }"
                                  titlelink="${showLinkToDetail?titlelarge:null }"/>
                      </div>
                    </c:if>
                  </c:if>
                  </div>
                </article>
              </li>
            </c:if>
            <c:if test="${value.ViewType == 'tit-dat-img' }">
              <li class="has-media media-to-${imageBlockPosition} has-media-complete ${width} <c:if test='${count%2==0}'> par</c:if>">
                <article class="media">
                  <div class="row">
                    <c:if test="${imageBlockPosition == 'top' or imageBlockPosition == 'left'}">
                      <c:if test="${imagePath!=null and imagePath!='' and fn:indexOf(imagePath, '???')==-1}">
                        <c:if test="${imageBlockPosition == 'top'}">
                          <div class="clearfix">
                        </c:if>
                        <div
                                class="image media-object col-xs-${imageBlockWidth} col-xxs-12 <c:if test='${imageBlockPosition == "top"}'>center-block col-centered</c:if>">
                          <sg:image imagePath="${imagePath}" width="${imageWidth}"
                                    height="${value.ImageElementHeight.isSet?imageHeight:null}"
                                    showBig="${imageshowbig}"
                                    gallery="gallery${value.Id }"
                                    urllink="${showLinkToDetail?linkDetail:null }"
                                    titlelink="${showLinkToDetail?titlelarge:null }"/>
                        </div>
                        <c:if test="${imageBlockPosition == 'top'}">
                          </div>
                        </c:if>
                      </c:if>
                    </c:if>
                    <div class="media-body container-fluid">
                      <c:if test="${empty showdate or showdate == 'true'}">
												<span class="date-element">
													<span class="fa fa-calendar" aria-hidden="true"></span>
													<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-time-new.jsp:d9451486-147d-11e7-8644-7fb253176922)">
                                                      <cms:param name="date">${date }</cms:param>
                                                      <cms:param name="formatdate">${formatdate }</cms:param>
                                                    </cms:include>
												</span>
                      </c:if>
                      <${titletagelement} class="title-element media-heading ${titletagelementsize}">
                      <c:if test="${showLinkToDetail}">
                        <a href="<cms:link>${linkDetail }</cms:link>" <c:if test="${not empty titleAttr}"> title='${titleAttr}'</c:if>>
                          <c:if test="${!cms.isOnlineProject and !published}">
                            <abbr title="Recurso poendiente de publicaci&oacute;n"><span class="fa fa-flag small"
                                                                                         aria-hidden="true"></span></abbr>
                          </c:if>
                          <c:out value="${title}"/>
                        </a>
                      </c:if>
                      <c:if test="${!showLinkToDetail}">
                        <c:if test="${!cms.isOnlineProject and !published}">
                          <abbr title="Recurso poendiente de publicaci&oacute;n"><span class="fa fa-flag small"
                                                                                       aria-hidden="true"></span></abbr>
                        </c:if>
                        <c:out value="${title}"/>
                      </c:if>
                    </${titletagelement}>
                    <c:if test="${value.ConfigButton.exists and showLinkToDetail}">
                      <div class="actions">
                        <cms:include
                                file="%(link.weak:/system/modules/com.saga.sagasuite.solrlist/elements/c-boton.jsp:2ea80592-a44c-11e3-8e81-f18cf451b707)">
                          <cms:param name="tipoboton">${value.ConfigButton.value.CssClass}</cms:param>
                          <cms:param name="iconboton">${value.ConfigButton.value.Icon}</cms:param>
                          <cms:param name="sizeboton">${value.ConfigButton.value.Size}</cms:param>
                          <cms:param name="positionboton">${value.ConfigButton.value.Position}</cms:param>
                          <cms:param name="textoboton">${value.ConfigButton.value.TextLink}</cms:param>
                          <cms:param name="linkboton">${linkDetail}</cms:param>
                          <cms:param name="targetboton">_self</cms:param>
                          <cms:param name="titleboton">${titlelarge}</cms:param>
                          <cms:param name="followboton">true</cms:param>
                        </cms:include>
                      </div>
                    </c:if>
                  </div>
                  <c:if test="${imageBlockPosition == 'right'}">
                    <c:if test="${imagePath!=null and imagePath!='' and fn:indexOf(imagePath, '???')==-1}">
                      <div class="image media-object col-xs-${imageBlockWidth} col-xxs-12">
                        <sg:image imagePath="${imagePath}" width="${imageWidth}"
                                  height="${value.ImageElementHeight.isSet?imageHeight:null}"
                                  showBig="${imageshowbig}"
                                  gallery="gallery${value.Id }"
                                  urllink="${showLinkToDetail?linkDetail:null }"
                                  titlelink="${showLinkToDetail?titlelarge:null }"/>
                      </div>
                    </c:if>
                  </c:if>
                  </div>
                </article>
              </li>
            </c:if>
            <c:if test="${value.ViewType == 'tit-des-img' }">
              <li class="has-media box default-box media-to-${imageBlockPosition} has-media-complete ${width} <c:if test='${count%2==0}'> par</c:if>">
                <article class="media">
                  <div class="row">
                    <c:if test="${imageBlockPosition == 'top' or imageBlockPosition == 'left'}">
                      <c:if test="${imagePath!=null and imagePath!='' and fn:indexOf(imagePath, '???')==-1}">
                        <c:if test="${imageBlockPosition == 'top'}">
                          <div class="clearfix">
                        </c:if>
                        <div
                                class="image media-object col-xs-${imageBlockWidth} col-xxs-12 <c:if test='${imageBlockPosition == "top"}'>center-block col-centered</c:if>">
                          <sg:image imagePath="${imagePath}" width="${imageWidth}"
                                    height="${value.ImageElementHeight.isSet?imageHeight:null}"
                                    showBig="${imageshowbig}"
                                    gallery="gallery${value.Id }"
                                    urllink="${showLinkToDetail?linkDetail:null }"
                                    titlelink="${showLinkToDetail?titlelarge:null }"/>
                        </div>
                        <c:if test="${imageBlockPosition == 'top'}">
                          </div>
                        </c:if>
                      </c:if>
                    </c:if>
                    <div class="media-body container-fluid">
                      <header>
                        <${titletagelement} class="title ${titletagelementsize}">
                        <c:if test="${showLinkToDetail}">
                          <a href="<cms:link>${linkDetail }</cms:link>" <c:if test="${not empty titleAttr}"> title='${titleAttr}'</c:if>>
                            <c:if test="${!cms.isOnlineProject and !published}">
                              <abbr title="Recurso poendiente de publicaci&oacute;n"><span class="fa fa-flag small"
                                                                                           aria-hidden="true"></span></abbr>
                            </c:if>
                            <c:out value="${title}"/>
                          </a>
                        </c:if>
                        <c:if test="${!showLinkToDetail}">
                          <c:if test="${!cms.isOnlineProject and !published}">
                            <abbr title="Recurso poendiente de publicaci&oacute;n"><span class="fa fa-flag small"
                                                                                         aria-hidden="true"></span></abbr>
                          </c:if>
                          <c:out value="${title}"/>
                        </c:if>
                      </${titletagelement}>
                      </header>
                      <c:if test="${not empty description and description!=null and fn:indexOf(description, '???')==-1}">
                        <div class="description">
                          <p>${description}</p>
                        </div>
                      </c:if>

                      <c:if test="${value.ConfigButton.exists and showLinkToDetail}">
                        <div class="actions">
                          <cms:include
                                  file="%(link.weak:/system/modules/com.saga.sagasuite.solrlist/elements/c-boton.jsp:2ea80592-a44c-11e3-8e81-f18cf451b707)">
                            <cms:param name="tipoboton">${value.ConfigButton.value.CssClass}</cms:param>
                            <cms:param name="iconboton">${value.ConfigButton.value.Icon}</cms:param>
                            <cms:param name="sizeboton">${value.ConfigButton.value.Size}</cms:param>
                            <cms:param name="positionboton">${value.ConfigButton.value.Position}</cms:param>
                            <cms:param name="textoboton">${value.ConfigButton.value.TextLink}</cms:param>
                            <cms:param name="linkboton">${linkDetail}</cms:param>
                            <cms:param name="targetboton">_self</cms:param>
                            <cms:param name="titleboton">${titlelarge}</cms:param>
                            <cms:param name="followboton">true</cms:param>
                          </cms:include>
                        </div>
                      </c:if>
                    </div>
                    <c:if test="${imageBlockPosition == 'right'}">
                      <c:if test="${imagePath!=null and imagePath!='' and fn:indexOf(imagePath, '???')==-1}">
                        <div class="image media-object col-xs-${imageBlockWidth} col-xxs-12">
                          <sg:image imagePath="${imagePath}" width="${imageWidth}"
                                    height="${value.ImageElementHeight.isSet?imageHeight:null}"
                                    showBig="${imageshowbig}"
                                    gallery="gallery${value.Id }"
                                    urllink="${showLinkToDetail?linkDetail:null }"
                                    titlelink="${showLinkToDetail?titlelarge:null }"/>
                        </div>
                      </c:if>
                    </c:if>
                  </div>
                </article>
              </li>
            </c:if>
            <c:if test="${value.ViewType == 'tit-dat-des-img' }">
              <li class="box default-box has-media media-to-${imageBlockPosition} has-media-complete ${width} <c:if test='${count%2==0}'> par</c:if>">
                <article class="media">
                  <div class="row">
                    <c:if test="${imageBlockPosition == 'top' or imageBlockPosition == 'left'}">
                      <c:if test="${imagePath!=null and imagePath!='' and fn:indexOf(imagePath, '???')==-1}">
                        <c:if test="${imageBlockPosition == 'top'}">
                          <div class="clearfix">
                        </c:if>
                        <div
                                class="image media-object col-xs-${imageBlockWidth} col-xxs-12 <c:if test='${imageBlockPosition == "top"}'>center-block col-centered</c:if>">
                          <sg:image imagePath="${imagePath}" width="${imageWidth}"
                                    height="${value.ImageElementHeight.isSet?imageHeight:null}"
                                    showBig="${imageshowbig}"
                                    gallery="gallery${value.Id }"
                                    urllink="${showLinkToDetail?linkDetail:null }"
                                    titlelink="${showLinkToDetail?titlelarge:null }"/>
                        </div>
                        <c:if test="${imageBlockPosition == 'top'}">
                          </div>
                        </c:if>
                      </c:if>
                    </c:if>
                    <div class="media-body container-fluid">
                      <c:if test="${empty showdate or showdate == 'true'}">
												<span class="date-element">
													<span class="fa fa-calendar" aria-hidden="true"></span>
													<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-time-new.jsp:d9451486-147d-11e7-8644-7fb253176922)">
                                                      <cms:param name="date">${date }</cms:param>
                                                      <cms:param name="formatdate">${formatdate }</cms:param>
                                                    </cms:include>
												</span>
                      </c:if>
                      <header>
                        <${titletagelement} class="title media-heading ${titletagelementsize}">
                        <c:if test="${showLinkToDetail}">
                          <a href="<cms:link>${linkDetail }</cms:link>" <c:if test="${not empty titleAttr}"> title='${titleAttr}'</c:if>>
                            <c:if test="${!cms.isOnlineProject and !published}">
                              <abbr title="Recurso poendiente de publicaci&oacute;n"><span class="fa fa-flag small"
                                                                                           aria-hidden="true"></span></abbr>
                            </c:if>
                            <c:out value="${title}"/>
                          </a>
                        </c:if>
                        <c:if test="${!showLinkToDetail}">
                          <c:if test="${!cms.isOnlineProject and !published}">
                            <abbr title="Recurso poendiente de publicaci&oacute;n"><span class="fa fa-flag small"
                                                                                         aria-hidden="true"></span></abbr>
                          </c:if>
                          <c:out value="${title}"/>
                        </c:if>
                      </${titletagelement}>
                      </header>
                      <c:if test="${not empty description and description!=null and fn:indexOf(description, '???')==-1}">
                        <div class="description">
                          <p>${description}</p>
                        </div>
                      </c:if>
                      <c:if test="${value.ConfigButton.exists and showLinkToDetail}">
                        <div class="actions">
                          <cms:include
                                  file="%(link.weak:/system/modules/com.saga.sagasuite.solrlist/elements/c-boton.jsp:2ea80592-a44c-11e3-8e81-f18cf451b707)">
                            <cms:param name="tipoboton">${value.ConfigButton.value.CssClass}</cms:param>
                            <cms:param name="iconboton">${value.ConfigButton.value.Icon}</cms:param>
                            <cms:param name="sizeboton">${value.ConfigButton.value.Size}</cms:param>
                            <cms:param name="positionboton">${value.ConfigButton.value.Position}</cms:param>
                            <cms:param name="textoboton">${value.ConfigButton.value.TextLink}</cms:param>
                            <cms:param name="linkboton">${linkDetail}</cms:param>
                            <cms:param name="targetboton">_self</cms:param>
                            <cms:param name="titleboton">${titlelarge}</cms:param>
                            <cms:param name="followboton">true</cms:param>
                          </cms:include>
                        </div>
                      </c:if>
                    </div>
                    <c:if test="${imageBlockPosition == 'right'}">
                      <c:if test="${imagePath!=null and imagePath!='' and fn:indexOf(imagePath, '???')==-1}">
                        <div class="image media-object col-xs-${imageBlockWidth} col-xxs-12">
                          <sg:image imagePath="${imagePath}" width="${imageWidth}"
                                    height="${value.ImageElementHeight.isSet?imageHeight:null}"
                                    showBig="${imageshowbig}"
                                    gallery="gallery${value.Id }"
                                    urllink="${showLinkToDetail?linkDetail:null }"
                                    titlelink="${showLinkToDetail?titlelarge:null }"/>
                        </div>
                      </c:if>
                    </c:if>
                  </div>
                </article>
              </li>
            </c:if>
          </c:if>
        </c:if>
        <%-- clearfix para xxs --%>
        <c:if test="${not empty columnsnumberxxs}">
          <c:if test="${count>0 and resourceForPages!=2 and count%2==0 and columnsnumberxxs=='2'}">
            <li class="visible-xxs col-xxs-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=3 and count%3==0 and columnsnumberxxs=='3'}">
            <li class="visible-xxs col-xxs-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=4 and count%4==0 and columnsnumberxxs=='4'}">
            <li class="visible-xxs col-xxs-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=6 and count%6==0 and columnsnumberxxs=='6'}">
            <li class="visible-xxs col-xxs-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=12 and count%12==0 and columnsnumberxxs=='12'}">
            <li class="visible-xxs col-xxs-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
        </c:if>
        <%-- clearfix para xs --%>
        <c:if test="${not empty columnsnumberxs}">
          <c:if test="${count>0 and resourceForPages!=2 and count%2==0 and columnsnumberxs=='2'}">
            <li class="visible-xs col-xs-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=3 and count%3==0 and columnsnumberxs=='3'}">
            <li class="visible-xs col-xs-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=4 and count%4==0 and columnsnumberxs=='4'}">
            <li class="visible-xs col-xs-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=6 and count%6==0 and columnsnumberxs=='6'}">
            <li class="visible-xs col-xs-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=12 and count%12==0 and columnsnumberxs=='12'}">
            <li class="visible-xs col-xs-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
        </c:if>
        <%-- clearfix para sm --%>
        <c:if test="${not empty columnsnumbersm}">
          <c:if test="${count>0 and resourceForPages!=2 and count%2==0 and columnsnumbersm=='2'}">
            <li class="visible-sm col-sm-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=3 and count%3==0 and columnsnumbersm=='3'}">
            <li class="visible-sm col-sm-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=4 and count%4==0 and columnsnumbersm=='4'}">
            <li class="visible-sm col-sm-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=6 and count%6==0 and columnsnumbersm=='6'}">
            <li class="visible-sm col-sm-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=12 and count%12==0 and columnsnumbersm=='12'}">
            <li class="visible-sm col-sm-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
        </c:if>
        <%-- clearfix para md --%>
        <c:if test="${not empty columnsnumbermd}">
          <c:if test="${count>0 and resourceForPages!=2 and count%2==0 and columnsnumbermd=='2'}">
            <li class="visible-md col-md-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=3 and count%3==0 and columnsnumbermd=='3'}">
            <li class="visible-md col-md-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=4 and count%4==0 and columnsnumbermd=='4'}">
            <li class="visible-md col-md-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=6 and count%6==0 and columnsnumbermd=='6'}">
            <li class="visible-md col-md-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=12 and count%12==0 and columnsnumbermd=='12'}">
            <li class="visible-md col-md-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
        </c:if>
        <%-- clearfix para lg --%>
        <c:if test="${not empty columnsnumberlg}">
          <c:if test="${count>0 and resourceForPages!=2 and count%2==0 and columnsnumberlg=='2'}">
            <li class="visible-lg col-lg-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=3 and count%3==0 and columnsnumberlg=='3'}">
            <li class="visible-lg col-lg-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=4 and count%4==0 and columnsnumberlg=='4'}">
            <li class="visible-lg col-lg-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=6 and count%6==0 and columnsnumberlg=='6'}">
            <li class="visible-lg col-lg-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
          <c:if test="${count>0 and resourceForPages!=12 and count%12==0 and columnsnumberlg=='12'}">
            <li class="visible-lg col-lg-12 clearfix" aria-hidden="true">
              <hr/>
            </li>
          </c:if>
        </c:if>

      </cms:contentload>
    </c:if>
    <c:if test="${info.resultSize == 0}">
      <c:if test="${newcontent == 'true' and value.Editable == 'true'}">
          <cms:edit createType="${resourcetypefornewcontent}" create="true" creationSiteMap="${value.CreatePath }">
            <div class="alert alert-success">
              <strong>Crea el primer recurso de tipo <em>${resourcetypefornewcontent}</em> desde la diana de edición de esta caja.</strong>
              <div>Si no ves la diana recuerda que tienes que estar en la vista correspondiente al tipo de recurso que vas a crear.</div>
            </div>
          </cms:edit>
      </c:if>
      <%-- Si queremos borrar la caja cuando estemos en online --%>
      <c:if
              test="${value.HideBoxIfEmpty.isSet and value.HideBoxIfEmpty=='true' and cms.requestContext.currentProject.onlineProject}">
        <script>
          $(document).ready(function () {
            $("#solrlist-${value.Id}").remove();
          });
        </script>
      </c:if>
      <div class="alert alert-warning">
          ${value.LabelNoResult }
      </div>
    </c:if>
  </cms:contentload>
</ul>
  <%-- botón de ver más resultados --%>
<c:if test="${not empty param.listlinkhref and param.showbuttonlist == 'true'}">
  <c:set var="linkhref">${param.listlinkhref}</c:set>
  <c:set var="linktarget">_self</c:set>
  <c:if test="${not empty param.listlinktarget}">
    <c:set var="linktarget">${param.listlinktarget}</c:set>
  </c:if>
  <c:set var="linktitle"><fmt:message key="key.element.read.more" /></c:set>
  <c:if test="${not empty param.listlinktitle}">
    <c:set var="linktitle">${param.listlinktitle}</c:set>
  </c:if>
  <c:set var="buttonlistposition">${param.buttonlistposition}</c:set>
  <c:set var="buttonclass">btn btn-lg btn-xlg btn-specific-main btn-bordered</c:set>
  <c:if test="${not empty param.buttonlistclass}">
    <c:set var="buttonclass">${param.buttonlistclass}</c:set>
  </c:if>
  <%-- comprobamos si existen más resultados de los definidos a mostrar en el listado y si es asi cargamos el boton de ver todos --%>
  <fmt:parseNumber value="${resultSize}" parseLocale="${cms.locale}" integerOnly="true" var="resultSize"/>
  <fmt:parseNumber value="${value.PageSize}" parseLocale="${cms.locale}" integerOnly="true" var="pageSize"/>
  <c:if test="${resultSize > 0 and resultSize > pageSize }">
    <div class="mt-30 text-${buttonlistposition}">
      <a class="${buttonclass}" target="${linktarget}" href="<cms:link>${linkhref}</cms:link>">
        <span class="fa fa-plus mr-10" aria-hidden="true"></span>${linktitle}
      </a>
    </div>
  </c:if>
</c:if>
</cms:bundle>