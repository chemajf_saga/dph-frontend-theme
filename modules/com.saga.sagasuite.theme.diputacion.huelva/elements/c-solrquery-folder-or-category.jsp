<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<c:set var="solrquery"></c:set>

<c:set var="parentfolder"></c:set>

<c:set var="showquery">${param.showquery}</c:set>
<%--Anadimos en primer lugar el filtro en base a la ruta raiz que es obligatoria --%>

<c:choose>
  <c:when test="${content.value.MultiSitesSearch.stringValue eq 'true'}">
    <c:set var="parentfolder">&fq=parent-folders:/</c:set>
  </c:when>
  <c:otherwise>
    <c:set var="parentfolder">&fq=parent-folders:${currentsite}/</c:set>
  </c:otherwise>
</c:choose>

<c:forEach var="path" items="${content.valueList['RootPath']}" varStatus="status">
  <c:if test="${not fn:startsWith(path, '/sites/') and not fn:startsWith(path, '/shared/')}">
    <c:set var="path">${currentSite}${path}</c:set>
  </c:if>
  <c:choose>
    <c:when test="${status.first}">
      <c:set var="parentfolder">"${path}"</c:set>
    </c:when>
    <c:otherwise>
      <c:set var="parentfolder">${parentfolder} OR "${path}"</c:set>
    </c:otherwise>
  </c:choose>
</c:forEach>
<c:set var="onlyparentfolder">parent-folders:${parentfolder}</c:set>
<c:set var="parentfolder">&fq=parent-folders:(${parentfolder})</c:set>
<c:set var="solrqueryParentfolder">${parentfolder}</c:set>
<c:set var="solrquery">${solrquery}${parentfolder}</c:set>

<%--En segundo lugar metemos el filtro por tipo de recursos --%>


<c:set var="resourcetype" value=""/>

<c:if test="${fn:length(content.valueList['ResourceType']) eq 1}">
  <c:set var="resourcetype">&fq=type:${value.ResourceType}</c:set>
  <%-- Mandamos variables para el resultlist-contentload para comprobar si solo se lista un tipo de recurso y habilitar la creación de nuevos recursos si no hay resultados con <cms:edit> --%>
  <c:set var="newcontent" scope="request">true</c:set>
  <c:set var="resourcetypefornewcontent" scope="request">${value.ResourceType}</c:set>
</c:if>
<c:if test="${fn:length(content.valueList['ResourceType']) gt 1}">
  <c:forEach var="elem" items="${content.valueList['ResourceType']}" varStatus="status">
    <c:if test="${status.first }">
      <c:set var="resourcetype">&fq=type:(${elem}</c:set>
    </c:if>
    <c:if test="${!status.first }">
      <c:set var="resourcetype">${resourcetype} OR "${elem}"</c:set>
    </c:if>
    <c:if test="${status.last }">
      <c:set var="resourcetype">${resourcetype})</c:set>
    </c:if>
  </c:forEach>
</c:if>
<c:set var="solrqueryResourcetype">${solrquery}${resourcetype}</c:set>
<c:set var="solrquery">${solrquery}${resourcetype}</c:set>

<%--Anadimos el filtro por categoria --%>
<c:set var="categorysolr" value=""/>

<%-- si se ha marcado que se lea la propiedad de categoria o se ha editado el campo de categoria por defecto cargamos el parametro de solr de category--%>

<c:if test="${(content.value.readPropertyCategory.exists && content.value.readPropertyCategory=='true') or value.Category.isSet}">
<c:set var="categorysolrfilter">&fq=category:</c:set>
</c:if>

<%-- Si se ha marcado el readPropertyCategory tenemos que buscar la propiedad category --%>
<c:if test="${content.value.readPropertyCategory.exists && content.value.readPropertyCategory=='true'}">
  <c:set var="categoryProperty"><cms:property name="category" file="search"/></c:set>
  <c:if test="${not empty categoryProperty}">
  	<%-- comprobamos si viene con la ruta completa y si es asi acortamos dejando desde /.categories/ --%>
  	<c:if test="${(fn:indexOf(categoryProperty, '.categories/')) != '-1'}">
		<c:set var="categoryProperty">${fn:substringAfter(categoryProperty, ".categories/")}</c:set>
	</c:if>
  </c:if>
</c:if>

<c:set var="categoryUserFilter" value=""/>
<c:forEach var="elem" items="${content.valueList.CategoryUserFilter}" varStatus="status">
  <c:set var="paramname">${elem.value.ParanName }</c:set>
  <c:if test="${not empty param[paramname]}">
    <c:choose>
	   <c:when test="${status.first and status.last}">
         <c:set var="categoryUserFilter">
           &fq=category_exact:(${param[paramname]})
         </c:set>
       </c:when>
       <c:when test="${status.first}">
         <c:set var="categoryUserFilter">
           &fq=category_exact:(${param[paramname]}
         </c:set>
       </c:when>
       <c:when test="${status.last}">
        <c:set var="categoryUserFilter">
          ${categoryUserFilter})
        </c:set>
       </c:when>
       <c:otherwise>
         <c:set var="categoryUserFilter">
           ${categoryUserFilter} AND ${param[paramname]}
         </c:set>
       </c:otherwise>
    </c:choose>
  </c:if>
</c:forEach>
<c:set var="solrqueryCatuserfilter">${solrquery}${categoryUserFilter}</c:set>
<c:set var="solrquery">${solrquery}${categoryUserFilter}</c:set>


<%--Anadimos el filtro de idioma de forma automatica --%>
<c:set var="localefilter">&fq=con_locales:${cms.locale}</c:set>
<%-- Comprobamos si lo que hay que listar es un binario o imagen para no tener en cuenta el locale ya que no disponen del mismo --%>
<c:if test="${value.ResourceType == 'binary' or value.ResourceType == 'image'}">
	<c:set var="localefilter">&fq=con_locales:*</c:set>
</c:if>
<c:set var="solrqueryLocalefilter">${solrquery}${localefilter}</c:set>
<c:set var="solrquery">${solrquery}${localefilter}</c:set>

<%--Anadimos los filtros personalizados --%>
<c:set var="customfilter" value=""/>
<c:forEach var="elem" items="${content.valueList['CustomFilter']}" varStatus="status">
  <c:set var="customfilter">${customfilter}${elem }</c:set>
</c:forEach>
<c:set var="solrqueryCustomfilter">${solrquery}${customfilter}</c:set>
<c:set var="solrquery">${solrquery}${customfilter}</c:set>

<%--Anadimos los filtros personalizados por jsp --%>
<c:set var="customfilterjsp" value="" scope="request"/>
<c:if test="${value.CustomFilterJsp.isSet}">
    <cms:include file="${value.CustomFilterJsp}"/>
</c:if>
<c:set var="solrqueryCustomfilterjsp">${solrquery}${customfilterjsp}</c:set>
<c:set var="solrquery">${solrquery}${customfilterjsp}</c:set>

<%--Anadimos el orden de la busqueda --%>
<c:set var="order" value=""/>
<c:if test="${value.CustomOrder.isSet }">
  <c:set var="order" value="&sort=${value.CustomOrder}"/>
</c:if>
<c:if test="${!value.CustomOrder.isSet }">
  <c:forEach var="elem" items="${content.valueList['Order']}" varStatus="status">
    <c:set var="order">${order}${elem}</c:set>
    <c:if test="${status.first}">
      <c:set var="order">&sort=${elem}</c:set>
    </c:if>
    <c:if test="${!status.first}">
      <c:set var="order">${order}, ${elem}</c:set>
    </c:if>
  </c:forEach>
</c:if>

<c:set var="rand"><%= java.lang.Math.round(java.lang.Math.random() * 10000) %></c:set>
<c:set var="orderAux">${fn:replace(order, "{rand}", rand )}</c:set>
<c:set var="solrquery">${solrquery}${orderAux}</c:set>

<%-- SI EXISTE CATEGORÍA DEFINIDA INCLUIMOS UN OR Y OTRA QUERY SIN EL PARENT FOLDER DE MANERA QUE NOS MUESTRE
 LOS RESULTADOS PERO SIN FILTAR POR CARPETA Y FILTRANDO POR CATEGORÍA --%>
<c:choose>
  <c:when test="${value.Category.isSet}">
    <c:forEach var="elem" items="${content.valueList['Category']}" varStatus="status">
      <c:set var="catOk">${elem}</c:set>
      <c:set var="lengtTotal" value="${fn:length(catOk)}"/>
      <c:set var="initCategoryPath" value="${fn:indexOf(catOk,'categories/')}"/>
      <c:set var="initCategoryPath" value="${initCategoryPath+fn:length('categories/')}"/>
      <c:set var="catOk" value="${fn:substring(catOk, initCategoryPath, lengtTotal)}"/>
        <c:if test="${status.first }">
          <c:set var="category">category:("${catOk}"</c:set>
        </c:if>
        <c:if test="${!status.first }">
          <c:set var="category">${category} OR "${catOk}"</c:set>
        </c:if>
        <c:if test="${status.last}">
          <c:set var="category">${category}<c:if test="${not empty categoryProperty}"> AND "${categoryProperty}"</c:if>)</c:set>
        </c:if>
    </c:forEach>
    <c:set var="solrquery">&fq=(${onlyparentfolder} OR ${category})${resourcetype}${categoryUserFilter}${localefilter}${customfilter}${customfilterjsp}${orderAux}</c:set>
  </c:when>
  <c:when test="${!value.Category.isSet and not empty categoryProperty}">
    <c:set var="category">category:"${categoryProperty}"</c:set>
    <c:set var="solrquery">&fq=(${onlyparentfolder} OR ${category})${resourcetype}${categoryUserFilter}${localefilter}${customfilter}${customfilterjsp}${orderAux}</c:set>
  </c:when>
  <c:otherwise>
    <c:set var="solrquery">${solrquery}</c:set>
  </c:otherwise>
</c:choose>

<c:set var="solrquery" scope="request">${solrquery}</c:set>

<c:if test="${not cms.requestContext.currentProject.onlineProject and showquery == 'true'}">
  <div class='alert alert-info'><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class='no-margin'>Mensaje de edici&oacute;n</h4><strong>Consulta realizada a SOLR:</strong><br>${solrquery}</div>
</c:if>