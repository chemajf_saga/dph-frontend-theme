<%@page buffer="none" session="false" taglibs="c,cms,fmt, fn" trimDirectiveWhitespaces="true"%>
	
<c:set var="linkboton" value="${param.linkboton}" />
<c:set var="targetboton" value="${param.targetboton}" />
<c:set var="titleboton" value="${param.titleboton}" />
<c:set var="followboton" value="${param.followboton}" />
<c:set var="sizeboton" value="${param.sizeboton}" />
<c:set var="positionboton" value="${param.positionboton}" />
<c:set var="tipoboton" value="${param.tipoboton}" />
<c:set var="textoboton" value="${param.textoboton}" />
<c:set var="positionboton" value="${param.positionboton}" />
<c:set var="iconboton" value="${param.iconboton}" />

<div class="action-btn clearfix <c:out value='${positionboton}' />">
							
	<a class="btn <c:out value='${tipoboton} ${sizeboton}' />" href="<cms:link>${linkboton}</cms:link>" target="${targetboton}" title="${titleboton}" <c:if test="${followboton!=null && followboton=='false' }">rel="nofollow"</c:if>>
		<c:if test="${not empty iconboton}">
			<%-- hacemos esta comprobacion para los recursos que usaban el campo de icono antiguo que nos traia como valor fa-check en lugar de check--%>
			<c:if test="${fn:startsWith(iconboton, 'fa-')}">
				<span class="fa ${iconboton }" aria-hidden="true">&nbsp;</span>
			</c:if>
			<c:if test="${!fn:startsWith(iconboton, 'fa-')}">
				<span aria-hidden="true" class="fa fa-${fn:toLowerCase(iconboton)}<c:if test="${fn:endsWith(fn:toLowerCase(iconboton), 'lightbulb')}">-o</c:if>">&nbsp;</span>
			</c:if>
		</c:if>						
		<c:if test="${textoboton!=null }">
			${textoboton}
		</c:if>
		<c:if test="${textoboton==null }">
			<fmt:message key="microcontent.label.boton.verdetalle" />
		</c:if>
	</a>							
</div>		
