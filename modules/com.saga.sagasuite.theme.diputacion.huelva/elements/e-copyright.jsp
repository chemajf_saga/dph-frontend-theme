<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<fmt:setLocale value="${cms.locale}"/>
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">

    <%-- Definimos las variables para el copyright en funcion de los campos del Theme Config --%>
    <c:set var="socialpages">${themeConfiguration.CustomFieldKeyValue.socialpages}</c:set>
    <c:set var="FacebookPageUrl">${themeConfiguration.FacebookPageUrl}</c:set>
    <c:set var="TwitterPageUrl">${themeConfiguration.TwitterPageUrl}</c:set>
    <c:set var="GooglePlusPageUrl">${themeConfiguration.GooglePlusPageUrl}</c:set>
    <c:set var="YouTubePageUrl">${themeConfiguration.YouTubePageUrl}</c:set>
    <c:set var="socialflickrpage">${themeConfiguration.CustomFieldKeyValue.socialflickrpage}</c:set>
    <c:set var="socialinstagrampage">${themeConfiguration.CustomFieldKeyValue.socialinstagrampage}</c:set>
    <c:set var="socialpinterestpage">${themeConfiguration.CustomFieldKeyValue.socialpinterestpage}</c:set>
    <c:set var="socialvimeopage">${themeConfiguration.CustomFieldKeyValue.socialvimeopage}</c:set>
    <c:set var="socialslidesharepage">${themeConfiguration.CustomFieldKeyValue.socialslidesharepage}</c:set>
    <c:set var="contactpage">${themeConfiguration.CustomFieldKeyValue.contactpage}</c:set>
    <c:set var="legalwarningpage">${themeConfiguration.CustomFieldKeyValue.legalwarningpage}</c:set>
    <c:set var="privacypolicypage">${themeConfiguration.CustomFieldKeyValue.privacypolicypage}</c:set>
    <c:set var="rsspage">${themeConfiguration.CustomFieldKeyValue.rsspage}</c:set>
    <c:set var="copyrighttext">${themeConfiguration.CustomFieldKeyValue.copyrighttext}</c:set>
    <c:set var="copyrightcomplete">${themeConfiguration.CustomFieldKeyValue.copyrightcomplete}</c:set>

    <%-- comprobamos si el campo de contenido completo del copyright está editado o no --%>
    <%-- si no está editado cargamos el contenido por defecto --%>
    <c:if test="${empty copyrightcomplete}">
        <div class="row">
            <div class="col-md-9">
                <ul class="list-inline no-margin">
                    <c:if test="${not empty copyrighttext}">
                        <li>
                                ${copyrighttext}
                        </li>
                    </c:if>
                    <c:if test="${not empty contactpage}">
                        <li>
                            <a href="<cms:link>${contactpage}</cms:link>"><fmt:message key="key.formatter.footer.contactpage" /></a>
                        </li>
                    </c:if>
                    <c:if test="${not empty legalwarningpage}">
                        <li aria-hidden="true">|</li>
                        <li>
                            <a accesskey="8" href="<cms:link>${legalwarningpage}</cms:link>"><fmt:message key="key.formatter.footer.legalwarningpage" /></a>
                        </li>
                    </c:if>
                    <c:if test="${not empty privacypolicypage}">
                        <li aria-hidden="true">|</li>
                        <li>
                            <a href="<cms:link>${privacypolicypage}</cms:link>"><fmt:message key="key.formatter.footer.privacypolicypage" /></a>
                        </li>
                    </c:if>
                </ul>
            </div>
            <c:if test="${socialpages == 'true'}">
                <div class="col-md-3 text-right">
                    <ul class="social-footer list-inline no-margin">
                        <c:if test="${not empty FacebookPageUrl}">
                            <li><a title="<fmt:message key="key.formatter.header.facebook" />" href="${FacebookPageUrl}" target="_blank"> <span class="fa fa-facebook" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.facebook" /></span></a></li>                </c:if>
                        <c:if test="${not empty TwitterPageUrl}">
                            <li><a title="<fmt:message key="key.formatter.header.twitter" />" href="${TwitterPageUrl}" target="_blank"> <span class="fa fa-twitter" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.twitter" /></span></a></li>
                        </c:if>
                        <c:if test="${not empty GooglePlusPageUrl}">
                            <li><a title="<fmt:message key="key.formatter.header.googleplus" />" href="${GooglePlusPageUrl}" target="_blank"> <span class="fa fa-google-plus" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.googleplus" /></span></a></li>
                        </c:if>
                        <c:if test="${not empty YouTubePageUrl}">
                            <li><a title="<fmt:message key="key.formatter.header.youtube" />" href="${YouTubePageUrl}" target="_blank"> <span class="fa fa-youtube-play" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.youtube" /></span></a></li>
                        </c:if>
                        <c:if test="${not empty socialflickrpage}">
                            <li><a title="<fmt:message key="key.formatter.header.flickr" />" href="${socialflickrpage}" target="_blank"> <span class="fa fa-flickr" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.flickr" /></span></a></li>
                        </c:if>
                        <c:if test="${not empty socialinstagrampage}">
                            <li><a title="<fmt:message key="key.formatter.header.instagram" />" href="${socialinstagrampage}" target="_blank"> <span class="fa fa-instagram" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.instagram" /></span></a></li>
                        </c:if>
                        <c:if test="${not empty socialpinterestpage}">
                            <li><a title="<fmt:message key="key.formatter.header.pinterest" />" href="${socialpinterestpage}" target="_blank"> <span class="fa fa-pinterest" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.pinterest" /></span></a></li>
                        </c:if>
                        <c:if test="${not empty socialvimeopage}">
                            <li><a title="<fmt:message key="key.formatter.header.vimeo" />" href="${socialvimeopage}" target="_blank"> <span class="fa fa-vimeo" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.vimeo" /></span></a></li>
                        </c:if>
                        <c:if test="${not empty socialslidesharepage}">
                            <li><a title="<fmt:message key="key.formatter.header.slideshare" />" href="${socialslidesharepage}" target="_blank"> <span class="fa fa-slideshare" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.slideshare" /></span></a></li>
                        </c:if>
                        <c:if test="${not empty rsspage}">
                            <li><a title="<fmt:message key="key.formatter.header.rss" />" href="${rsspage}"> <span class="fa fa-rss" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.rss" /></span></a></li>
                        </c:if>
                    </ul>
                </div>
            </c:if>
        </div>
    </c:if>
    <%-- si está editado cargamos el contenido completo definido en el themeconfig --%>
    <c:if test="${not empty copyrightcomplete}">
        ${copyrightcomplete}
    </c:if>
</cms:bundle>
