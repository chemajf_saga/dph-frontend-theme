<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sgcore" tagdir="/WEB-INF/tags/core/templates" %>
<%//System.out.println("-Inicio e-corporacion-item.jsp");%>
<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
	<c:set var="articleId"><sgcore:generated-id label="${param.title}"/></c:set>
	<div class="corporacion-item-wrapper box">
		<div class="element complete-mode with-media overlayed">
			<a href="#" title="Ver biografía de ${param.title}" class="data-corporacion-loader overlayer"
				data-tab="${param.idTab}" 
				data-iddiputado="${param.iddiputado}" 
				data-imagepathbig="${param.imagepathbig}" 
				data-title="${param.title}" 
				data-titleid="${articleId}" 
				data-loading="dph-galeria-loading" 
				data-targetcontainer="${param.idTab}" 
				data-url="<cms:link>/system/modules/com.saga.sagasuite.theme.diputacion.huelva/functions/f-corporacion-detalle.jsp</cms:link>"
				>
				<span class="hover-overlay" aria-hidden="true"></span>
				<span class="sr-only">Ver biografía de ${param.title}</span>
			</a>
			<div class="img-wrapper overlayed">
				<div class="img-wrapper-scale">
					<img src="http://w2.diphuelva.es/portalweb/diputados/Imagenes/${param.imagePath}"
						alt="${param.title}" height="180" width="480" class="img-responsive" />
				</div>
			</div>
			<div class="text-overlay equalheight-corporacion-text-overlay">
				<div class="wrapper text-center">
					<h2 class="title-item regular-text no-margin no-padding">
						${param.title}
					</h2>
				</div>
			</div>
		</div>
	</div>
</cms:bundle>
<%//System.out.println("-FIN e-corporacion-item.jsp");%>