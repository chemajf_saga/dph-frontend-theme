<%@ page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%@ page import="org.apache.commons.lang3.StringUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%//System.out.println("--Inicio e-corporacion-tab-content.jsp");%>

<%//System.out.println("---Inicio e-corporacion-tab-content.jsp");%>
<%-- Parámetro de entrada: siglas del partido --%>
<c:set var="siglas" value="${param.tabTitle}" />
<%
    String SIGLAS = pageContext.getAttribute("siglas").toString();
	//System.out.println(" SIGLAS: " + SIGLAS);

    List<Map<String, String>> res = new LinkedList<Map<String, String>>();
    if (StringUtils.isNotEmpty(SIGLAS)) {
        String sql = "SELECT cargo_municipio as nombreDiputado, persona as idDiputado, imagen_tesela, descripcion_imagen_tesela FROM public.v_diputados WHERE siglas_partido = '"
                        + SIGLAS + "' ORDER BY orden_partido";

        Connection conexion = null;
        Statement sentencia = null;
        ResultSet rs = null;

        try {
            Class.forName("org.postgresql.Driver");
        } catch (Exception e) {
            //System.out.println("ERROR (e-corporacion-tab-content.jsp): No se pudo cargar el puente JDBC-ODBC.");
            return;
        }
        //System.out.println("Puente JDBC-ODBC cargado (e-corporacion-tab-content.jsp)!");

        try {
            // DESA conexion = DriverManager.getConnection("jdbc:postgresql://192.168.131.30:5432/portalweb", "ocmsdph", "ocmsdph");
            /*PROD*/conexion = DriverManager.getConnection("jdbc:postgresql://192.168.136.33:5432/portalweb", "ocmsdph", "ocmsdph");
            //System.out.println("Conexion realizada (e-corporacion-tab-content.jsp)");
        } catch (Exception er) {
            //System.out.println("ERROR obteniendo conexion (e-corporacion-tab-content.jsp): " + er);
            er.printStackTrace();
        }

        try {
            sentencia = conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            //System.out.println("Sentencia creada (e-corporacion-tab-content.jsp)");
        } catch (Exception er) {
            //System.out.println("ERROR obteniendo sentencia (e-corporacion-tab-content.jsp): " + er);
            er.printStackTrace();
        }

        try {
            //System.out.println("Realizamos la consulta (e-corporacion-tab-content.jsp): " + sql);
            rs = sentencia.executeQuery(sql);
            int i = 0;
            while (rs.next()) {
                Map<String, String> aux = new HashMap<String, String>();
                aux.put("siglas_partido", SIGLAS);
                aux.put("nombreDiputado", rs.getString("nombreDiputado"));
                aux.put("idDiputado", rs.getString("idDiputado"));
                aux.put("imagen_tesela", rs.getString("imagen_tesela"));
                aux.put("descripcion_imagen_tesela", rs.getString("descripcion_imagen_tesela"));
                res.add(i, aux);
                i++;
            }

        } catch (Exception er) {
            //System.out.println("ERROR realizando consulta (e-corporacion-tab-content.jsp): " + er);
            er.printStackTrace();
        }
		/*System.out.println("Resltado de la consulta (e-corporacion-tab-content.jsp):");
        for(Map<String, String> m : res){
            Set<String> keys = m.keySet();
            for(String key : keys){
            	System.out.print("@"+key + "::" + m.get(key) + "");
            }
			System.out.println("@");
        }*/
    conexion.close();
    }
    pageContext.setAttribute("eltos", res);
%>
<%-- <c:set var="eltos" value="${res}" scope="request" /> --%>
<%//System.out.println("---FIN e-corporacion-tab-content.jsp");%>


<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
	<div class="h3 no-margin mb-30 mt-10 text-center">${param.tabTitle}</div>
	<div id="dph-galeria-loading" class="text-center" style="visibility: hidden">
		<span class="fa fa-spinner fa-pulse fa-3x fa-fw" aria-hidden="true"></span> <span class="sr-only">cargando...</span>
	</div>
	<ul class="list-unstyled main-ul row gutter-sm fade in" id="ul-${param.idTab}">
		<%-- Tenemos el resultado de la consulta en ${eltos}: List<Map<String, String>> --%>
		<ul style="display: flex; flex-wrap: wrap;">
			<c:forEach items="${eltos}" var="map">
				<c:forEach items="${map}" var="entry" varStatus="index" >
					<c:choose>
						<c:when test="${entry.key eq 'idDiputado'}">
							<c:set var="id_diputado" value="${entry.value}" />
						</c:when>
						<c:when test="${entry.key eq 'nombreDiputado'}">
							<c:set var="tit" value="${entry.value}" />
						</c:when>
						<c:when test="${entry.key eq 'imagen_tesela'}">
							<c:set var="img" value="${entry.value}" />
							<c:set var="img_big" value="${entry.value}" />
						</c:when>
						<c:when test="${entry.key eq 'descripcion_imagen_tesela'}"></c:when>
						<c:when test="${entry.key eq 'detail_text'}">
							<c:set var="detail_text" value="${entry.value}" />
						</c:when>
					</c:choose>
				</c:forEach>
				<li class="corporacion-item col-xxs-6 col-xs-4 col-sm-3 col-lg-2 mb-30">
					<cms:include
						file="%(link.strong:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/e-corporacion-item.jsp:122cb735-5474-11e8-a09e-63f9a58dae09)">
						<cms:param name="title">${tit}</cms:param>
						<cms:param name="imagePath">${img}</cms:param>
						<cms:param name="imagepathbig">${img_big}</cms:param>
						<cms:param name="iddiputado">${id_diputado}</cms:param>
					</cms:include>
				</li>
				<c:set var="tit" value="" />
				<c:set var="img" value="" />
				<c:set var="img_big" value="" />
				<c:set var="id_diputado" value="" />
			</c:forEach>
		</ul>
	</ul>
</cms:bundle>
<%//System.out.println("--FIN e-corporacion-tab-content.jsp");%>