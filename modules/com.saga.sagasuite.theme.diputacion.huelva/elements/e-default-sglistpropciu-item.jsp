<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ page import="org.opencms.flex.CmsFlexController" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="vt" uri="http://www.sagasoluciones.com/taglib/vote" %>
<%@ taglib prefix="cmt" uri="http://www.sagasoluciones.com/taglib/comment" %>
<%@ taglib prefix="habla" tagdir="/WEB-INF/tags/habla" %>
<%@ taglib prefix="sg" uri="http://saga.com" %>

<%-- Plantilla por defecto de los elementos del listado de propuestas ciudadanas --%>
<fmt:setLocale value="${cms.locale}"/>
<fmt:bundle basename="com.saga.opencms.habla.messages">
    <%--
<c:set var="cmsObject" value="<%=CmsFlexController.getCmsObject(request)%>"/>
    --%>


    <vt:voteinfo var="voteInfo" resource="${resource.id.stringValue}"/>
    <cmt:commentinfo var="commentInfo" type="extended" resource="${resource.id.stringValue}"/>
    <sg:readuser user="${resource.file.userCreated.stringValue}" var="creator"/>
    <%--
    <cms:contentload collector="singleFile" param="${cmsObject.getSitePath(resource)}">
        <cms:contentaccess var="res" />
    </cms:contentload>
    --%>

    <c:set var="resourceLink">
        <cms:link>${resource.filename}</cms:link>
    </c:set>
    <li class="proposal-item">
        <article class="media">
            <cms:device type="desktop,tablet">
                <div class="rate-resume media-object pull-left">
                    <c:set var="totalfavor">${voteInfo.numPro}</c:set>
                    <c:set var="totalcontra"> ${voteInfo.numAgainst}</c:set>
                    <c:set var="porcenfavor">${voteInfo.ratePro}</c:set>
                    <c:set var="porcencontra">${voteInfo.rateAgainst}</c:set>

                    <ul class="list-inline">
                        <c:if test="${porcenfavor == 0 and porcencontra == 0}">
                            <c:set var="porcenfavor" value="50" />
                            <c:set var="porcencontra" value="50" />
                        </c:if>
                        <li class="good <c:if test="${porcenfavor == 0 and porcencontra != 0 }">hide</c:if><c:if test="${porcencontra == 0 and porcenfavor != 0}">only</c:if>">
                            <a class="hastooltip" title="<fmt:message key='label.porcentaje.de.votos.a.favor'/>" href="${resourceLink}#resume-actions"><span class="fa fa-thumbs-up" aria-hidden="true"></span>&nbsp;<strong>${porcenfavor}</strong><small>%</small></a>
                        </li>
                        <li class="bad <c:if test="${porcencontra == 0 and porcenfavor != 0}">hide</c:if><c:if test="${porcenfavor == 0 and porcencontra != 0}">only</c:if>">
                            <a class="hastooltip" title="<fmt:message key='label.porcentaje.de.votos.en.contra'/>" href="${resourceLink}#resume-actions"><span class="fa fa-thumbs-down" aria-hidden="true"></span>&nbsp;<strong>${porcencontra }</strong><small>%</small></a>
                        </li>
                    </ul>
                </div>
            </cms:device>

            <div class="media-body">
                <h4><a href="${resourceLink}" title="<fmt:message key='label.ver.propuesta.ciudadana'/>">${resource.value.Title }</a></h4>
                <cms:device type="mobile">
                    <div class="rate-resume media-object pull-left">
                            <%-- <c:if test="${voteInfo.numPro != 0 or voteInfo.numAgainst != 0 }">	 --%>
                        <c:set var="totalfavor">${voteInfo.numPro}</c:set>
                        <c:set var="totalcontra"> ${voteInfo.numAgainst}</c:set>
                        <c:set var="porcenfavor">${voteInfo.ratePro}</c:set>
                        <c:set var="porcencontra">${voteInfo.rateAgainst}</c:set>

                        <ul class="list-inline">
                            <c:if test="${porcenfavor == 0 and porcencontra == 0}">
                                <c:set var="porcenfavor" value="50" />
                                <c:set var="porcencontra" value="50" />
                            </c:if>
                            <li class="good <c:if test="${porcenfavor == 0 and porcencontra != 0 }">hide</c:if><c:if test="${porcencontra == 0 and porcenfavor != 0}">only</c:if>">
                                <a class="hastooltip" title="<fmt:message key='label.porcentaje.de.votos.a.favor'/>" href="<cms:link>${link }</cms:link>"><i class="fa fa-thumbs-up"></i>&nbsp;<strong>${porcenfavor}</strong><small>%</small></a>
                            </li>
                            <li class="bad <c:if test="${porcencontra == 0 and porcenfavor != 0}">hide</c:if><c:if test="${porcenfavor == 0 and porcencontra != 0}">only</c:if>">
                                <a class="hastooltip" title="<fmt:message key='label.porcentaje.de.votos.en.contra'/>" href="<cms:link>${link }</cms:link>"><i class="fa fa-thumbs-down"></i>&nbsp;<strong>${porcencontra }</strong><small>%</small></a>
                            </li>
                        </ul>
                    </div>
                </cms:device>
                <div class="info">
                    <c:if test="${resource.value.Area.isSet or resource.value.AreaSecondary.isSet}">
                        <ul class="list-inline">
                            <c:if test="${resource.value.Area.isSet}">
                                <li>
                                    <a href="<cms:link>${resource.value.Area}</cms:link>" class="hastooltip" title="<fmt:message key='label.proposal.area.name'/> <cms:property name='Title' file='${resource.value.Area}'/>">
                                        <span class="fa fa-cube" aria-hidden="true"></span>&nbsp;
                                        <cms:property name="Title" file="${resource.value.Area}"/>
                                    </a>
                                </li>
                            </c:if>
                            <c:if test="${resource.value.AreaSecondary.isSet}">
                                <li>
                                    <a href="<cms:link>${resource.value.AreaSecondary}</cms:link>" class="hastooltip" title="<fmt:message key='label.proposal.area.name'/> <cms:property name='Title' file='${resource.value.AreaSecondary}'/>">
                                        <span class="fa fa-cube" aria-hidden="true"></span>&nbsp;
                                        <cms:property name="Title" file="${resource.value.AreaSecondary}"/>
                                    </a>
                                </li>
                            </c:if>
                        </ul>
                        <c:out value=" | "/>
                    </c:if>
                    <c:if test="${creator ne null}">
                        <c:set var="userfullname">
                            ${creator.firstname.concat(not empty creator.lastname ? ' '.concat(creator.lastname) : '' )}
                        </c:set>

					<span class="user">
						<c:choose>
                            <c:when test="${creator.enabled}">
                                <a href="<cms:link>/users/perfil-pub/</cms:link>?pId=${creator.id}" class="hastooltip" title="Usuario">
                                    <i class="fa fa-user"></i>&nbsp;${userfullname}
                                </a>
                            </c:when>
                            <c:otherwise>
                                <i class="fa fa-user"></i>&nbsp;${userfullname}
                            </c:otherwise>
                        </c:choose>
					</span>
                        <c:out value=" | "/>
                    </c:if>

				<span class="time"><i class="fa fa-clock-o"></i>&nbsp;<fmt:message key="label.proposal.published.name"/>
				  <cms:include file="%(link.strong:/system/modules/com.saga.opencms.habla/elements/e-time.jsp)">
                      <cms:param name="date">${resource.value.Date}</cms:param>
                      <cms:param name="showtime">false</cms:param>
                      <cms:param name="shortdate">true</cms:param>
                  </cms:include>
				</span>
                    <c:out value=" | "/>

				<span class="answers">
					<c:choose>
                        <c:when test="${commentInfo.hasGovernResponse}">
                            <i class="fa fa-check-square-o"></i>&nbsp;<fmt:message key="label.proposal.govern.response"/>
                            <fmt:formatDate pattern="dd/MM/yyyy hh:mm" value="${commentInfo.dateLastGovernResponse}"/>
                        </c:when>
                        <c:otherwise>
							<span class="no-info">
								<i class="fa fa-times-circle"></i>&nbsp;<fmt:message key="label.proposal.govern.no.response"/>
							</span>
                        </c:otherwise>
                    </c:choose>
				</span>

                </div>


                <div class="clearfix margin-top-10">
                    <a href="${resourceLink}" title="<fmt:message key='label.ver.propuesta.ciudadana'/>" class="btn btn-link btn-sm pull-right"><span class="fa fa-plus" aria-hidden="true"></span>&nbsp;Ver más</a>
                    <div class="info info-comments pull-left">
						<span class="score">
							<c:if test="${voteInfo.numPro == 0}">
                                <c:set var="totalfavor">0</c:set>
                            </c:if>
							<c:if test="${voteInfo.numPro != 0}">
                                <c:set var="totalfavor">${voteInfo.numPro}</c:set>
                            </c:if>
							<c:if test="${voteInfo.numAgainst == 0}">
                                <c:set var="totalcontra">0</c:set>
                            </c:if>
							<c:if test="${voteInfo.numAgainst != 0}">
                                <c:set var="totalcontra">${voteInfo.numAgainst}</c:set>
                            </c:if>

							<span class="success hastooltip" title="<fmt:message key='label.a.favor'/>" >
								<span class="fa-stack fa-lg" aria-hidden="true">
									<span class="fa fa-circle fa-stack-2x"></span>
									<span class="fa fa-thumbs-up fa-stack-1x fa-inverse"></span>
								</span>
								<span class="number">${totalfavor }</span>
							</span>
							&nbsp;&nbsp;
							<span class="danger hastooltip" title="<fmt:message key='label.en.contra'/>">
								<span class="fa-stack fa-lg" aria-hidden="true">
									<span class="fa fa-circle fa-stack-2x"></span>
									<span class="fa fa-thumbs-down fa-stack-1x fa-inverse"></span>
								</span>
								<span class="number">${totalcontra }</span>
							</span>
						</span>
						<span class="arguments">
							<a class="hastooltip success" href="${link }?fromlisttabshow=agrees#navtabs-comments" title="<fmt:message key='label.proposal.agrees'/>">
								<span class="fa-stack fa-lg" aria-hidden="true">
									<span class="fa fa-circle fa-stack-2x"></span>
									<span class="fa fa-comment fa-stack-1x fa-inverse"></span>
								</span>
                                <span class="number">${commentInfo.totalAgrees}</span>
                            </a>
                            &nbsp;&nbsp;
							<a class="hastooltip danger" href="${link }?fromlisttabshow=disagrees#navtabs-comments" title="<fmt:message key='label.proposal.disagrees'/>">
								<span class="fa-stack fa-lg" aria-hidden="true">
									<span class="fa fa-circle fa-stack-2x"></span>
									<span class="fa fa-comment fa-stack-1x fa-inverse"></span>
								</span>
                                <span class="number">${commentInfo.totalDisagrees}</span>
                            </a>
						</span>
                        &nbsp;&nbsp;
						<span class="comments">
							<a class="hastooltip" href="${resourceLink}#comments" title="<fmt:message key='label.proposal.comments'/>">
								<span class="fa-stack fa-lg" aria-hidden="true">
									<span class="fa fa-circle fa-stack-2x"></span>
									<span class="fa fa-comment fa-stack-1x fa-inverse"></span>
								</span>
                                <c:if test="${commentInfo.totalComments == 0}">
                                    <span class="number">0</span>
                                </c:if>
                                <c:if test="${commentInfo.totalComments != 0}">
                                    <span class="number">${commentInfo.totalComments}</span>
                                </c:if>
                            </a>
						</span>

                    </div>
                </div>
            </div>
        </article>
    </li>
</fmt:bundle>