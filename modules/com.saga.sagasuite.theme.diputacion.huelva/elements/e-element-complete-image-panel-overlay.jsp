<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.microcontent.messages">

<%-- cargamos el icono del titulo si hubiera --%>

	<c:if test="${element.value.IconTitle.isSet}">
		<c:set var="btnicontitle">${element.value.IconTitle}</c:set>
		<c:set var="titleicon">
			<span aria-hidden="true" class="fa fa-${fn:toLowerCase(btnicontitle)}<c:if test="${fn:endsWith(fn:toLowerCase(btnicontitle), 'lightbulb')}">-o</c:if>"></span>&nbsp;&nbsp;
		</c:set>
	</c:if>		


<%--Iniciamos el renderizado del elemento --%>

<c:if test="${(element.value.LinkConfig.exists and element.value.LinkConfig.value.Href.isSet) and !element.value.ConfigButton.exists}">
	<c:set var="linkclass">with-link</c:set>
</c:if>
<div class="element complete-mode with-media<c:out value=' ${linkclass}' />">
	<c:if test="${(element.value.LinkConfig.exists and element.value.LinkConfig.value.Href.isSet) and !element.value.ConfigButton.exists}">
		<a href="<cms:link>${element.value.LinkConfig.value.Href}</cms:link>" target="${element.value.LinkConfig.value.Target}" title="${element.value.LinkConfig.value.Title}" <c:if test="${element.value.LinkConfig.value.Follow!=null && element.value.LinkConfig.value.Follow=='false' }">rel="nofollow"</c:if>
				style="position: absolute;left: 0;top: 0;bottom: 0;right: 0;width: 100%;height: 100%;z-index: 1">
		</a>
	</c:if>
				<%-- BLOQUE DE TEXTO --%>

				<div class="text-overlay">
					<div class="wrapper">
					<%-- ******* BLOQUE TITULO ******** --%>

					<c:if test="${hidetitle == 'false' and (element.value.Headline.isSet or element.value.SubTitle.isSet)}">
						<header>
							<c:if test="${element.value.Headline.isSet}">
								<${titletag} class="title ${titlesize}" ${element.rdfa.Headline}>
									${titleicon} ${element.value.Headline}
								</${titletag}>
							</c:if>
							<c:if test="${element.value.SubTitle.isSet}">
								<${subtitletag} class="subtitle" ${element.rdfa.SubTitle}>${element.value.SubTitle}</${subtitletag}>
							</c:if>
						</header>
					</c:if>

					<%-- ******* BLOQUE DESCRIPCION ******** --%>
					<c:if test="${element.value.Description.exists and element.value.Description.value.ContentDescription.isSet}">
						<div class="description" ${element.value.Description.rdfa.ContentDescription}>
							${element.value.Description.value.ContentDescription}
						</div>
					</c:if>

					<%-- ******* BLOQUE BOTON ******** --%>

					<c:if test="${element.value.ConfigButton.exists
					and element.value.ConfigButton.value.TextLink.isSet
					and element.value.LinkConfig.exists
					and element.value.LinkConfig.value.Href.isSet}">
						<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-boton.jsp:06a371c6-b603-11e5-8e51-7fb253176922)">
							<cms:param name="tipoboton">${element.value.ConfigButton.value.CssClass}</cms:param>
							<cms:param name="positionboton">${element.value.ConfigButton.value.Position}</cms:param>
							<cms:param name="sizeboton">${element.value.ConfigButton.value.Size}</cms:param>
							<cms:param name="textoboton">${element.value.ConfigButton.value.TextLink}</cms:param>
							<cms:param name="iconboton">${element.value.ConfigButton.value.Icon}</cms:param>
							<cms:param name="linkboton">${element.value.LinkConfig.value.Href}</cms:param>
							<cms:param name="targetboton">${element.value.LinkConfig.value.Target}</cms:param>
							<cms:param name="titleboton">${element.value.LinkConfig.value.Title}</cms:param>
							<cms:param name="followboton">${element.value.LinkConfig.value.Follow}</cms:param>
						</cms:include>
					</c:if>
					</div>
				</div>
</div>
</cms:bundle>
