<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">

	<c:set var="id">${param.id}</c:set>

<%--Cargamos variables necesarias de las imagenes --%>
<c:if test="${element.value.Image.exists and element.value.Image.value.Image.isSet}">
	<c:set var="imagePath">${element.value.Image.value.Image }</c:set>
</c:if>
<%--Iniciamos el renderizado del elemento --%>

<div class="element overlayed complete-mode with-media<c:out value=' ${param.equalheight} ${param.captionbgcolor}' />">
	<c:if test="${element.value.LinkConfig.exists and element.value.LinkConfig.value.Href.isSet}">
		<a class="overlayer" href="<cms:link>${element.value.LinkConfig.value.Href}</cms:link>" title="${element.value.LinkConfig.value.Title}" target="${element.value.LinkConfig.value.Target}">
			<span class="sr-only">${element.value.Headline}</span>
		</a>
	</c:if>
	<div class="element-wrapper">
		<div class="media-wrapper">
			<c:if test="${not empty imagePath}">
				<c:set var="imageTitle"><cms:property name="Title_${cms.locale}" file="${imagePath}"/></c:set>
				<c:if test="${empty imageTitle}">
					<c:set var="imageTitle"><cms:property name="Title" file="${imagePath}"/></c:set>
				</c:if>
				<div class="wrapper-image">
					<cms:img src="${imagePath}" width="768" height="530" scaleType="2" alt="${imageTitle}" cssclass="img-responsive"/>
				</div>
			</c:if>
			<div class="header">
				<${titletag} class="title ${titlesize}" ${element.rdfa.Headline}>
					${element.value.Headline}
				</${titletag}>
			</div>
		</div>
		<div class="caption">
			<c:if test="${element.value.Description.exists && element.value.Description.value.ContentDescription.isSet}">
				<div class="description" ${element.value.Description.rdfa.ContentDescription}>
					${element.value.Description.value.ContentDescription}
				</div>
			</c:if>
		</div>
	</div>
</div>
</cms:bundle>
