<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ page import="org.opencms.flex.CmsFlexController" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>


<fmt:setLocale value="${cms.locale}"/>
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.workplace">


<a aria-expanded="false" data-toggle="dropdown" class="dropdown-toggle" href="<cms:link>${resultspage}</cms:link>">
	<span class="fa ${iconbutton}" aria-hidden="true"></span>
</a>
<div class="dropdown-menu">
	<form class="navbar-form" role="search" id="searchForm" name="searchForm" action="<cms:link>${resultspage}</cms:link>" method="get">
		<input type="hidden" name="searchaction" value="search">
		<input type="hidden" name="searchPage" value="1">
		<div class="input-group input-group">
			<input type="text" class="form-control" placeholder="${placeholder}" id="query" name="${targetfield}">
            <span class="input-group-btn">
                <button class="btn btn-specific-main" type="submit" name="submit" value="submit">
					<span class="fa ${iconbutton}" aria-hidden="true"></span>
				</button>
            </span>
		</div>
	</form>
</div>
</cms:bundle>