<%@page buffer="none" session="false" taglibs="c,cms,fmt, fn" trimDirectiveWhitespaces="true"%>
<%-- 
Recibimos por parametros la ruta de la imagen, el ancho y el pie. 

Todos los parametros del cms:img se pueden personalizar pasandolo por parametro, en caso de no venir, buscamos en propiedades, y sino ponemos valores por defecto

Propiedades que leemos:
- image.scaleType
- image.scalePosition
- image.scaleColor 
- image.width
- image.height

Parametros que recibe:
- scaleType
- scalePosition
- scaleColor 
- width
- height
- gallery
- showBig
- imagePath
- footer
- cssClass
- thumbnails
- inline
- title

El alt y title de la imagen segun la propiedad Title y Description de la imagen.

Si el ancho de la imagen es 0, se deja con el tamano original.
--%>


<c:set var="imagePath" value="${param.imagePath}" />
<c:set var="imageName" value="${imagePath}" />


<c:if test="${fn:indexOf(imagePath, '?') != - 1}">
	<c:set var="imageName" value="${fn:substringBefore(imagePath, '?')}" />
</c:if>

<%-- Si no hay imagen que mostrar no hacemos nada --%>
<c:if test="${imagePath!=null}">

	<%-- *** scaleType *** --%>
	<c:set var="scaleType" value="${param.scaleType}" />
	<c:if test="${scaleType==null}">
	<c:set var="scaleType"><cms:property name="image.scaleType" file="search"/></c:set>
	</c:if>
	<c:if test="${scaleType==null || scaleType==''}">
	<c:set var="scaleType" value="2" /> <%-- Scale to exact target size, crop what does not fit  --%>
	</c:if>
	
	<%-- *** scalePosition *** --%>
	<c:set var="scalePosition" value="${param.scalePosition}" />
	<c:if test="${scalePosition==null}">
	<c:set var="scalePosition"><cms:property name="image.scalePosition" file="search"/></c:set>
	</c:if>
	<c:if test="${scalePosition==null}">
	<c:set var="scalePosition" value="0" /> <%-- POSITION CENTER --%>
	</c:if>
	
	<%-- *** scaleColor *** --%>
	<c:set var="scaleColor" value="${param.scaleColor}" />
	<c:if test="${scaleColor==null}">
	<c:set var="scaleColor"><cms:property name="image.scaleColor" file="search"/></c:set>
	</c:if>
	<c:if test="${scaleColor==null}">
	<c:set var="scaleColor" value="#FFF" /> <%-- BLANCO --%>
	</c:if>
	
	<%-- *** title, description y pie *** --%>
	<c:set var="imageTitle">${param.title}</c:set>
	<c:set var="imageDescription">${param.description}</c:set>
	<c:set var="footer">${param.footer }</c:set>
	
	<c:if test="${empty imageDescription || fn:indexOf(imageDescription,'???')>-1}">
		<c:set var="imageDescription">${cms:vfs(pageContext).property[imageName]['Description']}</c:set>
	</c:if>
	<c:if test="${empty imageTitle || fn:indexOf(imageTitle,'???')>-1}">
		<c:set var="imageTitle">${cms:vfs(pageContext).property[imageName]['Title']}</c:set>
	</c:if>	

	<%-- *** WIDTH y HEIGHT *** --%>
	<c:set var="imageWidth">${param.width}</c:set>
	<c:set var="imageHeight">${param.height}</c:set>
	<c:set var="widthDeafult"><cms:property name="image.width" file="search"/></c:set>
	<c:set var="heightDefault"><cms:property name="image.height" file="search"/></c:set>
	
	<%--Si el ancho de la imagen no es configurado cogemos el valor por defecto de la plantilla. Si es 0 cogemos el original --%>
	<c:if test="${empty imageWidth || fn:indexOf(imageWidth,'???')>-1}">
		<c:if test="${fn:indexOf(imagePath,'w:') == -1}">
			<c:if test="${empty widthDeafult }">
				<c:set var="imageWidth" value="null"/>
			</c:if>
			<c:if test="${not empty widthDeafult }">
				<c:set var="imageWidth">${widthDeafult }</c:set>
			</c:if>
		</c:if>
	</c:if>
	
	<%-- Si es 0, dejamos el formato original --%>
	<c:if test="${imageWidth=='0'}">
		<c:set var="imageWidth" value="null"/>
	</c:if>	
	
	<c:if test="${empty imageHeight || fn:indexOf(imageHeight,'???')>-1}">
		<c:if test="${fn:indexOf(imagePath,'h:') == -1}">
			<c:if test="${empty heightDeafult }">
				<c:set var="imageHeight" value="null"/>
			</c:if>
			<c:if test="${not empty heightDeafult }">
				<c:set var="imageHeight">${heightDeafult }</c:set>
			</c:if>
		</c:if>
	</c:if>
	
	<c:if test="${imageHeight=='0'}">	
		<c:set var="imageHeight" value="null"/>		
	</c:if>
	
	<%-- thumbnails --%>
	<c:if test="${param.thumbnails!=null && param.thumbnails=='true' }">
		<c:set var="thumbnail">thumbnail</c:set>
	</c:if>

	<%-- GENERAMOS EL HTML --%>
	<c:if test="${param.showBig == null || param.showBig == 'true' }">
		<c:if test="${param.inline == null || param.inline == 'false' }">
			<div class="saga-imagen ${param.cssClass }">
		</c:if>
		<c:if test="${param.inline != null || param.inline == 'true' }">
			<span class="saga-imagen ${param.cssClass }">
		</c:if>
				<a href="<cms:link>${imageName}</cms:link>" title="${imageDescription}" class="wrapper-image zoom-filter ${thumbnail}"
					rel="prettyPhoto[${empty param.gallery?"gallery1":param.gallery}]">			
					<div class="overlay-zoom"> 
						<img <c:if test="${param.inline == null || param.inline == 'false' }">class="img-responsive"</c:if> <cms:img src="${imagePath}" partialTag="true" scaleType="${scaleType }" 
								scalePosition="${scalePosition }" scaleColor="${scaleColor }" scaleQuality="100" width="${imageWidth}"  height="${imageHeight }" 
								alt="${imageTitle}" title="${imageTitle}"/> />
			            <div class="zoom-icon">
	                    	<i class="fa fa-search-plus"></i>
	                    </div>
                    </div>		
				</a>
			<c:if test="${not empty footer}"><span class="saga-imagen-pie">${footer}</span></c:if>
		<c:if test="${param.inline == null || param.inline == 'false' }">
			</div>
		</c:if>
		<c:if test="${param.inline != null || param.inline == 'true' }">
			</span>
		</c:if>
	</c:if>
	
	<c:if test="${param.showBig != null && param.showBig == 'false' }">
		<c:if test="${param.inline == null || param.inline == 'false' }">
			<div class="saga-imagen ${param.cssClass }">
		</c:if>
		<c:if test="${param.inline != null || param.inline == 'true' }">
			<span class="saga-imagen ${param.cssClass }">
		</c:if>
			<span class="wrapper-image ${thumbnail}">
				<img <c:if test="${param.inline == null || param.inline == 'false' }">class="img-responsive"</c:if> <cms:img src="${imagePath}" partialTag="true" scaleType="${scaleType }" 
						scalePosition="${scalePosition }" scaleColor="${scaleColor }" scaleQuality="100" width="${imageWidth}"  height="${imageHeight }" 
						alt="${imageTitle}" title="${imageTitle}"/> />
				<c:if test="${not empty footer}"><span class="saga-imagen-pie">${footer}</span></c:if>
			</span>
		<c:if test="${param.inline == null || param.inline == 'false' }">
			</div>
		</c:if>
		<c:if test="${param.inline != null || param.inline == 'true' }">
			</span>
		</c:if>
	</c:if>
	
</c:if>
