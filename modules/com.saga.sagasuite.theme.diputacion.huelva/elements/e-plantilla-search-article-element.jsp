<%@page buffer="none" session="false" taglibs="c,cms,fmt, fn" trimDirectiveWhitespaces="true" import="java.util.*,org.opencms.search.*"%>

<%-- Cargamos el locale en una variable para los campos solr --%>

<fmt:setLocale value="${cms.locale}" />
<c:set var="locale" value="${cms.locale}" />


<%-- 
Este es el element de ejemplo para el modo de visualización

Recibimos en variables por defecto los siguientes campos:
- linkDetail - Ruta donde se encuentra el elemento. No olvidar incluir este parámetro entre las etiquetas de <cms:link> para su correcto funcionamiento como enlace al detalle del elemento
- title - Título del elemento
- description - Descripción del elemento
- date - Fecha NO formateada del elemento. Un ejemplo de formateado para la misma sería: <fmt:formatDate value="${cms:convertDate(param.date)}" pattern="dd/MM/yyyy"/>
- imagePath - Ruta de la imagen del elemento

Ejemplo de uso: ${linkDetail}



####### Si necesitamos acceder a otro campo indexado por Solr la forma de acceder es esta: #######

### Accedemos al objeto de busqueda que nos llega en el request "resource"


####### Para un campo simple guardandolo ademas en una variable #######

<c:set var="title">${resource.getField('xmltitle_'.concat(cms.locale).concat('_s'))}</c:set>

####### Para un campo fecha #######

<c:set var="date">${resource.getDateField('xmldate_'.concat(cms.locale).concat('_dt'))}</c:set>


####### Para un campo multiple (por ejemplo, categorias) #######

### debe darse de alta el campo en los searchsettings del xsd para poder acceder a su titulo

<c:set var="category">${resource.getMultivaluedField('xmlcategory_'.concat(cms.locale).concat('_s'))}</c:set>

### para acceder a cada categoria sin la ruta del site por delante debe hacerse esto:

${fn:substringAfter(category,cms.requestContext.siteRoot)}</c:set>

### resultado final:

<c:forEach items="${category}" var="cat" varStatus="status">
	<c:set var="catshort">${fn:substringAfter(cat,cms.requestContext.siteRoot)}</c:set>
	<cms:property name="Title" file="${catshort}"/>
</c:forEach>


--%>

<c:set var="equal">equalheight-three</c:set>

<c:set var="image">/.template-elements/imagenes-estructura/no-img-default.png</c:set>

<c:if test="${imagePath!=null and imagePath!='' and fn:indexOf(imagePath, '???')==-1}">
	<c:set var="image">${imagePath}</c:set>
</c:if>

<li class="no-spacing-row box default-box list-element-super mb-50">
	<div class="row">
		<div class="col-xs-${imageBlockWidth} col-xxs-12 ${equal}" style="background-image: url('<cms:link>${image}</cms:link>');background-position: center center;background-color: transparent;background-size: cover;color:inherit;border: solid 1px #ddd;border-right:none;display:table;table-layout:fixed;">
			<a class="overlay-link" href="<cms:link>${linkDetail }</cms:link>" title="${titlelarge }"><span class="hover-overlay"></span></a>
		</div>
	<div class="col-xs-${textBlockWidth} col-xxs-12 description ${equal}">
		<div class="description-content">
			<div class="info">
				<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.search/elements/e-time.jsp:e8af25ec-f4c3-11e4-b076-01e4df46f753)">
					<cms:param name="date">${date }</cms:param>
					<cms:param name="shortdate">true</cms:param>
				</cms:include>
			</div>
			<header class="mb-20">
				<${titletagelement} class="title-element ${titletagelementsize}">
					<a href="<cms:link>${linkDetail }</cms:link>">
						${title}
					</a>
				</${titletagelement}>
			</header>
			<c:if test="${not empty description}">
				<div>
					${description}
				</div>
			</c:if>
		</div>
	</div>
	</div>
</li>