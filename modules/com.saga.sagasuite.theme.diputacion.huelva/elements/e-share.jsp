<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" import="org.opencms.main.OpenCms,org.opencms.file.CmsObject,org.opencms.file.CmsResource,org.opencms.jsp.CmsJspActionElement,org.opencms.jsp.util.CmsJspStandardContextBean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<fmt:setLocale value="${cms.locale}"/>
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
    <%
        CmsObject cmsObject = new CmsJspActionElement(pageContext, request, response).getCmsObject();
        //Capturamos el recurso de detalle que estamos mostrando
        CmsJspStandardContextBean contextBean = CmsJspStandardContextBean.getInstance(request);
        CmsResource r = null;
        if (contextBean.isDetailRequest()) {
            r = contextBean.getDetailContent();
        } else {
            r = cmsObject.readResource(cmsObject.getRequestContext().getUri());
        }

        String permaLink = OpenCms.getLinkManager().getOnlineLink(cmsObject, r.getRootPath()) ;
        pageContext.setAttribute("permaLink", permaLink);

        //Comprobamos el estado para mostrar aviso en caso de no estar publicado
        if(!r.getState().isUnchanged()) {
            pageContext.setAttribute("noPublish", true);
        }
    %>

    <c:set var="socialshareresourcelink"><cms:link>${param.socialshareresource}</cms:link></c:set>
    <cms:contentload collector="singleFile" param="${socialshareresourcelink}" editable="false">
        <cms:contentaccess var="content" />

        <div class="element parent sg-share share-box">
            <div class="wrapper clearfix">
                <c:forEach var="elem" items="${content.subValueList['Element']}" varStatus="status">
                    <c:set var="element" value="${elem }"/>
                    <c:choose>
                        <%-- Code --%>
                        <c:when test="${elem.name == 'Code'}">
                            ${element.value.Code }
                        </c:when>
                        <%-- Custom --%>
                        <c:when test="${elem.name == 'Custom'}">
                            <button class="btn btn-specific-main hastooltip tooltip-right" title="<fmt:message key="key.element.share.share"/>" data-target="#sharebox" data-toggle="collapse" aria-expanded="false" aria-controls="sharebox">
                                <span class="pe-7s-share" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.element.share.share.access"/></span>
                            </button>
                            <ul class="list-inline icons32 collapse" id="sharebox">
                                <c:if test="${elem.value.ShowFacebook == 'true'}">
                                    <li>
                                        <a href="http://www.facebook.com/sharer/sharer.php?u=${permaLink}" target="_blank" class="redes facebook sgshare-facebook-share" title="<fmt:message key="key.element.share.facebook"/>">
                                            <fmt:message key="key.element.share.facebook.access"/>
                                        </a>
                                    </li>
                                </c:if>
                                <c:if test="${elem.value.ShowTwitter == 'true'}">
                                    <li>
                                        <a href="http://twitter.com/home?status=${titlePage }:${permaLink }" target="_blank" class="redes twitter" title="<fmt:message key="key.element.share.twitter"/>">
                                            <fmt:message key="key.element.share.twitter.access"/>
                                        </a>
                                    </li>
                                </c:if>
                                <c:if test="${elem.value.ShowGooglePlus == 'true'}">
                                    <li>
                                        <a id="linkGoogle" href="https://plus.google.com/share?url=${permaLink }" target="_blank" class="redes google" title="<fmt:message key="key.element.share.googleplus"/>">
                                            <fmt:message key="key.element.share.googleplus.access"/>
                                        </a>
                                    </li>
                                </c:if>
                                <c:if test="${elem.value.ShowLinkedin == 'true'}">
                                    <li>
                                        <a href="http://www.linkedin.com/shareArticle?mini=true&url=${permaLink }&title=${titlePage }&summary=&source=" target="_blank" class="redes linkedin" title="<fmt:message key="key.element.share.linkedin"/>">
                                            <fmt:message key="key.element.share.linkedin.access"/>
                                        </a>
                                    </li>
                                </c:if>
                                <c:if test="${elem.value.ShowPinterest == 'true'}">
                                    <li>
                                        <a href="http://pinterest.com/pin/create/button/?url=${permaLink }&description=${titlePage }" target="_blank" class="redes pinterest" title="<fmt:message key="key.element.share.pinterest"/>">
                                            <fmt:message key="key.element.share.pinterest.access"/>
                                        </a>
                                    </li>
                                </c:if>
                                <c:if test="${elem.value.ShowDelicious == 'true'}">
                                    <li>
                                        <a href="http://delicious.com/save?v=5&url=${permaLink }&title=${titlePage }" target="_blank" class="redes delicious" title="<fmt:message key="key.element.share.delicious"/>">
                                            <fmt:message key="key.element.share.delicious.access"/>
                                        </a>
                                    </li>
                                </c:if>
                            </ul>
                        </c:when>
                    </c:choose>
                </c:forEach>
            </div>
        </div>
    </cms:contentload>
</cms:bundle>
