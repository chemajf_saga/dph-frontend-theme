<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<fmt:setLocale value="${cms.locale}"/>
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
	<%-- leemos la propiedad de 'titulo para encabezado' si la hubiere para usarla como texto de encabezado de pagina --%>
	<c:set var="propheadingtitle">
		<cms:property name="sagasuite.headingtitle" file="search" />
	</c:set>
	<c:set var="subheadertitlefrompage"><cms:property name="NavText" file="search" /></c:set>
	<c:if test="${empty subheadertitlefrompage }">
		<c:set var="subheadertitlefrompage"><cms:property name="Title" file="search" /></c:set>
	</c:if>
	<c:if test="${empty propheadingtitle }">
		<c:set var="subheadertitle">${subheadertitlefrompage}</c:set>
	</c:if>
	<c:if test="${not empty propheadingtitle }">
		<c:set var="subheadertitle">${propheadingtitle}</c:set>
	</c:if>


	<c:set var="propheadingtitle">
		<cms:property name="sagasuite.headingtitle" file="search" />
	</c:set>
	<c:set var="hidebreadcrumb">
		<cms:property name="sagasuite.hidepageheadingbreadcrumb" file="search" />
	</c:set>
	<c:set var="titleblockmodifierclass"></c:set>
	<c:if test="${hidebreadcrumb == 'true'}">
		<c:set var="titleblockmodifierclass">no-breadcrumb</c:set>
	</c:if>

	<%-- Definimos el tipo de etiqueta html para nuestro encabezado --%>
	<c:set var="titletag">h1</c:set>
	<c:if test="${cms.detailRequest}">
		<c:set var="titletag">div</c:set>
	</c:if>
	<%-- leemos la propiedad de 'imagen para encabezado' si la hubiere para usarla como imagen de fondo de encabezado de pagina y machacamos lo que tenga el .themeconfig--%>
	<c:set var="pageheadingimage">
		<cms:property name="sagasuite.imagetitle" file="search" />
	</c:set>

	<c:if test="${empty pageheadingimage}">
		<%-- leemos el campo de 'pageheadingimage' del .themeconfig para usarlo como imagen de fondo en el encabezado --%>
		<c:set var="pageheadingimage">${themeConfiguration.CustomFieldKeyValue.pageheadingimage}</c:set>
	</c:if>
	<c:if test="${fn:contains(pageheadingimage, '?')}">
		<c:set var="pageheadingimage">${fn:substringBefore(pageheadingimage, '?')}</c:set>
	</c:if>
	<c:set var="pageheadingimagetitle"><cms:property name="Title_${cms.locale}" file="${pageheadingimage}"/></c:set>
	<c:if test="${empty pageheadingimagetitle}">
		<c:set var="pageheadingimagetitle"><cms:property name="Title" file="${pageheadingimage}"/></c:set>
	</c:if>

	<%-- Leemos si existe imagen de encabezado: logotipo --%>
	<c:set var="pageheadingimagemain"></c:set>
	<c:set var="pageheadingimagemain">
		<cms:property name="sagasuite.imagetitlemain" file="search" />
	</c:set>
	<c:if test="${fn:contains(pageheadingimagemain, '?')}">
		<c:set var="pageheadingimagemainpath">${fn:substringBefore(pageheadingimagemain, '?')}</c:set>
	</c:if>
	<c:set var="pageheadingimagemaintitle"><cms:property name="Title_${cms.locale}" file="${pageheadingimagemainpath}"/></c:set>
	<c:if test="${empty pageheadingimagemaintitle}">
		<c:set var="pageheadingimagemaintitle"><cms:property name="Title" file="${pageheadingimagemainpath}"/></c:set>
	</c:if>

	<%-- Leemos la propiedad de "Oscurecer fondo" para controlar si incluimos la capa de overlay o no --%>
	<c:set var="bgdark"></c:set>
	<c:set var="bgdark">
		<cms:property name="sagasuite.pageheadingbgdark" file="search" />
	</c:set>
	<c:if test="${not empty bgdark and bgdark == 'false'}">
		<c:set var="bgdarkclass"></c:set>
	</c:if>
	<c:if test="${empty bgdark or bgdark == 'true'}">
		<c:set var="bgdarkclass">breadcrumbs-bgdark</c:set>
	</c:if>
	<%-- Leemos la propiedad de "Mostrar menú de anclas" para controlar el alto y el tamaño de la imagen de encabezado --%>
	<c:set var="showportadamenu"></c:set>
	<c:set var="showportadamenu">
		<cms:property name="dph.portadashowmenu" file="search" />
	</c:set>

	<%--<c:set var="styleimage">background-image:url('<cms:link>${pageheadingimage}</cms:link>');background-repeat:no-repeat;background-color:transparent;background-position:center center;background-size:cover;color:inherit</c:set>--%>
	<c:set var="styleheight">height:380px;</c:set>
	<c:set var="styleheightwrapper">height:190px;</c:set>
	<%-- Guardamos la ruta del enlace a la portdada de la zona --%>
	<c:set var="actualUrl">${cms.requestContext.uri}</c:set>
	<c:set var="actualUrlLevels" value="${fn:split(actualUrl,'/' )}" />
	<c:set var="actualFirstLevel" value="${actualUrlLevels[0]}" />
	<c:set var="actualUrlFirstLevel" value="/${actualFirstLevel}/" />
	<c:if test="${not empty param.dphportada and param.dphportada == 'true'}">
		<c:set var="dphportada">true</c:set>
		<c:set var="styleheight">height:500px;</c:set>
		<c:set var="styleheightwrapper">height:250px;</c:set>
		<c:set var="showmenumodifierclass">portada-menu</c:set>
		<c:if test="${not empty showportadamenu and showportadamenu == 'false' and not empty pageheadingimagemain}">
			<c:set var="styleheight">height:500px;</c:set>
			<c:set var="styleheightwrapper">height:320px;</c:set>
			<c:set var="showmenumodifierclass">portada-no-menu</c:set>
		</c:if>
	</c:if>
	<div class="parent header-navigation">
		<div class="breadcrumbs<c:out value=' ${bgdarkclass} ${showmenumodifierclass}' />">
			<div class="table-block-wrapper" data-parallax="scroll" data-class-name="parallax-subheader" data-speed="0.05" data-image-src="<cms:link>${pageheadingimage}</cms:link>" style="${styleheight}">
				<div class="container table-block ${titleblockmodifierclass}" style="${styleheightwrapper}">
					<%-- Si no hay imagen principal definida en las propiedades --%>
					<c:if test="${empty pageheadingimagemain}">
						<${titletag} class="title-block table-cell no-margin">
								<span class="title-block-text disp-block">
									<a href="${actualUrlFirstLevel}" title="<fmt:message key='key.element.subheader.go.zone.home' />">
										<span class="inline-b title-block-main-text">${subheadertitle}</span>
									</a>
									<c:if test="${not empty propheadingtitle and propheadingtitle != subheadertitlefrompage }">
										<span class="inline-b v-align-m title-block-sec-text">${subheadertitlefrompage}</span>
									</c:if>
								</span>
						</${titletag}>
					</c:if>
					<%-- Si hay imagen principal definida en las propiedades --%>
					<c:if test="${not empty pageheadingimagemain}">
						<div class="title-block table-cell no-margin">
							<div class="title-block-text disp-block">
								<${titletag} class="sr-only pos-absolute">${subheadertitle}</${titletag}>
								<c:set var="imgmodifierclass"></c:set>
								<c:if test="${not empty propheadingtitle and propheadingtitle != subheadertitlefrompage }">
									<c:set var="imgmodifierclass">no-margin inline-b</c:set>
								</c:if>
								<c:set var="imgheight">130</c:set>
								<%-- si es portada --%>
								<c:if test="${dphportada == 'true'}">
									<c:set var="imgheight">130</c:set>
									<c:if test="${not empty showportadamenu and showportadamenu == 'false'}">
										<c:set var="imgheight">180</c:set>
									</c:if>
								</c:if>
								<a href="${actualUrlFirstLevel}" title="<fmt:message key='key.element.subheader.go.zone.home' />">
									<cms:img src="${pageheadingimagemain}" cssclass="img-responsive ${imgmodifierclass}" height="${imgheight}" />
								</a>
								<c:if test="${not empty propheadingtitle and propheadingtitle != subheadertitlefrompage }">
									<span class="inline-b title-block-sec-text ml-40">${subheadertitlefrompage}</span>
								</c:if>
							</div>
						</div>
					</c:if>
					<%//Ocultamos las migas de pan para movil%>
					<c:if test="${empty hidebreadcrumb or hidebreadcrumb == 'false'}">
						<cms:device type="desktop,tablet">
							<cms:include file="%(link.weak:/system/modules/com.saga.sagasuite.menu/elements/nav-breadcrumb-table-cell.jsp:1738bb9f-d3c5-11e5-afc7-7fb253176922)">
								<cms:param name="startlevel">0</cms:param>
							</cms:include>
						</cms:device>
					</c:if>
				</div>
			</div>
		</div><!--/table-block-->
		<c:if test="${not empty pageheadingimagetitle}">
			<script>
				$(window).load(function() {
					$('.parallax-mirror img').attr('alt', '${pageheadingimagetitle}');
				});
			</script>
		</c:if>
	</div><!--/breadcrumbs-->
</cms:bundle>