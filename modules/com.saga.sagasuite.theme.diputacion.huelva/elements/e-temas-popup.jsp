<%@ page import="org.opencms.relations.CmsCategoryService" %>
<%@ page import="org.opencms.jsp.util.CmsJspStandardContextBean" %>
<%@ page import="org.opencms.file.CmsObject" %>
<%@ page import="org.opencms.relations.CmsCategory" %>
<%@ page import="java.util.List" %>
<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates" %>

<fmt:setLocale value="${cms.locale}"/>
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
    <%
        CmsObject cmso = CmsJspStandardContextBean.getInstance(request).getVfs().getCmsObject();
        CmsCategoryService catSer = CmsCategoryService.getInstance();
        List<CmsCategory> cmsCategories = catSer.readCategories(cmso, "/temas", false, "/.categories");
        pageContext.setAttribute("categories", cmsCategories);
    %>

    <h2>
        <fmt:message key="key.template.element.tematicas" />
    </h2>
    <ul class="list-unstyled row list-tematicas">
        <c:forEach items="${categories}" var="category" varStatus="status">
            <c:set var="title">
                <sg:readProperty path="${category.rootPath}" prop="Title_${cms.locale}"/>
            </c:set>
            <c:if test="${empty title}">
                <c:set var="title">
                    <sg:readProperty path="${category.rootPath}" prop="Title"/>
                </c:set>
            </c:if>
            <c:set var="iconlibrary">fa</c:set>
            <c:set var="icon">fa fa-cube</c:set>
            <c:set var="iconimage"></c:set>
            <c:set var="icon">
                <sg:readProperty path="${category.rootPath}" prop="sagasuite.tema.icono"/>
            </c:set>
            <c:set var="iconimage">
                <sg:readProperty path="${category.rootPath}" prop="sagasuite.tema.iconoimagen"/>
            </c:set>
            <c:set var="url">
                <sg:readProperty path="${category.rootPath}" prop="sagasuite.tema.page"/>
            </c:set>
            <c:set var="iconlibrary">
                <sg:readProperty path="${category.rootPath}" prop="sagasuite.tema.biblioteca.icono"/>
            </c:set>
            <li class="col-xxs-6 col-xs-4 col-md-3">
                <a href="<cms:link>${url}</cms:link>" class="no-text equalheighttemapopupsboxes">
                    <c:if test="${empty iconimage}">
                        <c:if test="${iconlibrary == 'fa'}">
                            <span class="icon-tematica fa fa-${icon} fs24" aria-hidden="true"></span>
                        </c:if>
                        <c:if test="${iconlibrary == 'pe'}">
                            <span class="icon-tematica ${icon} fs36" aria-hidden="true"></span>
                        </c:if>
                    </c:if>
                    <c:if test="${not empty iconimage}">
                        <c:set var="iconimagealt">
                            <cms:property name="Title" file="${iconimage}" />
                        </c:set>
                        <cms:img src="${iconimage}" alt="${iconimagealt}" width="24"/>
                    </c:if>
                    <span class="title-text">${title}</span>
                </a>
            </li>
            <c:if test="${status.count > 1 and status.count % 2 == 0}">
                <li class="hidden-xs hidden-sm hidden-md hidden-lg col-xxs-12 clearflix no-margin" aria-hidden="true"></li>
            </c:if>
            <c:if test="${status.count > 2 and status.count % 3 == 0}">
                <li class="hidden-xxs hidden-md hidden-lg col-xs-12 clearflix no-margin" aria-hidden="true"></li>
            </c:if>
            <c:if test="${status.count > 3 and status.count % 4 == 0}">
                <li class="hidden-xxs hidden-xs hidden-sm col-md-12 clearflix no-margin" aria-hidden="true"></li>
            </c:if>
        </c:forEach>
    </ul>
    <script>
        (function(){
            jQuery.fn.equalHeights = function() {
                var maxHeight = 0;
                // get the maximum height
                this.each(function(){
                    var $this = jQuery(this);
                    if ($this.height() > maxHeight) { maxHeight = $this.height(); }
                });
                // set the elements height
                this.each(function(){
                    var $this = jQuery(this);
                    $this.height(maxHeight);
                });
            };

            jQuery(window).load(function() {
                var classes = ["equalheighttemapopupsboxes","equalheighttemapopupsboxes-two"];
                for (var i = 0, j = classes.length; i < j; i++) {
                    jQuery('.' + classes[i]).equalHeights();
                }
            });
        })();
    </script>
</cms:bundle>
