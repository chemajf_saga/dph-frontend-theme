<%@ page import="org.opencms.relations.CmsCategoryService" %>
<%@ page import="org.opencms.jsp.util.CmsJspStandardContextBean" %>
<%@ page import="org.opencms.file.CmsObject" %>
<%@ page import="org.opencms.relations.CmsCategory" %>
<%@ page import="java.util.List" %>
<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates" %>

<fmt:setLocale value="${cms.locale}"/>
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
    <%
        CmsObject cmso = CmsJspStandardContextBean.getInstance(request).getVfs().getCmsObject();
        CmsCategoryService catSer = CmsCategoryService.getInstance();
        List<CmsCategory> cmsCategories = catSer.readCategories(cmso, "/temas", false, "/.categories");
        pageContext.setAttribute("categories", cmsCategories);
    %>

    <ul class="list-inline list-tematicas">
        <li aria-hidden="true" class="small">
            <fmt:message key="key.template.element.tematicas" />
        </li>
        <c:forEach items="${categories}" var="category">
            <c:set var="title">
                <sg:readProperty path="${category.rootPath}" prop="Title_${cms.locale}"/>
            </c:set>
            <c:if test="${empty title}">
                <c:set var="title">
                    <sg:readProperty path="${category.rootPath}" prop="Title"/>
                </c:set>
            </c:if>
            <c:set var="iconlibrary">fa</c:set>
            <c:set var="icon">fa fa-cube</c:set>
            <c:set var="iconimage"></c:set>
            <c:set var="icon">
                <sg:readProperty path="${category.rootPath}" prop="sagasuite.tema.icono"/>
            </c:set>
            <c:set var="iconimage">
                <sg:readProperty path="${category.rootPath}" prop="sagasuite.tema.iconoimagen"/>
            </c:set>
            <c:set var="url">
                <sg:readProperty path="${category.rootPath}" prop="sagasuite.tema.page"/>
            </c:set>
            <c:set var="iconlibrary">
                <sg:readProperty path="${category.rootPath}" prop="sagasuite.tema.biblioteca.icono"/>
            </c:set>
            <li>
                <a href="<cms:link>${url}</cms:link>" class="hastooltip no-text">
                    <c:if test="${empty iconimage}">
                        <c:if test="${iconlibrary == 'fa'}">
                            <span class="icon-tematica fa fa-${icon} fs24" aria-hidden="true"></span>
                        </c:if>
                        <c:if test="${iconlibrary == 'pe'}">
                            <span class="icon-tematica ${icon} fs36" aria-hidden="true"></span>
                        </c:if>
                    </c:if>
                    <c:if test="${not empty iconimage}">
                        <c:set var="iconimagealt">
                            <cms:property name="Title" file="${iconimage}" />
                        </c:set>
                        <cms:img src="${iconimage}" alt="${iconimagealt}" width="24"/>
                    </c:if>
                    <span class="sr-only">${title}</span>
                </a>
            </li>
        </c:forEach>
    </ul>
</cms:bundle>
