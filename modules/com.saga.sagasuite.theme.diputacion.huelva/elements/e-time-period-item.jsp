<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
	<c:set var="spacer" value=" "/>
	<c:set var="initime"></c:set>
	<c:set var="fintime"></c:set>
	<c:if test="${param.showinitime=='true' }">
		<c:set var="initime">
			<c:set var="spacer" value=" "/>
			&nbsp;<span class="small"><fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="'('HH:mm')'" type="date" /></span>
		</c:set>
	</c:if>
	<c:if test="${param.showfintime=='true' }">
		<c:set var="fintime">
			&nbsp;<span class="small"><fmt:formatDate value="${cms:convertDate(param.findate)}" pattern="'('HH:mm')'" type="date" /></span>
		</c:set>
	</c:if>
	<c:if test="${not empty param.inidate }">
		<c:set var="inidate">
			<time datetime="<fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ" />">
				<fmt:formatDate value="${cms:convertDate(param.inidate)}" type="date" dateStyle="LONG"/>
			</time>
		</c:set>
		<c:set var="mesini"><fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="MMMM" type="date" /></c:set>
		<c:set var="yearini"><fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="YYYY" type="date" /></c:set>
	</c:if>
	<c:if test="${not empty param.findate }">
		<c:set var="inidate">
			<fmt:formatDate value="${cms:convertDate(param.inidate)}" type="date" pattern="dd MMMM"/>
		</c:set>
		<c:set var="dayini"><fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="dd" type="date" /></c:set>
		<c:set var="findate">
			<fmt:formatDate value="${cms:convertDate(param.findate)}" type="date"  pattern="dd MMMM"/>
		</c:set>
		<c:set var="dayfin"><fmt:formatDate value="${cms:convertDate(param.findate)}" pattern="dd" type="date" /></c:set>
		<c:set var="mesfin"><fmt:formatDate value="${cms:convertDate(param.findate)}" pattern="MMMM" type="date" /></c:set>
		<c:set var="yearfin"><fmt:formatDate value="${cms:convertDate(param.findate)}" pattern="YYYY" type="date" /></c:set>
	</c:if>
	<c:set var="showbothyears">false</c:set>
	<c:if test="${yearini != yearfin }">
		<c:set var="showbothyears">true</c:set>
	</c:if>
	<c:set var="showbothmonths">false</c:set>
	<c:if test="${mesini != mesfin }">
		<c:set var="showbothmonths">true</c:set>
	</c:if>
	<c:set var="completedate">
		<time datetime="<fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ" />">
			${inidate}${initime}
		</time>
	</c:set>
	<c:if test="${not empty param.findate }">
		<c:set var="completedate">
			<time datetime="<fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ" />">
				${dayini}${initime}${spacer}-${spacer}${dayfin}${fintime}${spacer}${mesini}${spacer}${yearfin}
			</time>
		</c:set>
		<c:if test="${showbothyears == 'true'}">
			<c:set var="completedate">
				<time datetime="<fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ" />">
					${inidate}${initime}${spacer}${yearini}${spacer}-${spacer}${findate}${fintime}${spacer}${yearfin}
				</time>
			</c:set>
		</c:if>
		<c:if test="${showbothyears == 'false' and showbothmonths == 'true'}">
			<c:set var="completedate">
				<time datetime="<fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ" />">
					${inidate}${initime}${spacer}-${spacer}${findate}${fintime}${spacer}${yearfin}
				</time>
			</c:set>
		</c:if>
	</c:if>
	${completedate}
</cms:bundle>