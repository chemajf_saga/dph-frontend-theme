<%@page buffer="none" session="false" taglibs="c,cms,fmt"%>
<c:if test="${not empty param.inidate }">

	<c:if test="${empty param.showtime || param.showtime==false }">
		<c:if test="${not empty param.findate }">
			<time datetime="<fmt:formatDate value="${cms:convertDate(param.date)}" pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ" />">
				<c:set var="mesini"><fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="MMMM" type="date" /></c:set>
				<c:set var="mesfin"><fmt:formatDate value="${cms:convertDate(param.findate)}" pattern="MMMM" type="date" /></c:set>
				<c:if test="${mesini == mesfin }">
					<c:if test="${empty param.showinitime || param.showinitime==false}">
						<fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="'Del' dd ' '" type="date" />
					</c:if>
					<c:if test="${param.showinitime==true}">
						<fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="'Del' dd 'a las' HH:mm" type="date" />
					</c:if>
					<c:if test="${empty param.showfintime || param.showfintime==false}">
						<fmt:formatDate value="${cms:convertDate(param.findate)}" pattern="'al' dd 'de' MMMM 'de' yyyy" type="date" />
					</c:if>
					<c:if test="${param.showfintime==true}">
						<fmt:formatDate value="${cms:convertDate(param.findate)}" pattern="'al' dd 'de' MMMM 'de' yyyy 'a las' HH:mm" type="date" />
					</c:if>					
				</c:if>
				<c:if test="${mesini != mesfin }">
					<c:if test="${empty param.showinitime || param.showinitime==false}">
						<fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="'Del' dd 'de' MMMM ' '" type="date" />
					</c:if>
					<c:if test="${param.showinitime==true}">
						<fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="'Del' dd 'de' MMMM 'a las' HH:mm" type="date" />
					</c:if>
					<c:if test="${empty param.showfintime || param.showfintime==false}">
						<fmt:formatDate value="${cms:convertDate(param.findate)}" pattern="'al' dd 'de' MMMM 'de' yyyy" type="date" />
					</c:if>
					<c:if test="${param.showfintime==true}">
						<fmt:formatDate value="${cms:convertDate(param.findate)}" pattern="'al' dd 'de' MMMM 'de' yyyy 'a las' HH:mm" type="date" />
					</c:if>	
				</c:if>				
			</time>
		</c:if>
		<c:if test="${empty param.findate }">
			<time datetime="<fmt:formatDate value="${cms:convertDate(param.date)}" pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ" />">
				<c:if test="${empty param.showinitime || param.showinitime==false}">
					<fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="EEEE',' dd 'de' MMMM 'de' yyyy" type="date" />
				</c:if>
				<c:if test="${param.showinitime==true}">
					<fmt:formatDate value="${cms:convertDate(param.inidate)}" pattern="EEEE',' dd 'de' MMMM 'de' yyyy 'a las' HH:mm" type="date" />
				</c:if>
			</time>
		</c:if>			
	</c:if>
	
	
</c:if>
