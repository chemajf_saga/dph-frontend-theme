﻿<%@page buffer="none" session="false" taglibs="c,cms,fmt"%>
<c:if test="${not empty param.date }">
	<c:if test="${param.showtime==true }">
		<c:if test="${empty param.onlytime || param.onlytime==false }">
			<time datetime="<fmt:formatDate value="${cms:convertDate(param.date)}" pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ" />">
				<fmt:formatDate value="${cms:convertDate(param.date)}" dateStyle="SHORT" timeStyle="SHORT" type="both" />
			</time>
		</c:if>
		<c:if test="${param.onlytime==true }">
			<time datetime="<fmt:formatDate value="${cms:convertDate(param.date)}" pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ" />">
				<fmt:formatDate value="${cms:convertDate(param.date)}" dateStyle="SHORT" timeStyle="SHORT" type="time" />
			</time>
		</c:if>		
	</c:if>
	<c:if test="${empty param.showtime || param.showtime==false }">
		<c:if test="${empty param.shortdate || param.shortdate==false }">
			<time datetime="<fmt:formatDate value="${cms:convertDate(param.date)}" pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ" />">
				<fmt:formatDate value="${cms:convertDate(param.date)}" pattern="EEEE',' dd 'de' MMMM 'de' yyyy" type="date" />
			</time>
		</c:if>
		<c:if test="${param.shortdate==true }">
			<time datetime="<fmt:formatDate value="${cms:convertDate(param.date)}" pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ" />">
				<fmt:formatDate value="${cms:convertDate(param.date)}" pattern="dd 'de' MMMM 'de' yyyy" type="date" />
			</time>
		</c:if>	
	</c:if>
</c:if>
