<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>


<fmt:setLocale value="${cms.locale}"/>
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
    <c:set var="showweather">${themeConfiguration.CustomFieldKeyValue.showweather}</c:set>
    <c:if test="${showweather == 'true'}">
        <c:set var="urlweather">${themeConfiguration.CustomFieldKeyValue.urlweather}</c:set>
        <c:if test="${empty urlweather}">
            <c:set var="urlweather">http://www.eltiempo24.es/Sevilla-ES0SE0093.html</c:set>
        </c:if>
    </c:if>
    <c:set var="urlweathersection">${themeConfiguration.CustomFieldKeyValue.urlweathersection}</c:set>
    <c:if test="${empty urlweathersection}">
        <c:set var="urlweathersection">/municipio/el-tiempo/</c:set>
    </c:if>

    <c:set var="endurlweather">${fn:substringAfter(urlweather, '-ES')}</c:set>
    <c:set var="idweather">${fn:substringBefore(endurlweather, '.html')}</c:set>
    <a class="w_x_linket24" href="<cms:link>${urlweathersection}</cms:link>" title="<fmt:message key="key.element.weather.titlelink" /> ">
        <div>
            <script src='http://www.eltiempo24.es/svc/remote/miniwidget/?point=ES${idweather}&l=0&temp=1'></script>
        </div>
    </a>
</cms:bundle>
