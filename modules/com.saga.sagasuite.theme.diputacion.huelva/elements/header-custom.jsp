<%@page buffer="none" session="false" taglibs="c,cms,fmt"%>
<fmt:setLocale value="${cms.locale}" />

<cms:bundle basename="com.saga.sagasuite.header">
	<c:set var="toggleaccess"><fmt:message key="label.header.menu.access.toggle"/></c:set>
	<c:set var="backhome"><fmt:message key="label.message.back.home"/></c:set>
	<c:set var="buscador"><fmt:message key="label.message.search"/></c:set>
	<c:set var="menuname"><fmt:message key="label.menu.principal"/></c:set>
</cms:bundle>

<%-- MODELO CABECERA DESKTOP --%>

<cms:device type="desktop">

<div id="headerblock">

	<div id="${content.value.Id }" class="header-cons">
		<div class="header-top clearfix">
			<div class="container">			
				<c:if test="${content.value.Block3.exists }">
					<div class="pull-right">
						<c:set var="elements" value="${content.subValueList['Block3'] }" scope="request"/>
						<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
					</div>
				</c:if>
				<c:if test="${content.value.Block2.exists }">
					<div class="pull-right">
						<c:set var="elements" value="${content.subValueList['Block2'] }" scope="request"/>
						<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
					</div>
				</c:if>	
			</div>
		</div>
		<div class="header-inf">
			<div class="wrapper">
				<div class="container">
					<div class="row">
						<div class="col-sm-6 clearfix logo-block">
							<c:if test="${content.value.Block4.exists }">
								<c:set var="elements" value="${content.subValueList['Block4'] }" scope="request"/>
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
							</c:if>
						</div>
						<div class="col-sm-6 right-block">
							<div class="clearfix">
								<div class="pull-right">
									<c:if test="${content.value.Block5.exists }">
										<c:set var="elements" value="${content.subValueList['Block5'] }" scope="request"/>
										<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
									</c:if>											
								</div>
							</div>
							<div class="clearfix">
								<c:if test="${content.value.Block1.exists }">
									<div class="pull-right">
										<c:set var="elements" value="${content.subValueList['Block1'] }" scope="request"/>
										<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
									</div>
								</c:if>							
							</div>																
						</div>
					</div>
				</div>
			</div>			
			<div id="navigation">
				<div class="clearfix container">
					<c:if test="${content.value.Block6.exists }">
						<c:set var="elements" value="${content.subValueList['Block6'] }" scope="request"/>
						<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
					</c:if>
				</div>
			</div>	
		</div>
	</div>

</div>

</cms:device>
<%-- FIN DESKTOP --%>


<%-- MODELO CABECERA TABLET --%>

<cms:device type="tablet">

<div id="headerblock">

	<div id="${content.value.Id }" class="header-cons header-responsive header-tablet">
			<div id="header-sup">
				<div class="header-top clearfix">
					<c:if test="${content.value.Block1.exists }">
						<div class="pull-left">
							<c:set var="elements" value="${content.subValueList['Block1'] }" scope="request"/>
							<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
						</div>
					</c:if>
					<c:if test="${content.value.Block3.exists }">
						<div class="pull-right">
							<c:set var="elements" value="${content.subValueList['Block3'] }" scope="request"/>
							<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
						</div>
					</c:if>
					<c:if test="${content.value.Block2.exists }">
						<div class="pull-right">
							<c:set var="elements" value="${content.subValueList['Block2'] }" scope="request"/>
							<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
						</div>
					</c:if>	
				</div>
			</div>		
			<div class="navbar navbar-default">
				<div class="row">
						<div class="logo-block col-xxs-10">
							<c:if test="${content.value.Block4.exists }">
								<c:set var="elements" value="${content.subValueList['Block4'] }" scope="request"/>
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
							</c:if>
						</div>
						<div class="col-xxs-2">
							<div class="access">
								<a href="#" class="navbar-brand to-menu" data-target=".navbar-responsive-collapse" data-toggle="collapse"><span class="sr-only">${menuname }</span><span class="fa fa-reorder"></span></a>
								<%-- <a href="#" class="navbar-brand to-search" data-target=".navbar-search-collapse" data-toggle="collapse"><span class="sr-only">${buscador }</span><span class="fa fa-search"></span></a> --%>
							</div>
						</div>
				</div>
			</div>
			<div id="navigation">
				<div class="navbar navbar-default" role="navigation">
						<div class="navbar-responsive-collapse navbar-collapse collapse">
							<c:if test="${content.value.Block5.exists}">
								<div class="clearfix">
									<div class="buscador-block">
										<c:if test="${content.value.Block5.exists }">
											<c:set var="elements" value="${content.subValueList['Block5'] }" scope="request"/>
											<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
										</c:if>	
									</div>											
								</div>
							</c:if>																		
 							<c:if test="${content.value.Block6.exists }">
								<c:set var="elements" value="${content.subValueList['Block6'] }" scope="request"/>
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
							</c:if>
							<c:if test="${content.value.Block7.exists }">
								<c:set var="elements" value="${content.subValueList['Block7'] }" scope="request"/>
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
							</c:if>													
						</div>
				</div>				
			</div>	
	</div>

</div>
</cms:device>
<%-- FIN TABLET --%>

<%-- MODELO CABECERA MOVIL --%>

<cms:device type="mobile">

<div id="headerblock">

	<div id="${content.value.Id }" class="header-cons header-responsive header-mobile">
			<div class="navbar navbar-default">
				<div class="row">
					<div class="logo-block col-xxs-10">
						<c:if test="${content.value.Block4.exists }">
							<c:set var="elements" value="${content.subValueList['Block4'] }" scope="request"/>
							<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
						</c:if>
					</div>
					<div class="col-xxs-2">
						<div class="access">
							<a href="#" class="navbar-brand to-menu" data-target=".navbar-responsive-collapse" data-toggle="collapse"><span class="sr-only">${menuname }</span><span class="fa fa-reorder"></span></a>
							<%-- <a href="#" class="navbar-brand to-search" data-target=".navbar-search-collapse" data-toggle="collapse"><span class="sr-only">${buscador }</span><span class="fa fa-search"></span></a> --%>
						</div>
					</div>
				</div>
			</div>

			<div id="navigation">
				<div class="navbar navbar-default" role="navigation">
						<div class="navbar-responsive-collapse navbar-collapse collapse">
							<c:if test="${content.value.Block5.exists}">
								<div class="clearfix">
									<div class="buscador-block">
										<c:if test="${content.value.Block5.exists }">
											<c:set var="elements" value="${content.subValueList['Block5'] }" scope="request"/>
											<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
										</c:if>
									</div>													
								</div>
							</c:if>												
							<c:if test="${content.value.Block6.exists }">
								<c:set var="elements" value="${content.subValueList['Block6'] }" scope="request"/>
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
							</c:if>
							<c:if test="${content.value.Block7.exists }">
								<c:set var="elements" value="${content.subValueList['Block7'] }" scope="request"/>
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
							</c:if>
							<c:if test="${content.value.Block1.exists }">
								<c:set var="elements" value="${content.subValueList['Block1'] }" scope="request"/>
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
							</c:if>													
							<c:if test="${content.value.Block3.exists }">	
								<c:set var="elements" value="${content.subValueList['Block3'] }" scope="request"/>
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
							</c:if>
							<c:if test="${content.value.Block2.exists }">
								<c:set var="elements" value="${content.subValueList['Block2'] }" scope="request"/>
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
							</c:if>							
						</div>
					</div>				
				</div>	
	</div>

</div>
</cms:device>
<%-- FIN MOVIL --%>
