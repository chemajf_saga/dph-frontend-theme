<%@page buffer="none" session="false" taglibs="c,cms,fmt, fn" import="java.util.*, org.opencms.search.*, org.opencms.file.*"%>
<fmt:setLocale value="${cms.locale}" />
<fmt:bundle basename="com.saga.opencms.laspalmas.messages">
<%-- 
Recibimos por parametros el recurso que vamos a mostrar 
- resource (CmsSearchResource o ContentAccessWrapper)
- value: Variable value tiene todo el fichero de configuracion del xml de listado
- cmsObject: Tiene el objeto CmsObject con los permisos del usuario actual
- link: enlace al detalle del recurso
--%>

<li class="mix <c:out value='${width} ${categoryclass}'/>">
	<article class="equalheight">
		<div class="media">
			<div class="media-object pull-left">
				<a href="<cms:link>${resource.value.Enlace.value.Href }</cms:link>" title="${resource.value.Title}">
			  <span class="badge">${resource.value.Grupo}</span>
			  </a>
			</div>
			<div class="media-body">			
				<h4 class="title-element">
					<a href="<cms:link>${resource.value.Enlace.value.Href }</cms:link>" title="${resource.value.Title}">
						${resource.value.Title }
					</a>
				</h4>
			</div>
		</div>	
	</article>
</li>
</fmt:bundle>
