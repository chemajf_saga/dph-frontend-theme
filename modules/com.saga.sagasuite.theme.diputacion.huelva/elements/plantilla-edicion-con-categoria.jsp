<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%-- 
Recibimos por parametros el recurso que vamos a mostrar 
- resource (CmsSearchResource o ContentAccessWrapper)
- value: Variable value tiene todo el fichero de configuracion del xml de listado
- cmsObject: Tiene el objeto CmsObject con los permisos del usuario actual
- link: enlace al detalle del recurso
--%>

<li class="simple title-date-desc ${width}">
	<article>
		<div class="row">
			<c:if test="${value.ViewType == 'tit-img' or value.ViewType == 'tit-dat-img' or value.ViewType == 'tit-des-img' or value.ViewType == 'tit-dat-des-img'}">
				<c:if test="${not empty imagePath}">
					<div class="col-sm-${value.ImageBlockWidth}">
						<cms:img src="${imagePath}" width="${imageWidth}" height="${value.ImageElementHeight.isSet?imageHeight:null}" cssclass="img-responsive" scaleType="2" style="border:solid 1px #ddd"/>
					</div>
				</c:if>
			</c:if>
			<div class="container-fluid media-body">
				<span class="date-element"><i class="fa fa-calendar">&nbsp;</i>
					<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-time.jsp:c6f25204-97ae-11e3-8047-f18cf451b707)">
						<cms:param name="date">${date }</cms:param>
					</cms:include>
				</span>
				<h4 class="title-element">
					<a href="<cms:link>${linkDetail }</cms:link>">
						<c:if test="${!cms.isOnlineProject and !published}">
							<abbr title="Recurso pendiente de publicaci&oacute;n"><span class="fa fa-flag small" aria-hidden="true"></span></abbr>
						</c:if>
						${title }
					</a>
				</h4>
				<c:if test="${not empty description}">
				<div class="description">
					${description}
				</div>
				</c:if>
				<c:if test="${resource.value.Category.isSet }">
					<c:set var="categorias" value="${fn:split(resource.value.Category, ',')}" />
					<ul class="list-unstyled list-inline mt-15">
						<c:forEach items="${categorias }" var="category" varStatus="status">
							<c:if test="${!status.first}">&nbsp;&nbsp;</c:if>
							<li><strong style="color:#777">#<cms:property name="Title" file="${category }"/></strong></li>
						</c:forEach>
					</ul>
				</c:if>
			</div>
		</div>
		<%--<div class="description">${description}</div>--%>
	</article>
</li>