<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
<%-- 
Recibimos por parametros el recurso que vamos a mostrar 
- resource (CmsSearchResource o ContentAccessWrapper) el contenido xml del recurso
- value: Variable value tiene todo el fichero de configuracion del xml de listado
- cmsObject: Tiene el objeto CmsObject con los permisos del usuario actual
- linkDetail: enlace al detalle del recurso
- title: título del recurso definido en el listado y limitados sus caracteres si se ha definido en el listado
- titlelarge: título del recurso sin acortar si se ha definido limitación de caracteres
- date: fecha
- imagePath: imagen
- description: descripción
- imageWidth: ancho en píxeles de la imagen
- imageHeight: alto en píxeles de la imagen
- showLinkToDetail: mostrar o no el enlace al detalle del recurso
- width: clase completa que define el ancho para grid del elemento listado (si el listado tiene definido 'mostrar en columnas')
- titletagelement: tipo de encabezado para el recurso (h1, h2, h3... etc.)
- titletagelementsize: tamaño de letra para el encabezado
- imageBlockPosition: posición del bloque de imagen con respecto al contenido textual
- imageBlockWidth: ancho del bloque de imagen con respecto al ancho completo del recurso
- formatdate: formato de fecha
--%>

<li class="item-featured-date ${width }">
	<article>
		<div class="media">
			<div class="media-left media-middle">
				<c:set var="date">${resource.value.FichaEvento.value.FechaInicio }</c:set>
				<div class="media-object hastooltip" title="<fmt:formatDate dateStyle='FULL' value='${cms:convertDate(date)}' type='date' />">
					<time class="sr-only" datetime="<fmt:formatDate value="${cms:convertDate(date)}" pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ" />">
						<fmt:formatDate dateStyle="FULL" value="${cms:convertDate(date)}" type="date" />
					</time>
					<c:set var="dateday">
						<fmt:formatDate value="${cms:convertDate(date)}" pattern="EEE" />
					</c:set>
					<c:set var="datedaynumber">
						<fmt:formatDate value="${cms:convertDate(date)}" pattern="dd"/>
					</c:set>
					<c:set var="datemonth">
						<fmt:formatDate value="${cms:convertDate(date)}" pattern="MMM"/>
					</c:set>
					<div class="day upper">${dateday}</div>
					<div class="daynumber">${datedaynumber}</div>
					<div class="month upper">${datemonth}</div>
				</div>
			</div>
			<a class="media-body media-middle" href="<cms:link>${linkDetail}</cms:link>">
				<span class="media-heading">
					${title}
				</span>
			</a>
		</div>
	</article>
</li>
</cms:bundle>