<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
<%-- 
Recibimos por parametros el recurso que vamos a mostrar 
- resource (CmsSearchResource o ContentAccessWrapper) el contenido xml del recurso
- value: Variable value tiene todo el fichero de configuracion del xml de listado
- cmsObject: Tiene el objeto CmsObject con los permisos del usuario actual
- linkDetail: enlace al detalle del recurso
- title: título del recurso definido en el listado y limitados sus caracteres si se ha definido en el listado
- titlelarge: título del recurso sin acortar si se ha definido limitación de caracteres
- date: fecha
- imagePath: imagen
- description: descripción
- imageWidth: ancho en píxeles de la imagen
- imageHeight: alto en píxeles de la imagen
- showLinkToDetail: mostrar o no el enlace al detalle del recurso
- width: clase completa que define el ancho para grid del elemento listado (si el listado tiene definido 'mostrar en columnas')
- titletagelement: tipo de encabezado para el recurso (h1, h2, h3... etc.)
- titletagelementsize: tamaño de letra para el encabezado
- imageBlockPosition: posición del bloque de imagen con respecto al contenido textual
- imageBlockWidth: ancho del bloque de imagen con respecto al ancho completo del recurso
- formatdate: formato de fecha
--%>

<li class="item-featured-date main-item-featured-date ${width }">
	<div class="media-wrapper no-margin no-padding row row-table-xs row-table-no-gutter">
		<div class="media-date col-xxs-12 col-xs-2">
			<c:set var="date">${resource.value.FichaEvento.value.FechaInicio }</c:set>
			<div class="media-object tooltip-right" title="<fmt:formatDate dateStyle='FULL' value='${cms:convertDate(date)}' type='date' />">
				<time class="sr-only" datetime="<fmt:formatDate value="${cms:convertDate(date)}" pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ" />">
					<fmt:formatDate dateStyle="FULL" value="${cms:convertDate(date)}" type="date" />
				</time>
				<c:set var="dateday">
					<fmt:formatDate value="${cms:convertDate(date)}" pattern="EEE" />
				</c:set>
				<c:set var="datedaynumber">
					<fmt:formatDate value="${cms:convertDate(date)}" pattern="dd"/>
				</c:set>
				<c:set var="datemonth">
					<fmt:formatDate value="${cms:convertDate(date)}" pattern="MMM"/>
				</c:set>
				<div class="day upper">${dateday}</div>
				<div class="daynumber">${datedaynumber}</div>
				<div class="month upper">${datemonth}</div>
			</div>
		</div>
		<c:if test="${not empty imagePath}">
			<c:if test="${fn:indexOf(imagePath, '?') != - 1}">
				<c:set var="imagePath" value="${fn:substringBefore(imagePath, '?')}" />
			</c:if>
			<c:set var="imagePath">${imagePath}?__scale=h:480,w:480,t:2,c:transparent,q:100</c:set>
			<div class="media-img col-xxs-12 col-xs-3" style="background-size: cover;background-position: center center;background-image: url('<cms:link>${imagePath}</cms:link>')">
				<span class="sr-only">${imageTitle}</span>
			</div>
		</c:if>
		<div class="media-info col-xxs-12 col-xs-7">
			<${titletagelement} class="title-element ${titletagelementsize}">
				<a href="<cms:link>${linkDetail}</cms:link>">
					${title}
				</a>
			</${titletagelement}>
			<div class="time mt-10">
				<span class="pe-7s-date fs18 mr-5 v-align-m" aria-hidden="true"></span>
				<span class="v-align-m strong">
					<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.evento/elements/e-time-period.jsp:c8ff5c68-d38f-11e4-930e-01e4df46f753)">
						<cms:param name="inidate">${resource.value.FichaEvento.value.FechaInicio }</cms:param>
						<c:if test="${resource.value.FichaEvento.value.FechaFin.isSet }">
							<cms:param name="findate">${resource.value.FichaEvento.value.FechaFin }</cms:param>
						</c:if>
						<cms:param name="showtime">false</cms:param>
					</cms:include>
				</span>
			</div>
			<c:if test="${resource.value.Municipio.isSet }">
				<div class="info mt-10">
					<c:set var="municipios" value="${fn:split(resource.value.Municipio, ',')}" />
					<c:forEach items="${municipios }" var="municipio" varStatus="status">
						<c:if test="${status.count != 1}">
							<span class="ml-5 mr-5 v-align-m inline-b">|</span>
						</c:if>
						<span class="municipio-element inline-b">
							<c:if test="${status.count == 1}">
								<span class="pe-7s-map-marker fs18 mr-5 v-align-m" aria-hidden="true"></span>
							</c:if>
							<c:set var="municipiotitle"><cms:property name="Title_${locale}" file="${municipio}"/></c:set>
							<c:if test="${empty municipiotitle}">
								<c:set var="municipiotitle"><cms:property name="Title" file="${municipio}"/></c:set>
							</c:if>
							<span class="v-align-m">${municipiotitle}</span>
						</span>
					</c:forEach>
				</div>
			</c:if>

			<c:if test="${resource.value.Program.isSet }">
				<div class="info-program mt-10">
						<a class="program-element inline-b hastooltip" href="<cms:link>${resource.value.Program}</cms:link>" title="<fmt:message key='key.element.program.belong' />">
							<span class="pe-7s-note2 fs18 mr-5 v-align-m" aria-hidden="true"></span>
							<c:set var="programtitle"><cms:property name="Title_${locale}" file="${resource.value.Program}"/></c:set>
							<c:if test="${empty programtitle}">
								<c:set var="programtitle"><cms:property name="Title" file="${resource.value.Program}"/></c:set>
							</c:if>
							<span class="v-align-m">${programtitle}</span>
						</a>
				</div>
			</c:if>
		</div>
	</div>
</li>
</cms:bundle>