<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">

	<li class="${width} mb-30">
		<c:if test="${imagePath==null or imagePath=='' or fn:indexOf(imagePath, '???')!=-1}">
			<c:set var="imagePath">/.template-elements/imagenes-estructura/no-img-default.jpg</c:set>
		</c:if>
		<a class="overlayed overlayed-dark overlayed-table no-text" title="<fmt:message key="key.element.view.album" />${titlelarge}" href="<cms:link>${linkDetail}</cms:link>">
			<c:set var="titlelocale">Title_${cms.locale}</c:set>
			<c:set var="imageTitle">
				${cms:vfs(pageContext).property[imagePath][titlelocale]}
			</c:set>
			<c:if test="${empty imageTitle}">
				<c:set var="imageTitle">
					${cms:vfs(pageContext).property[imagePath]['Title']}
				</c:set>
			</c:if>
			<cms:img cssclass="img-responsive" width="${imageWidth}" height="${imageHeight}" src="${imagePath}" alt="${imageTitle}" scaleType="2"/>
			<span class="table-block overlayer text-center">
				<span class="table-cell fs18 h3 no-margin p-30">
					<span class="pe-7s-albums mb-10 fs50 disp-block" aria-hidden="true"></span>
					${title}
				</span>
			</span>
		</a>
	</li>
</cms:bundle>