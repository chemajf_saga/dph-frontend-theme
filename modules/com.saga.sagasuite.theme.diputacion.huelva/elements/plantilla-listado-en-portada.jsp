<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">

<c:choose>
	<c:when test="${pos == 'first'}">
		<c:set var="equal">equalheight-four</c:set>
	</c:when>
	<c:otherwise>
		<c:set var="equal">equalheight-nine</c:set>
		<c:if test="${not empty equalheight}">
			<c:set var="equal">${equalheight}</c:set>
		</c:if>
	</c:otherwise>
</c:choose>
	<c:set var="locale">${cms.locale}</c:set>
	<%-- Establecemos la imagen por defecto definida en el .themeconfig por si no viene imagen en el recurso --%>
	<c:if test="${not empty themeConfiguration.CustomFieldKeyValue.defaultimg}">
		<c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg}</c:set>
		<c:if test="${count > 1 and count %2 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg2}">
			<c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg2}</c:set>
		</c:if>
		<c:if test="${count > 1 and count %3 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg3}">
			<c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg3}</c:set>
		</c:if>
		<c:if test="${count > 1 and count %4 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg4}">
			<c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg4}</c:set>
		</c:if>
		<c:if test="${count > 1 and count %5 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg5}">
			<c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg5}</c:set>
		</c:if>
	</c:if>
	<c:if test="${imagePath!=null and imagePath!='' and fn:indexOf(imagePath, '???')==-1}">
		<c:set var="image">${imagePath}</c:set>
	</c:if>

	<c:set var="widthtesela">grid-item-width-1</c:set>
	<c:set var="heighttesela">grid-item-height-1</c:set>
	<c:if test="${resource.value.ListWidth.isSet}">
		<c:set var="widthtesela">grid-item-width-${resource.value.ListWidth}</c:set>
	</c:if>
	<c:if test="${resource.value.ListHeight.isSet}">
		<c:set var="heighttesela">grid-item-width-${resource.value.ListHeight}</c:set>
	</c:if>

<li class="element sg-microcontent sg-microcontent-panel sg-microcontent-overlay box box-featured-new default-box mb-30 ${heighttesela} grid-masonry-item ${widthtesela}">
	<article style="background-image:url('<cms:link>${image}</cms:link>');background-size:cover;background-position: center center;background-repeat: no-repeat;" class="element parent sg-microcontent sg-microcontent-panel sg-microcontent-overlay box box-featured-new default-box">
		<div class="wrapper">
			<div class="element complete-mode with-media">
				<a class="overlay-link" href="<cms:link>${linkDetail }</cms:link>" title="<fmt:message key='key.list.element.go.to' />${titlelarge}">
					<span class="hover-overlay" aria-hidden="true"></span>
					<span class="sr-only">${titlelarge}</span>
				</a>
				<div class="text-overlay">
					<div class="wrapper">
						<header>
							<${titletagelement} class="title-element ${titletagelementsize}">
								${title}
							</${titletagelement}>
							<div class="info-date">
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-time-new.jsp:d9451486-147d-11e7-8644-7fb253176922)">
									<cms:param name="date">${date }</cms:param>
									<cms:param name="formatdate">${formatdate }</cms:param>
								</cms:include>
							</div>
						</header>
					</div>
				</div>
			</div>
		</div>
	</article>
</li>
</cms:bundle>