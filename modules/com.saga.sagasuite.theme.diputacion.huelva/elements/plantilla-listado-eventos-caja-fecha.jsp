<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
<%-- 
Recibimos por parametros el recurso que vamos a mostrar 
- resource (CmsSearchResource o ContentAccessWrapper) el contenido xml del recurso
- value: Variable value tiene todo el fichero de configuracion del xml de listado
- cmsObject: Tiene el objeto CmsObject con los permisos del usuario actual
- linkDetail: enlace al detalle del recurso
- title: título del recurso definido en el listado y limitados sus caracteres si se ha definido en el listado
- titlelarge: título del recurso sin acortar si se ha definido limitación de caracteres
- date: fecha
- imagePath: imagen
- description: descripción
- imageWidth: ancho en píxeles de la imagen
- imageHeight: alto en píxeles de la imagen
- showLinkToDetail: mostrar o no el enlace al detalle del recurso
- width: clase completa que define el ancho para grid del elemento listado (si el listado tiene definido 'mostrar en columnas')
- titletagelement: tipo de encabezado para el recurso (h1, h2, h3... etc.)
- titletagelementsize: tamaño de letra para el encabezado
- imageBlockPosition: posición del bloque de imagen con respecto al contenido textual
- imageBlockWidth: ancho del bloque de imagen con respecto al ancho completo del recurso
- formatdate: formato de fecha
--%>

<li class="item-featured-date ${width }">
	<div class="media media-wrapper media-wrapper-sm no-padding">
		<div class="media-date media-date-sm media-left media-middle">
			<c:set var="date">${resource.value.FichaEvento.value.FechaInicio }</c:set>
			<div class="media-object tooltip-right" title="<fmt:formatDate dateStyle='FULL' value='${cms:convertDate(date)}' type='date' />">
				<time class="sr-only" datetime="<fmt:formatDate value="${cms:convertDate(date)}" pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ" />">
					<fmt:formatDate dateStyle="FULL" value="${cms:convertDate(date)}" type="date" />
				</time>
				<c:set var="dateday">
					<fmt:formatDate value="${cms:convertDate(date)}" pattern="EEE" />
				</c:set>
				<c:set var="datedaynumber">
					<fmt:formatDate value="${cms:convertDate(date)}" pattern="dd"/>
				</c:set>
				<c:set var="datemonth">
					<fmt:formatDate value="${cms:convertDate(date)}" pattern="MMM"/>
				</c:set>
				<div class="day upper">${dateday}</div>
				<div class="daynumber">${datedaynumber}</div>
				<div class="month upper">${datemonth}</div>
			</div>
		</div>
		<a class="media-info media-body media-middle" href="<cms:link>${linkDetail}</cms:link>">
			<c:if test="${resource.value.Municipio.isSet }">
				<div class="info">
					<c:set var="municipios" value="${fn:split(resource.value.Municipio, ',')}" />
					<c:forEach items="${municipios }" var="municipio" varStatus="status">
						<c:if test="${status.count != 1}">
							<span class="ml-5 mr-5 v-align-m inline-b">|</span>
						</c:if>
						<span class="info-element inline-b">
							<c:if test="${status.count == 1}">
								<span class="pe-7s-map-marker fs18 v-align-m" aria-hidden="true"></span>
							</c:if>
							<c:set var="municipiotitle"><cms:property name="Title_${locale}" file="${municipio}"/></c:set>
							<c:if test="${empty municipiotitle}">
								<c:set var="municipiotitle"><cms:property name="Title" file="${municipio}"/></c:set>
							</c:if>
							<span class="v-align-m">${municipiotitle}</span>
						</span>
					</c:forEach>
				</div>
			</c:if>
			<span class="media-heading h5">
				${title}
			</span>
			<c:if test="${not empty description}">
				<span class="disp-block mt-5 description">${description}</span>
			</c:if>
		</a>
	</div>
</li>
</cms:bundle>