<%@page buffer="none" session="false" taglibs="c,cms,fmt, fn" import="java.util.*, org.opencms.search.*, org.opencms.file.*"%>
<%-- 
Recibimos por parametros el recurso que vamos a mostrar 
- resource (CmsSearchResource o ContentAccessWrapper)
- value: Variable value tiene todo el fichero de configuracion del xml de listado
- cmsObject: Tiene el objeto CmsObject con los permisos del usuario actual
- link: enlace al detalle del recurso
--%>

<li class="item has-media no-padding ${width }">
	<article class="media">
		<c:if test="${resource.value.ImageMain.isSet }">
			<div class="image media-object">
				<a href="<cms:link>${linkDetail }</cms:link>" title="${resource.value.Title}">
				<cms:include file="%(link.weak:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/e-image.jsp:92ab73ef-e555-11e7-a388-63f9a58dae09)">
					<cms:param name="imagePath">${imagePath}</cms:param>
					<cms:param name="showBig">false</cms:param>
					<cms:param name="thumbnails">true</cms:param>
					<cms:param name="title">${resource.value.Title }</cms:param>
					<cms:param name="width">${imageWidth}</cms:param>
					<cms:param name="height">${imageHeight }</cms:param>					
				</cms:include>
				</a>
			</div>
		</c:if>
		<div class="image media-body text-center">
			<h4 class="title-element media-heading">
				<a href="<cms:link>${linkDetail }</cms:link>" title="${resource.value.Title}">
					${resource.value.Title}
				</a>	
			</h4>
		</div>	
	</article>	
</li>