<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">

<c:if test="${count == '1' and not empty viewinrow}">
	<c:set var="width">col-xxs-12</c:set>
	<c:set var="pos">first</c:set>
</c:if>
<c:choose>
	<c:when test="${pos == 'first'}">
		<c:set var="equal">equalheight-four</c:set>
	</c:when>
	<c:otherwise>
		<c:set var="equal">equalheight-nine</c:set>
		<c:if test="${not empty equalheight}">
			<c:set var="equal">${equalheight}</c:set>
		</c:if>
	</c:otherwise>
</c:choose>
	<c:set var="locale">${cms.locale}</c:set>
	<c:set var="image">/.template-elements/imagenes-estructura/no-img-default.png</c:set>

	<c:if test="${imagePath!=null and imagePath!='' and fn:indexOf(imagePath, '???')==-1}">
		<c:set var="image">${imagePath}</c:set>
	</c:if>

	<c:if test="${(imagePath==null or imagePath=='' or fn:indexOf(imagePath, '???')!=-1) and ((resource.value.Content.value.Media.exists and resource.value.Content.value.Media.value.VideoMain.exists and resource.value.Content.value.Media.value.VideoMain.value.Code.isSet) or (resource.value.Content.value.Media.exists and resource.value.Content.value.Media.value.MediaMultiple.exists and resource.value.Content.value.Media.value.MediaMultiple.value.MediaMultipleElements.exists and resource.value.Content.value.Media.value.MediaMultiple.value.MediaMultipleElements.value.Video.isSet))}">
		<c:set var="morevideos">true</c:set>
		<c:forEach var="contentblock" items="${resource.valueList.Content}" varStatus="statusvideo">
			<c:if test="${morevideos and ((contentblock.value.Media.exists and contentblock.value.Media.value.VideoMain.exists and contentblock.value.Media.value.VideoMain.value.Code.isSet) or (contentblock.value.Media.exists and contentblock.value.Media.value.MediaMultiple.exists and contentblock.value.Media.value.MediaMultiple.value.MediaMultipleElements.exists and contentblock.value.Media.value.MediaMultiple.value.MediaMultipleElements.value.Video.isSet))}">
				<c:set var="morevideos">false</c:set>
				<c:if test="${contentblock.value.Media.exists and contentblock.value.Media.value.VideoMain.exists and contentblock.value.Media.value.VideoMain.value.Code.isSet}">
					<c:set var="codeVideo">${contentblock.value.Media.value.VideoMain.value.Code}</c:set>
				</c:if>
				<c:if test="${contentblock.value.Media.exists and contentblock.value.Media.value.MediaMultiple.exists and contentblock.value.Media.value.MediaMultiple.value.MediaMultipleElements.exists and contentblock.value.Media.value.MediaMultiple.value.MediaMultipleElements.value.Video.isSet}">
					<c:set var="codeVideo">${contentblock.value.Media.value.MediaMultiple.value.MediaMultipleElements.value.Video}</c:set>
				</c:if>
				<%
					String codeVideo = "" + pageContext.getAttribute("codeVideo");
					int inicio = codeVideo.indexOf("/embed/") + "/embed/".length();
					String codeYoutube = codeVideo.substring(inicio, inicio + 11);
					pageContext.setAttribute("codeYoutube", codeYoutube);
				%>
			</c:if>
		</c:forEach>
		<c:set var="image">http://img.youtube.com/vi/${codeYoutube }/0.jpg</c:set>
	</c:if>

<li class="${width} no-spacing-row list-element-super mb-50 ${pos}">
	<div class="row">
		<div class="col-xs-6 col-xxs-12 wrapper-media-bg-img ${equal}" style="background-image: url('<cms:link>${image}</cms:link>');background-position: center center;background-color: transparent;background-size: cover;color:inherit;border: solid 1px #ddd;border-right:none;display:table;table-layout:fixed;">
			<a class="overlay-link" href="<cms:link>${linkDetail }</cms:link>" title="${titlelarge }"><span class="hover-overlay"></span></a>
			<div class="wrapper-content">
				<div class="wrapper-element">
					<div class="text-wrapper">
						<${titletagelement} class="title-element ${titletagelementsize}">
							${title}
						</${titletagelement}>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-6 col-xxs-12 description ${equal}">
			<div class="description-content">
				<div class="info">
					<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-time-new.jsp:d9451486-147d-11e7-8644-7fb253176922)">
						<cms:param name="date">${date }</cms:param>
						<cms:param name="formatdate">${formatdate }</cms:param>
					</cms:include>
				</div>
				<c:if test="${not empty description}">
					<div>
							${description}
					</div>
				</c:if>
				<c:if test="${resource.value.Category.isSet }">
					<div class="categories">
						<%--<c:if test="${!value.TitleLink.exists or !value.TitleLink.value.Href.isSet }">
							<c:set var="buscadorlinktext"></c:set>
							<c:set var="buscadorlinkhref">/temas/noticias/</c:set>
						</c:if>
						<c:if test="${value.TitleLink.exists and value.TitleLink.value.Title.isSet }">
							<c:set var="buscadorlinktext">${value.TitleLink.value.Title}</c:set>
						</c:if>
						<c:if test="${value.TitleLink.exists and value.TitleLink.value.Href.isSet }">
							<c:set var="buscadorlinkhref">${value.TitleLink.value.Href}</c:set>
						</c:if>
						<c:set var="categorias" value="${fn:split(resource.value.Category, ',')}" />
						<c:set var="counter" value="0" />
						<c:forEach items="${categorias }" var="category" varStatus="status">
							<c:set var="categorytitle"><cms:property name="Title" file="${category }"/></c:set>
							<c:set var="categorytitletemas" value="${fn:indexOf(category, '/temas/')}" />
							<c:set var="categoryshort" value="${fn:replace(category, '/.categories/','')}" />
							<c:if test="${categorytitletemas != -1}">
								<c:set var="counter" value="${counter + 1}" />
								<c:set var="linkquery">?buscadorthemesfield-1=&buscadorthemesfield-2_d1=&buscadorthemesfield-2_d2=&buscadorthemesfield-3=${categoryshort}&numfield=3&searchaction=search&searchPage=1&submit=Buscar</c:set>
								<c:if test="${counter > 1}">&nbsp;&nbsp;&nbsp;</c:if>
								<a href="<cms:link>${buscadorlinkhref}${linkquery}</cms:link>" title="${buscadorlinktext}">#${categorytitle }</a>
							</c:if>
						</c:forEach>--%>
						<c:set var="categorias" value="${fn:split(resource.value.Category, ',')}" />
						<c:set var="counter" value="0" />
						<c:forEach items="${categorias }" var="category" varStatus="status">
							<%--====== Comprobamos si existe la propiedad Title especifica para el locale actual (Title_es, Title_en...) para la categoria.
											Si es asi usamos esa propiedad. Si no existe, usamos la propiedad Title =============================--%>
							<c:set var="categorytitle"><cms:property name="Title_${locale}" file="${category}"/></c:set>
							<c:if test="${empty categoryPropertyTitle}">
								<c:set var="categorytitle"><cms:property name="Title" file="${category}"/></c:set>
							</c:if>
							<c:set var="categorypagenews"><cms:property name="sagasuite.tema.page" file="${category }"/>noticias/</c:set>
							<c:set var="categoryhastemas" value="${fn:indexOf(category, '/temas/')}" />
							<c:if test="${categoryhastemas != -1}">
								<c:set var="counter" value="${counter + 1}" />
								<c:if test="${counter > 1}">&nbsp;&nbsp;&nbsp;</c:if>
								<a href="<cms:link>${categorypagenews}</cms:link>" title="<fmt:message key="key.formatter.list.themes.btn.allnews" />}">#${categorytitle }</a>
							</c:if>
						</c:forEach>
					</div>
				</c:if>
					<%-- Boton --%>
				<c:if test="${showbutton == 'true' or value.ConfigButton.exists}">
					<c:if test="${value.ConfigButton.exists and value.ConfigButton.value.TextLink.isSet}">
						<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/e-boton.jsp:924e5fb7-e555-11e7-a388-63f9a58dae09)">
							<cms:param name="tipoboton">${value.ConfigButton.value.CssClass}</cms:param>
							<cms:param name="positionboton">${value.ConfigButton.value.Position} mt-30</cms:param>
							<cms:param name="sizeboton">${value.ConfigButton.value.Size}</cms:param>
							<cms:param name="textoboton">${value.ConfigButton.value.TextLink}</cms:param>
							<cms:param name="iconboton">${value.ConfigButton.value.Icon}</cms:param>
							<cms:param name="linkboton">${linkDetail}</cms:param>
							<cms:param name="targetboton">_self</cms:param>
						</cms:include>
					</c:if>
					<c:if test="${(showbutton == 'true') and (!value.ConfigButton.value.TextLink.isSet)}">
						<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/e-boton.jsp:924e5fb7-e555-11e7-a388-63f9a58dae09)">
							<cms:param name="tipoboton">btn-specific-main</cms:param>
							<cms:param name="positionboton">text-left mt-30</cms:param>
							<cms:param name="sizeboton">btn-md</cms:param>
							<cms:param name="textoboton"><fmt:message key="label.read.more.link" /><span class="sr-only">&nbsp;"${titlelarge}"</span></cms:param>
							<cms:param name="iconboton">fa-plus</cms:param>
							<cms:param name="linkboton">${linkDetail}</cms:param>
							<cms:param name="targetboton">_self</cms:param>
						</cms:include>
					</c:if>
				</c:if>
			</div>
		</div>
	</div>
</li>
</cms:bundle>