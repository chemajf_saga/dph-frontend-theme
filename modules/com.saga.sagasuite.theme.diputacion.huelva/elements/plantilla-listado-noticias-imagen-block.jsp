<%@page buffer="none" session="false" taglibs="c,cms,fmt, fn" import="java.util.*, org.opencms.search.*, org.opencms.file.*"%>
<%-- 
Recibimos por parametros el recurso que vamos a mostrar 
- resource (CmsSearchResource o ContentAccessWrapper)
- value: Variable value tiene todo el fichero de configuracion del xml de listado
- cmsObject: Tiene el objeto CmsObject con los permisos del usuario actual
- link: enlace al detalle del recurso
--%>

<div class="item has-media ${width }">
	<article class="media">
		<c:if test="${imagePath!=null and imagePath!='' and fn:indexOf(imagePath, '???')==-1}">
			<div class="image media-object margin-bottom-10">
				<a href="<cms:link>${linkDetail }</cms:link>" title="${resource.value.Title}">
				<cms:include file="%(link.weak:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/e-image.jsp:92ab73ef-e555-11e7-a388-63f9a58dae09)">
					<cms:param name="imagePath">${imagePath}</cms:param>
					<cms:param name="showBig">false</cms:param>
					<cms:param name="thumbnails">false</cms:param>
					<cms:param name="title">${resource.value.Title }</cms:param>
					<cms:param name="width">${imageWidth}</cms:param>
					<cms:param name="height">${imageHeight }</cms:param>					
				</cms:include>
				</a>
			</div>
		</c:if>
		<div class="image media-body">
			<span class="date-element"><i class="fa fa-calendar">&nbsp;</i>
				<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-time.jsp:c6f25204-97ae-11e3-8047-f18cf451b707)">
					<cms:param name="date">${date }</cms:param>
					<cms:param name="shortdate">true</cms:param>
				</cms:include>
			</span>								
			<h4 class="title-element media-heading">
	            <a href="<cms:link>${linkDetail }</cms:link>" title="${title }">${title }</a>
	        </h4>
		</div>	
	</article>	
</div>