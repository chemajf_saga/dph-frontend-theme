<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%-- 
Recibimos por parametros el recurso que vamos a mostrar 
- resource (CmsSearchResource o ContentAccessWrapper)
- value: Variable value tiene todo el fichero de configuracion del xml de listado
- cmsObject: Tiene el objeto CmsObject con los permisos del usuario actual
- link: enlace al detalle del recurso
--%>
<c:if test="${count == '1' and not empty viewinrow}">
	<c:set var="width">col-xxs-12</c:set>
</c:if>
<li class="${width} list-element-super list-element-super-dest">
	<c:if test="${imagePath==null or imagePath=='' or fn:indexOf(imagePath, '???')!=-1}">
		<c:set var="imagePath">/.template-elements/imagenes-estructura/no-img-default.jpg</c:set>
	</c:if>
	<c:if test="${imagePath!=null and imagePath!='' and fn:indexOf(imagePath, '???')==-1}">
		<c:set var="imagePath">${imagePath}</c:set>
	</c:if>
	<a class="overlay-link" href="<cms:link>${linkDetail }</cms:link>" title="${titlelarge }"><span class="hover-overlay"></span></a>
	<div style="background-image: url('<cms:link>${imagePath}</cms:link>');background-position: center center;background-color: transparent;background-size: cover;">
		<div class="wrapper-content">
			<div class="wrapper-element">
				<div class="text-wrapper">
					<div class="info">
						<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-time.jsp:ea2c737f-df65-11e4-bcf9-01e4df46f753)">
							<cms:param name="date">${date }</cms:param>
							<cms:param name="formatdate">${formatdate }</cms:param>
						</cms:include>
					</div>
					<h3>
						${title}
					</h3>
					<c:if test="${not empty description}">
						<div class="description">
							${description}
						</div>
					</c:if>
				</div>
			</div>
		</div>
	</div>
</li>