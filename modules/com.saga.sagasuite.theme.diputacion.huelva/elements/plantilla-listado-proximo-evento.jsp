<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
<%-- 
Recibimos por parametros el recurso que vamos a mostrar 
- resource (CmsSearchResource o ContentAccessWrapper) el contenido xml del recurso
- value: Variable value tiene todo el fichero de configuracion del xml de listado
- cmsObject: Tiene el objeto CmsObject con los permisos del usuario actual
- linkDetail: enlace al detalle del recurso
- title: título del recurso definido en el listado y limitados sus caracteres si se ha definido en el listado
- titlelarge: título del recurso sin acortar si se ha definido limitación de caracteres
- date: fecha
- imagePath: imagen
- description: descripción
- imageWidth: ancho en píxeles de la imagen
- imageHeight: alto en píxeles de la imagen
- showLinkToDetail: mostrar o no el enlace al detalle del recurso
- width: clase completa que define el ancho para grid del elemento listado (si el listado tiene definido 'mostrar en columnas')
- titletagelement: tipo de encabezado para el recurso (h1, h2, h3... etc.)
- titletagelementsize: tamaño de letra para el encabezado
- imageBlockPosition: posición del bloque de imagen con respecto al contenido textual
- imageBlockWidth: ancho del bloque de imagen con respecto al ancho completo del recurso
- formatdate: formato de fecha
--%>

<li class="item-thumbnail has-media media-to-top has-media-complete ${width }">
	<article class="media no-padding">
		<c:if test="${imagePath!=null and imagePath!='' and fn:indexOf(imagePath, '???')==-1}">
			<c:set var="imageTitle"><cms:property name="Title_${cms.locale}" file="${imagePath}"/></c:set>
			<c:if test="${empty imageTitle}">
				<c:set var="imageTitle"><cms:property name="Title" file="${imagePath}"/></c:set>
			</c:if>
			<div class="image media-object">
				<div class="saga-imagen">
					<a href="${linkDetail}" class="wrapper-image">
						<cms:img src="${imagePath}" width="${imageWidth}" height="${imageHeight}" alt="${imageTitle}" scaleType="2" cssclass="img-responsive" />
					</a>
				</div>
			</div>
		</c:if>
		<div class="media-body">
			<div class="info">
				<c:if test="${not empty date}">
					<span class="info-element date-element inline-b">
						<span class="pe-7s-date fs20 v-align-m mr-5" aria-hidden="true"></span>
						<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/e-time-period-item.jsp:ee1472e5-1185-11e8-a1f9-63f9a58dae09)">
							<cms:param name="inidate">${date }</cms:param>
							<c:if test="${resource.value.FichaEvento.value.ShowInicioTime == 'true' }">
								<cms:param name="showinitime">true</cms:param>
							</c:if>
							<c:if test="${resource.value.FichaEvento.value.FechaFin.isSet }">
								<cms:param name="findate">${resource.value.FichaEvento.value.FechaFin }</cms:param>
							</c:if>
							<c:if test="${resource.value.FichaEvento.value.ShowFinTime == 'true' }">
								<cms:param name="showfintime">true</cms:param>
							</c:if>
						</cms:include>
					</span>
				</c:if>
				<c:if test="${resource.value.Municipio.isSet }">
						<c:set var="municipios" value="${fn:split(resource.value.Municipio, ',')}" />
						<c:forEach items="${municipios }" var="municipio" varStatus="status">
							<c:set var="marclass">ml-15</c:set>
							<c:if test="${status.count != 1}">
								<c:set var="marclass"></c:set>
								<span class="ml-5 mr-5 v-align-m inline-b">|</span>
							</c:if>
						<span class="info-element municipio-element inline-b ${marclass}">
							<c:if test="${status.count == 1}">
								<span class="pe-7s-map-marker fs20 v-align-m" aria-hidden="true"></span>
							</c:if>
							<c:set var="municipiotitle"><cms:property name="Title_${locale}" file="${municipio}"/></c:set>
							<c:if test="${empty municipiotitle}">
								<c:set var="municipiotitle"><cms:property name="Title" file="${municipio}"/></c:set>
							</c:if>
							<span class="inline-b">${municipiotitle}</span>
						</span>
						</c:forEach>
				</c:if>
			</div>
			<${titletagelement} class="media-heading ${titletagelementsize}">
	            <a href="<cms:link>${linkDetail }</cms:link>">${title }</a>
	        </${titletagelement}>
			<c:if test="${not empty description}">
				<div class="description">
					<p>${description}</p>
				</div>
			</c:if>
		</div>	
	</article>	
</li>
</cms:bundle>