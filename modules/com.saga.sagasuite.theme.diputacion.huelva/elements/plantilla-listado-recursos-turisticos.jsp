<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">

	<c:set var="equal">equalheight-three</c:set>
	<c:set var="locale">${cms.locale}</c:set>
	<c:set var="image">/.template-elements/imagenes-estructura/no-img-default.png</c:set>

	<c:if test="${imagePath!=null and imagePath!='' and fn:indexOf(imagePath, '???')==-1}">
		<c:set var="image">${imagePath}</c:set>
	</c:if>
	<c:if test="${count > 0 and count % 2 != 0}">
		<c:set var="bgclassmofifier">-dark</c:set>
	</c:if>
	<li class="${width} mb-30">
		<figure style="background-image: url('<cms:link>${imagePath}</cms:link>');background-repeat: no-repeat;background-size: cover;background-position: center center;height: 300px" class="element parent sg-icon-box feature-block cover-effect inverted-effect feature-block-vertical feature-block-defaultsize feature-block-border-no-border equalheight-five">
			<figcaption class="feature-block-brand${bgclassmofifier}">
				<div class="table-block hover-out">
					<div class="table-cell">
						<div class="headline-box">
							<div class="title">
								<span class="title-text">
								${title}
								</span>
							</div>
						</div>
						<div class="icon-wrapper icon-bg-no-bg-icon icon-border-no-border-icon">
							<span aria-hidden="true" class="icon-color-no-icon-color fa fa- pe-7s-map fs70"></span>
						</div>
					</div>
				</div>
				<div class="table-block hover-in">
					<div class="table-cell">
						<div class="headline-box">
							<div class="h4 extra-title">${title}</div>
						</div>
						<div class="description">
							${description}
						</div>
						<%-- Si es NO es desktop pintamos el enlace por debajo de la descripción en un enlace generico para poder hacer click sobre el mismo --%>
						<cms:device type="tablet,mobile">
							<div class="actions">
								<a href="<cms:link>${linkDetail }</cms:link>"><fmt:message key="label.read.more.link" /></a>
							</div>
						</cms:device>
					</div>
				</div>
			</figcaption>
			<%-- Si es desktop pintamos el enlace superpuesto sobre la caja ya que podemos hacer hover para activar el efecto --%>
			<cms:device type="desktop">
				<a href="<cms:link>${linkDetail }</cms:link>" title="<fmt:message key="label.read.more.link" />"></a>
			</cms:device>
		</figure>
	</li>
</cms:bundle>