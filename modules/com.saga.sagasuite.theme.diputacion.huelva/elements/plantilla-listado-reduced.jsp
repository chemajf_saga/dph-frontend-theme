<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
<%-- 
Recibimos por parametros el recurso que vamos a mostrar 
- resource (CmsSearchResource o ContentAccessWrapper) el contenido xml del recurso
- value: Variable value tiene todo el fichero de configuracion del xml de listado
- cmsObject: Tiene el objeto CmsObject con los permisos del usuario actual
- linkDetail: enlace al detalle del recurso
- title: título del recurso definido en el listado y limitados sus caracteres si se ha definido en el listado
- titlelarge: título del recurso sin acortar si se ha definido limitación de caracteres
- date: fecha
- imagePath: imagen
- description: descripción
- imageWidth: ancho en píxeles de la imagen
- imageHeight: alto en píxeles de la imagen
- showLinkToDetail: mostrar o no el enlace al detalle del recurso
- width: clase completa que define el ancho para grid del elemento listado (si el listado tiene definido 'mostrar en columnas')
- titletagelement: tipo de encabezado para el recurso (h1, h2, h3... etc.)
- titletagelementsize: tamaño de letra para el encabezado
- imageBlockPosition: posición del bloque de imagen con respecto al contenido textual
- imageBlockWidth: ancho del bloque de imagen con respecto al ancho completo del recurso
- formatdate: formato de fecha
--%>

<li class="item mb-20 ${width }">
	<div class="media-wrapper no-margin no-padding row row-table-xs gutter-sm">
		<div class="media-object-img col-xxs-12 col-xs-4">
			<c:if test="${not empty imagePath}">
				<c:if test="${fn:indexOf(imagePath, '?') != - 1}">
					<c:set var="imagePath" value="${fn:substringBefore(imagePath, '?')}" />
				</c:if>
				<cms:img src="${imagePath}" cssclass="img-responsive" scaleType="2" width="${imageWidth}" height="${imageHeight}" alt="${imageTitle}" />
			</c:if>
		</div>
		<div class="media-body-title col-xxs-12 col-xs-8">
			<${titletagelement} class="title-element ${titletagelementsize}">
				<a href="<cms:link>${linkDetail}</cms:link>">
					${title}
				</a>
			</${titletagelement}>
		</div>
	</div>
</li>
</cms:bundle>