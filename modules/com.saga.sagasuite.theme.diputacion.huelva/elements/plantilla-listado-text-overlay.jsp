<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">

	<c:set var="locale">${cms.locale}</c:set>
	<%-- Establecemos la imagen por defecto definida en el .themeconfig por si no viene imagen en el recurso --%>
	<c:if test="${not empty themeConfiguration.CustomFieldKeyValue.defaultimg}">
		<c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg}</c:set>
		<c:if test="${count > 1 and count %2 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg2}">
			<c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg2}</c:set>
		</c:if>
		<c:if test="${count > 1 and count %3 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg3}">
			<c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg3}</c:set>
		</c:if>
		<c:if test="${count > 1 and count %4 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg4}">
			<c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg4}</c:set>
		</c:if>
		<c:if test="${count > 1 and count %5 == 0 and not empty themeConfiguration.CustomFieldKeyValue.defaultimg5}">
			<c:set var="image">${themeConfiguration.CustomFieldKeyValue.defaultimg5}</c:set>
		</c:if>
	</c:if>
	<c:if test="${imagePath!=null and imagePath!='' and fn:indexOf(imagePath, '???')==-1}">
		<c:set var="image">${imagePath}</c:set>
	</c:if>


	<li class="element list-element-super box box-overlay default-box mb-30 ${width}">
		<article style="min-height: 180px;background-image:url('<cms:link>${image}</cms:link>');background-size:cover;background-position: center center;background-repeat: no-repeat;" class="wrapper-element table-block equalheight-${value.Id}">
			<a class="overlay-link" href="<cms:link>${linkDetail }</cms:link>" title="<fmt:message key='key.list.element.go.to' />${titlelarge}">
				<span class="hover-overlay" aria-hidden="true"></span>
				<span class="sr-only">${titlelarge}</span>
			</a>
			<div class="element complete-mode with-media table-cell w100">
				<div class="text-overlay">
					<div class="wrapper">
						<header>
							<${titletagelement} class="title-element ${titletagelementsize}">
								${title}
							</${titletagelement}>
							<c:if test="${empty showdate or showdate == 'true'}">
								<div class="info-date">
									<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-time-new.jsp:d9451486-147d-11e7-8644-7fb253176922)">
										<cms:param name="date">${date }</cms:param>
										<cms:param name="formatdate">${formatdate }</cms:param>
									</cms:include>
								</div>
							</c:if>
						</header>
					</div>
				</div>
			</div>
		</article>
	</li>
</cms:bundle>