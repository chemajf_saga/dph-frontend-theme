<%@page buffer="none" session="false" taglibs="c,cms,fmt, fn" import="java.util.*, org.opencms.search.*, org.opencms.file.*"%>
<%-- 
Recibimos por parametros el recurso que vamos a mostrar 
- resource (CmsSearchResource o ContentAccessWrapper)
- value: Variable value tiene todo el fichero de configuracion del xml de listado
- cmsObject: Tiene el objeto CmsObject con los permisos del usuario actual
- link: enlace al detalle del recurso
--%>

<li class="item col-sm-4 text-center">
	<div class="item-wrapper">
		<a class="thumbnail" href="<cms:link>${link }</cms:link>" title="${resource.value.Title }">
		<c:if test="${resource.value.Content.value.TextBlock.value.Media.value.VideoMain.exists and resource.value.Content.value.TextBlock.value.Media.value.VideoMain.value.Code.isSet }">
			<div class="video">

				<c:set var="codevideo">${resource.value.Content.value.TextBlock.value.Media.value.VideoMain.value.Code}</c:set>
				<%
				String codeVideo = ""+pageContext.getAttribute("codevideo");
				int inicio=codeVideo.indexOf("/embed/")+"/embed/".length();
				String codeYoutube = codeVideo.substring(inicio,inicio+11);
				pageContext.setAttribute("codeYoutube",codeYoutube);
				%>
				<img src="http://img.youtube.com/vi/${codeYoutube }/0.jpg" class="img-responsive thumbnail-video" />	
			</div>
		</c:if>
			<div class="headline">
				<h4>
					${resource.value.Title }
				</h4>
			</div>
		</a>	
	</div>
</li>
<%-- hacemos que cada dos elementos rompamos el float para crear filas --%>
<c:if test="${count%3==2 }">
	<div class="separate clearfix col-sm-12"></div>
</c:if>