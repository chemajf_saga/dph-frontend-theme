<%@ page import="org.opencms.jsp.CmsJspLoginBean" %>
<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<%
  CmsJspLoginBean loginBean = new CmsJspLoginBean(pageContext, request, response);
%>
<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
      <ul class="nav navbar-nav navbar-right">
        <c:choose>
          <c:when test="<%=!loginBean.isLoggedIn()%>">
            <li class="dropdown registro">
              <a class="dropdown-toggle" href="${cms.vfs.link['/users/registro/']}" data-toggle="dropdown">
                <span class="fa fa-user" aria-hidden="true">&nbsp;</span>&nbsp;<fmt:message key="label.registrate"/>
                <span class="fa fa-angle-down" aria-hidden="true">&nbsp;</span>
              </a>
              <ul class="dropdown-menu">
                <li>
                  <a href="${cms.vfs.link['/users/registro/']}">
                    <fmt:message key="label.create_account"/>
                  </a>
                </li>
                <li class="divider"></li>
                <li>
                  <a href="${cms.vfs.link['/users/login/index.html?requestAuth=true&provider=facebook&scope=email']}">
                    <span class="fa fa-facebook" aria-hidden="true">&nbsp;</span> <fmt:message key="label.registrate.facebook"/>
                  </a>
                </li>
                <li>
                  <a href="${cms.vfs.link['/users/login/index.html?requestAuth=true&provider=twitter&scope=email']}">
                    <span class="fa fa-twitter" aria-hidden="true">&nbsp;</span> <fmt:message key="label.registrate.twitter"/>
                  </a>
                </li>
              </ul>
            </li>
            <li>
              <a title="<fmt:message key="label.login"/>" href="${cms.vfs.link['/users/login/']}">
                <span class="fa fa-sign-in" aria-hidden="true">&nbsp;</span>&nbsp;&nbsp;<fmt:message key="label.login"/>
              </a>
            </li>
          </c:when>
          <c:otherwise>
            <li>
              <a title="<fmt:message key="label.logout"/>" href="${cms.vfs.link['/users/login/index.html?logout=true']}">
                <span class="fa fa-sign-out" aria-hidden="true">&nbsp;</span>&nbsp;&nbsp;<fmt:message key="label.logout"/>
              </a>
            </li>
          </c:otherwise>
        </c:choose>
      </ul>
</cms:bundle>