<%@ page import="org.opencms.jsp.CmsJspLoginBean" %>
<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
 
<%
  CmsJspLoginBean loginBean = new CmsJspLoginBean(pageContext, request, response);
%>
<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
 
  <cms:device type="desktop,tablet">
    <nav class="navbar navbar-inverse navbar-top">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" title="Ir a la web de la Diputación de Sevilla" href="http://www.dipusevilla.es/">
            <img style="width: 40px;" src="${cms.vfs.link['/.galleries/imagenes-estructura/logo-diputacion.png']}" alt=""/>
          </a>
          <button data-target=".navbar-top-header" data-toggle="collapse" class="navbar-toggle" type="button">
            <span class="fa fa-bars" aria-hidden="true"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse navbar-top-header">
          <ul class="nav navbar-nav">
            <li>
              <a title="<fmt:message key="label.ayuda"/>" href="${cms.vfs.link['/es/mas-informacion/preguntas-frecuentes/']}">
                <span class="fa fa-support">&nbsp;</span>&nbsp;&nbsp;<fmt:message key="label.ayuda"/>
              </a>
            </li>
            <li>
              <a title="<fmt:message key="label.mapaweb"/>" href="${cms.vfs.link['/es/mas-informacion/mapa-web/']}">
                <span class="fa fa-sitemap">&nbsp;</span>&nbsp;&nbsp;<fmt:message key="label.mapaweb"/>
              </a>
            </li>			
          </ul>
         </div>
      </div>
    </nav>
  </cms:device>
 
  <cms:device type="mobile">
    <li>
      <a title="<fmt:message key="label.ayuda"/>" href="${cms.vfs.link['/es/mas-informacion/preguntas-frecuentes/']}">
        <span class="fa fa-support">&nbsp;</span>&nbsp;&nbsp;<fmt:message key="label.ayuda"/>
      </a>	  
    </li>
	  <li>
		  <a title="<fmt:message key="label.mapaweb"/>" href="${cms.vfs.link['/es/mas-informacion/mapa-web/']}">
			  <span class="fa fa-sitemap">&nbsp;</span>&nbsp;&nbsp;<fmt:message key="label.mapaweb"/>
		  </a>
	  </li>	
  </cms:device>
 
</cms:bundle>