<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">

	<cms:formatter var="content" val="value" rdfa="rdfa">
		<c:choose>
			<c:when test="${cms.element.inMemoryOnly}">
				<h1 class="title">
					Por favor, edite este recurso.
				</h1>
			</c:when>
			<c:otherwise>

				<%-- Cargamos la variable con el id especifico del recurso para su uso posterior--%>
				<c:set var="idresource" value="${content.id}" scope="request"></c:set>
				<c:set var="id">${fn:substringBefore(idresource, '-')}</c:set>

				<c:if test="${cms.element.setting.marginbottom.value != '0'}">
					<c:set var="marginClass">margin-bottom-${cms.element.setting.marginbottom.value}</c:set>
				</c:if>

				<c:if test="${not empty cms.element.settings.classmainbox }">
					<c:set var="classmainbox">${cms.element.settings.classmainbox}</c:set>
				</c:if>

				<c:if test="${not empty cms.element.settings.fontsize }">
					<c:set var="fontsize">${cms.element.settings.fontsize}</c:set>
				</c:if>

				<c:if test="${not empty cms.element.settings.buttontype }">
					<c:set var="buttontype">${cms.element.settings.buttontype }</c:set>
				</c:if>
				<c:if test="${not empty cms.element.settings.buttonsize }">
					<c:set var="buttonsize">${cms.element.settings.buttonsize }</c:set>
				</c:if>

				<c:set var="hidetitle" >false</c:set>
				<c:if test="${cms.element.settings.hidetitle == 'true' }">
					<c:set var="hidetitle" >true</c:set>
				</c:if>

				<%-- Definimos el tipo de etiqueta html para nuestro encabezado --%>

				<c:set var="titletag" >h3</c:set>
				<c:set var="subtitletag" >h4</c:set>

				<c:if test="${not empty cms.element.settings.titletag }">
					<c:set var="titletag" >${cms.element.settings.titletag }</c:set>
					<c:if test="${titletag == 'h1'}">
						<c:set var="subtitletag" >h2</c:set>
					</c:if>
					<c:if test="${titletag == 'h2'}">
						<c:set var="subtitletag" >h3</c:set>
					</c:if>
					<c:if test="${titletag == 'h3'}">
						<c:set var="subtitletag" >h4</c:set>
					</c:if>
					<c:if test="${titletag == 'h4'}">
						<c:set var="subtitletag" >h5</c:set>
					</c:if>
				</c:if>

				<%-- Definimos las variables que gestionan el boton y la descripcion) --%>

				<c:if test="${cms.element.settings.showbutton == 'true' }">
					<c:set var="showbutton" >true</c:set>
				</c:if>

				<c:if test="${not empty cms.element.settings.textbutton}">
					<c:set var="textbutton" value="${cms.element.settings.textbutton}" />
				</c:if>
				<c:if test="${value.Teaser.isSet or value.ListDescription.isSet or value.LongTeaser.isSet}">
					<c:if test="${value.LongTeaser.isSet}">
						<c:set var="description" value="${value.LongTeaser}" />
					</c:if>
					<c:if test="${value.Teaser.isSet}">
						<c:set var="description" value="${value.Teaser}" />
					</c:if>
					<c:if test="${value.ListDescription.isSet}">
						<c:set var="description" value="${value.ListDescription}" />
					</c:if>

					<c:if test="${not empty cms.element.settings.maxdescriptioncharacters}">
						<c:set var="maxdescriptioncharacters" value="${cms.element.settings.maxdescriptioncharacters}" />
						<%
							String description = ""+pageContext.getAttribute ("description");
							Integer descriptionMaxCharacter = Integer.parseInt(pageContext.getAttribute ("maxdescriptioncharacters")+"");
							if (description.length() > descriptionMaxCharacter)
								description = description.substring(0,descriptionMaxCharacter)+ "...";
							pageContext.setAttribute("description",description);
						%>
					</c:if>
				</c:if>

				<%-- Definimos las variables que gestionan la posicion y el ancho del bloque de imagen (solo si hay imagen disponible) --%>

				<c:if test="${value.ListImage.isSet or value.Media.value.ImageMain.exists or value.Media.value.ImageMain.value.Image.isSet or value.Content.value.Media.value.ImageMain.exists or value.Content.value.Media.value.MediaMultiple.exists}">

					<c:set var="image" value="true" />
					<c:if test="${value.Content.value.Media.value.MediaMultiple.exists
	and value.Content.value.Media.value.MediaMultiple.value.MediaMultipleElements.exists
	and value.Content.value.Media.value.MediaMultiple.value.MediaMultipleElements.value.ImageMain.exists
	and value.Content.value.Media.value.MediaMultiple.value.MediaMultipleElements.value.ImageMain.value.Image.isSet}">
						<c:set var="imagePath">
							${value.Content.value.Media.value.MediaMultiple.value.MediaMultipleElements.value.ImageMain.value.Image}
						</c:set>
					</c:if>
					<c:if test="${value.Content.value.Media.exists
	and value.Content.value.Media.value.ImageMain.exists
	and value.Content.value.Media.value.ImageMain.value.Image.isSet}">
						<c:set var="imagePath">
							${value.Content.value.Media.value.ImageMain.value.Image}
						</c:set>
					</c:if>
					<c:if test="${value.Media.exists and value.Media.value.ImageMain.exists and value.Media.value.ImageMain.value.Image.isSet}">
						<c:set var="imagePath">
							${value.Media.value.ImageMain.value.Image}
						</c:set>
					</c:if>
					<c:if test="${value.ListImage.isSet}">
						<c:set var="imagePath">
							${value.ListImage.isSet}
						</c:set>
					</c:if>
				</c:if>

				<%-- controlamos que venga imagen --%>

				<c:set var="imageposition">none</c:set>
				<c:if test="${image == 'true'}">
					<c:if test="${not empty cms.element.settings.imageposition}">
						<c:set var="imageposition">${cms.element.settings.imageposition}</c:set>
					</c:if>
				</c:if>
				<%-- por defecto el ancho es 12 (100%) --%>
				<c:set var="imagewidth" value="12" />

				<c:if test="${imageposition == 'top'}">
					<c:set var="imagewidth">container-fluid</c:set>
				</c:if>
				<%-- si la imagen va a la izquierda o a la derecha se aplica el setting de ancho --%>
				<c:if test="${imageposition == 'left' or imageposition == 'right'}">
					<c:if test="${not empty cms.element.settings.imagewidth}">
						<c:set var="imagewidth" value="${cms.element.settings.imagewidth}" />
					</c:if>
				</c:if>

				<c:set var="equalver">equalheightarticlever-${id}</c:set>

				<c:if test="${not empty cms.element.settings.equalheight}">
					<c:set var="equalver">${cms.element.settings.equalheight}</c:set>
				</c:if>

				<div class="element parent sg-blog sg-microcontent sg-microcontent-panel box media-to-${imageposition}<c:out value=' ${marginClass} ${classmainbox} ${fontsize}' />"  style="border: solid 1px #ddd">

					<div class="element complete-mode with-media no-spacing-row">
						<div class="row object-aside">
							<div class="overlayed wrapper-media-bg-img col-sm-${imagewidth}<c:out value=" ${equalver}" /><c:if test='${imageposition == "right"}'> pull-right</c:if>" style="background-image: url('<cms:link>${imagePath}</cms:link>');background-position: center center;background-color: transparent;background-size: cover;color:inherit;min-height:100px">
								<a class="overlayer" href="<cms:link>${content.filename}</cms:link>"><span class="sr-only"><fnt:message key="key.go.to" />"${value.Title}"</span></a>
							</div>
							<div class="container-fluid media-body ${equalver}">
								<div class="media-body-wrapper" style="padding: 30px">
										<%-- Encabezado --%>
									<c:if test="${hidetitle == 'false' and (value.Title.isSet or value.SubTitle.isSet)}">
									<header>
										<${titletag} class="title" ${rdfa.Title}>
										<a href="<cms:link>${content.filename}</cms:link>">
												${value.Title}
										</a>
									</${titletag}>
									<c:if test="${value.SubTitle.isSet}">
									<${subtitletag} class="subtitle" ${rdfa.SubTitle}>${value.SubTitle}</${subtitletag}>
								</c:if>
								</header>
								</c:if>
									<%-- Descripcion --%>
								<c:if test="${not empty description}">
									<div class="description">
											${description}
									</div>
								</c:if>
								<c:if test="${value.Category.isSet }">
									<c:set var="pageurl">${themeConfiguration.CustomFieldKeyValue.UrlArticlesPage}</c:set>
									<c:if test="${empty pageurl}">
										<c:set var="pageurl">/articulos/</c:set>
									</c:if>

									<div class="categories mt-30">
										<c:set var="categorias" value="${fn:split(value.Category, ',')}" />
										<c:set var="counter" value="0" />

										<c:forEach items="${categorias }" var="category" varStatus="status">
											<%--====== Comprobamos si existe la propiedad Title especifica para el locale actual (Title_es, Title_en...) para la categoria.
                                            Si es asi usamos esa propiedad. Si no existe, usamos la propiedad Title =============================--%>
											<c:set var="categorytitle"><cms:property name="Title_${locale}" file="${category}"/></c:set>
											<c:if test="${empty categoryPropertyTitle}">
												<c:set var="categorytitle"><cms:property name="Title" file="${category}"/></c:set>
											</c:if>
											<c:set var="categoryhastipo" value="${fn:indexOf(category, '/tipo-turismo/')}" />
											<c:set var="categoryhasmunicipio" value="${fn:indexOf(category, '/municipios/')}" />
											<c:set var="categoryhasrecursos" value="${fn:indexOf(category, '/recursos-turisticos/')}" />
											<c:set var="categoryshort" value="${fn:replace(category, '/.categories/','')}" />
											<c:if test="${categoryhastipo != -1}">
												<c:set var="targetfield">3</c:set>
											</c:if>
											<c:if test="${categoryhasmunicipio != -1}">
												<c:set var="targetfield">4</c:set>
											</c:if>
											<c:if test="${categoryhasrecursos != -1}">
												<c:set var="targetfield">5</c:set>
											</c:if>
											<c:if test="${categoryhastipo != -1 or categoryhasmunicipio != -1 or categoryhasrecursos != -1}">
												<c:set var="counter" value="${counter + 1}" />
												<c:set var="linkquery">?buscadorthemesfield-1=&buscadorthemesfield-2_d1=&buscadorthemesfield-2_d2=&buscadorthemesfield-${targetfield}=${categoryshort}&numfield=3&searchaction=search&searchPage=1&submit=Buscar</c:set>
												<c:if test="${counter > 1}">&nbsp;&nbsp;&nbsp;</c:if>
												<a class="inline-b" href="<cms:link>${pageurl}${linkquery}</cms:link>" title="<fmt:message key="key.view.all.articles.category" />"><strong>#${categorytitle }</strong></a>
											</c:if>
										</c:forEach>
									</div>
								</c:if>
									<%-- Boton --%>
								<c:if test="${showbutton == 'true'}">
									<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-boton.jsp:06a371c6-b603-11e5-8e51-7fb253176922)">
										<cms:param name="tipoboton">${buttontype}</cms:param>
										<cms:param name="positionboton">text-right mt-30</cms:param>
										<cms:param name="sizeboton">${buttonsize}</cms:param>
										<cms:param name="textoboton">${textbutton} <span class="sr-only">"${value.Title}"</span></cms:param>
										<cms:param name="iconboton">fa-plus</cms:param>
										<cms:param name="linkboton">${content.filename}</cms:param>
										<cms:param name="targetboton">_self</cms:param>
									</cms:include>
								</c:if>
							</div>
						</div><%-- Fin de container-fluid --%>
					</div><%-- Fin de row --%>
				</div> <%-- Fin de element --%>
				<script>
					(function(){
						jQuery.fn.equalHeights = function() {
							var maxHeight = 0;
							// get the maximum height
							this.each(function(){
								var $this = jQuery(this);
								if ($this.height() > maxHeight) { maxHeight = $this.height(); }
							});
							// set the elements height
							this.each(function(){
								var $this = jQuery(this);
								$this.height(maxHeight);
							});
						};

						jQuery(document).ready(function() {
							var classes = ["equalheightarticlehor-${id}","equalheightarticlever-${id}"];
							for (var i = 0, j = classes.length; i < j; i++) {
								jQuery('.' + classes[i]).equalHeights();
							}
						});
					})();
				</script>
				</div><%-- Fin de caja --%>

			</c:otherwise>
		</c:choose>
	</cms:formatter>
</cms:bundle>