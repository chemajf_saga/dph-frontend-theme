<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates" %>
<%@ taglib prefix="ev" tagdir="/WEB-INF/tags/evento"%>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.evento.page">

<cms:formatter var="content" val="value" rdfa="rdfa">
<c:choose>
	<c:when test="${cms.element.inMemoryOnly}">
				<h1 class="title">
					Edite este recurso!!
				</h1>
	</c:when>
<c:otherwise>
<%-- Cargamos la variable con el id especifico del recurso para su uso posterior--%>
<c:set var="idresource" value="${content.id}" scope="request"/>
<c:set var="id">${fn:substringBefore(idresource, '-')}</c:set>

	<c:if test="${cms.element.settings.ical eq 'true'}">
		<c:set var="_evExportUri">
			<ev:eventExportUri event="${content.id.stringValue}"/>
		</c:set>
	</c:if>

<c:set var="locale">${cms.locale}</c:set>

<c:if test="${cms.element.setting.marginbottom.value != '0'}">
    <c:set var="marginClass">margin-bottom-${cms.element.setting.marginbottom.value}</c:set>
</c:if>
<c:if test="${not empty cms.element.settings.classmainbox }">
    <c:set var="classmainbox">${cms.element.settings.classmainbox}</c:set>
</c:if>

	<%-- Definimos el tipo de etiqueta html para nuestro encabezado --%>

	<c:set var="titletag">h1</c:set>
	<c:set var="subtitletag">h2</c:set>

	<c:if test="${not empty cms.element.settings.titletag }">
		<c:set var="titletag" scope="request">${cms.element.settings.titletag }</c:set>
		<c:if test="${titletag == 'h1'}">
			<c:set var="subtitletag">h2</c:set>
		</c:if>
		<c:if test="${titletag == 'h2'}">
			<c:set var="subtitletag">h3</c:set>
		</c:if>
		<c:if test="${titletag == 'h3'}">
			<c:set var="subtitletag">h4</c:set>
		</c:if>
		<c:if test="${titletag == 'h4'}">
			<c:set var="subtitletag">h5</c:set>
		</c:if>
		<c:if test="${titletag == 'h5'}">
			<c:set var="subtitletag">h6</c:set>
		</c:if>
		<c:if test="${titletag == 'div'}">
			<c:set var="subtitletag">div</c:set>
		</c:if>
	</c:if>
	<%-- si se ha editado el subtitulo lo mandamos en el request para que se tenga en cuenta en la seccion o en el bloque de texto por si se edita el titulo de seccion o de bloque --%>
	<c:if test="${value.SubTitle.isSet }">
		<c:set var="subtitletag" scope="request">${subtitletag}</c:set>
	</c:if>
	<%-- si no se ha editado el subtitulo lo mandamos vacio en el request --%>
	<c:if test="${!value.SubTitle.isSet }">
		<c:set var="subtitletag" scope="request"></c:set>
	</c:if>
	<%-- guardamos en una variable la ruta del themeconfig que le corresponde al recurso en funcion de la rama donde se este pintando. despues lo mandamos como parametro en el href del enlace para generar el pdf --%>
	<c:set var="themeconfigpath">${cms.vfs.context.siteRoot}${themeconfigpath}<sg:resourcePathLookBack filename=".themeconfig"/></c:set>

<article class="articulo parent element evento <c:out value=' ${marginClass} ${classmainbox} ' />">

<div class="wrapper">

<c:if test="${not cms.element.settings.hidetitle}">
	<!-- Cabecera del articulo -->
	<header class="headline">
		<c:if test="${cms.element.settings.generatepdf == 'true' }">
			<a class="btn btn-default btn-sm btn-pdf hidden-xs hidden-xxs pull-right hastooltip" title="<fmt:message key="label.download.title.pdf" />" href="<cms:pdf format='%(link.weak:/system/modules/com.saga.sagasuite.evento/elements/f-evento-detail-pdf.jsp:6b76b27a-1122-11e6-9a85-7fb253176922)' content='${content.filename}' locale='${locale}'/>?themeconfigpath=${themeconfigpath}" target="pdf">
				<span class="fa fa-file-pdf-o" aria-hidden="true"></span>&nbsp;<span class="btn-pdf-text"><fmt:message key="label.download.pdf" /></span>
			</a>
		</c:if>
		<c:if test="${cms.element.settings.ical eq 'true'}">
			<a class="btn btn-default btn-sm hidden-xs hidden-xxs pull-right hastooltip" title="<fmt:message key="label.export_ical" />" href="${_evExportUri}">
				<span class="fa fa-calendar" aria-hidden="true"></span>
			</a>
		</c:if>
		<c:if test="${cms.element.settings.subscribe == 'true' }">
			<sg:button-subscribe resource="${cms.element.resource}" css="hidden-xs hidden-xxs pull-right hastooltip"/>
		</c:if>
			<span class="time">
				<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.evento/elements/e-time-period.jsp:c8ff5c68-d38f-11e4-930e-01e4df46f753)">
					<cms:param name="inidate">${value.FichaEvento.value.FechaInicio }</cms:param>
					<c:if test="${value.FichaEvento.value.FechaFin.isSet }">
						<cms:param name="findate">${value.FichaEvento.value.FechaFin }</cms:param>
					</c:if>
					<cms:param name="showtime">false</cms:param>
				</cms:include>
			</span>
			<${titletag} class="title" ${rdfa.Title}>${value.Title}</${titletag}>
			<c:if test="${value.SubTitle.isSet }">
					<${subtitletag} class="subtitle" ${rdfa.SubTitle}>${value.SubTitle}</${subtitletag}>
			</c:if>
			<c:if test="${value.Category.isSet }">
				<div class="h5 upper municipio">
					<c:set var="categorias" value="${fn:split(value.Category, ',')}" />
					<c:set var="counter" value="0" />
					<c:forEach items="${categorias }" var="category" varStatus="status">
						<c:set var="categoryhasmunicipio" value="${fn:indexOf(category, '/municipios/')}" />
						<c:if  test="${categoryhasmunicipio != -1}">
							<%--====== Comprobamos si existe la propiedad Title especifica para el locale actual (Title_es, Title_en...) para la categoria.
							Si es asi usamos esa propiedad. Si no existe, usamos la propiedad Title =============================--%>
							<c:set var="categorytitle"><cms:property name="Title_${locale}" file="${category}"/></c:set>
							<c:if test="${empty categoryPropertyTitle}">
								<c:set var="categorytitle"><cms:property name="Title" file="${category}"/></c:set>
							</c:if>
							<c:set var="categoryshort" value="${fn:replace(category, '/.categories/','')}" />
							<a class="hastooltip" href="<cms:link>/zona-cultura/agenda/?buscadorthemeseventsfield-4=${categoryshort}</cms:link>" title="Ver todos los eventos de este municipio"><span class="pe-7s-map-marker mr-10" aria-hidden="true"></span>${categorytitle}</a>
						</c:if>
					</c:forEach>
				</div>
			</c:if>
		<c:if test="${cms.element.settings.generatepdf == 'true' or cms.element.settings.subscribe == 'true' }">
			<div class="visible-xxs visible-xs text-right margin-top-15">
				<c:if test="${cms.element.settings.subscribe == 'true' }">
					<sg:button-subscribe resource="${cms.element.resource}" css="visible-xxs-inline-block visible-xs-inline-block hastooltip"/>
				</c:if>
				<c:if test="${cms.element.settings.ical eq 'true'}">
					<a class="btn btn-default btn-sm visible-xs-inline-block visible-xxs-inline-block hastooltip" title="<fmt:message key="label.export_ical" />" href="${_evExportUri}" >
						<span class="fa fa-calendar" aria-hidden="true"></span>
					</a>
				</c:if>
				<c:if test="${cms.element.settings.generatepdf == 'true'}">
					<a class="btn btn-default btn-pdf btn-sm visible-xs-inline-block visible-xxs-inline-block hastooltip" title="<fmt:message key="label.download.title.pdf" />" href="<cms:pdf format='%(link.weak:/system/modules/com.saga.sagasuite.evento/elements/f-evento-detail-pdf.jsp:6b76b27a-1122-11e6-9a85-7fb253176922)' content='${content.filename}' locale='${locale}'/>?themeconfigpath=${themeconfigpath}" target="pdf">
						<span class="fa fa-file-pdf-o" aria-hidden="true"></span>&nbsp;<span class="btn-pdf-text"><fmt:message key="label.download.pdf" /></span>
					</a>
				</c:if>
			</div>
		</c:if>
		<div class="tags mt-30">
			<c:set var="categorias" value="${fn:split(value.Category, ',')}" />
			<c:set var="counter" value="0" />
			<ul class="list-unstyled list-inline ml-0 no-margin">
				<li><a class="hastooltip" href="<cms:link>/zona-cultura/agenda/</cms:link>" title="Ver todos los eventos con este tag"><strong>#Formación</strong></a></li>
				<li><a class="hastooltip" href="<cms:link>/zona-cultura/agenda/</cms:link>" title="Ver todos los eventos con este tag"><strong>#Gestión económica</strong></a></li>
			</ul>
		</div>
	</header>
</c:if>

	<c:if test="${cms.element.settings.hidetitle}">
		<c:if test="${cms.element.settings.generatepdf == 'true'}">
			<div class="clearfix margin-bottom-15">
				<c:if test="${cms.element.settings.generatepdf == 'true' }">
					<a class="btn btn-default btn-pdf btn-sm pull-right" title="<fmt:message key="label.download.title.pdf" />" href="<cms:pdf format='%(link.weak:/system/modules/com.saga.sagasuite.evento/elements/f-evento-detail-pdf.jsp:6b76b27a-1122-11e6-9a85-7fb253176922)' content='${content.filename}' locale='${locale}'/>" target="pdf">
						<span class="fa fa-file-pdf-o" aria-hidden="true"></span>&nbsp;<span class="btn-pdf-text"><fmt:message key="label.download.pdf" /></span>
					</a>
				</c:if>
				<c:if test="${cms.element.settings.ical eq 'true'}">
					<a class="btn btn-default btn-sm pull-right" title="<fmt:message key="label.export_ical" />" href="${_evExportUri}">
						<span class="fa fa-calendar" aria-hidden="true"></span>
					</a>
				</c:if>
				<c:if test="${cms.element.settings.subscribe == 'true' }">
					<sg:button-subscribe resource="${cms.element.resource}" css="pull-right"/>
				</c:if>
			</div>
		</c:if>
	</c:if>

<c:if test="${value.FichaEvento.exists}">
	<c:set var="fichaevento" value="${value.FichaEvento}" scope="request"/>
</c:if>

	<%-- Pintamos la ficha y el media principal --%>
	<c:set var="contentPrincipal" value="${content}" scope="request"></c:set>
	<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.evento/elements/e-evento-block.jsp:52d87257-b5db-11e5-8e51-7fb253176922)">
		<cms:param name="contentId">${id}-contentmain</cms:param>
	</cms:include>

	<%-- Pintamos los bloques de contenido --%>
	<c:if test="${value.Content.exists}">

		<c:forEach var="elem" items="${content.valueList.Content}" varStatus="status">
			<c:set var="contentblock" value="${elem}" scope="request"></c:set>
			<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-default-contentblock.jsp:e9a36d37-df65-11e4-bcf9-01e4df46f753)">
				<cms:param name="contentId">${id}-contentblock-${status.count}</cms:param>
			</cms:include>
		</c:forEach>

	</c:if> <%-- Fin cierre comprobacion de seccion --%>

	<!-- Pie del articulo -->
	<c:if test="${content.value.ShowFooter == 'true' }">
		<div class="posted">
			<span class="fa fa-calendar" aria-hidden="true"></span>&nbsp;&nbsp;
			<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-time.jsp:ea2c737f-df65-11e4-bcf9-01e4df46f753)">
				<cms:param name="date">${value.Date }</cms:param>
				<cms:param name="showtime">false</cms:param>
			</cms:include>
		</div>
		<c:if test="${value.Author.exists }">
			<div class="autor"><span class="fa fa-user" aria-hidden="true"></span>&nbsp;&nbsp;${value.Author.value.Author }</div>
		</c:if>
		<c:if test="${value.Source.isSet }">
			<div class="fuente"><span class="fa fa-book" aria-hidden="true"></span>&nbsp;&nbsp;${value.Source}</div>
		</c:if>
	</c:if>
</div> <!-- Fin de wrapper -->
</article>
	<c:set var="titletag"></c:set>
	<c:set var="subtitletag"></c:set>
	<c:set var="titlesection"></c:set>
	<c:set var="titleblock"></c:set>
	<c:set var="titlegallery"></c:set>
</c:otherwise>
</c:choose>
</cms:formatter>
</cms:bundle>