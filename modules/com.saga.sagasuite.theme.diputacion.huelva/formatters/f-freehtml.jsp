<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.opencms.microcontent.messages">
<cms:formatter var="content" val="value" rdfa="rdfa">

<c:choose>
	<c:when test="${cms.element.inMemoryOnly}">
			<header class="headline">
				<hgroup>
					<h1 class="title">Edite este recurso!</h1>
				</hgroup>
			</header>
	</c:when>
	<c:otherwise>
<div class="element parent sg-freehtml ${value.CssClass}">
	<div class="wrapper">				
		<c:if test="${value.Title.isSet and value.VerTituloRecurso == 'true'}">
			<div class="headline">
				<div class="title h3">
					${value.Title}
				</div>
			</div>
		</c:if>
		<c:if test="${value.Content.isSet}">
			<div>
				${value.Content}
			</div>
		</c:if>
		<c:if test="${value.Code.isSet}">
			<div>
				${value.Code}
			</div>
		</c:if>		
	</div>	
</div>

</c:otherwise>
</c:choose>
</cms:formatter>
</cms:bundle>