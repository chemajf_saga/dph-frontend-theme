<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${cms.locale}" />
<fmt:bundle basename="com.saga.opencms.microcontent.messages">
<cms:formatter var="content" val="value" rdfa="rdfa">

<c:choose>
	<c:when test="${cms.element.inMemoryOnly}">

			<header>
				<h1 class="title">Edite este recurso!</h1>
			</header>

	</c:when>
	<c:otherwise>
	
<c:if test="${cms.element.setting.marginbottom.value != '0'}">
    <c:set var="marginClass">margin-bottom-${cms.element.setting.marginbottom.value}</c:set>
</c:if>

<c:if test="${not empty cms.element.settings.classmainbox }">
    <c:set var="classmainbox">${cms.element.settings.classmainbox}</c:set>
</c:if>

<c:if test="${not empty cms.element.settings.fontsize }">
    <c:set var="fontsize">${cms.element.settings.fontsize}</c:set>
</c:if>

<c:if test="${not empty cms.element.settings.wellbox }">
	<c:set var="wellbox" scope="request">well ${cms.element.settings.wellbox}</c:set>
</c:if>

<c:set var="hidetitle" scope="request">false</c:set>
<c:if test="${cms.element.settings.hidetitle == 'true' }">
	<c:set var="hidetitle" scope="request">true</c:set>
</c:if>

<c:if test="${not empty cms.element.settings.equalheight}">
	<c:set var="equalheight">${cms.element.settings.equalheight}</c:set>
</c:if>

<%-- Definimos el tipo de etiqueta html para nuestro encabezado --%>

<c:set var="titletag" scope="request">h3</c:set>
<c:set var="subtitletag" scope="request">h4</c:set>

<c:if test="${not empty cms.element.settings.titletag }">
	<c:set var="titletag" scope="request">${cms.element.settings.titletag }</c:set>
	<c:if test="${titletag == 'h1'}">
		<c:set var="subtitletag" scope="request">h2</c:set>
	</c:if>
	<c:if test="${titletag == 'h2'}">
		<c:set var="subtitletag" scope="request">h3</c:set>
	</c:if>
	<c:if test="${titletag == 'h3'}">
		<c:set var="subtitletag" scope="request">h4</c:set>
	</c:if>
	<c:if test="${titletag == 'h4'}">
		<c:set var="subtitletag" scope="request">h5</c:set>
	</c:if>
	<c:if test="${titletag == 'h5'}">
		<c:set var="subtitletag" scope="request">h6</c:set>
	</c:if>
	<c:if test="${titletag == 'div'}">
		<c:set var="subtitletag" scope="request">div</c:set>
	</c:if>
</c:if>

<%-- definimos el tamaño del encabezado --%>

<c:if test="${not empty cms.element.settings.titlesize }">
	<c:set var="titlesize" scope="request">${cms.element.settings.titlesize }</c:set>
</c:if>

<c:if test="${not empty cms.element.settings.parallax }">
	<c:set var="parallax" value="${cms.element.settings.parallax }" />
</c:if>

<c:if test="${not empty cms.element.settings.height }">
	<c:set var="height">display:table;width:100%;table-layout:fixed;height:1px;min-height: ${cms.element.settings.height }px;</c:set>
</c:if>

<c:forEach var="elem" items="${content.subValueList['Element']}" varStatus="status">
	<c:set var="bgimage"></c:set>
	<c:if test="${not empty themeConfiguration.CustomFieldKeyValue.defaultimg}">
		<c:set var="bgimage">${themeConfiguration.CustomFieldKeyValue.defaultimg}</c:set>
	</c:if>
	<c:if test="${elem.name == 'CompleteImage' and elem.value.Image.value.Image.isSet}">
		<c:set var="bgimage" value="${elem.value.Image.value.Image}" />
	</c:if>
</c:forEach>
	<c:set var="spacer" value=" "/>
	<c:if test="${empty bgimage}">
		<c:set var="bgattributes">style="${height}"</c:set>
	</c:if>
	<c:if test="${not empty bgimage}">
		<c:if test="${parallax =='true' }">
			<c:set var="bgattributes">style="${height}" data-parallax="scroll" data-image-src="<cms:link>${bgimage}</cms:link>"</c:set>
		</c:if>
		<c:if test="${empty parallax or parallax !='true' }">
			<c:set var="bgattributes">style="${height}background-image:url('<cms:link>${bgimage}</cms:link>');background-size:cover;background-position: center center;background-repeat: no-repeat;"</c:set>
		</c:if>
	</c:if>
	<c:if test="${value.Id.isSet}">
		<c:set var="idattr">id="${value.Id}"</c:set>
	</c:if>

	<div class="element parent sg-microcontent sg-microcontent-panel sg-microcontent-overlay box<c:out value=' ${marginClass} ${classmainbox} ${value.CssClass} ${fontsize} ${equalheight}'/>"${spacer}${bgattributes}${spacer}${idattr}>

	<c:forEach var="elem" items="${content.subValueList['Element']}" varStatus="status">
				<c:set var="element" value="${elem }" scope="request"/>
					<c:choose>
						<%-- VIEWTYPE: Solo Imagen: Banner --%>
						<c:when test="${elem.name == 'OnlyImage'}">
							<div class="alert alert-warning alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong>Necesitas una imagen o un vídeo.</strong> Para poder usar este formatter de texto superpuesto necesitas crear un elemento completo con imagen.
							</div>
						</c:when>
						<%-- VIEWTYPE: Solo Texto: OnlyText --%>
						<c:when test="${elem.name == 'OnlyText'}">
							<div class="alert alert-warning alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong>Necesitas una imagen o un vídeo.</strong> Para poder usar este formatter de texto superpuesto necesitas crear un elemento completo con imagen.
							</div>
						</c:when>
						<%-- VIEWTYPE: Completo con imagen: Titulo, descripcion, imagen, enlace --%>
						<c:when test="${elem.name == 'CompleteImage'}">
							<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/e-element-complete-image-panel-overlay.jsp:9276f661-e555-11e7-a388-63f9a58dae09)">
								<cms:param name="count">${status.count}</cms:param>
							</cms:include>
						</c:when>

						<%-- VIEWTYPE: Completo con video: Titulo, descripcion, video, enlace --%>
						<c:when test="${elem.name == 'CompleteVideo' or elem.name == 'CompleteVideoFile'}">
							<div class="alert alert-warning alert-dismissible fade in" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<strong>Necesitas una imagen o un vídeo.</strong> Para poder usar este formatter de texto superpuesto necesitas crear un elemento completo con imagen.
							</div>
						</c:when>
					</c:choose>
			</c:forEach>

</div>
</c:otherwise>
</c:choose>
	<c:set var="titletag" scope="request"></c:set>
	<c:set var="titlesize" scope="request"></c:set>
	<c:set var="subtitletag" scope="request"></c:set>
	<c:set var="hidetitle" scope="request"></c:set>
</cms:formatter>
</fmt:bundle>
