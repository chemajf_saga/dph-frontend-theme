<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.opencms.microcontent.messages">
<cms:formatter var="content" val="value" rdfa="rdfa">

<c:choose>
	<c:when test="${cms.element.inMemoryOnly}">
			<header class="clearfix alert alert-info">
				<h1 class="title">Edite este recurso!</h1>
			</header>
	</c:when>
	<c:otherwise>

<%-- Cargamos la variable con el id especifico del recurso para su uso posterior--%>
<c:set var="idresource" value="${content.id}"></c:set>
<c:set var="id">${fn:substringBefore(idresource, '-')}</c:set>
	
<c:if test="${cms.element.setting.marginbottom.value != '0'}">
    <c:set var="marginClass">margin-bottom-${cms.element.setting.marginbottom.value}</c:set>
</c:if>

<c:if test="${not empty cms.element.settings.classmainbox }">
    <c:set var="classmainbox">${cms.element.settings.classmainbox}</c:set>
</c:if>

<c:if test="${not empty cms.element.settings.captionbgcolor }">
	<c:set var="captionbgcolor">${cms.element.settings.captionbgcolor}</c:set>
</c:if>

<%-- Definimos el tipo de etiqueta html para nuestro encabezado --%>

<c:set var="titletag" scope="request">h3</c:set>
<c:set var="subtitletag" scope="request">h4</c:set>

<c:if test="${not empty cms.element.settings.titletag }">
	<c:set var="titletag" scope="request">${cms.element.settings.titletag }</c:set>
</c:if>

<%-- definimos el tamaño del encabezado --%>

<c:if test="${not empty cms.element.settings.titlesize }">
	<c:set var="titlesize" scope="request">${cms.element.settings.titlesize }</c:set>
</c:if>
<c:set var="equalheight">equalheight-area-boxes</c:set>
<c:if test="${not empty cms.element.settings.equalheight}">
	<c:set var="equalheight">${cms.element.settings.equalheight}</c:set>
</c:if>

<div class="element parent sg-microcontent sg-microcontent-panel sg-microcontent-panel-area box <c:out value=' ${marginClass} ${classmainbox} ${value.CssClass}' />" <c:if test="${value.Id.isSet}">id="${value.Id}"</c:if>>

	<c:forEach var="elem" items="${content.subValueList['Element']}" varStatus="status">
		<c:set var="element" value="${elem }" scope="request"/>
		<div class="wrapper">
			<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/e-element-complete-image-panel.jsp:92850025-e555-11e7-a388-63f9a58dae09)">
				<cms:param name="count">${status.count}</cms:param>
				<cms:param name="id">${id}</cms:param>
				<cms:param name="equalheight">equalheight-area-boxes-${equalheight}</cms:param>
				<cms:param name="captionbgcolor">${captionbgcolor}</cms:param>
			</cms:include>
		</div>
	</c:forEach>
</div>
</c:otherwise>
</c:choose>
<c:set var="titletag" scope="request"></c:set>
<c:set var="titlesize" scope="request"></c:set>
<c:set var="subtitletag" scope="request"></c:set>
<c:set var="hidetitle" scope="request"></c:set>
</cms:formatter>
</cms:bundle>
