<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">

<cms:formatter var="content" val="value" rdfa="rdfa">
<c:choose>
	<c:when test="${cms.element.inMemoryOnly}">
			<header><h1 class="title">Edite este recurso para configurar el listado</h1></header>
	</c:when>
	<c:otherwise>
	
<c:if test="${cms.element.setting.marginbottom.value != '0'}">
    <c:set var="marginClass">margin-bottom-${cms.element.setting.marginbottom.value}</c:set>
</c:if>
<c:if test="${not empty cms.element.settings.classmainbox }">
    <c:set var="classmainbox">${cms.element.settings.classmainbox}</c:set>
</c:if>
<c:if test="${cms.element.settings.featuredtitle =='true' }">
    <c:set var="featuredtitle">featured-title</c:set>
</c:if>
		<c:set var="showquery">false</c:set>
		<c:if test="${empty param.showquery }">
			<c:if test="${not empty cms.element.settings.showquery }">
				<c:set var="showquery">${cms.element.settings.showquery }</c:set>
			</c:if>
		</c:if>
		<c:if test="${not empty param.showquery }">
			<c:set var="showquery">${param.showquery }</c:set>
		</c:if>

		<%-- Definimos el tipo de etiqueta html para nuestro encabezado y encabezado de elementos que se listan --%>

<c:set var="titletag">h2</c:set>
<c:set var="titletagelement" scope="request">h3</c:set>

<c:if test="${not empty cms.element.settings.titletag }">
	<c:set var="titletag">${cms.element.settings.titletag }</c:set>
	<c:if test="${titletag == 'h1'}">
		<c:set var="titletagelement" scope="request">h2</c:set>
	</c:if>
	<c:if test="${titletag == 'h2'}">
		<c:set var="titletagelement" scope="request">h3</c:set>
	</c:if>
	<c:if test="${titletag == 'h3'}">
		<c:set var="titletagelement" scope="request">h4</c:set>
	</c:if>
	<c:if test="${titletag == 'h4'}">
		<c:set var="titletagelement" scope="request">h5</c:set>
	</c:if>
	<c:if test="${titletag == 'h5'}">
		<c:set var="titletagelement" scope="request">h6</c:set>
	</c:if>
	<c:if test="${titletag == 'div'}">
		<c:set var="titletagelement" scope="request">div</c:set>
	</c:if>
</c:if>

<c:if test="${not empty cms.element.settings.titletagsize }">
	<c:set var="titletagsize">${cms.element.settings.titletagsize }</c:set>
</c:if>
<c:if test="${not empty cms.element.settings.titletagicon }">
	<c:set var="titletagicon">${cms.element.settings.titletagicon }</c:set>
</c:if>
<c:if test="${not empty cms.element.settings.titletagelementsize }">
	<c:set var="titletagelementsize" scope="request">${cms.element.settings.titletagelementsize }</c:set>
</c:if>
<c:if test="${not empty cms.element.settings.titletagelement }">
	<c:set var="titletagelement" scope="request">${cms.element.settings.titletagelement }</c:set>
</c:if>
<c:if test="${not empty cms.element.settings.equalheight }">
	<c:set var="equalheight" scope="request">${cms.element.settings.equalheight }</c:set>
</c:if>


	<c:set var="showdate" scope="request">${cms.element.settings.showdate }</c:set>
	<c:set var="showcat" scope="request">${cms.element.settings.showcat }</c:set>
	<c:set var="showbutton" scope="request">${cms.element.settings.showbutton }</c:set>

		<%-- definimos que se muestre el titulo de la categoria en el titulo del listado --%>
<c:if test="${not empty cms.element.settings.showcategorytitle and cms.element.settings.showcategorytitle == 'true' }">
	<c:set var="categoryTitle"></c:set>
	<%-- Detectamos cual es el locale principal --%>
	<c:set var="locales"><cms:property name="locale-available" file="search" /></c:set>
	<c:set var="locales" value="${fn:split(locales, ',')}" />
	<c:set var="localemain" value="${locales[0]}"/>

	<%-- Si se ha marcado el readPropertyCategory tenemos que buscar la propiedad category --%>
	<c:if test="${content.value.readPropertyCategory.exists && content.value.readPropertyCategory=='true'}">
		<c:set var="categoryProperty"><cms:property name="category" file="search"/></c:set>
		<c:set var="categoryTitle">
			<cms:property name="Title" file="${categoryProperty}"/>
			<c:if test="${locale != localemain}">
				<cms:property name="Title_${locale}" file="${categoryProperty}"/>
			</c:if>
		</c:set>
	</c:if>
</c:if>

<div id="solrlist-${value.Id}" class="element parent list sg-solrlist<c:out value=' ${marginClass} ${classmainbox} ${value.CssClass}' />">


<div class="wrapper">

<%-- Cargamos las variables que luego vamos a ir necesitando a lo largo --%>
<c:set var="cmsObject" value="${cms.vfs.cmsObject }" scope="request"/>
<c:set var="value" value="${value }" scope="request" />
<c:set var="content" value="${content }" scope="request" />
<c:set var="currentSite" scope="request">${cms.requestContext.siteRoot }</c:set>
<c:set var="currentProject" scope="request">${cms.requestContext.currentProject }</c:set>
<c:set var="locale" scope="request">${cms.locale}</c:set>

<c:if test="${value.FormatDate.isSet }">
	<c:set var="formatdate" scope="request">${value.FormatDate}</c:set>
</c:if>
<c:if test="${!value.FormatDate.isSet }">
	<c:set var="formatdate" scope="request">${value.FormatDate}</c:set>
</c:if>

<%-- Variables que definen si se muestran los resultados en cuadricula --%>

<c:if test="${value.ViewInRow.exists and value.ViewInRow=='true'}">

	<c:set var="viewinrow" scope="request">row</c:set>

	<c:set var="elementsbyrow" scope="request">${value.NumberElementsByRow}</c:set>

	<c:set var="elementsbyrowsm" scope="request">${value.NumberElementsByRowSm}</c:set>

	<c:set var="elementsbyrowmd" scope="request">${value.NumberElementsByRowMd}</c:set>

	<c:set var="elementsbyrowlg" scope="request">${value.NumberElementsByRowLg}</c:set>

	<%-- Definimos los anchos de columna para las distintas resoluciones --%>

		<c:choose>
			<c:when test="${elementsbyrow == 1}">
				<c:set var="widthxs" >col-xs-12</c:set>
			</c:when>
			<c:when test="${elementsbyrow == 2}">
				<c:set var="widthxs" >col-xs-6</c:set>
			</c:when>
			<c:when test="${elementsbyrow == 3}">
				<c:set var="widthxs" >col-xs-4</c:set>
			</c:when>
			<c:when test="${elementsbyrow == 4}">
				<c:set var="widthxs" >col-xs-3</c:set>
			</c:when>
			<c:when test="${elementsbyrow == 6}">
				<c:set var="widthxs" >col-xs-2</c:set>
			</c:when>
			<c:when test="${elementsbyrow == 12}">
				<c:set var="widthxs" >col-xs-1</c:set>
			</c:when>
		</c:choose>

		<c:choose>
			<c:when test="${elementsbyrowsm == 1}">
				<c:set var="widthsm" >col-sm-12</c:set>
			</c:when>
			<c:when test="${elementsbyrowsm == 2}">
				<c:set var="widthsm" >col-sm-6</c:set>
			</c:when>
			<c:when test="${elementsbyrowsm == 3}">
				<c:set var="widthsm" >col-sm-4</c:set>
			</c:when>
			<c:when test="${elementsbyrowsm == 4}">
				<c:set var="widthsm" >col-sm-3</c:set>
			</c:when>
			<c:when test="${elementsbyrowsm == 6}">
				<c:set var="widthsm" >col-sm-2</c:set>
			</c:when>
			<c:when test="${elementsbyrowsm == 12}">
				<c:set var="widthsm" >col-sm-1</c:set>
			</c:when>
		</c:choose>

		<c:choose>
			<c:when test="${elementsbyrowmd == 1}">
				<c:set var="widthmd" >col-md-12</c:set>
			</c:when>
			<c:when test="${elementsbyrowmd == 2}">
				<c:set var="widthmd" >col-md-6</c:set>
			</c:when>
			<c:when test="${elementsbyrowmd == 3}">
				<c:set var="widthmd" >col-md-4</c:set>
			</c:when>
			<c:when test="${elementsbyrowmd == 4}">
				<c:set var="widthmd" >col-md-3</c:set>
			</c:when>
			<c:when test="${elementsbyrowmd == 6}">
				<c:set var="widthmd" >col-md-2</c:set>
			</c:when>
			<c:when test="${elementsbyrowmd == 12}">
				<c:set var="widthmd" >col-md-1</c:set>
			</c:when>
		</c:choose>

		<c:choose>
			<c:when test="${elementsbyrowlg == 1}">
				<c:set var="widthlg" >col-lg-12</c:set>
			</c:when>
			<c:when test="${elementsbyrowlg == 2}">
				<c:set var="widthlg" >col-lg-6</c:set>
			</c:when>
			<c:when test="${elementsbyrowlg == 3}">
				<c:set var="widthlg" >col-lg-4</c:set>
			</c:when>
			<c:when test="${elementsbyrowlg == 4}">
				<c:set var="widthlg" >col-lg-3</c:set>
			</c:when>
			<c:when test="${elementsbyrowlg == 6}">
				<c:set var="widthlg" >col-lg-2</c:set>
			</c:when>
			<c:when test="${elementsbyrowlg == 12}">
				<c:set var="widthlg" >col-lg-1</c:set>
			</c:when>
		</c:choose>

	<%-- Concatenamos todos los anchos por resolucion en la misma variable --%>

	<c:set var="width" scope="request"><c:out value="col-xxs-12 ${widthxs} ${widthsm} ${widthmd} ${widthlg}" /></c:set>


</c:if>
<c:if test="${not value.ViewInRow.exists or value.ViewInRow=='false'}">
	<c:set var="viewinrow" scope="request"></c:set>
	<c:set var="elementsbyrow" scope="request"></c:set>
	<c:set var="elementsbyrowsm" scope="request"></c:set>
	<c:set var="elementsbyrowmd" scope="request"></c:set>
	<c:set var="elementsbyrowlg" scope="request"></c:set>
	<c:set var="width" scope="request"></c:set>
</c:if>

	<%-- En función del campo de posición de la imagen mandamos el valor necesario para el media object --%>
	<c:if test="${value.ImageElementPosition.isSet }">
		<c:set var="imageBlockPosition" scope="request">${value.ImageElementPosition }</c:set>
	</c:if>
	<c:if test="${!value.ImageElementPosition.isSet }">
		<c:set var="imageBlockPosition" scope="request">left</c:set>
	</c:if>
	<c:if test="${not empty cms.element.settings.showdate}">
		<c:set var="showdate" scope="request">${cms.element.settings.showdate}</c:set>
	</c:if>

	<c:if test="${cms.element.settings.dividertop}">
		<!-- Separador del recurso -->
		<hr class="divider">
	</c:if>

	<c:if test="${not cms.element.settings.hidetitle}">
		<div class="section-title">
			<header class="headline <c:out value='${featuredtitle}' />">
				<${titletag} class="title ${titletagsize}" <c:if test="${!value.DisplayTitle.isSet }">${rdfa.Title}</c:if> <c:if test="${value.DisplayTitle.isSet }">${rdfa.DisplayTitle}</c:if>>
				<c:if test="${value.DisplayTitle.isSet }">
					<c:if test="${not empty titletagicon}"><span class="v-align-m inline-b mr-10 fs40 ${titletagicon}" aria-hidden="true"></span></c:if>
					${value.DisplayTitle}${categoryTitle}
				</c:if>
				<c:if test="${!value.DisplayTitle.isSet }">
					<c:if test="${not empty titletagicon}"><span class="v-align-m inline-b mr-10 fs40 ${titletagicon}" aria-hidden="true"></span></c:if>
					${value.Title}${categoryTitle}
				</c:if>
			</${titletag}>
			</header>
		</div>
	</c:if>
	<c:if test="${value.TextHeader.isSet}">
		<div class="list-top-text" ${rdfa.TextHeader}>
			${value.TextHeader}
		</div>
	</c:if>

	<%-- Listamos el contenido que sera publicado en el futuro --%>
	<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-datereleased-content.jsp:e5699e8a-d223-11e4-a69e-01e4df46f753)"/>

	<%-- Hacemos el include de la jsp que se encarga de crear la query de solr en base a la configuracion realizada --%>
	<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/c-solrquery-folder-or-category.jsp:eb009647-3e2b-11e8-b79c-63f9a58dae09)">
			<cms:param name="showquery">${showquery}</cms:param>
	</cms:include>

	<%-- USER FILTER --%>
	<c:if test="${content.value.CategoryUserFilter.isSet }">
		<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-user-filter.jsp:65202f86-877c-11e4-bce0-000c2904937e)">
			<cms:param name="catlevel">${content.value.CategoryUserFilter.value.CatLevel}</cms:param>
		</cms:include>
	</c:if>

	<%-- LISTADO --%>

	<%-- Comprobamos si lo que hay que listar es un binario o un XML --%>
	<c:if test="${value.ResourceType == 'binary' or value.ResourceType == 'image'}">
		<%-- JSP que realiza la consulta a solr utilizando un resourceload --%>
		<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-resultlist-resourceload.jsp:059883b1-db4e-11e3-9a12-f18cf451b707)"/>
	</c:if>
	<c:if test="${value.ResourceType != 'binary' and value.ResourceType != 'image'}">
		<%--JSP que realiza la consulta a solr utilizando un contentload--%>
		<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/c-resultlist-contentload-masonry.jsp:6042d778-04f2-11e8-a124-63f9a58dae09)"/>
	</c:if>

	<%-- JSP que muestra la paginacion en base a la configuracion realizada y al numero de resultados encontrados --%>
	<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-pagination.jsp:0f3c868f-454d-11e3-8ac6-2182af1ef88f)"/>
	<c:if test="${value.TextFooter.isSet}">
		<div class="list-bottom-text" ${rdfa.TextFooter}>
			${value.TextFooter}
		</div>
	</c:if>
	</div>
	<c:if test="${cms.element.settings.dividerbottom}">
		<!-- Separador del recurso -->
		<hr class="divider">
	</c:if>
</div>
<%-- Limpiamos los parametros que van en el request para que no los cojan otros listados en la pagina --%>
<c:set var="linkDetail" scope="request"></c:set>
<c:set var="formatdate" scope="request"></c:set>
<c:set var="title" scope="request"></c:set>
<c:set var="titlelarge" scope="request"></c:set>
<c:set var="description" scope="request"></c:set>
<c:set var="date" scope="request"></c:set>
<c:set var="imagePath" scope="request"></c:set>
<c:set var="descriptionMaxCharacter" scope="request"></c:set>
<c:set var="titleMaxCharacter" scope="request"></c:set>
<c:set var="imageWidth" scope="request"></c:set>
<c:set var="imageHeight" scope="request"></c:set>
<c:set var="width" scope="request"></c:set>
<c:set var="imageBlockPosition" scope="request"></c:set>
<c:set var="imageBlockWidth" scope="request"></c:set>
<c:set var="textBlockWidth" scope="request"></c:set>
<c:set var="value"  scope="request" />
<c:set var="content"  scope="request" />
<c:set var="currentSite" scope="request"></c:set>
<c:set var="currentProject" scope="request"></c:set>
<c:set var="locale" scope="request"></c:set>
<c:set var="titletagelement" scope="request"></c:set>
<c:set var="equalheight" scope="request"></c:set>
<c:set var="showdate" scope="request"></c:set>
</c:otherwise>
</c:choose>
</cms:formatter>
</cms:bundle>