<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
<cms:formatter var="content" val="value" rdfa="rdfa">

	<c:choose>
		<c:when test="${cms.element.inMemoryOnly}">
			<div class="template-container bg-gray-lighter big-block">
				<div class="container">
					<header><h1 class="title">Edite este recurso para configurar el listado</h1></header>
				</div>
			</div>
		</c:when>
		<c:otherwise>

			<c:if test="${cms.element.setting.marginbottom.value != '0'}">
				<c:set var="marginClass">margin-bottom-${cms.element.setting.marginbottom.value}</c:set>
			</c:if>
			<c:if test="${not empty cms.element.settings.classmainbox }">
				<c:set var="classmainbox">${cms.element.settings.classmainbox}</c:set>
			</c:if>
			<c:if test="${not empty cms.element.settings.classpadding }">
				<c:set var="classpadding">${cms.element.settings.classpadding}</c:set>
			</c:if>
			<c:if test="${not empty cms.element.settings.classlist }">
				<c:set var="classlist">${cms.element.settings.classlist}</c:set>
			</c:if>
			<c:if test="${not empty cms.element.settings.sectionid }">
				<c:set var="sectionid">id="${cms.element.settings.sectionid}"</c:set>
			</c:if>
			<c:if test="${cms.element.settings.featuredtitle =='true' }">
				<c:set var="featuredtitle">featured-title</c:set>
			</c:if>
			<c:set var="spacer" value=" " />
			<%-- Definimos el tipo de etiqueta html para nuestro encabezado y encabezado de elementos que se listan --%>

			<c:set var="titletag">h2</c:set>
			<c:set var="titletagelement">h3</c:set>

			<c:if test="${not empty cms.element.settings.titletag }">
				<c:set var="titletag">${cms.element.settings.titletag }</c:set>
				<c:if test="${titletag == 'h1'}">
					<c:set var="titletagelement">h2</c:set>
				</c:if>
				<c:if test="${titletag == 'h2'}">
					<c:set var="titletagelement">h3</c:set>
				</c:if>
				<c:if test="${titletag == 'h3'}">
					<c:set var="titletagelement">h4</c:set>
				</c:if>
				<c:if test="${titletag == 'h4'}">
					<c:set var="titletagelement">h5</c:set>
				</c:if>
				<c:if test="${titletag == 'h5'}">
					<c:set var="titletagelement">h6</c:set>
				</c:if>
				<c:if test="${titletag == 'div'}">
					<c:set var="titletagelement">div</c:set>
				</c:if>
			</c:if>

			<c:if test="${not empty cms.element.settings.titletagsize }">
				<c:set var="titletagsize">${cms.element.settings.titletagsize }</c:set>
			</c:if>
			<c:if test="${not empty cms.element.settings.titletagicon }">
				<c:set var="titletagicon">${cms.element.settings.titletagicon }</c:set>
			</c:if>
			<c:if test="${not empty cms.element.settings.titletagelement }">
				<c:set var="titletagelement">${cms.element.settings.titletagelement }</c:set>
			</c:if>

			<c:if test="${not empty cms.element.settings.titletagelementsize }">
				<c:set var="titletagelementsize">${cms.element.settings.titletagelementsize }</c:set>
			</c:if>
			<c:set var="titleblockfeaturedevent"><fmt:message key="key.formatter.featured.event.title" /></c:set>
			<c:set var="titleblocknextevents"><fmt:message key="key.formatter.next.events.title" /></c:set>
			<c:if test="${not empty cms.element.settings.titleblockfeaturedevent }">
				<c:set var="titleblockfeaturedevent">${cms.element.settings.titleblockfeaturedevent }</c:set>
			</c:if>
			<c:if test="${not empty cms.element.settings.titleblocknextevents }">
				<c:set var="titleblocknextevents">${cms.element.settings.titleblocknextevents }</c:set>
			</c:if>

			<c:if test="${not empty cms.element.settings.showcategorytitle and cms.element.settings.showcategorytitle == 'true' }">
				<c:set var="categoryTitle"></c:set>
				<%-- Detectamos cual es el locale principal --%>
				<c:set var="locales"><cms:property name="locale-available" file="search" /></c:set>
				<c:set var="locales" value="${fn:split(locales, ',')}" />
				<c:set var="localemain" value="${locales[0]}"/>

				<%-- Si se ha marcado el readPropertyCategory tenemos que buscar la propiedad category --%>
				<c:if test="${content.value.readPropertyCategory.exists && content.value.readPropertyCategory=='true'}">
					<c:set var="categoryProperty"><cms:property name="category" file="search"/></c:set>
					<c:set var="categoryTitle">
						<cms:property name="Title" file="${categoryProperty}"/>
						<c:if test="${locale != localemain}">
							<cms:property name="Title_${locale}" file="${categoryProperty}"/>
						</c:if>
					</c:set>
				</c:if>
			</c:if>
				<%-- Cargamos las variables que luego vamos a ir necesitando a lo largo --%>
			<c:set var="cmsObject" value="${cms.vfs.cmsObject }" scope="request"/>
			<c:set var="value" value="${value }" scope="request" />
			<c:set var="content" value="${content }" scope="request" />
			<c:set var="currentSite" scope="request">${cms.requestContext.siteRoot }</c:set>
			<c:set var="currentProject" scope="request">${cms.requestContext.currentProject }</c:set>
			<c:set var="locale" scope="request">${cms.locale}</c:set>
			<div>
			<%-- Comprobación de si hay resultados previa al renderizado completo del recurso --%>
			<%-- Hacemos el include de la jsp que se encarga de crear la query de solr en base a la configuracion realizada --%>
			<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/c-solrquery-folder-or-category.jsp:eb009647-3e2b-11e8-b79c-63f9a58dae09)">
				<cms:param name="showquery">false</cms:param>
			</cms:include>
			<%--JSP que realiza la consulta a solr utilizando un contentload--%>
			<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/c-resultlist-contentload-check.jsp:fb784e2f-4df2-11e8-ac56-63f9a58dae09)" />
			<%-- si hay resultados --%>
				<c:if test="${withResults == 'true' and value.ResourceType == 'dphevento'}">
					<div class="template-container big-block<c:out value=' ${marginClass} ${classmainbox} ${classpadding}' />"${spacer}${sectionid}>
						<div class="container">
							<div id="solrlist-${value.Id}" class="element parent list sg-solrlist<c:out value=' ${classlist} ${value.CssClass}' />">
								<div class="wrapper">
									<c:if test="${value.FormatDate.isSet }">
										<c:set var="formatdate">${value.FormatDate}</c:set>
									</c:if>
									<c:if test="${!value.FormatDate.isSet }">
										<c:set var="formatdate">large</c:set>
									</c:if>
									<c:if test="${not empty cms.element.settings.showdate}">
										<c:set var="showdate">${cms.element.settings.showdate}</c:set>
									</c:if>

									<c:if test="${not cms.element.settings.hidetitle}">
										<div class="section-title">
											<header class="headline <c:out value='${featuredtitle}' />">
												<${titletag} class="title ${titletagsize}" <c:if test="${!value.DisplayTitle.isSet }">${rdfa.Title}</c:if> <c:if test="${value.DisplayTitle.isSet }">${rdfa.DisplayTitle}</c:if>>
													<c:if test="${value.DisplayTitle.isSet }">
														<c:if test="${not empty titletagicon}"><span class="v-align-m inline-b mr-10 fs30 ${titletagicon}" aria-hidden="true"></span></c:if>
														${value.DisplayTitle}${categoryTitle}
													</c:if>
													<c:if test="${!value.DisplayTitle.isSet }">
														<c:if test="${not empty titletagicon}"><span class="v-align-m inline-b mr-10 fs30 ${titletagicon}" aria-hidden="true"></span></c:if>
														${value.Title}${categoryTitle}
													</c:if>
												</${titletag}>
											</header>
										</div>
									</c:if>
								<c:if test="${value.TextHeader.isSet}">
									<div class="list-top-text" ${rdfa.TextHeader}>
											${value.TextHeader}
									</div>
								</c:if>

									<%-- Listamos el contenido que sera publicado en el futuro --%>
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-datereleased-content.jsp:e5699e8a-d223-11e4-a69e-01e4df46f753)">
									<cms:param name="hide">${cms.element.setting.hidedatereleased.value}</cms:param>
								</cms:include>

									<%-- Hacemos el include de la jsp que se encarga de crear la query de solr en base a la configuracion realizada --%>
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/c-solrquery-folder-or-category.jsp:eb009647-3e2b-11e8-b79c-63f9a58dae09)">
									<cms:param name="showquery">${showquery}</cms:param>
								</cms:include>

									<%-- USER FILTER --%>
								<c:if test="${content.value.CategoryUserFilter.isSet }">
									<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-user-filter.jsp:65202f86-877c-11e4-bce0-000c2904937e)">
										<cms:param name="catlevel">${content.value.CategoryUserFilter.value.CatLevel}</cms:param>
									</cms:include>
								</c:if>

									<%-- LISTADO --%>


									<%--JSP que realiza la consulta a solr utilizando un contentload--%>
									<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/c-resultlist-contentload-eventos.jsp:55eb62d2-4e06-11e8-ac56-63f9a58dae09)">
										<cms:param name="titletagelementsize">${titletagelementsize}</cms:param>
										<cms:param name="titletagelement">${titletagelement}</cms:param>
										<cms:param name="titleblockfeaturedevent">${titleblockfeaturedevent}</cms:param>
										<cms:param name="titleblocknextevents">${titleblocknextevents}</cms:param>
										<cms:param name="formatdate">${formatdate}</cms:param>
										<cms:param name="showdate">${showdate}</cms:param>
										<c:if test="${value.TitleLink.exists and value.TitleLink.value.Href.isSet}">
											<cms:param name="listlinkhref">${value.TitleLink.value.Href}</cms:param>
											<cms:param name="listlinktarget">${value.TitleLink.value.Target}</cms:param>
											<cms:param name="listlinkfollow">${value.TitleLink.value.Follow}</cms:param>
											<cms:param name="listlinktitle">${value.TitleLink.value.Title}</cms:param>
											<cms:param name="showbuttonlist">${cms.element.settings.showbuttonlist}</cms:param>
											<cms:param name="buttonlistposition">${cms.element.settings.buttonlistposition}</cms:param>
											<cms:param name="buttonlistclass">${cms.element.settings.buttonlistclass}</cms:param>
										</c:if>
									</cms:include>

									<%-- JSP que muestra la paginacion en base a la configuracion realizada y al numero de resultados encontrados --%>
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-pagination.jsp:0f3c868f-454d-11e3-8ac6-2182af1ef88f)"/>
								<c:if test="${value.TextFooter.isSet}">
									<div class="list-bottom-text" ${rdfa.TextFooter}>
										${value.TextFooter}
									</div>
								</c:if>
							</div>
						</div>
					</div>
				</div>
			</c:if>
			<c:if test="${!cms.requestContext.currentProject.onlineProject and value.ResourceType != 'dphevento'}">
				<div class="template-container<c:out value=' ${marginClass} ${classmainbox} ${classpadding}' />"${spacer}${sectionid}>
					<div class="container">
						<div class="alert alert-warning no-margin">
							<h4 class="no-margin mb-15">${value.Title}</h4>
							<strong>Mensaje de edición:</strong> El tipo de recurso que puede listar este formatter es <strong>'dphevento' (actividad/evento de DPH)</strong>.
						</div>
					</div>
				</div>
			</c:if>
			<c:if test="${!cms.requestContext.currentProject.onlineProject and withResults == 'false' and value.ResourceType == 'dphevento'}">
				<div class="template-container<c:out value=' ${marginClass} ${classmainbox} ${classpadding}' />"${spacer}${sectionid}>
					<div class="container">
						<div class="alert alert-info no-margin">
							<h4 class="no-margin mb-15">${value.Title}</h4>
							<strong>Mensaje de edición:</strong> No existen resultados para este listado. En su versión online no será visible.
						</div>
					</div>
				</div>
			</c:if>
			</div>
			<%-- Limpiamos los parametros que van en el request para que no los cojan otros listados en la pagina --%>
			<c:set var="solrquery" scope="request"></c:set>
			<c:set var="linkDetail" scope="request"></c:set>
			<c:set var="formatdate" scope="request"></c:set>
			<c:set var="title" scope="request"></c:set>
			<c:set var="titlelarge" scope="request"></c:set>
			<c:set var="description" scope="request"></c:set>
			<c:set var="date" scope="request"></c:set>
			<c:set var="imagePath" scope="request"></c:set>
			<c:set var="descriptionMaxCharacter" scope="request"></c:set>
			<c:set var="titleMaxCharacter" scope="request"></c:set>
			<c:set var="imageWidth" scope="request"></c:set>
			<c:set var="imageHeight" scope="request"></c:set>
			<c:set var="width" scope="request"></c:set>
			<c:set var="imageBlockPosition" scope="request"></c:set>
			<c:set var="imageBlockWidth" scope="request"></c:set>
			<c:set var="value" value="" scope="request" />
			<c:set var="content" value="" scope="request" />
			<c:set var="currentSite" scope="request"></c:set>
			<c:set var="currentProject" scope="request"></c:set>
			<c:set var="locale" scope="request"></c:set>
			<c:set var="titletagelement" scope="request"></c:set>
			<c:set var="titletagelementsize" scope="request"></c:set>
			<c:set var="columns" value="" scope="request" />
			<c:set var="columnsnumberxxs" value="" scope="request" />
			<c:set var="columnsnumberxs" value="" scope="request" />
			<c:set var="columnsnumbersm" value="" scope="request" />
			<c:set var="columnsnumbermd" value="" scope="request" />
			<c:set var="columnsnumberlg" value="" scope="request" />
			<c:set var="width" value="" scope="request" />
			<c:set var="formatdate" value="" scope="request" />
			<c:set var="showdate" value="" scope="request" />
			<c:set var="newcontent" scope="request" value="" />
			<c:set var="resourcetypefornewcontent" scope="request" value="" />
			<c:set var="withResults" scope="request" value="" />
		</c:otherwise>
	</c:choose>
</cms:formatter>
</cms:bundle>