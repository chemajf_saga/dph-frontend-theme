<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
<cms:formatter var="content" val="value" rdfa="rdfa">
	<fmt:setLocale value="${cms.locale}" />

	<c:choose>
		<c:when test="${cms.element.inMemoryOnly}">
			<header><h1 class="title">Edite este recurso para configurar el listado</h1></header>
		</c:when>
		<c:otherwise>

			<c:if test="${cms.element.setting.marginbottom.value != '0'}">
				<c:set var="marginClass">margin-bottom-${cms.element.setting.marginbottom.value}</c:set>
			</c:if>
			<c:if test="${not empty cms.element.settings.classmainbox }">
				<c:set var="classmainbox">${cms.element.settings.classmainbox}</c:set>
			</c:if>
			<c:if test="${cms.element.settings.featuredtitle =='true' }">
				<c:set var="featuredtitle">featured-title</c:set>
			</c:if>
			<c:if test="${cms.element.settings.showquery =='true' }">
				<c:set var="showquery">true</c:set>
			</c:if>

			<%-- Definimos el tipo de etiqueta html para nuestro encabezado y encabezado de elementos que se listan --%>

			<c:set var="titletag">h2</c:set>
			<c:set var="titletagelement">h3</c:set>

			<c:if test="${not empty cms.element.settings.titletag }">
				<c:set var="titletag">${cms.element.settings.titletag }</c:set>
				<c:if test="${titletag == 'h1'}">
					<c:set var="titletagelement">h2</c:set>
				</c:if>
				<c:if test="${titletag == 'h2'}">
					<c:set var="titletagelement">h3</c:set>
				</c:if>
				<c:if test="${titletag == 'h3'}">
					<c:set var="titletagelement">h4</c:set>
				</c:if>
				<c:if test="${titletag == 'h4'}">
					<c:set var="titletagelement">h5</c:set>
				</c:if>
				<c:if test="${titletag == 'h5'}">
					<c:set var="titletagelement">h6</c:set>
				</c:if>
				<c:if test="${titletag == 'div'}">
					<c:set var="titletagelement">div</c:set>
				</c:if>
			</c:if>

			<c:if test="${not empty cms.element.settings.titletagsize }">
				<c:set var="titletagsize">${cms.element.settings.titletagsize }</c:set>
			</c:if>

			<c:if test="${not empty cms.element.settings.titletagelement }">
				<c:set var="titletagelement">${cms.element.settings.titletagelement }</c:set>
			</c:if>

			<c:if test="${not empty cms.element.settings.titletagelementsize }">
				<c:set var="titletagelementsize">${cms.element.settings.titletagelementsize }</c:set>
			</c:if>
			<c:if test="${not empty cms.element.settings.showcategorytitle and cms.element.settings.showcategorytitle == 'true' }">
				<c:set var="categoryTitle"></c:set>
				<%-- Detectamos cual es el locale principal --%>
				<c:set var="locales"><cms:property name="locale-available" file="search" /></c:set>
				<c:set var="locales" value="${fn:split(locales, ',')}" />
				<c:set var="localemain" value="${locales[0]}"/>

				<%-- Si se ha marcado el readPropertyCategory tenemos que buscar la propiedad category --%>
				<c:if test="${content.value.readPropertyCategory.exists && content.value.readPropertyCategory=='true'}">
					<c:set var="categoryProperty"><cms:property name="category" file="search"/></c:set>
					<c:set var="categoryTitle">
						<cms:property name="Title" file="${categoryProperty}"/>
						<c:if test="${locale != localemain}">
							<cms:property name="Title_${locale}" file="${categoryProperty}"/>
						</c:if>
					</c:set>
				</c:if>
			</c:if>

			<div id="solrlist-${value.Id}" class="element parent list sg-solrlist<c:out value=' ${marginClass} ${classmainbox} ${value.CssClass}' />">

				<div class="wrapper">

						<%-- Cargamos las variables que luego vamos a ir necesitando a lo largo --%>
					<c:set var="cmsObject" value="${cms.vfs.cmsObject }" scope="request"/>
					<c:set var="value" value="${value }" scope="request" />
					<c:set var="content" value="${content }" scope="request" />
					<c:set var="currentSite" scope="request">${cms.requestContext.siteRoot }</c:set>
					<c:set var="currentProject" scope="request">${cms.requestContext.currentProject }</c:set>
					<c:set var="locale" scope="request">${cms.locale}</c:set>

					<c:if test="${value.FormatDate.isSet }">
						<c:set var="formatdate">${value.FormatDate}</c:set>
					</c:if>
					<c:if test="${!value.FormatDate.isSet }">
						<c:set var="formatdate">large</c:set>
					</c:if>

						<%-- Variables que definen si se muestran los resultados en cuadricula que dependen de los campos del recurso--%>

					<c:if test="${value.ViewInRow.exists and value.ViewInRow=='true'}">
						<c:set var="columns">row</c:set>
						<c:set var="columnsnumberxxs">${value.NumberElementsByRowXxs}</c:set>
						<c:set var="columnsnumberxs">${value.NumberElementsByRow}</c:set>
						<c:set var="columnsnumbersm">${value.NumberElementsByRowSm}</c:set>
						<c:set var="columnsnumbermd">${value.NumberElementsByRowMd}</c:set>
						<c:set var="columnsnumberlg">${value.NumberElementsByRowLg}</c:set>
						<%-- Definimos los anchos de columna para las distintas resoluciones --%>
						<c:choose>
							<c:when test="${columnsnumberxxs == 1}">
								<c:set var="widthxxs" >col-xxs-12</c:set>
							</c:when>
							<c:when test="${columnsnumberxxs == 2}">
								<c:set var="widthxxs" >col-xxs-6</c:set>
							</c:when>
							<c:when test="${columnsnumberxxs == 3}">
								<c:set var="widthxxs" >col-xxs-4</c:set>
							</c:when>
							<c:when test="${columnsnumberxxs == 4}">
								<c:set var="widthxxs" >col-xxs-3</c:set>
							</c:when>
							<c:when test="${columnsnumberxxs == 6}">
								<c:set var="widthxxs" >col-xxs-2</c:set>
							</c:when>
							<c:when test="${columnsnumberxxs == 12}">
								<c:set var="widthxxs" >col-xxs-1</c:set>
							</c:when>
						</c:choose>
						<c:choose>
							<c:when test="${columnsnumberxs == 1}">
								<c:set var="widthxs" >col-xs-12</c:set>
							</c:when>
							<c:when test="${columnsnumberxs == 2}">
								<c:set var="widthxs" >col-xs-6</c:set>
							</c:when>
							<c:when test="${columnsnumberxs == 3}">
								<c:set var="widthxs" >col-xs-4</c:set>
							</c:when>
							<c:when test="${columnsnumberxs == 4}">
								<c:set var="widthxs" >col-xs-3</c:set>
							</c:when>
							<c:when test="${columnsnumberxs == 6}">
								<c:set var="widthxs" >col-xs-2</c:set>
							</c:when>
							<c:when test="${columnsnumberxs == 12}">
								<c:set var="widthxs" >col-xs-1</c:set>
							</c:when>
						</c:choose>

						<c:choose>
							<c:when test="${columnsnumbersm == 1}">
								<c:set var="widthmd" >col-sm-12</c:set>
							</c:when>
							<c:when test="${columnsnumbersm == 2}">
								<c:set var="widthmd" >col-sm-6</c:set>
							</c:when>
							<c:when test="${columnsnumbersm == 3}">
								<c:set var="widthmd" >col-sm-4</c:set>
							</c:when>
							<c:when test="${columnsnumbersm == 4}">
								<c:set var="widthmd" >col-sm-3</c:set>
							</c:when>
							<c:when test="${columnsnumbersm == 6}">
								<c:set var="widthmd" >col-sm-2</c:set>
							</c:when>
							<c:when test="${columnsnumbersm == 12}">
								<c:set var="widthmd" >col-sm-1</c:set>
							</c:when>
						</c:choose>

						<c:choose>
							<c:when test="${columnsnumbermd == 1}">
								<c:set var="widthmd" >col-md-12</c:set>
							</c:when>
							<c:when test="${columnsnumbermd == 2}">
								<c:set var="widthmd" >col-md-6</c:set>
							</c:when>
							<c:when test="${columnsnumbermd == 3}">
								<c:set var="widthmd" >col-md-4</c:set>
							</c:when>
							<c:when test="${columnsnumbermd == 4}">
								<c:set var="widthmd" >col-md-3</c:set>
							</c:when>
							<c:when test="${columnsnumbermd == 6}">
								<c:set var="widthmd" >col-md-2</c:set>
							</c:when>
							<c:when test="${columnsnumbermd == 12}">
								<c:set var="widthmd" >col-md-1</c:set>
							</c:when>
						</c:choose>

						<c:choose>
							<c:when test="${columnsnumberlg == 1}">
								<c:set var="widthlg" >col-lg-12</c:set>
							</c:when>
							<c:when test="${columnsnumberlg == 2}">
								<c:set var="widthlg" >col-lg-6</c:set>
							</c:when>
							<c:when test="${columnsnumberlg == 3}">
								<c:set var="widthlg" >col-lg-4</c:set>
							</c:when>
							<c:when test="${columnsnumberlg == 4}">
								<c:set var="widthlg" >col-lg-3</c:set>
							</c:when>
							<c:when test="${columnsnumberlg == 6}">
								<c:set var="widthlg" >col-lg-2</c:set>
							</c:when>
							<c:when test="${columnsnumberlg == 12}">
								<c:set var="widthlg" >col-lg-1</c:set>
							</c:when>
						</c:choose>

						<%-- Concatenamos todos los anchos por resolucion en la misma variable --%>

						<c:set var="width"><c:out value="${widthxxs} ${widthxs} ${widthsm} ${widthmd} ${widthlg}" /></c:set>

					</c:if>
					<c:if test="${not value.ViewInRow.exists or value.ViewInRow=='false'}">
						<c:set var="columns"></c:set>
						<c:set var="columnsnumberxxs"></c:set>
						<c:set var="columnsnumberxs"></c:set>
						<c:set var="columnsnumbersm"></c:set>
						<c:set var="columnsnumbermd"></c:set>
						<c:set var="columnsnumberlg"></c:set>
						<c:set var="width"></c:set>
					</c:if>

						<%-- En función de los campos de posición y ancho de la imagen mandamos el valor necesario para el media object --%>
					<c:if test="${value.ImageElementPosition.isSet }">
						<c:set var="imageBlockPosition">${value.ImageElementPosition }</c:set>
					</c:if>
					<c:if test="${!value.ImageElementPosition.isSet }">
						<c:set var="imageBlockPosition">left</c:set>
					</c:if>

					<c:if test="${value.ImageBlockWidth.isSet}">
						<c:set var="imageBlockWidth" value="${value.ImageBlockWidth }"/>
					</c:if>
					<c:if test="${!value.ImageBlockWidth.isSet}">
						<c:if test="${imageBlockPosition == 'top' }">
							<c:set var="imageBlockWidth" value="12"/>
						</c:if>
						<c:if test="${imageBlockPosition != 'top' }">
							<c:set var="imageBlockWidth" value="3"/>
						</c:if>
					</c:if>

						<%-- valores de las variables que vienen de los settings de columnas e imagen --%>

						<%-- bloque imagen --%>
					<c:if test="${not empty cms.element.settings.imageposition }">
						<c:set var="imageBlockPosition">${cms.element.settings.imageposition}</c:set>
					</c:if>
					<c:if test="${not empty cms.element.settings.imagewidth }">
						<c:set var="imageBlockWidth">${cms.element.settings.imagewidth}</c:set>
					</c:if>

						<%-- columnas --%>
					<c:if test="${cms.element.settings.columns =='true' }">
						<c:set var="columns">row</c:set>
						<c:set var="columnsnumberxxs">1</c:set>
						<c:set var="columnsnumberxs">2</c:set>
						<c:set var="columnsnumbersm">3</c:set>
						<c:set var="columnsnumbermd"></c:set>
						<c:set var="columnsnumberlg"></c:set>
						<%-- columns xxs --%>
						<c:if test="${not empty cms.element.settings.columnsnumberxxs }">
							<c:set var="columnsnumberxxs">${cms.element.settings.columnsnumberxxs}</c:set>
							<c:if test="${columnsnumberxxs == '1'}">
								<c:set var="widthxxs">col-xxs-12</c:set>
							</c:if>
							<c:if test="${columnsnumberxxs == '2'}">
								<c:set var="widthxxs">col-xxs-6</c:set>
							</c:if>
							<c:if test="${columnsnumberxxs == '3'}">
								<c:set var="widthxxs">col-xxs-4</c:set>
							</c:if>
							<c:if test="${columnsnumberxxs == '4'}">
								<c:set var="widthxxs">col-xxs-3</c:set>
							</c:if>
							<c:if test="${columnsnumberxxs == '6'}">
								<c:set var="widthxxs">col-xxs-2</c:set>
							</c:if>
						</c:if>
						<%-- columns xs --%>
						<c:if test="${not empty cms.element.settings.columnsnumberxs }">
							<c:set var="columnsnumberxs">${cms.element.settings.columnsnumberxs}</c:set>
							<c:if test="${columnsnumberxs == '1'}">
								<c:set var="widthxs">col-xs-12</c:set>
							</c:if>
							<c:if test="${columnsnumberxs == '2'}">
								<c:set var="widthxs">col-xs-6</c:set>
							</c:if>
							<c:if test="${columnsnumberxs == '3'}">
								<c:set var="widthxs">col-xs-4</c:set>
							</c:if>
							<c:if test="${columnsnumberxs == '4'}">
								<c:set var="widthxs">col-xs-3</c:set>
							</c:if>
							<c:if test="${columnsnumberxs == '6'}">
								<c:set var="widthxs">col-xs-2</c:set>
							</c:if>
						</c:if>
						<%-- columns sm --%>
						<c:if test="${not empty cms.element.settings.columnsnumbersm }">
							<c:set var="columnsnumbersm">${cms.element.settings.columnsnumbersm}</c:set>
							<c:if test="${columnsnumbersm == '1'}">
								<c:set var="widthsm">col-sm-12</c:set>
							</c:if>
							<c:if test="${columnsnumbersm == '2'}">
								<c:set var="widthsm">col-sm-6</c:set>
							</c:if>
							<c:if test="${columnsnumbersm == '3'}">
								<c:set var="widthsm">col-sm-4</c:set>
							</c:if>
							<c:if test="${columnsnumbersm == '4'}">
								<c:set var="widthsm">col-sm-3</c:set>
							</c:if>
							<c:if test="${columnsnumbersm == '6'}">
								<c:set var="widthsm">col-sm-2</c:set>
							</c:if>
						</c:if>
						<%-- columns md --%>
						<c:if test="${not empty cms.element.settings.columnsnumbermd }">
							<c:set var="columnsnumbermd">${cms.element.settings.columnsnumbermd}</c:set>
							<c:if test="${columnsnumbermd == '1'}">
								<c:set var="widthmd">col-md-12</c:set>
							</c:if>
							<c:if test="${columnsnumbermd == '2'}">
								<c:set var="widthmd">col-md-6</c:set>
							</c:if>
							<c:if test="${columnsnumbermd == '3'}">
								<c:set var="widthmd">col-md-4</c:set>
							</c:if>
							<c:if test="${columnsnumbermd == '4'}">
								<c:set var="widthmd">col-md-3</c:set>
							</c:if>
							<c:if test="${columnsnumbermd == '6'}">
								<c:set var="widthmd">col-md-2</c:set>
							</c:if>
						</c:if>
						<%-- columns lg --%>
						<c:if test="${not empty cms.element.settings.columnsnumberlg }">
							<c:set var="columnsnumberlg">${cms.element.settings.columnsnumberlg}</c:set>
							<c:if test="${columnsnumberlg == '1'}">
								<c:set var="widthlg">col-lg-12</c:set>
							</c:if>
							<c:if test="${columnsnumberlg == '2'}">
								<c:set var="widthlg">col-lg-6</c:set>
							</c:if>
							<c:if test="${columnsnumberlg == '3'}">
								<c:set var="widthlg">col-lg-4</c:set>
							</c:if>
							<c:if test="${columnsnumberlg == '4'}">
								<c:set var="widthlg">col-lg-3</c:set>
							</c:if>
							<c:if test="${columnsnumberlg == '6'}">
								<c:set var="widthlg">col-lg-2</c:set>
							</c:if>
						</c:if>
						<c:set var="width"><c:out value="${widthxxs} ${widthxs} ${widthsm} ${widthmd} ${widthlg} " /></c:set>
					</c:if>
					<c:if test="${not empty cms.element.settings.showdate}">
						<c:set var="showdate">${cms.element.settings.showdate}</c:set>
					</c:if>

					<c:if test="${cms.element.settings.dividertop}">
						<!-- Separador del recurso -->
						<hr class="divider">
					</c:if>
					<c:if test="${not cms.element.settings.hidetitle}">
						<div class="section-title">
							<header class="headline <c:out value='${featuredtitle}' />">
								<${titletag} class="title ${titletagsize}" <c:if test="${!value.DisplayTitle.isSet }">${rdfa.Title}</c:if> <c:if test="${value.DisplayTitle.isSet }">${rdfa.DisplayTitle}</c:if>>
								<c:if test="${value.DisplayTitle.isSet }">
									<c:if test="${not empty titletagicon}"><span class="v-align-m inline-b mr-10 fs30 ${titletagicon}" aria-hidden="true"></span></c:if>
									${value.DisplayTitle}${categoryTitle}
								</c:if>
								<c:if test="${!value.DisplayTitle.isSet }">
									<c:if test="${not empty titletagicon}"><span class="v-align-m inline-b mr-10 fs30 ${titletagicon}" aria-hidden="true"></span></c:if>
									${value.Title}${categoryTitle}
								</c:if>
							</${titletag}>
							</header>
						</div>
					</c:if>
				<c:if test="${value.TextHeader.isSet}">
					<div class="list-top-text" ${rdfa.TextHeader}>
							${value.TextHeader}
					</div>
				</c:if>

					<%-- Listamos el contenido que sera publicado en el futuro --%>
				<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-datereleased-content.jsp:e5699e8a-d223-11e4-a69e-01e4df46f753)">
					<cms:param name="hide">${cms.element.setting.hidedatereleased.value}</cms:param>
				</cms:include>

					<%-- Hacemos el include de la jsp que se encarga de crear la query de solr en base a la configuracion realizada --%>
				<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/c-solrquery.jsp:5e67a60b-059f-11e8-a124-63f9a58dae09)">
					<cms:param name="showquery">${showquery}</cms:param>
				</cms:include>

					<%-- USER FILTER --%>
				<c:if test="${content.value.CategoryUserFilter.isSet }">
					<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-user-filter.jsp:65202f86-877c-11e4-bce0-000c2904937e)">
						<cms:param name="catlevel">${content.value.CategoryUserFilter.value.CatLevel}</cms:param>
					</cms:include>
				</c:if>

					<%-- LISTADO --%>

					<%-- Comprobamos si lo que hay que listar es un binario o un XML --%>
				<c:if test="${value.ResourceType == 'binary' or value.ResourceType == 'image'}">
					<%-- JSP que realiza la consulta a solr utilizando un resourceload --%>
					<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-resultlist-resourceload.jsp:059883b1-db4e-11e3-9a12-f18cf451b707)"/>
				</c:if>
				<c:if test="${value.ResourceType != 'binary' and value.ResourceType != 'image'}">
					<%--JSP que realiza la consulta a solr utilizando un contentload--%>
					<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/c-resultlist-contentload.jsp:922b6e63-e555-11e7-a388-63f9a58dae09)">
						<cms:param name="columns">${columns}</cms:param>
						<cms:param name="columnsnumberxxs">${columnsnumberxxs}</cms:param>
						<cms:param name="columnsnumberxs">${columnsnumberxs}</cms:param>
						<cms:param name="columnsnumbersm">${columnsnumbersm}</cms:param>
						<cms:param name="columnsnumbermd">${columnsnumbermd}</cms:param>
						<cms:param name="columnsnumberlg">${columnsnumberlg}</cms:param>
						<cms:param name="width">${width}</cms:param>
						<cms:param name="titletagelementsize">${titletagelementsize}</cms:param>
						<cms:param name="titletagelement">${titletagelement}</cms:param>
						<cms:param name="imageBlockPosition">${imageBlockPosition}</cms:param>
						<cms:param name="imageBlockWidth">${imageBlockWidth}</cms:param>
						<cms:param name="formatdate">${formatdate}</cms:param>
						<cms:param name="showdate">${showdate}</cms:param>
						<c:if test="${value.TitleLink.exists and value.TitleLink.value.Href.isSet}">
							<cms:param name="listlinkhref">${value.TitleLink.value.Href}</cms:param>
							<cms:param name="listlinktarget">${value.TitleLink.value.Target}</cms:param>
							<cms:param name="listlinkfollow">${value.TitleLink.value.Follow}</cms:param>
							<cms:param name="listlinktitle">${value.TitleLink.value.Title}</cms:param>
							<cms:param name="showbuttonlist">${cms.element.settings.showbuttonlist}</cms:param>
							<cms:param name="buttonlistposition">${cms.element.settings.buttonlistposition}</cms:param>
							<cms:param name="buttonlistclass">${cms.element.settings.buttonlistclass}</cms:param>
						</c:if>
					</cms:include>
				</c:if>

					<%-- JSP que muestra la paginacion en base a la configuracion realizada y al numero de resultados encontrados --%>
				<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.solrlist/elements/c-pagination.jsp:0f3c868f-454d-11e3-8ac6-2182af1ef88f)"/>
				<c:if test="${value.TextFooter.isSet}">
					<div class="list-bottom-text" ${rdfa.TextFooter}>
							${value.TextFooter}
					</div>
				</c:if>
			</div>
			<c:if test="${cms.element.settings.dividerbottom}">
				<!-- Separador del recurso -->
				<hr class="divider">
			</c:if>
			</div>
			<%-- Limpiamos los parametros que van en el request para que no los cojan otros listados en la pagina --%>
			<c:set var="solrquery" scope="request"></c:set>
			<c:set var="linkDetail" scope="request"></c:set>
			<c:set var="formatdate" scope="request"></c:set>
			<c:set var="title" scope="request"></c:set>
			<c:set var="titlelarge" scope="request"></c:set>
			<c:set var="description" scope="request"></c:set>
			<c:set var="date" scope="request"></c:set>
			<c:set var="imagePath" scope="request"></c:set>
			<c:set var="descriptionMaxCharacter" scope="request"></c:set>
			<c:set var="titleMaxCharacter" scope="request"></c:set>
			<c:set var="imageWidth" scope="request"></c:set>
			<c:set var="imageHeight" scope="request"></c:set>
			<c:set var="width" scope="request"></c:set>
			<c:set var="imageBlockPosition" scope="request"></c:set>
			<c:set var="imageBlockWidth" scope="request"></c:set>
			<c:set var="value" value="" scope="request" />
			<c:set var="content" value="" scope="request" />
			<c:set var="currentSite" scope="request"></c:set>
			<c:set var="currentProject" scope="request"></c:set>
			<c:set var="locale" scope="request"></c:set>
			<c:set var="titletagelement" scope="request"></c:set>
			<c:set var="titletagelementsize" scope="request"></c:set>
			<c:set var="columns" value="" scope="request" />
			<c:set var="columnsnumberxxs" value="" scope="request" />
			<c:set var="columnsnumberxs" value="" scope="request" />
			<c:set var="columnsnumbersm" value="" scope="request" />
			<c:set var="columnsnumbermd" value="" scope="request" />
			<c:set var="columnsnumberlg" value="" scope="request" />
			<c:set var="width" value="" scope="request" />
			<c:set var="formatdate" value="" scope="request" />
			<c:set var="showdate" value="" scope="request" />
			<c:set var="newcontent" scope="request" value="" />
			<c:set var="resourcetypefornewcontent" scope="request" value="" />
		</c:otherwise>
	</c:choose>
</cms:formatter>
</cms:bundle>