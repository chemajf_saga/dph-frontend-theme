<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates" %>

<fmt:setLocale value="${cms.locale}"/>
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">

  <cms:formatter var="content" val="value" rdfa="rdfa">
    <c:choose>
      <c:when test="${cms.element.inMemoryOnly}">
        <h1 class="title">
          Por favor, edite este recurso.
        </h1>
      </c:when>
      <c:otherwise>

        <%-- Definimos el tipo de etiqueta html para nuestro encabezado --%>

        <c:set var="titletag">h1</c:set>
        <c:set var="subtitletag">h2</c:set>

        <c:if test="${not empty cms.element.settings.titletag }">
          <c:set var="titletag" scope="request">${cms.element.settings.titletag }</c:set>
          <c:if test="${titletag == 'h1'}">
            <c:set var="subtitletag">h2</c:set>
          </c:if>
          <c:if test="${titletag == 'h2'}">
            <c:set var="subtitletag">h3</c:set>
          </c:if>
          <c:if test="${titletag == 'h3'}">
            <c:set var="subtitletag">h4</c:set>
          </c:if>
          <c:if test="${titletag == 'h4'}">
            <c:set var="subtitletag">h5</c:set>
          </c:if>
          <c:if test="${titletag == 'h5'}">
            <c:set var="subtitletag">div</c:set>
          </c:if>
          <c:if test="${titletag == 'div'}">
            <c:set var="subtitletag">div</c:set>
          </c:if>
        </c:if>
        <%-- si se ha editado el subtitulo lo mandamos en el request para que se tenga en cuenta en la seccion o en el bloque de texto por si se edita el titulo de seccion o de bloque --%>
        <c:if test="${value.SubTitle.isSet }">
          <c:set var="subtitletag" scope="request">${subtitletag}</c:set>
        </c:if>
        <%-- si no se ha editado el subtitulo lo mandamos vacio en el request --%>
        <c:if test="${!value.SubTitle.isSet }">
          <c:set var="subtitletag" scope="request"></c:set>
        </c:if>
        <%-- Cargamos la variable con el id especifico del recurso para su uso posterior--%>
        <c:set var="idresource" value="${content.id}" scope="request"/>
		<c:set var="id">${fn:substringBefore(idresource, '-')}</c:set>
		
		<%-- guardamos en una variable la ruta del themeconfig que le corresponde al recurso en funcion de la rama donde se este pintando. despues lo mandamos como parametro en el href del enlace para generar el pdf --%>
		<c:set var="themeconfigpath">${cms.vfs.context.siteRoot}${themeconfigpath}<sg:resourcePathLookBack filename=".themeconfig"/></c:set>

        <c:if test="${value.Category.isSet }">
          <c:set var="categorias" value="${fn:split(value.Category, ',')}" />
          <c:forEach items="${categorias }" var="category" varStatus="status">
            <c:set var="categoryhasrecursospaquetes" value="${fn:indexOf(category, '/recursos-turisticos/Paque')}" />
            <c:if test="${categoryhasrecursospaquetes != -1}">
              <c:set var="categoryresourceshort" value="${fn:replace(category, '/.categories/recursos-turisticos/','')}" />
              <c:set var="categoryresourceshort" value="${fn:substringBefore(categoryresourceshort, '/')}" />
              <c:set var="categoryresourceshortend" scope="request" value="${fn:toLowerCase(categoryresourceshort)}" />
            </c:if>
          </c:forEach>
        </c:if>

        <article class="articulo parent articulo-blog ${categoryresourceshortend}">
          <div class="wrapper">
            <!-- Cabecera del articulo -->
            <header class="headline">
				  <c:if test="${value.Title.isSet }">
					  <${titletag} class="title" ${rdfa.Title}>${value.Title}</${titletag}>
				  </c:if>
				  <c:if test="${value.SubTitle.isSet }">
					  <${subtitletag} class="subtitle" ${rdfa.SubTitle}>${value.SubTitle}</${subtitletag}>
				  </c:if>
            <c:if test="${value.Category.isSet }">
              <div class="h5 upper municipio">
                <c:set var="categorias" value="${fn:split(value.Category, ',')}" />
                <c:set var="counter" value="0" />
                <c:forEach items="${categorias }" var="category" varStatus="status">
                  <c:set var="categoryhasmunicipio" value="${fn:indexOf(category, '/municipios/')}" />
                  <c:if  test="${categoryhasmunicipio != -1}">
                    <%--====== Comprobamos si existe la propiedad Title especifica para el locale actual (Title_es, Title_en...) para la categoria.
                    Si es asi usamos esa propiedad. Si no existe, usamos la propiedad Title =============================--%>
                    <c:set var="categorytitle"><cms:property name="Title_${locale}" file="${category}"/></c:set>
                    <c:if test="${empty categoryPropertyTitle}">
                      <c:set var="categorytitle"><cms:property name="Title" file="${category}"/></c:set>
                    </c:if>
                    <c:set var="categoryshort" value="${fn:replace(category, '/.categories/','')}" />
                    <a class="hastooltip" href="<cms:link>/zona-cultura/contenidos/?buscadorthemesfield-4=${categoryshort}</cms:link>" title="Ver todos los artículos de este municipio"><span class="pe-7s-map-marker mr-10" aria-hidden="true"></span>${categorytitle}</a>
                  </c:if>
                </c:forEach>
              </div>
            </c:if>
        <%--<c:if test="${value.Category.isSet }">
          <c:set var="pageurl">${themeConfiguration.CustomFieldKeyValue.UrlArticlesPage}</c:set>
          <c:if test="${empty pageurl}">
            <c:set var="pageurl">/articulos/</c:set>
          </c:if>
          <div class="categories mt-20">
            <c:set var="categorias" value="${fn:split(value.Category, ',')}" />
            <c:set var="counter" value="0" />

            <c:forEach items="${categorias }" var="category" varStatus="status">
                        <c:set var="categorytitle"><cms:property name="Title_${locale}" file="${category}"/></c:set>
                        <c:if test="${empty categoryPropertyTitle}">
                          <c:set var="categorytitle"><cms:property name="Title" file="${category}"/></c:set>
                        </c:if>
                        <c:set var="categoryhastipo" value="${fn:indexOf(category, '/tipo-turismo/')}" />
                        <c:set var="categoryhasmunicipio" value="${fn:indexOf(category, '/municipios/')}" />
                        <c:set var="categoryhasrecursos" value="${fn:indexOf(category, '/recursos-turisticos/')}" />
                        <c:set var="categoryshort" value="${fn:replace(category, '/.categories/','')}" />
                        <c:if test="${categoryhastipo != -1}">
                          <c:set var="targetfield">3</c:set>
                        </c:if>
                        <c:if test="${categoryhasmunicipio != -1}">
                          <c:set var="targetfield">4</c:set>
                        </c:if>
                        <c:if test="${categoryhasrecursos != -1}">
                          <c:set var="targetfield">5</c:set>
                        </c:if>
                        <c:if test="${categoryhastipo != -1 or categoryhasmunicipio != -1 or categoryhasrecursos != -1}">
                          <c:set var="counter" value="${counter + 1}" />
                          <c:set var="linkquery">?buscadorthemesfield-1=&buscadorthemesfield-2_d1=&buscadorthemesfield-2_d2=&buscadorthemesfield-${targetfield}=${categoryshort}&numfield=3&searchaction=search&searchPage=1&submit=Buscar</c:set>
                          <c:if test="${counter > 1}">&nbsp;&nbsp;&nbsp;</c:if>
                          <a class="inline-b" href="<cms:link>${pageurl}${linkquery}</cms:link>" title="<fmt:message key="key.view.all.articles.category" />"><strong>#${categorytitle }</strong></a>
                        </c:if>
                      </c:forEach>
                    </div>
                  </c:if>--%>
          <div class="tags mt-30">
            <c:set var="categorias" value="${fn:split(value.Category, ',')}" />
            <c:set var="counter" value="0" />
            <ul class="list-unstyled list-inline ml-0 no-margin">
              <li><a class="hastooltip" href="<cms:link>/zona-cultura/contenidos/</cms:link>" title="Ver todos los artículos con este tag"><strong>#Formación</strong></a></li>
              <li><a class="hastooltip" href="<cms:link>/zona-cultura/contenidos/</cms:link>" title="Ver todos los artículos con este tag"><strong>#Cultura y ocio</strong></a></li>
              <li><a class="hastooltip" href="<cms:link>/zona-cultura/contenidos/</cms:link>" title="Ver todos los artículos con este tag"><strong>#Diputación</strong></a></li>
              <li><a class="hastooltip" href="<cms:link>/zona-cultura/contenidos/</cms:link>" title="Ver todos los artículos con este tag"><strong>#Recursos</strong></a></li>
            </ul>
          </div>
            </header>

            <c:if test="${cms.element.settings.generatepdf == 'true' }">
              <div class="clearfix btn-pdf-wrapper">
                <a class="btn btn-default btn-pdf btn-sm pull-right tooltip-left" title="<fmt:message key="label.download.title.pdf" />" href="<cms:pdf format='%(link.weak:/system/modules/com.saga.sagasuite.core/elements/f-content-pdf.jsp:4aa2da36-4074-11e7-8f23-7fb253176922)' content='${content.filename}' locale='${cms.locale}'/>?themeconfigpath=${themeconfigpath}" target="pdf">
                  <span class="fa fa-file-pdf-o" aria-hidden="true"></span>&nbsp;<span class="btn-pdf-text"><fmt:message key="label.download.pdf" /></span>
                </a>
              </div>
            </c:if>

            <c:if test="${value.Entradilla.isSet }">
              <div class="entradilla" ${rdfa.Entradilla}>
                  ${value.Entradilla }
              </div>
            </c:if>

            <c:if test="${value.Content.exists}">
              <c:forEach var="elem" items="${content.valueList.Content}" varStatus="status">
                <c:set var="contentblock" value="${elem}" scope="request"></c:set>
                <cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-default-contentblock.jsp:e9a36d37-df65-11e4-bcf9-01e4df46f753)">
					<cms:param name="contentId">${id}-contentblock-${status.count}</cms:param>
                </cms:include>
              </c:forEach>
            </c:if>

          </div>
          <!-- Fin de wrapper -->
        </article>
        <c:set var="titletag"></c:set>
        <c:set var="subtitletag"></c:set>
        <c:set var="titlesection"></c:set>
        <c:set var="titleblock"></c:set>
        <c:set var="titlegallery"></c:set>
      </c:otherwise>
    </c:choose>
  </cms:formatter>
</cms:bundle>