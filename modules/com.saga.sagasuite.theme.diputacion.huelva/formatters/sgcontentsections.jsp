<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.textcontent.messages">
	<cms:formatter var="content" val="value" rdfa="rdfa">
		<c:choose>
			<c:when test="${cms.element.inMemoryOnly}">
				<h1 class="title">
					Edite este recurso!!
				</h1>
			</c:when>
			<c:otherwise>
				<%-- Cargamos la variable con el id especifico del recurso para su uso posterior--%>

				<c:set var="idresource" value="${content.id}" scope="request"></c:set>
				<c:set var="id">${fn:substringBefore(idresource, '-')}</c:set>
				<c:if test="${cms.element.setting.marginbottom.value != '0'}">
					<c:set var="marginClass">margin-bottom-${cms.element.setting.marginbottom.value}</c:set>
				</c:if>

				<c:if test="${not empty cms.element.settings.classmainbox }">
					<c:set var="classmainbox">${cms.element.settings.classmainbox}</c:set>
				</c:if>

				<%-- formato caja --%>

				<c:if test="${cms.element.settings.box == 'true'}">
					<c:set var="box">box default-box</c:set>
				</c:if>

				<c:if test="${not empty cms.element.settings.fontsize and cms.element.settings.box == 'true'}">
					<c:set var="fontsize">${cms.element.settings.fontsize}</c:set>
				</c:if>

				<c:if test="${not empty cms.element.settings.wellbox }">
					<c:set var="wellbox">well ${cms.element.settings.wellbox}</c:set>
				</c:if>

				<c:if test="${not empty cms.element.settings.equalheight }">
					<c:set var="equalheight">${cms.element.settings.equalheight }</c:set>
				</c:if>

				<c:if test="${cms.element.settings.bordered == 'true' }">
					<c:set var="bordered">bordered-box</c:set>
				</c:if>

				<c:if test="${cms.element.settings.featuredtitle =='true' }">
					<c:set var="featuredtitle">featured-title</c:set>
				</c:if>

				<%-- Definimos el tipo de etiqueta html para nuestro encabezado --%>

				<c:set var="titletag" scope="request">h2</c:set>
				<c:set var="subtitletag" scope="request">h3</c:set>

				<c:if test="${not empty cms.element.settings.titletag }">
					<c:set var="titletag" scope="request">${cms.element.settings.titletag }</c:set>
					<c:if test="${titletag == 'h1'}">
						<c:set var="subtitletag">h2</c:set>
					</c:if>
					<c:if test="${titletag == 'h2'}">
						<c:set var="subtitletag">h3</c:set>
					</c:if>
					<c:if test="${titletag == 'h3'}">
						<c:set var="subtitletag">h4</c:set>
					</c:if>
					<c:if test="${titletag == 'h4'}">
						<c:set var="subtitletag">h5</c:set>
					</c:if>
					<c:if test="${titletag == 'h5'}">
						<c:set var="subtitletag">h6</c:set>
					</c:if>
					<c:if test="${titletag == 'div'}">
						<c:set var="subtitletag">div</c:set>
					</c:if>
				</c:if>
				<%-- Release 1.5.5.2 - Se añade el setting de tamaño de encabezado--%>

				<%-- tamaño del encabezado --%>
				<c:if test="${not empty cms.element.settings.titletagsize }">
					<c:set var="titletagsize">${cms.element.settings.titletagsize}</c:set>
					<c:if test="${titletagsize == 'h1'}">
						<c:set var="subtitletagsize">h2</c:set>
					</c:if>
					<c:if test="${titletagsize == 'h2'}">
						<c:set var="subtitletagsize">h3</c:set>
					</c:if>
					<c:if test="${titletagsize == 'h3'}">
						<c:set var="subtitletagsize">h4</c:set>
					</c:if>
					<c:if test="${titletagsize == 'h4'}">
						<c:set var="subtitletagsize">h5</c:set>
					</c:if>
					<c:if test="${titletagsize == 'h5'}">
						<c:set var="subtitletagsize">div</c:set>
					</c:if>
					<c:if test="${titletagsize == 'div'}">
						<c:set var="subtitletagsize">div</c:set>
					</c:if>
				</c:if>
				<%-- si se ha editado el subtitulo lo mandamos en el request para que se tenga en cuenta en la seccion o en el bloque de texto por si se edita el titulo de seccion o de bloque --%>
				<c:if test="${value.SubTitle.isSet }">
					<c:set var="subtitletag" scope="request">${subtitletag}</c:set>
				</c:if>
				<%-- si no se ha editado el subtitulo lo mandamos vacio en el request --%>
				<c:if test="${!value.SubTitle.isSet }">
					<c:set var="subtitletag" scope="request"></c:set>
				</c:if>

				<c:if test="${cms.element.settings.animatedbox =='true' }">
					<c:set var="animatedbox">animated-box</c:set>
				</c:if>

	<%-- guardamos en una variable la ruta del themeconfig que le corresponde al recurso en funcion de la rama donde se este pintando. despues lo mandamos como parametro en el href del enlace para generar el pdf --%>
	<c:set var="themeconfigpath">${cms.vfs.context.siteRoot}${themeconfigpath}<sg:resourcePathLookBack filename=".themeconfig"/></c:set>

				<article class="<c:if test='${cms.element.settings.box != "true"}'>articulo</c:if> element parent content-section <c:out value=' ${marginClass} ${classmainbox} ${box} ${animatedbox} ${wellbox} ${equalheight} ${bordered}' />">
					<div class="wrapper <c:out value='${value.CssClass} ' />">
						<c:if test="${not cms.element.settings.hidetitle}">
							<!-- Cabecera del articulo -->
							<header class="headline <c:out value='${featuredtitle}' />">
							<c:if test="${cms.element.settings.subscribe == 'true' }">
								<sg:button-subscribe resource="${cms.element.resource}" css="hidden-xs hidden-xxs pull-right"/>
							</c:if>
							<${titletag} class="title ${titletagsize}" ${rdfa.Title}>${value.Title}</${titletag}>
							<c:if test="${value.SubTitle.isSet }">
								<${subtitletag} class="subtitle ${subtitletagsize}" ${rdfa.SubTitle}>${value.SubTitle}</${subtitletag}>
							</c:if>
							</header>
						</c:if>

						<c:if test="${value.Content.exists}">

							<c:forEach var="contentList" items="${content.valueList.Content}" varStatus="status">
								<c:set var="contentNameField">Content[${status.count }]</c:set>
								<c:forEach var="elem" items="${content.subValueList[contentNameField]}" varStatus="status2">
									<c:choose>
										<%-- TEXTO SIMPLE --%>
										<c:when test="${elem.name == 'TextSimple'}">
											<c:set var="contentblock" value="${elem}" scope="request"></c:set>
											<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-simpletext.jsp:ea1d0a2b-df65-11e4-bcf9-01e4df46f753)">
												<cms:param name="contentId">${id}-firstlevelblock-${status.count }-textsimple-${status2.count}</cms:param>
											</cms:include>
										</c:when>
										<%-- BLOQUE DE TEXTO --%>
										<c:when test="${elem.name == 'TextBlock'}">
											<c:set var="contentblock" value="${elem}" scope="request"></c:set>
											<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-default-contentblock.jsp:e9a36d37-df65-11e4-bcf9-01e4df46f753)">
												<cms:param name="contentId">${id}-firstlevelblock-${status.count }-contentblock-${status2.count}</cms:param>
											</cms:include>
										</c:when>
										<%-- SECCIONES --%>
										<c:when test="${elem.name == 'ListSection'}">

											<c:set var="viewMode" value="${elem.value.ViewMode}" scope="request"/>
											<c:set var="navPosition" value="${elem.value.NavPosition}" scope="request"/>
											<c:set var="menuId" value="${elem.value.MenuId}" scope="request"/>

											<c:if test="${elem.value.MenuTitle.exists and elem.value.MenuTitle.isSet}">
												<c:set var="menuTitle" value="${elem.value.MenuTitle}" scope="request"/>
											</c:if>
											<c:set var="listSection" value="${elem}" scope="request"/>
											<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-listsections.jsp:e9eafd9f-df65-11e4-bcf9-01e4df46f753)">
												<cms:param name="date">${value.Date }</cms:param>
												<cms:param name="showtime">false</cms:param>
												<cms:param name="contentId">${id}-first-level-block-${status.count }-sections-${status2.count}</cms:param>
												<%-- parametro para definir si por defecto el primer bloque se muestra abierto en la vista de accordion. Asi si deseamos crear un formatter con el primer bloque cerrado nos resultara mas facil --%>
												<cms:param name="firstaccordionblockopen">true</cms:param>
											</cms:include>
										</c:when>
									</c:choose>
								</c:forEach>
							</c:forEach>

						</c:if> <%-- Fin cierre comprobacion de seccion --%>

							<%-- Notas al pie --%>

						<c:if test="${value.NotaAlPie.isSet}">
							<div class="notas-pie small well" ${content.rdfa.NotaAlPie}>
									${content.value.NotaAlPie}
							</div>
						</c:if>
						<c:if test="${cms.element.settings.generatepdf == 'true' or cms.element.settings.subscribe == 'true' }">
							<div class="clearfix margin-top-15">
								<c:if test="${cms.element.settings.generatepdf == 'true' }">
									<a class="btn btn-default btn-pdf btn-sm pull-right hastooltip" title="<fmt:message key="label.download.title.pdf" />" href="<cms:pdf format='%(link.weak:/system/modules/com.saga.sagasuite.core/elements/f-textcontent-pdf.jsp:b912ac68-b5fd-11e5-8e51-7fb253176922)' content='${content.filename}' locale='${cms.locale}'/>?themeconfigpath=${themeconfigpath}" target="pdf">
										<span class="fa fa-file-pdf-o" aria-hidden="true"></span>&nbsp;<span class="btn-pdf-text"><fmt:message key="label.download.pdf" /></span>
									</a>
								</c:if>
							</div>
						</c:if>
						<!-- Pie del articulo -->
						<c:if test="${value.ShowFooter == 'true' }">
							<div class="posted">
								<span class="fa fa-calendar" aria-hidden="true"></span>&nbsp;&nbsp;
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-time.jsp:ea2c737f-df65-11e4-bcf9-01e4df46f753)">
									<cms:param name="date">${value.Date }</cms:param>
									<cms:param name="showtime">false</cms:param>
								</cms:include>
							</div>
							<c:if test="${value.Author.exists }">
								<div class="autor"><span class="fa fa-user" aria-hidden="true"></span>&nbsp;&nbsp;${value.Author.value.Author }</div>
							</c:if>
							<c:if test="${value.Source.isSet }">
								<div class="fuente"><span class="fa fa-book" aria-hidden="true"></span>&nbsp;&nbsp;${value.Source}</div>
							</c:if>
						</c:if>

					</div> <!-- Fin de wrapper -->
					<c:if test="${cms.element.settings.borderedbottom == 'true' }">
						<hr class="divider" />
					</c:if>
				</article>
				<c:set var="titletag"></c:set>
				<c:set var="subtitletag"></c:set>
				<c:set var="titlesection"></c:set>
				<c:set var="titleblock"></c:set>
				<c:set var="titlegallery"></c:set>

			</c:otherwise>
		</c:choose>
	</cms:formatter>
</cms:bundle>