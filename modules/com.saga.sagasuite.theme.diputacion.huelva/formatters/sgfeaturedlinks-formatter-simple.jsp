<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" import="java.util.Date"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.microcontent.messages">
    <cms:formatter var="content">
        <c:choose>
            <c:when test="${cms.element.inMemoryOnly}">
                <h1 class="title">
                    Edite este recurso!!
                </h1>
            </c:when>
            <c:otherwise>

                <div class="element parent sg-featuredlink box small-box">
                        <div class="headline">
                            <div class="title h3">${content.value.Title}</div>
                        </div>
                    <c:if test="${content.value.Description.exists and content.value.Description.value.ContentDescription.isSet}">
                        <div class="description" ${content.value.Description.rdfa.ContentDescription}>
                                ${content.value.Description.value.ContentDescription}
                        </div>
                    </c:if>
                    <div class="wrapper">
                            <ul class="nav scrollbar">
                                <c:forEach items="${content.valueList.Links}" var="link">
                                    <%-- Comprobamos la visibilidad del elemento --%>
                                    <jsp:useBean id="now" class="java.util.Date" />
                                    <c:set var="init" value=""/>
                                    <c:set var="end" value=""/>
                                    <c:if test="${link.value.DateVisibilityInit.exists}">
                                        <c:set var="init" value="${cms:convertDate(link.value.DateVisibilityInit)}"/>
                                    </c:if>
                                    <c:if test="${link.value.DateVisibilityEnd.exists}">
                                        <c:set var="end" value="${cms:convertDate(link.value.DateVisibilityEnd)}"/>
                                    </c:if>
									<c:if test="${link.value.CssClass.isSet}">
                                        <c:set var="cssclasslink" value="${link.value.CssClass}"/>
                                    </c:if>
                                    <c:if test="${(empty init and empty end) or
								  ((init!=null and init lt now) and
								   (end!=null and end gt now))}">
                                        <li class="${cssclasslink}">
                                            <a href="<cms:link>${link.value.Href}</cms:link>" target="${link.value.Target}">
                                                <c:if test="${link.value.Icon.isSet}">
                                                    <span aria-hidden="true" class="fa fa-${fn:toLowerCase(link.value.Icon.stringValue)}<c:if test="${fn:endsWith(fn:toLowerCase(link.value.Icon.stringValue), 'lightbulb')}">-o</c:if>">&nbsp;</span>
                                                </c:if>
                                                <c:if test="${link.value.IconImage.isSet}">
                                                    <cms:img src="${link.value.IconImage}" />
                                                </c:if>
                                                <span class="flink-title">${link.value.Title}</span>
                                            </a>
                                        </li>
                                    </c:if>
                                </c:forEach>
                            </ul>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
    </cms:formatter>
</cms:bundle>