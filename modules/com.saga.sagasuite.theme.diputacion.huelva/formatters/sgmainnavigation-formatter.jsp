<%@ page buffer="none" session="false" trimDirectiveWhitespaces="true" import="com.saga.sagasuite.mainnavigation.view.MainNavigationView" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
    <cms:formatter var="content" val="value" rdfa="rdfa">

        <%-- cargamos todas las variables del themeconfig usadas en la cabecera --%>

        <c:set var="Logo">${themeConfiguration.Logo}</c:set>
        <c:set var="Entity">${themeConfiguration.Entity}</c:set>
        <c:set var="LogoEntity">${themeConfiguration.LogoEntity}</c:set>
        <c:set var="EntityLink">${themeConfiguration.CustomFieldKeyValue.EntityLink}</c:set>
        <c:set var="socialpages">${themeConfiguration.CustomFieldKeyValue.socialpages}</c:set>
        <c:set var="FacebookPageUrl">${themeConfiguration.FacebookPageUrl}</c:set>
        <c:set var="TwitterPageUrl">${themeConfiguration.TwitterPageUrl}</c:set>
        <c:set var="GooglePlusPageUrl">${themeConfiguration.GooglePlusPageUrl}</c:set>
        <c:set var="YouTubePageUrl">${themeConfiguration.YouTubePageUrl}</c:set>
        <c:set var="socialflickrpage">${themeConfiguration.CustomFieldKeyValue.socialflickrpage}</c:set>
        <c:set var="socialinstagrampage">${themeConfiguration.CustomFieldKeyValue.socialinstagrampage}</c:set>
        <c:set var="socialpinterestpage">${themeConfiguration.CustomFieldKeyValue.socialpinterestpage}</c:set>
        <c:set var="socialvimeopage">${themeConfiguration.CustomFieldKeyValue.socialvimeopage}</c:set>
        <c:set var="socialslidesharepage">${themeConfiguration.CustomFieldKeyValue.socialslidesharepage}</c:set>
        <c:set var="rsspage">${themeConfiguration.CustomFieldKeyValue.rsspage}</c:set>
        <c:set var="sitemappage">${themeConfiguration.CustomFieldKeyValue.sitemappage}</c:set>
        <c:set var="searchpage">${themeConfiguration.CustomFieldKeyValue.searchpage}</c:set>
        <c:set var="accessibilitypage">${themeConfiguration.CustomFieldKeyValue.accessibilitypage}</c:set>
        <c:set var="contactpage">${themeConfiguration.CustomFieldKeyValue.contactpage}</c:set>
        <c:set var="faqpage">${themeConfiguration.CustomFieldKeyValue.faqpage}</c:set>
        <c:set var="incidencespage">${themeConfiguration.CustomFieldKeyValue.incidencespage}</c:set>
        <c:set var="newspage">${themeConfiguration.CustomFieldKeyValue.newspage}</c:set>


        <c:choose>
            <c:when test="${cms.element.inMemoryOnly}">
                <h1 class="title">
                    Edite este recurso!!
                </h1>
            </c:when>
            <c:otherwise>
                <div class="mega-menu">
                    <%
                        MainNavigationView mainNavigationView = new MainNavigationView(pageContext, request, response);
                        mainNavigationView.handleRequest();
                    %>

                        <%-- ABRIMOS EL CONTENEDOR PRINCIPAL DE LA CABECERA --%>
                    <nav class="navbar nav-section-general mega-menu <c:if test='${mainNavigationAccess.existsCssClass()}'> ${mainNavigationAccess.getCssClass()}</c:if>" <c:if test='${mainNavigationAccess.existsId()}'>id='${mainNavigationAccess.getId()}'</c:if>>
                        <%-- BLOQUE DE TOP BAR - visible > 768px //////////////////////////////////////////////////////////////// --%>
                            <div class="topbar-inverse hidden-xs hidden-xxs">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-12">
                                                <%-- si no existe bloque de topbar en el recurso de cabecera: usamos el themeconfig --%>
                                            <c:if test="${!mainNavigationAccess.existsTopBar()}">
                                                <%-- enlaces informacion general --%>
                                                <ul class="list-unstyled list-inline pull-left">
                                                    <c:if test="${not empty sitemappage}">
                                                        <li><a class="tooltip-bottom" accesskey="3" title="<fmt:message key="key.formatter.header.sitemap" />" href="${sitemappage}"> <span class="fa fa-sitemap" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.sitemap" /></span></a></li>
                                                    </c:if>
                                                    <c:if test="${not empty searchpage}">
                                                        <li><a class="tooltip-bottom" accesskey="4" title="<fmt:message key="key.formatter.header.searchpage" />" href="${searchpage}"> <span class="fa fa-search" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.searchpage" /></span></a></li>
                                                    </c:if>
                                                    <c:if test="${not empty accessibilitypage}">
                                                        <li><a class="tooltip-bottom" accesskey="0" title="<fmt:message key="key.formatter.header.accessibilitypage" />" href="${accessibilitypage}"> <span class="fa fa-wheelchair-alt" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.accessibilitypage" /></span></a></li>
                                                    </c:if>
                                                    <c:if test="${not empty contactpage}">
                                                        <li><a class="tooltip-bottom" accesskey="9" title="<fmt:message key="key.formatter.header.contactpage" />" href="${contactpage}"> <span class="fa fa-envelope-o" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.contactpage" /></span></a></li>
                                                    </c:if>
                                                    <c:if test="${not empty newspage}">
                                                        <li><a class="tooltip-bottom" accesskey="2" title="<fmt:message key="key.formatter.header.newspage" />" href="${newspage}"> <span class="fa fa-newspaper-o" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.newspage" /></span></a></li>
                                                    </c:if>
                                                    <c:if test="${not empty faqpage}">
                                                        <li><a class="tooltip-bottom" accesskey="5" title="<fmt:message key="key.formatter.header.faqpage" />" href="${faqpage}"> <span class="fa fa-life-saver" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.faqpage" /></span></a></li>
                                                    </c:if>
                                                    <c:if test="${not empty incidencespage}">
                                                        <li><a class="tooltip-bottom" accesskey="7" title="<fmt:message key="key.formatter.header.incidencespage" />" href="${incidencespage}"> <span class="fa fa-bullhorn" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.incidencespage" /></span></a></li>
                                                    </c:if>
                                                </ul>
                                            </c:if>
                                                <%-- si existe bloque de topbar en el recurso de cabecera --%>
                                            <c:if test="${mainNavigationAccess.existsTopBar()}">
                                                ${mainNavigationAccess.getTopBar()}
                                            </c:if>
                                                <%-- si no existe bloque de merchant en el recurso de cabecera: usamos el themeconfig --%>
                                            <c:if test="${!mainNavigationAccess.existsMerchant()}">
                                                <%-- redes sociales y el tiempo--%>
                                                <c:if test="${socialpages == 'true'}">
                                                    <ul class="list-unstyled list-inline social-list pull-right">
                                                        <c:if test="${not empty FacebookPageUrl}">
                                                            <li><a title="<fmt:message key="key.formatter.header.facebook" />" class="facebook tooltip-bottom" href="${FacebookPageUrl}" target="_blank"> <span class="fa fa-facebook" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.facebook" /></span></a></li>
                                                        </c:if>
                                                        <c:if test="${not empty TwitterPageUrl}">
                                                            <li><a title="<fmt:message key="key.formatter.header.twitter" />" class="twitter tooltip-bottom" href="${TwitterPageUrl}" target="_blank"> <span class="fa fa-twitter" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.twitter" /></span></a></li>
                                                        </c:if>
                                                        <c:if test="${not empty GooglePlusPageUrl}">
                                                            <li><a title="<fmt:message key="key.formatter.header.googleplus" />" class="googleplus tooltip-bottom" href="${GooglePlusPageUrl}" target="_blank"> <span class="fa fa-google-plus" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.googleplus" /></span></a></li>
                                                        </c:if>
                                                        <c:if test="${not empty YouTubePageUrl}">
                                                            <li><a title="<fmt:message key="key.formatter.header.youtube" />" class="youtube tooltip-bottom" href="${YouTubePageUrl}" target="_blank"> <span class="fa fa-youtube-play" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.youtube" /></span></a></li>
                                                        </c:if>
                                                        <c:if test="${not empty socialflickrpage}">
                                                            <li><a title="<fmt:message key="key.formatter.header.flickr" />" class="flickr tooltip-bottom" href="${socialflickrpage}" target="_blank"> <span class="fa fa-flickr" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.flickr" /></span></a></li>
                                                        </c:if>
                                                        <c:if test="${not empty socialinstagrampage}">
                                                            <li><a title="<fmt:message key="key.formatter.header.instagram" />" class="instagram tooltip-bottom" href="${socialinstagrampage}" target="_blank"> <span class="fa fa-instagram" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.instagram" /></span></a></li>
                                                        </c:if>
                                                        <c:if test="${not empty socialpinterestpage}">
                                                            <li><a title="<fmt:message key="key.formatter.header.pinterest" />" class="pinterest tooltip-bottom" href="${socialpinterestpage}" target="_blank"> <span class="fa fa-pinterest" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.pinterest" /></span></a></li>
                                                        </c:if>
                                                        <c:if test="${not empty socialvimeopage}">
                                                            <li><a title="<fmt:message key="key.formatter.header.vimeo" />" class="vimeo tooltip-bottom" href="${socialvimeopage}" target="_blank"> <span class="fa fa-vimeo" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.vimeo" /></span></a></li>
                                                        </c:if>
                                                        <c:if test="${not empty socialslidesharepage}">
                                                            <li><a title="<fmt:message key="key.formatter.header.slideshare" />" class="pinterest tooltip-bottom" href="${socialslidesharepage}" target="_blank"> <span class="fa fa-slideshare" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.slideshare" /></span></a></li>
                                                        </c:if>
                                                        <c:if test="${not empty rsspage}">
                                                            <li><a title="<fmt:message key="key.formatter.header.rss" />" class="rss tooltip-bottom" href="${rsspage}"> <span class="fa fa-rss" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.rss" /></span></a></li>
                                                        </c:if>
                                                    </ul>
                                                </c:if>
                                            </c:if>
                                            <%-- si existe bloque de merchant en el recurso de cabecera --%>
                                            <c:if test="${mainNavigationAccess.existsMerchant()}">
                                                ${mainNavigationAccess.getMerchant()}
                                            </c:if>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        <div class="navbar-menu">
                            <div class="container">
                                <%-- BLOQUE DE LOGO Y MENU --%>
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#headernavbar" aria-expanded="false" aria-controls="headernavbar">
                                        <span class="sr-only"><fmt:message key='key.formatter.header.toggle.navigation' /></span>
                                        <span class="nat-icon menu-icon" aria-hidden="true"></span>
                                    </button>
                                        <%-- visible en collapse --%>
                                    <c:if test="${mainNavigationAccess.existsSearch()}">
                                        <div class="hidden-lg">
                                                ${mainNavigationAccess.getSearch()}
                                        </div>
                                    </c:if>
                                    <c:if test="${not empty Logo}">
                                        <c:set var="imageName">${Logo}</c:set>
                                        <c:set var="imageDescription">${cms:vfs(pageContext).property[imageName]['Title']}</c:set>
                                        <c:if test="${mainNavigationAccess.existsLink()}">
                                            <c:set var="imageLink" value="${mainNavigationAccess.getLink()}"/>
                                            <a class="navbar-brand tooltip-right" accesskey="1" href="${imageLink.getHref()}"<c:if test="${imageLink.existsAlt()}"> title="${imageLink.getAlt()}"</c:if><c:if test="${imageLink.existsTarget()}"> target="${imageLink.getTarget()}"</c:if>>
                                                <cms:img src="${Logo}" alt="${imageDescription}" cssclass="img-responsive"/>
                                            </a>
                                        </c:if>
                                        <c:if test="${!mainNavigationAccess.existsLink()}">
                                            <div class="navbar-brand">
                                                <cms:img src="${Logo}" width="170" scaleType="2" alt="${imageDescription}" cssclass="img-responsive"/>
                                            </div>
                                        </c:if>
                                    </c:if>
                                </div><%-- fin navbar-header --%>
                                <c:if test="${mainNavigationAccess.existsAnySection()}">
                                    <div id="headernavbar" class="main-navigation-collapse navbar-collapse collapse">
                                        <%-- BLOQUE DE TOP BAR - visible < 768px  /////////////////////////////////////////////////////////////////// --%>
                                        <div class="topbar-inverse hidden-sm hidden-md hidden-lg">
                                            <div class="container">
                                                <div class="row">
                                                        <%-- si no existe bloque de topbar en el recurso de cabecera: usamos el themeconfig --%>
                                                    <c:if test="${!mainNavigationAccess.existsTopBar()}">
                                                        <%-- enlaces informacion general --%>
                                                        <div class="col-xs-6">
                                                            <ul class="list-unstyled list-inline social-list">
                                                                <c:if test="${not empty sitemappage}">
                                                                    <li><a class="tooltip-bottom" accesskey="3" title="<fmt:message key="key.formatter.header.sitemap" />" href="${sitemappage}"> <span class="fa fa-sitemap" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.sitemap" /></span></a></li>
                                                                </c:if>
                                                                <c:if test="${not empty searchpage}">
                                                                    <li><a class="tooltip-bottom" accesskey="4" title="<fmt:message key="key.formatter.header.searchpage" />" href="${searchpage}"> <span class="fa fa-search" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.searchpage" /></span></a></li>
                                                                </c:if>
                                                                <c:if test="${not empty accessibilitypage}">
                                                                    <li><a class="tooltip-bottom" accesskey="0" title="<fmt:message key="key.formatter.header.accessibilitypage" />" href="${accessibilitypage}"> <span class="fa fa-wheelchair-alt" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.accessibilitypage" /></span></a></li>
                                                                </c:if>
                                                                <c:if test="${not empty contactpage}">
                                                                    <li><a class="tooltip-bottom" accesskey="9" title="<fmt:message key="key.formatter.header.contactpage" />" href="${contactpage}"> <span class="fa fa-envelope-o" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.contactpage" /></span></a></li>
                                                                </c:if>
                                                                <c:if test="${not empty newspage}">
                                                                    <li><a class="tooltip-bottom" accesskey="2" title="<fmt:message key="key.formatter.header.newspage" />" href="${newspage}"> <span class="fa fa-newspaper-o" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.newspage" /></span></a></li>
                                                                </c:if>
                                                                <c:if test="${not empty faqpage}">
                                                                    <li><a class="tooltip-bottom" accesskey="5" title="<fmt:message key="key.formatter.header.faqpage" />" href="${faqpage}"> <span class="fa fa-life-saver" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.faqpage" /></span></a></li>
                                                                </c:if>
                                                                <c:if test="${not empty incidencespage}">
                                                                    <li><a class="tooltip-bottom" accesskey="7" title="<fmt:message key="key.formatter.header.incidencespage" />" href="${incidencespage}"> <span class="fa fa-bullhorn" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.incidencespage" /></span></a></li>
                                                                </c:if>
                                                            </ul>
                                                        </div>
                                                    </c:if>
                                                        <%-- si existe bloque de topbar en el recurso de cabecera --%>
                                                    <c:if test="${mainNavigationAccess.existsTopBar()}">
                                                        <div class="col-xs-6">
                                                                ${mainNavigationAccess.getTopBar()}
                                                        </div>
                                                    </c:if>
                                                        <%-- si no existe bloque de merchant en el recurso de cabecera: usamos el themeconfig --%>
                                                    <c:if test="${!mainNavigationAccess.existsMerchant()}">
                                                        <%-- redes sociales y el tiempo--%>
                                                        <c:if test="${socialpages == 'true'}">
                                                            <div class="col-xs-6">
                                                                <ul class="list-unstyled list-inline social-list pull-right">
                                                                    <c:if test="${not empty FacebookPageUrl}">
                                                                        <li><a title="<fmt:message key="key.formatter.header.facebook" />" class="facebook tooltip-bottom" href="${FacebookPageUrl}" target="_blank"> <span class="fa fa-facebook" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.facebook" /></span></a></li>
                                                                    </c:if>
                                                                    <c:if test="${not empty TwitterPageUrl}">
                                                                        <li><a title="<fmt:message key="key.formatter.header.twitter" />" class="twitter tooltip-bottom" href="${TwitterPageUrl}" target="_blank"> <span class="fa fa-twitter" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.twitter" /></span></a></li>
                                                                    </c:if>
                                                                    <c:if test="${not empty GooglePlusPageUrl}">
                                                                        <li><a title="<fmt:message key="key.formatter.header.googleplus" />" class="googleplus tooltip-bottom" href="${GooglePlusPageUrl}" target="_blank"> <span class="fa fa-google-plus" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.googleplus" /></span></a></li>
                                                                    </c:if>
                                                                    <c:if test="${not empty YouTubePageUrl}">
                                                                        <li><a title="<fmt:message key="key.formatter.header.youtube" />" class="youtube tooltip-bottom" href="${YouTubePageUrl}" target="_blank"> <span class="fa fa-youtube-play" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.youtube" /></span></a></li>
                                                                    </c:if>
                                                                    <c:if test="${not empty socialflickrpage}">
                                                                        <li><a title="<fmt:message key="key.formatter.header.flickr" />" class="flickr tooltip-bottom" href="${socialflickrpage}" target="_blank"> <span class="fa fa-flickr" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.flickr" /></span></a></li>
                                                                    </c:if>
                                                                    <c:if test="${not empty socialinstagrampage}">
                                                                        <li><a title="<fmt:message key="key.formatter.header.instagram" />" class="instagram tooltip-bottom" href="${socialinstagrampage}" target="_blank"> <span class="fa fa-instagram" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.instagram" /></span></a></li>
                                                                    </c:if>
                                                                    <c:if test="${not empty socialpinterestpage}">
                                                                        <li><a title="<fmt:message key="key.formatter.header.pinterest" />" class="pinterest tooltip-bottom" href="${socialpinterestpage}" target="_blank"> <span class="fa fa-pinterest" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.pinterest" /></span></a></li>
                                                                    </c:if>
                                                                    <c:if test="${not empty socialvimeopage}">
                                                                        <li><a title="<fmt:message key="key.formatter.header.vimeo" />" class="vimeo tooltip-bottom" href="${socialvimeopage}" target="_blank"> <span class="fa fa-vimeo" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.vimeo" /></span></a></li>
                                                                    </c:if>
                                                                    <c:if test="${not empty socialslidesharepage}">
                                                                        <li><a title="<fmt:message key="key.formatter.header.slideshare" />" class="pinterest tooltip-bottom" href="${socialslidesharepage}" target="_blank"> <span class="fa fa-slideshare" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.slideshare" /></span></a></li>
                                                                    </c:if>
                                                                    <c:if test="${not empty rsspage}">
                                                                        <li><a title="<fmt:message key="key.formatter.header.rss" />" class="rss tooltip-bottom" href="${rsspage}"> <span class="fa fa-rss" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.formatter.header.accesstext" /><fmt:message key="key.formatter.header.rss" /></span></a></li>
                                                                    </c:if>
                                                                </ul>
                                                            </div>
                                                        </c:if>
                                                    </c:if>
                                                        <%-- si existe bloque de merchant en el recurso de cabecera --%>
                                                    <c:if test="${mainNavigationAccess.existsMerchant()}">
                                                        <div class="col-xs-6">
                                                                ${mainNavigationAccess.getMerchant()}
                                                        </div>
                                                    </c:if>
                                                </div>
                                            </div>
                                        </div>
                                            <%-- Enlaces que despliegan del Menu principal --%>
                                            <ul class="nav navbar-nav navbar-right">
                                                    <%-- Recorremos el menu principal (lista de secciones) --%>
                                                <c:forEach var="seccion" items="${mainNavigationAccess.getSections()}" varStatus="sectionstatus">
                                                    <li class="mainnav-lvl1 dropdown dropdown-mega ${seccion.getClazz()}">
                                                        <c:if test="${seccion.existsTitle()}">
                                                            <c:if test="${seccion.existsRowList()}">
                                                                <a class="mainnav-lvl1 dropdown-toggle" aria-controls="collapsemegamenu${sectionstatus.count}" data-toggle="collapse" data-target="#collapsemegamenu${sectionstatus.count}" role="button" aria-haspopup="true" aria-expanded="false" href="<c:if test='${seccion.existsLink()}'>${seccion.getLink().getHref()}</c:if>">
                                                                        ${seccion.getTitle()}
                                                                    <c:if test="${seccion.existsIcon()}"><span class="fa fa-${seccion.getIcon()} pull-right" aria-hidden="true"></span></c:if>
                                                                    <span class="sr-only"><fmt:message key='key.formatter.header.toggle.subsections' /></span>
                                                                </a>
                                                            </c:if>
                                                            <c:if test="${!seccion.existsRowList()}">
                                                                <a class="mainnav-lvl1" href="<c:if test='${seccion.existsLink()}'>${seccion.getLink().getHref()}</c:if>">
                                                                        ${seccion.getTitle()}
                                                                    <c:if test="${seccion.existsIcon()}"><span class="fa fa-${seccion.getIcon()} pull-right" aria-hidden="true"></span></c:if>
                                                                </a>
                                                            </c:if>
                                                        </c:if>
                                                        <c:if test="${seccion.existsRowList()}">
                                                            <ul class="dropdown-menu-collapse collapse" id="collapsemegamenu${sectionstatus.count}">
                                                                <li>
                                                                    <div class="mega-menu-content">
                                                                        <div class="container submenu-wrapper">
                                                                            <%-- Recorremos las filas que componen la seccion --%>
                                                                            <c:forEach var="fila" items="${seccion.getRowList()}">
                                                                                <%-- Cada fila puede ser automatica o manual --%>
                                                                                <c:if test="${fila.isAutomatic()}">
                                                                                    <c:if test="${fila.existsTitle()}">
                                                                                        <div class="heading heading-lg">${fila.getTitle()}</div>
                                                                                    </c:if>
                                                                                    <div class="row">
                                                                                            <%-- Si la fila es automatica significa que crea una navegacion de forma automatica --%>
                                                                                            ${fila.getNavigation()}
                                                                                    </div>

                                                                                </c:if>
                                                                                <c:if test="${fila.isManual()}">
                                                                                        <c:if test="${fila.existsTitle()}">
                                                                                            <div class="heading heading-lg">${fila.getTitle()}</div>
                                                                                        </c:if>
                                                                                        <div class="row">
                                                                                            <%-- Cada fila manual esta formada por un numero de columnas configuradas en el recurso --%>
                                                                                        <c:forEach var="columna" items="${fila.getColumnList()}">
                                                                                            <div class="col-md-${columna.getWidth()} mainnav-column <c:if test='${columna.existsCssClass()}'>${columna.getCssClass()}</c:if>">

                                                                                                    <%-- Pintamos el titulo y enlace --%>
                                                                                                <c:if test="${columna.existsLink() && columna.existsTitle()}">
                                                                                                    <c:set var="link" value="${columna.getLink()}" />
                                                                                                    <div class="heading heading-md">
                                                                                                        <a href="${link.getHref()}"
                                                                                                           <c:if test="${link.existsTarget()}">target="${link.getTarget()}"</c:if>
                                                                                                           <c:if test="${link.existsAlt()}">title="${link.getAlt()}"</c:if>>
                                                                                                            <c:if test="${columna.existsIcon()}"><span class="fa fa-${columna.getIcon()}"></span></c:if>${columna.getTitle()}
                                                                                                        </a>
                                                                                                    </div>
                                                                                                </c:if>
                                                                                                <c:if test="${!columna.existsLink() && columna.existsTitle()}">
                                                                                                    <div class="heading heading-md"><c:if test="${columna.existsIcon()}"><span class="fa fa-${columna.getIcon()}"></span></c:if>${columna.getTitle()}</div>
                                                                                                </c:if>
                                                                                                    <%-- Cada columna esta formada por elementos --%>
                                                                                                <c:forEach var="elemento" items="${columna.getElementList()}">
                                                                                                    <%-- mostramos el contenido del elemento --%>
                                                                                                    <c:if test="${elemento.existsContent()}">${elemento.getContent()}</c:if>
                                                                                                </c:forEach>
                                                                                            </div>
                                                                                        </c:forEach>
                                                                                    </div>
                                                                                </c:if>
                                                                            </c:forEach>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <button class="btn-close" id="btnclosemainmenucollapse${sectionstatus.count}"><span class="nat-icon close-icon" aria-hidden="true"></span><span class="sr-only">Close</span></button>
                                                                </li>
                                                            </ul>
                                                        </c:if>
                                                    </li>
                                                </c:forEach>
                                                <%-- visible no collapse --%>
                                                <c:if test="${mainNavigationAccess.existsSearch()}">
                                                    <li class="hidden-xxs hidden-xs hidden-sm hidden-md pull-right">
                                                    ${mainNavigationAccess.getSearch()}
                                                    </li>
                                                </c:if>
                                                <%-- end visible no collapse --%>
                                            </ul>
                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </nav>

                        <%-- BLOQUES NO USADOS --%>
                        <%--
                        <c:if test="${mainNavigationAccess.existsMinorNavBar()}">
                            ${mainNavigationAccess.getMinorNavBar()}
                        </c:if>
                        <c:if test="${mainNavigationAccess.existsTeaser()}">
                            ${mainNavigationAccess.getTeaser()}
                        </c:if>
                        --%>
                </div>
            </c:otherwise>
        </c:choose>
    </cms:formatter>
</cms:bundle>
