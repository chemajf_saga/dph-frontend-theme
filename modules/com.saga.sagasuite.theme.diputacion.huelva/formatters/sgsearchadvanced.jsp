<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<cms:secureparams />
<fmt:setLocale value="${cms.locale}" />

<cms:formatter var="content" val="value" rdfa="rdfa">
<c:choose>
	<c:when test="${cms.element.inMemoryOnly}">
				<h1 class="title">
					Edite este recurso!!
				</h1>
	</c:when>
<c:otherwise>

<c:set var="messageproperty" value="${value.MessagesProperties }" scope="request" />
<c:if test="${!value.MessagesProperties.isSet }">
	<c:set var="messageproperty" value="com.saga.sagasuite.search.workplace" scope="request"/>
</c:if>

<cms:bundle basename="${messageproperty }">

<c:if test="${cms.element.setting.marginbottom.value != '0'}">
    <c:set var="marginClass">margin-bottom-${cms.element.setting.marginbottom.value}</c:set>
</c:if>
<c:if test="${not empty cms.element.settings.classmainbox }">
    <c:set var="classmainbox">${cms.element.settings.classmainbox}</c:set>
</c:if>
<c:if test="${cms.element.settings.featuredtitle =='true' }">
	<c:set var="featuredtitle">featured-title</c:set>
</c:if>
<%-- bloque imagen --%>
<c:if test="${not empty cms.element.settings.imageposition }">
	<c:set var="imagepositionsetting">${cms.element.settings.imageposition}</c:set>
</c:if>
<c:if test="${not empty cms.element.settings.imagewidth }">
	<c:set var="imagewidthsetting">${cms.element.settings.imagewidth}</c:set>
</c:if>

<%-- columnas --%>
<c:if test="${cms.element.settings.columns =='true' }">
	<c:set var="columns">row</c:set>
	<c:set var="columnsnumberxxs">1</c:set>	
	<c:set var="columnsnumberxs">2</c:set>	
	<c:set var="columnsnumbersm">3</c:set>
	<c:set var="columnsnumbermd"></c:set>
	<c:set var="columnsnumberlg"></c:set>
	<%-- columns xxs --%>
	<c:if test="${not empty cms.element.settings.columnsnumberxxs }">
		<c:set var="columnsnumberxxs">${cms.element.settings.columnsnumberxxs}</c:set>
		<c:if test="${columnsnumberxxs == '1'}">
			<c:set var="widthcolumnsxxs">col-xxs-12</c:set>
		</c:if>
		<c:if test="${columnsnumberxxs == '2'}">
			<c:set var="widthcolumnsxxs">col-xxs-6</c:set>
		</c:if>
		<c:if test="${columnsnumberxxs == '3'}">
			<c:set var="widthcolumnsxxs">col-xxs-4</c:set>
		</c:if>
		<c:if test="${columnsnumberxxs == '4'}">
			<c:set var="widthcolumnsxxs">col-xxs-3</c:set>
		</c:if>
		<c:if test="${columnsnumberxxs == '6'}">
			<c:set var="widthcolumnsxxs">col-xxs-2</c:set>
		</c:if>		
	</c:if>
	<%-- columns xs --%>
	<c:if test="${not empty cms.element.settings.columnsnumberxs }">
		<c:set var="columnsnumberxs">${cms.element.settings.columnsnumberxs}</c:set>
		<c:if test="${columnsnumberxs == '1'}">
			<c:set var="widthcolumnsxs">col-xs-12</c:set>
		</c:if>
		<c:if test="${columnsnumberxs == '2'}">
			<c:set var="widthcolumnsxs">col-xs-6</c:set>
		</c:if>
		<c:if test="${columnsnumberxs == '3'}">
			<c:set var="widthcolumnsxs">col-xs-4</c:set>
		</c:if>
		<c:if test="${columnsnumberxs == '4'}">
			<c:set var="widthcolumnsxs">col-xs-3</c:set>
		</c:if>
		<c:if test="${columnsnumberxs == '6'}">
			<c:set var="widthcolumnsxs">col-xs-2</c:set>
		</c:if>		
	</c:if>
		<%-- columns sm --%>
	<c:if test="${not empty cms.element.settings.columnsnumbersm }">
		<c:set var="columnsnumbersm">${cms.element.settings.columnsnumbersm}</c:set>
		<c:if test="${columnsnumbersm == '1'}">
			<c:set var="widthcolumnssm">col-sm-12</c:set>
		</c:if>
		<c:if test="${columnsnumbersm == '2'}">
			<c:set var="widthcolumnssm">col-sm-6</c:set>
		</c:if>
		<c:if test="${columnsnumbersm == '3'}">
			<c:set var="widthcolumnssm">col-sm-4</c:set>
		</c:if>
		<c:if test="${columnsnumbersm == '4'}">
			<c:set var="widthcolumnssm">col-sm-3</c:set>
		</c:if>
		<c:if test="${columnsnumbersm == '6'}">
			<c:set var="widthcolumnssm">col-sm-2</c:set>
		</c:if>		
	</c:if>
		<%-- columns md --%>
	<c:if test="${not empty cms.element.settings.columnsnumbermd }">
		<c:set var="columnsnumbermd">${cms.element.settings.columnsnumbermd}</c:set>
		<c:if test="${columnsnumbermd == '1'}">
			<c:set var="widthcolumnsmd">col-md-12</c:set>
		</c:if>
		<c:if test="${columnsnumbermd == '2'}">
			<c:set var="widthcolumnsmd">col-md-6</c:set>
		</c:if>
		<c:if test="${columnsnumbermd == '3'}">
			<c:set var="widthcolumnsmd">col-md-4</c:set>
		</c:if>
		<c:if test="${columnsnumbermd == '4'}">
			<c:set var="widthcolumnsmd">col-md-3</c:set>
		</c:if>	
		<c:if test="${columnsnumbermd == '6'}">
			<c:set var="widthcolumnsmd">col-md-2</c:set>
		</c:if>			
	</c:if>
		<%-- columns lg --%>
	<c:if test="${not empty cms.element.settings.columnsnumberlg }">
		<c:set var="columnsnumberlg">${cms.element.settings.columnsnumberlg}</c:set>
		<c:if test="${columnsnumberlg == '1'}">
			<c:set var="widthcolumnslg">col-lg-12</c:set>
		</c:if>
		<c:if test="${columnsnumberlg == '2'}">
			<c:set var="widthcolumnslg">col-lg-6</c:set>
		</c:if>
		<c:if test="${columnsnumberlg == '3'}">
			<c:set var="widthcolumnslg">col-lg-4</c:set>
		</c:if>
		<c:if test="${columnsnumberlg == '4'}">
			<c:set var="widthcolumnslg">col-lg-3</c:set>
		</c:if>
		<c:if test="${columnsnumberlg == '6'}">
			<c:set var="widthcolumnslg">col-lg-2</c:set>
		</c:if>		
	</c:if>
	<c:set var="widthcolumns"><c:out value="${widthcolumnsxxs} ${widthcolumnsxs} ${widthcolumnssm} ${widthcolumnsmd} ${widthcolumnslg} " /></c:set>
</c:if>

	<%-- gestionamos setting de tipo de recurso ===================================================================--%>

	<c:if test="${not empty cms.element.settings.resourcetype }">
		<c:set var="settingresourcetype" scope="request">${cms.element.settings.resourcetype}</c:set>
	</c:if>

	<%-- Definimos el tipo de etiqueta html para nuestro encabezado y encabezado de elementos que se listan --%>

	<c:set var="titletag">h1</c:set>
	<c:set var="titletagelement">h2</c:set>

	<c:if test="${not empty cms.element.settings.titletag }">
		<c:set var="titletag">${cms.element.settings.titletag }</c:set>
		<c:if test="${titletag == 'h1'}">
			<c:set var="titletagelement">h2</c:set>
		</c:if>
		<c:if test="${titletag == 'h2'}">
			<c:set var="titletagelement">h3</c:set>
		</c:if>
		<c:if test="${titletag == 'h3'}">
			<c:set var="titletagelement">h4</c:set>
		</c:if>
		<c:if test="${titletag == 'h4'}">
			<c:set var="titletagelement">h5</c:set>
		</c:if>
		<c:if test="${titletag == 'h5'}">
			<c:set var="titletagelement">h6</c:set>
		</c:if>
		<c:if test="${titletag == 'div'}">
			<c:set var="titletagelement">div</c:set>
		</c:if>
	</c:if>

	<c:if test="${not empty cms.element.settings.titletagelement }">
		<c:set var="titletagelement">${cms.element.settings.titletagelement }</c:set>
	</c:if>
	
	<c:if test="${not empty cms.element.settings.titletagsize }">
		<c:set var="titletagsize">${cms.element.settings.titletagsize }</c:set>
	</c:if>

	<c:if test="${not empty cms.element.settings.titletagelementsize }">
		<c:set var="titletagelementsize">${cms.element.settings.titletagelementsize }</c:set>
	</c:if>
	
<div class="element parent sg-search advanced <c:out value=' ${marginClass} ${classmainbox} ' />" id="sg-search-${value.Id}">

<div class="wrapper <c:if test='${value.ShowResults == "true" }'>hasresults </c:if> <c:out value='${value.CssClass}' />">


<%-- Cargamos las variables que luego vamos a ir necesitando a lo largo --%>
<c:set var="cmsObject" value="${cms.vfs.cmsObject }" scope="request"/>
<c:set var="value" value="${value }" scope="request" />
<c:set var="content" value="${content }" scope="request" />
<c:set var="currentSite" scope="request">${cms.requestContext.siteRoot }</c:set>
<c:set var="currentProject" scope="request">${cms.requestContext.currentProject }</c:set>
<c:set var="isEdited" scope="request">${cms.edited}</c:set>

<%-- Si se ha marcado el readPropertyCategory tenemos que buscar la propiedad category --%>
<c:if test="${content.value.readPropertyCategory.exists && content.value.readPropertyCategory=='true'}">
	<c:set var="categoryProperty" scope="request"><cms:property name="category" file="search"/></c:set>
	<%-- lo guardamos tambien con el formato que lee el filtro de categorias y lo enviamos en el request para la jsp de filter.jsp --%>
	<c:set var="filtercategoryProperty" scope="request">${fn:substringAfter(categoryProperty, "/.categories/")}</c:set>
</c:if>

<c:if test="${not cms.element.settings.hidetitle && value.HideTitle != 'true'}">
<header class="headline <c:out value='${featuredtitle}' />">
		<${titletag} class="title ${titletagsize}">${value.Title }</${titletag}>
</header>
</c:if>

<c:if test="${value.TextForm.isSet }">
	<div class="tstextform">${value.TextForm }</div>
</c:if>

<%-- gestionamos si se muestra y/o se carga el formulario de búsqueda ===============================================================--%>

<c:set var="loadform" value="true" />
<c:set var="showform" scope="request"></c:set>

<c:if test="${value.NotLoadForm.isSet and value.NotLoadForm == 'true'}">
	<c:set var="loadform" value="false" />
</c:if>

<c:if test="${value.ShowForm.exists and value.ShowForm == 'false'}">
	<c:set var="showform" scope="request">hide</c:set>
</c:if>

<%-- si hay setting manda para cargar el formulario o no --%>
<c:if test="${not empty cms.element.settings.loadform }">
	<c:set var="loadform">${cms.element.settings.loadform}</c:set>
</c:if>

<%-- si hay setting manda para mostrar el formulario o no (se manda el valor de la clase css para ocultar o no) --%>
<c:if test="${not empty cms.element.settings.showform }">
	<c:set var="showform" scope="request">${cms.element.settings.showform }</c:set>
</c:if>

<c:if test="${loadform == 'true'}">
	<%-- JSP que muestra los filtros configurados en el recurso --%>
	<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.search/elements/c-filter.jsp:f1ba3624-c697-11e2-a183-69f9ccfec931)"/>
</c:if>


<c:if test="${value.TextHeader.isSet }">
	<div class="tstextheader">${value.TextHeader }</div>
</c:if>

<%-- gestionamos mostrar resultados --%>

<c:set var="showresults"></c:set>

<c:if test="${value.ShowResults != null and value.ShowResults == 'true' }">
	<c:set var="showresults">${value.ShowResults}</c:set>
</c:if>

<%-- se comprueba el setting que es el que manda --%>
<c:if test="${not empty cms.element.settings.showresults }">
	<c:if test="${cms.element.settings.showresults == 'false'}">
		<c:set var="showresults">false</c:set>
	</c:if>
	<c:if test="${cms.element.settings.showresults == 'true'}">
		<c:set var="showresults">true</c:set>
	</c:if>
</c:if>

<c:if test="${not empty showresults and showresults == 'true'}">

	<%-- Hacemos el include de la jsp que se encarga de realizar la busqueda sobre solr en base a los parametros recibidos --%>
	<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/e-search-solrquery.jsp:27280c8c-1d2d-11e8-b7b8-63f9a58dae09)"/>
	<%-- JSP que muestra los resultados obtenidos en la busqueda realizada --%>
	<c:if test="${value.ViewType != 'file' }">
	<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.search/elements/c-resultlist.jsp:2c26bd1b-c2c8-11e2-9372-69f9ccfec931)">
		<cms:param name="columns">${columns}</cms:param>
		<cms:param name="columnsnumberxxs">${columnsnumberxxs}</cms:param>
		<cms:param name="columnsnumberxs">${columnsnumberxs}</cms:param>
		<cms:param name="columnsnumbersm">${columnsnumbersm}</cms:param>
		<cms:param name="columnsnumbermd">${columnsnumbermd}</cms:param>
		<cms:param name="columnsnumberlg">${columnsnumberlg}</cms:param>
		<cms:param name="widthcolumns">${widthcolumns}</cms:param>
		<cms:param name="titletagelementsize">${titletagelementsize}</cms:param>
		<cms:param name="titletagelement">${titletagelement}</cms:param>
		<cms:param name="imagepositionsetting">${imagepositionsetting}</cms:param>
		<cms:param name="imagewidthsetting">${imagewidthsetting}</cms:param>
	</cms:include>
	</c:if>
	<c:if test="${value.ViewType == 'file' }">
	<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.search/elements/c-resultlist-table-files.jsp:3881daf7-adca-11e3-a428-f18cf451b707)"/>
	</c:if>	
	<%-- JSP que muestra la paginacion en base a la configuracion realizada y al numero de resultados encontrados --%>
	<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.search/elements/c-pagination.jsp:630ed814-c449-11e2-9372-69f9ccfec931)"/>

</c:if>

<c:if test="${value.TextFooter.isSet }">
	<div class="tstextfooter">${value.TextFooter }</div>
</c:if>

<%-- Script para los select --%>
	<script type="text/javascript">
	$ (document).ready(function(){

		<c:if test="${value.DropDownForm!=null && value.DropDownForm == 'true' }">
			// PARA CAMBIAR LA ORIENTACION DE LA FLECHA DE DESPLIEGUE EN EL LOS FILTROS

			$('#filters-${value.Id }').on('shown.bs.collapse', function () {
				console.log('filter shown');
				$('[aria-controls="filters-${value.Id }"][aria-expanded="true"] .fa').removeClass('fa-angle-down').addClass('fa-angle-up');
				$('[aria-controls="filters-${value.Id }"][aria-expanded="true"] .filter-link-text').html('<fmt:message key="formatter.filter.contract"/>');
			});

			$('#filters-${value.Id }').on('hidden.bs.collapse', function () {
				$('[aria-controls="filters-${value.Id }"][aria-expanded="false"] .fa').removeClass('fa-angle-up').addClass('fa-angle-down');
				$('[aria-controls="filters-${value.Id }"][aria-expanded="false"] .filter-link-text').html('<fmt:message key="formatter.filter.expand"/>');
			});
		</c:if>
		$('input').iCheck({
			labelHover: false,
			cursor: true
		});
		if($('.sg-search.advanced select').length > 0){
			$('.sg-search.advanced select').select2({
				theme: "bootstrap",
				width: "100%",
				language: {
					noResults: function () {
						<c:if test="${cms.locale == 'es'}">
							return "No se encontraron resultados";
						</c:if>
						<c:if test="${cms.locale == 'en'}">
								return "No results found";
						</c:if>
						<c:if test="${cms.locale == 'de'}">
								return "Keine Übereinstimmungen gefunden";
						</c:if>
						<c:if test="${cms.locale == 'fr'}">
								return "Aucun résultat trouvé";
						</c:if>
						<c:if test="${cms.locale == 'it'}">
								return "Nessun risultato trovato";
						</c:if>
						<c:if test="${cms.locale == 'pt'}">
								return "Sem resultados";
						</c:if>
						<c:if test="${cms.locale == 'ca'}">
								return "No es van trobar resultats";
						</c:if>
					}
				}
			});
		}else {}
	});
	</script>
</div>
</div>
</cms:bundle>
	<%-- Limpiamos los parametros que van en el request para que no los cojan otros buscadores en la pagina --%>
	<c:set var="cmsObject" scope="request"></c:set>
	<c:set var="value" scope="request"></c:set>
	<c:set var="content" scope="request"></c:set>
	<c:set var="currentSite" scope="request"></c:set>
	<c:set var="currentProject" scope="request"></c:set>
	<c:set var="isEdited" scope="request"></c:set>
	<c:set var="titletagelement" scope="request"></c:set>
	<c:set var="titletagelementsize" scope="request"></c:set>
	<c:set var="categoryProperty" scope="request"></c:set>
	<c:set var="resourcetype" scope="request"></c:set>
	<c:set var="showform" scope="request"></c:set>
	<c:set var="widthcolumns" scope="request"></c:set>
	<c:set var="imagepositionsetting" scope="request"></c:set>
	<c:set var="imagewidthsetting" scope="request"></c:set>
</c:otherwise>
</c:choose>	
</cms:formatter>