<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" buffer="none" session="false"
	trimDirectiveWhitespaces="true"%>
<%@ page import="java.sql.*"%>
<%@ page import="org.apache.commons.lang3.StringUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%//System.out.println("-Inicio corporacion-db-detalle.jsp");%>
<%-- Parámetro de entrada: identificador de la persona --%>
<c:set var="dipu" value="${param.diputado}" />
<%
String PERSONA = request.getParameter("diputado");

String texto = "";
String img = "";
if (StringUtils.isNotEmpty(PERSONA)) {
    String sql = "SELECT d.curriculum as textoDetalle, d.imagen_ficha as imgDetalle FROM public.v_diputados as d WHERE d.persona = '"
                    + PERSONA + "'";

    Connection conexion = null;
    Statement sentencia = null;
    ResultSet rs = null;

    try {
        Class.forName("org.postgresql.Driver");
    } catch (Exception e) {
        //JOptionPane.showMessageDialog(null, "No se pudo cargar el puente JDBC-ODBC.");
        //System.out.println("ERROR (corporacion-db-detalle.jsp): No se pudo cargar el puente JDBC-ODBC.");
        return;
    }
    //System.out.println("Puente JDBC-ODBC cargado! (corporacion-db-detalle.jsp)");

    try {
        // DESA conexion = DriverManager.getConnection("jdbc:postgresql://192.168.131.30:5432/portalweb", "ocmsdph", "ocmsdph");
        /*PROD*/conexion = DriverManager.getConnection("jdbc:postgresql://192.168.136.33:5432/portalweb", "ocmsdph", "ocmsdph");
        //System.out.println("Conexion realizada (corporacion-db-detalle.jsp)");
        sentencia = conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        //System.out.println("Sentencia creada (corporacion-db-detalle.jsp)");
    } catch (Exception er) {
        //System.out.println("ERROR (corporacion-db-detalle.jsp): " + er);
        er.printStackTrace();
    }

    try {
        //System.out.println("Realizamos la consulta (corporacion-db-detalle.jsp): " + sql);
        rs = sentencia.executeQuery(sql);
        while (rs.next()) {
            texto = rs.getString("textoDetalle").toString();
            img = rs.getString("imgDetalle").toString();

        }

    } catch (Exception er) {
        //System.out.println("ERROR (corporacion-db-detalle.jsp): " + er);
        er.printStackTrace();
    }
    conexion.close();
} else {

}
    //System.out.println("RES: dt ["+texto+"]");
    //System.out.println("RES: img ["+img+"]");
    pageContext.setAttribute("dt", texto);
    pageContext.setAttribute("img", img);
%>
<div>
	<c:set var="dt" value="${dt}" scope="request" />
	<c:set var="img" value="${img}" scope="request" />
</div>
<%//System.out.println("-FIN corporacion-db-detalle.jsp");%>