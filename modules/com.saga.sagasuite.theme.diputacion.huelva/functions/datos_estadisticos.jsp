<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<fmt:setLocale value="${cms.locale}"/>
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">

    <%-- Definimos las variables para los datos estadísticos en funcion de los campos del Theme Config --%>
    <c:set var="claveDatosEstadisticos">${themeConfiguration.CustomFieldKeyValue.claveDatosEstadisticos}</c:set>

    <div class="row">
        <div class="col-sm-12">
            <iframe src="https://portal.dipusevilla.es/SIE/generaFichaPublicaAction.action?clave=${claveDatosEstadisticos}&amp;entidad=00000" width="100%" height="500" style="border:none"></iframe>
        </div>
    </div>
</cms:bundle>
