<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<fmt:setLocale value="${cms.locale}"/>
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">

    <%-- Definimos las variables para las redes sociales en funcion de los campos del Theme Config --%>

    <c:set var="FacebookPageUrl">${themeConfiguration.FacebookPageUrl}</c:set>
    <c:set var="TwitterPageUrl">${themeConfiguration.TwitterPageUrl}</c:set>
    <c:set var="GooglePlusPageUrl">${themeConfiguration.GooglePlusPageUrl}</c:set>
    <c:set var="YouTubePageUrl">${themeConfiguration.YouTubePageUrl}</c:set>
    <c:set var="socialflickrpage">${themeConfiguration.CustomFieldKeyValue.socialflickrpage}</c:set>
    <c:set var="socialinstagrampage">${themeConfiguration.CustomFieldKeyValue.socialinstagrampage}</c:set>
    <c:set var="socialpinterestpage">${themeConfiguration.CustomFieldKeyValue.socialpinterestpage}</c:set>
    <c:set var="socialvimeopage">${themeConfiguration.CustomFieldKeyValue.socialvimeopage}</c:set>
    <c:set var="socialslidesharepage">${themeConfiguration.CustomFieldKeyValue.socialslidesharepage}</c:set>
    <c:set var="rsspage">${themeConfiguration.CustomFieldKeyValue.rsspage}</c:set>


    <article class="articulo">
        <div class="contentblock">
            <div class="contentblock-texto">
                <ul class="nav">
                    <c:if test="${not empty FacebookPageUrl}">
                        <li aria-hidden="true"><hr class="divider no-margin" /></li>
                        <li>
                            <a href="${FacebookPageUrl}" class="facebook no-icon" target="_blank">
                                <span class="fa-stack fa-lg inline-b v-align-m fs36 mr-10" aria-hidden="true">
                                  <span class="fa fa-circle fa-stack-2x"></span>
                                  <span class="fa fa-facebook fa-stack-1x fa-inverse"></span>
                                </span>
                                <span class="v-align-m inline-b fs18">
                                    <fmt:message key="key.formatter.header.facebook" />
                                </span>
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${not empty TwitterPageUrl}">
                        <li aria-hidden="true"><hr class="divider no-margin" /></li>
                        <li>
                            <a href="${TwitterPageUrl}" class="twitter no-icon" target="_blank">
                                <span class="fa-stack fa-lg inline-b v-align-m fs36 mr-10" aria-hidden="true">
                                  <span class="fa fa-circle fa-stack-2x"></span>
                                  <span class="fa fa-twitter fa-stack-1x fa-inverse"></span>
                                </span>
                                <span class="v-align-m inline-b fs18">
                                    <fmt:message key="key.formatter.header.twitter" />
                                </span>
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${not empty GooglePlusPageUrl}">
                        <li aria-hidden="true"><hr class="divider no-margin" /></li>
                        <li>
                            <a href="${GooglePlusPageUrl}" class="googleplus no-icon" target="_blank">
                                <span class="fa-stack fa-lg inline-b v-align-m fs36 mr-10" aria-hidden="true">
                                  <span class="fa fa-circle fa-stack-2x"></span>
                                  <span class="fa fa-google-plus fa-stack-1x fa-inverse"></span>
                                </span>
                                <span class="v-align-m inline-b fs18">
                                    <fmt:message key="key.formatter.header.googleplus" />
                                </span>
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${not empty YouTubePageUrl}">
                        <li aria-hidden="true"><hr class="divider no-margin" /></li>
                        <li>
                            <a href="${YouTubePageUrl}" class="youtube no-icon" target="_blank">
                                <span class="fa-stack fa-lg inline-b v-align-m fs36 mr-10" aria-hidden="true">
                                  <span class="fa fa-circle fa-stack-2x"></span>
                                  <span class="fa fa-youtube-play fa-stack-1x fa-inverse"></span>
                                </span>
                                <span class="v-align-m inline-b fs18">
                                    <fmt:message key="key.formatter.header.youtube" />
                                </span>
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${not empty socialflickrpage}">
                        <li aria-hidden="true"><hr class="divider no-margin" /></li>
                        <li>
                            <a href="${socialflickrpage}" class="flickr no-icon" target="_blank">
                                <span class="fa-stack fa-lg inline-b v-align-m fs36 mr-10" aria-hidden="true">
                                  <span class="fa fa-circle fa-stack-2x"></span>
                                  <span class="fa fa-flickr fa-stack-1x fa-inverse"></span>
                                </span>
                                <span class="v-align-m inline-b fs18">
                                    <fmt:message key="key.formatter.header.flickr" />
                                </span>
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${not empty socialinstagrampage}">
                        <li aria-hidden="true"><hr class="divider no-margin" /></li>
                        <li>
                            <a href="${socialinstagrampage}" class="instagram no-icon" target="_blank">
                                <span class="fa-stack fa-lg inline-b v-align-m fs36 mr-10" aria-hidden="true">
                                  <span class="fa fa-circle fa-stack-2x"></span>
                                  <span class="fa fa-instagram fa-stack-1x fa-inverse"></span>
                                </span>
                                <span class="v-align-m inline-b fs18">
                                    <fmt:message key="key.formatter.header.instagram" />
                                </span>
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${not empty socialpinterestpage}">
                        <li aria-hidden="true"><hr class="divider no-margin" /></li>
                        <li>
                            <a href="${socialpinterestpage}" class="pinterest no-icon" target="_blank">
                                <span class="fa-stack fa-lg inline-b v-align-m fs36 mr-10" aria-hidden="true">
                                  <span class="fa fa-circle fa-stack-2x"></span>
                                  <span class="fa fa-pinterest fa-stack-1x fa-inverse"></span>
                                </span>
                                <span class="v-align-m inline-b fs18">
                                    <fmt:message key="key.formatter.header.pinterest" />
                                </span>
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${not empty socialvimeopage}">
                        <li aria-hidden="true"><hr class="divider no-margin" /></li>
                        <li>
                            <a href="${socialvimeopage}" class="vimeo no-icon" target="_blank">
                                <span class="fa-stack fa-lg inline-b v-align-m fs36 mr-10" aria-hidden="true">
                                  <span class="fa fa-circle fa-stack-2x"></span>
                                  <span class="fa fa-vimeo fa-stack-1x fa-inverse"></span>
                                </span>
                                <span class="v-align-m inline-b fs18">
                                    <fmt:message key="key.formatter.header.vimeo" />
                                </span>
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${not empty socialslidesharepage}">
                        <li aria-hidden="true"><hr class="divider no-margin" /></li>
                        <li>
                            <a href="${socialslidesharepage}" class="slideshare no-icon" target="_blank">
                                <span class="fa-stack fa-lg inline-b v-align-m fs36 mr-10" aria-hidden="true">
                                  <span class="fa fa-circle fa-stack-2x"></span>
                                  <span class="fa fa-slideshare fa-stack-1x fa-inverse"></span>
                                </span>
                                <span class="v-align-m inline-b fs18">
                                    <fmt:message key="key.formatter.header.slideshare" />
                                </span>
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${not empty rsspage}">
                        <li aria-hidden="true"><hr class="divider no-margin" /></li>
                        <li>
                            <a href="${rsspage}" class="rss no-icon">
                                <span class="fa-stack fa-lg inline-b v-align-m fs36 mr-10" aria-hidden="true">
                                  <span class="fa fa-circle fa-stack-2x"></span>
                                  <span class="fa fa-rss fa-stack-1x fa-inverse"></span>
                                </span>
                                <span class="v-align-m inline-b fs18">
                                    <fmt:message key="key.formatter.header.rss" />
                                </span>
                            </a>
                        </li>
                    </c:if>
                    <li aria-hidden="true"><hr class="divider no-margin" /></li>
                </ul>
            </div>
        </div>
    </article>
</cms:bundle>
