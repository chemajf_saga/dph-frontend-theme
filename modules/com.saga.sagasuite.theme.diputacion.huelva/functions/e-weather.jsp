<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>


<fmt:setLocale value="${cms.locale}"/>
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
    <c:set var="showweather">${themeConfiguration.CustomFieldKeyValue.showweather}</c:set>
    <c:if test="${showweather == 'true'}">
        <c:set var="urlweather">${themeConfiguration.CustomFieldKeyValue.urlweather}</c:set>
        <c:if test="${empty urlweather}">
            <c:set var="urlweather">http://www.eltiempo24.es/Sevilla-ES0SE0093.html</c:set>
        </c:if>
    </c:if>
    <c:set var="endurlweather">${fn:substringAfter(urlweather, '-ES')}</c:set>
    <c:set var="idweather">${fn:substringBefore(endurlweather, '.html')}</c:set>
<c:choose>
    <c:when test="${cms.element.inMemoryOnly}">
        <header><h1 class="title">Debe recargar la página para que se cargue el contenido</h1></header>
    </c:when>

    <c:otherwise>
<div id="weatherbox">
    <%-- para resoluciones > 768px --%>
    <div id="tiempo_w990" class="w100 hidden-xxs hidden-xs">
        <div></div>
        <div>

        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                var weatherwrapperwidthlg = $('#tiempo_w990').width();
                var sclg = document.createElement("script");
                sclg.type = "text/javascript";
                sclg.src = "//www.tiempo.es/widload/es/hor/" + weatherwrapperwidthlg + "/162/110/es${idweather}/w990.js";
                // Use any selector
                $("#tiempo_w990").append(sclg);
            });
        </script>
    </div>
    <%-- para resoluciones < 768px --%>
    <div id="tiempo_w768" class="w100 hidden-sm hidden-md hidden-lg">
        <div></div>
        <div>

        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                var weatherwrapperwidthxs = $('#tiempo_w768').width();
                var scxs = document.createElement("script");
                scxs.type = "text/javascript";
                scxs.src = "//www.tiempo.es/widload/es/ver/" + weatherwrapperwidthxs + "/343/110/es${idweather}/w768.js";
                // Use any selector
                $("#tiempo_w768").append(scxs);
            });
        </script>
    </div>
</div>
</c:otherwise>
</c:choose>
</cms:bundle>
