<%@ page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%//System.out.println("-Inicio f-corporacion-detalle.jsp");%>
<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
	<cms:include
		file="%(link.strong:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/functions/corporacion-db-detalle.jsp)">
		<cms:param name="diputado" value="${param.iddiputado}" />
	</cms:include>
	<article class="articulo element parent dph-servicio dph-servicio-corporacion-detalle fade" id="${param.titleid}">
		<button type="button" class="close corporacion-detalle-close" data-target="ul-${param.targetcontainer}"
			aria-label="Close">Volver a la lista</button>
		<div class="wrapper">
			<header class="headline">
				<h1 class="title">${param.title}</h1>
			</header>
			<div class="contentblock">
				<div class="contentblock-texto row">
					<div class="media-object col-xs-4 col-xxs-12 mb-20">
						<%-- <cms:img src="${param.imagepathbig}" alt="${param.title}" width="480" cssclass="img-responsive" /> --%>
						<img src="http://w2.diphuelva.es/portalweb/diputados/Imagenes/${img}" alt="${param.title}" width="480"
							class="img-responsive" />
						<!-- <ul class="text-center list-inline dph-servicio-corporacion-detalle-redes">
							<li class="no-padding">
								<a href="https://www.facebook.com/IgnacioCaraballoHU"
									class="disp-block hastooltip no-icon bg-icon-facebook" target="_blank"
									title="Ir a la pÃ¡gina de Facebook de Ignacio Caraballo Romero">
								<span class="fa fa-facebook" aria-hidden="true"></span>
								</a>
							</li>
							<li class="no-padding">
								<a href="https://twitter.com/@_Caraballo_"
									class="disp-block hastooltip no-icon bg-icon-twitter" target="_blank"
									title="Ir al Twitter de Ignacio Caraballo Romero">
									<span class="fa fa-twitter" aria-hidden="true"></span>
								</a>
							</li>
						</ul> -->
					</div>
					<div class="col-xs-8 col-xxs-12">
						<c:out value="${dt}" escapeXml="false" />
					</div>
				</div>
			</div>
		</div>
	</article>
</cms:bundle>
<%//System.out.println("-FIN f-corporacion-detalle.jsp");%>