<%@ page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
    //System.out.println("-Inicio f-corporacion.jsp");
%>
<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
	<div class="element parent dph-servicio dph-servicio-corporacion" id="corporacionmaincontainer">
		<div class="sg-tabs sg-tabs-estandard">
			<div class="tab-v1" role="tabpanel">
				<%
				    //System.out.println("---Inicio f-corporacion.jsp");
					List<Map<String, String>> res = new LinkedList<Map<String, String>>();
					String sql = "SELECT siglas_partido FROM public.v_diputados GROUP BY siglas_partido ORDER BY siglas_partido";
					Connection conexion = null;
					Statement sentencia = null;
					ResultSet rs = null;

					try {
						Class.forName("org.postgresql.Driver");
						//System.out.println("Puente JDBC-ODBC cargado (f-corporacion.jsp)!");
					} catch (Exception e) {
						//System.out.println("ERROR (f-corporacion.jsp): No se pudo cargar el puente JDBC-ODBC.");
						e.printStackTrace();
						return;
					}

					try {
					    /*DESA*/conexion = DriverManager.getConnection("jdbc:postgresql://192.168.131.30:5432/portalweb", "ocmsdph", "ocmsdph");
					    //PROD  conexion = DriverManager.getConnection("jdbc:postgresql://192.168.136.33:5432/portalweb", "ocmsdph", "ocmsdph");
						//System.out.println("Conexion realizada (f-corporacion.jsp)");
					} catch (Exception er) {
						//System.out.println("ERROR obteniendo conexion (f-corporacion.jsp): " + er);
						er.printStackTrace();
						return;
					}

					try {
						sentencia = conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
						//System.out.println("Sentencia creada (f-corporacion.jsp)");
					} catch (Exception er) {
						//System.out.println("ERROR obteniendo sentencia (f-corporacion.jsp): " + er);
						er.printStackTrace();
						return;
					}
					int i = 0;
					try {
						//System.out.println("Realizamos la consulta (f-corporacion.jsp): " + sql);
						rs = sentencia.executeQuery(sql);
						while (rs.next()) {
							Map<String, String> aux = new HashMap<String, String>();
							aux.put("siglas_partido", rs.getString("siglas_partido"));
							res.add(i, aux);
							i++;
						}
					} catch (Exception er) {
						//System.out.println("ERROR realizando consulta (f-corporacion.jsp): " + er);
						er.printStackTrace();
						return;
					}
					if (res.size() > 0) {
						pageContext.setAttribute("partidos", res);
					}

					conexion.close();
				%>
				<ul class="nav nav-tabs nav-justified" role="tablist">
					<c:forEach items="${partidos}" var="map" varStatus="indexPartidos">
						<c:forEach items="${map}" var="entry">
							<c:choose>
								<c:when test="${entry.key eq 'siglas_partido'}">
									<c:set var="siglas" value="${entry.value}" />
									<c:set var="siglas_low" value="${fn:toLowerCase(siglas)}" />
									<c:set var="siglas_low_r" value="${fn:replace(siglas_low, ' ', '-')}" />
									<!--*${siglas}*${siglas_low}*${siglas_low_r}*<br/>-->
									<c:set var="act" value="" />
									<c:if test="${indexPartidos.count == 1 }">
										<c:set var="act" value="active" />
									</c:if>
								</c:when>
							</c:choose>
						</c:forEach>
						<li class="${act}" role="presentation">
							<a class="hastooltip" href="#${siglas_low_r}" role="tab" data-toggle="tab" id="tab-${siglas_low}" title="${siglas}">
							<cms:img src="/system/modules/com.saga.sagasuite.theme.diputacion.huelva/skins/skin-default/images/${siglas_low_r}.png"
								cssclass="v-align-m inline-b" alt="${siglas}" height="60" /><span class="sr-only">${siglas}</span>
							</a>
						</li>
					</c:forEach>
				</ul>
				<div class="tab-content">
					<c:forEach items="${partidos}" var="map" varStatus="indexPartidos">
						<c:forEach items="${map}" var="entry">
							<c:choose>
								<c:when test="${entry.key eq 'siglas_partido'}">
									<c:set var="siglas" value="${entry.value}" />
									<c:set var="siglas_low" value="${fn:toLowerCase(siglas)}" />
									<c:set var="siglas_low_r" value="${fn:replace(siglas_low, ' ', '-')}" />
									<c:set var="act" value="" />
									<c:if test="${indexPartidos.count == 1 }">
										<c:set var="act" value="active in" />
									</c:if>
								</c:when>
							</c:choose>
						</c:forEach>
						<div id="${siglas_low_r}" role="tabpanel" class="tab-pane fade ${act}">
							<cms:include
								file="%(link.strong:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/e-corporacion-tab-content.jsp:9f5849d3-546a-11e8-a09e-63f9a58dae09)">
								<cms:param name="idTab" value="${siglas_low_r}" />
								<cms:param name="tabTitle">${siglas}</cms:param>
								<cms:param name="count">13</cms:param>
							</cms:include>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
		<script>

			// Evento onclick para el botón cargar contenido multimedia en la caja destacada
			$(document).ready(function () {
				$('.data-corporacion-loader').on('click', execute);
				// cuando cambiamos de tab y el usuario no ha cerrado la vista de detalle previamente, lo hacemos automáticamente
				$('.dph-servicio-corporacion a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
					console.log(e.relatedTarget);
					var previousActiveTab = $(e.relatedTarget).attr('href'); // previous active tab
					$(previousActiveTab+' .corporacion-detalle-close').click();
				});
				var hashToLoad = document.location.hash;
				var hashToLoad = hashToLoad.replace('#','');
				console.log('hash to load: '+hashToLoad);
				var $linkToLoad = $('[data-titleid="'+hashToLoad+'"]');
				if($linkToLoad.length){
					console.log('link to load: ',$linkToLoad);
					var idTabToSHow = $linkToLoad.parents('.tab-pane').attr('id');
					$('[href="#'+ idTabToSHow +'"]').tab('show');
					$linkToLoad.click();
				}
			});

			// ejecuta carga por AJAX
			function execute(e){
				e.preventDefault();
				try {
					var $btn = $(e.target);
					var ctxt = {btn: $btn};
					load(ctxt);

					loading(ctxt);

					var params = {};
					params.targetcontainer = ctxt.targetcontainer;
					params.title = ctxt.title;
					params.titleid = ctxt.titleid;
					params.imagepathbig = ctxt.imagepathbig;
					params.iddiputado = ctxt.iddiputado;
					if(history.pushState) {
						history.pushState(null, null, '#'+ctxt.titleid);
					}
					else {
						location.hash = "#" + ctxt.titleid;
					}
					$.post(ctxt.url, params)
							.done(function (data) {
								if (data.trim() === "") {
									$('#ul-' + ctxt.targetcontainer).removeClass('in');
									$('#ul-' + ctxt.targetcontainer).css('display','none');
									$('#' + ctxt.targetcontainer).append('No hay nada que mostrar').unbind('click');
								} else {
									$('#ul-' + ctxt.targetcontainer).removeClass('in');
									$('#ul-' + ctxt.targetcontainer).css('display','none');
									$('#' + ctxt.targetcontainer).append(data);
									$('#' + ctxt.targetcontainer +' .articulo').addClass('in');
									$('.corporacion-detalle-close').on('click', function(e){
										e.preventDefault();
										var $btnclose = $(e.target);
										var $articleClose = $btnclose.parent();
										var ulShow = '#'+$btnclose.data('target');
										$(ulShow).css('display','block');
										$(ulShow).addClass('in');
										$articleClose.remove();
									});
								}
							})
							.fail(function (err) {
								console.error("ERROR execute", ctxt, err);
							})
							.always(function () {
								update(ctxt);
							})
				} catch (err) {
					console.error(err);
				}
			}

			function load(ctxt) {
				var $btn = ctxt.btn;
				var datas = $btn.data();
				ctxt.targetcontainer = datas.targetcontainer;
				ctxt.title = datas.title;
				ctxt.titleid = datas.titleid;
				ctxt.imagepathbig = datas.imagepathbig;
				ctxt.iddiputado = datas.iddiputado;
				ctxt.url = datas.url;
				ctxt.loading = datas.loading;
			}

			function loading(ctxt) {
				$('#' + ctxt.targetcontainer).css('visibility','hidden');
				$('#' + ctxt.loading).css('visibility','visible');
			}

			function update(ctxt) {
				var btn = $('.' + ctxt.class);
				$('#' + ctxt.loading).css('visibility','hidden');
				$('#' + ctxt.targetcontainer).css('visibility','visible');
			}

		</script>
	</div>
</cms:bundle>
<%//System.out.println("-FIN f-corporacion.jsp");%>