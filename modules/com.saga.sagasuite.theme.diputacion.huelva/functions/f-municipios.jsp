<%@page import="org.opencms.main.CmsLog"%>
<%@page import="org.apache.commons.logging.Log"%>
<%@page import="java.nio.charset.Charset"%>
<%@page import="java.io.Reader"%>
<%@page import="org.apache.solr.internal.csv.CSVParser"%>
<%@page import="org.opencms.file.CmsObject"%>
<%@page import="org.opencms.workplace.CmsWorkplaceAction"%>
<%@	page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ page import="java.sql.*"%>
<%@ page import="org.apache.commons.lang3.StringUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	Log LOG = CmsLog.getLog("com.saga.sagasuite.theme.diputacion.huelva.functions.f-municipios.jsp");
    LOG.debug("---Inicio f-municipios.jsp");

	String idMunicipio = request.getParameter("idMuni") != null ? request.getParameter("idMuni") : "NULL";
	LOG.debug("idMunicipio: [" + idMunicipio + "]");
%>
<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
		<%-- En el atributo municipio extá la información del municipio --%>
		<article class="articulo element parent sg-tabs sg-tabs-estandard dph-servicio dph-servicio-municipios" id="municipiosmaincontainer">
<%
	// Objetos para la consulta
	Connection conexion = null;
	Statement sentencia = null;
	ResultSet rs = null;
	String sql = "";
	
	try {
		Class.forName("org.postgresql.Driver");
		LOG.debug("Puente JDBC-ODBC cargado (f-municipios.jsp)!");
	} catch (Exception e) {
		LOG.error("ERROR (f-municipios.jsp): No se pudo cargar el puente JDBC-ODBC.");
		return;
	}

	try {
	    // DESA conexion = DriverManager.getConnection("jdbc:postgresql://192.168.131.30:5432/portalweb", "ocmsdph", "ocmsdph");
	    /*PROD*/conexion = DriverManager.getConnection("jdbc:postgresql://192.168.136.33:5432/portalweb", "ocmsdph", "ocmsdph");
		LOG.debug("Conexion realizada (f-municipios.jsp)");
	} catch (Exception er) {
		LOG.error("ERROR obteniendo conexion (f-municipios.jsp): " + er);
		er.printStackTrace();
	}

	try {
		sentencia = conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		LOG.debug("Sentencia creada (f-municipios.jsp)");
	} catch (Exception er) {
		LOG.error("ERROR obteniendo sentencia (f-municipios.jsp): " + er);
		er.printStackTrace();
	}
	if(!idMunicipio.equalsIgnoreCase("NULL")){
		//Si tenemos el id de municipio mostramos el detalle
	    
		CmsObject cmsObjectAdmin = CmsWorkplaceAction.getInstance().getCmsAdminObject();

		Municipio municipio = new Municipio();
		if (StringUtils.isNotEmpty(idMunicipio)) {
			sql = "SELECT  p1.nom_poblacion, lista_de(p2.nom_poblacion) as nucleos, p1.bandera,  p1.escudo,  "
					+ "p1.hombres,  p1.mujeres,  p1.habitantes,  p1.hombres_agrupados,  p1.mujeres_agrupadas,  "
					+ "p1.habitantes_agrupados,  p1.fecha_padron,  p1.latitud,  p1.longitud,  p1.nivel_zoom,  "
					+ "p1.limites,  p1.distancia_capital,  p1.contenido as idGaleria,  p1.corporacion_municipal,  "
					+ "p1.enlaces_interes,  p1.texto,  p1.ayto_direccion,  p1.ayto_cpostal,  p1.ayto_telefono,  "
					+ "p1.ayto_fax,  p1.ayto_email,  p1.ayto_web, SUBSTR(p1.codigo_ine::TEXT,0,6) as codigo_ine, "
					+ "p1.color_fondo, p1.color_texto, p1.alineacion FROM v_poblaciones p1 "
					+ "LEFT OUTER JOIN v_poblaciones p2 ON p1.poblacion = p2.poblacion_sup WHERE p1.poblacion = '"
					+ idMunicipio
					+ "' GROUP BY 1,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30";

			try {
				LOG.debug("Realizamos la consulta (f-municipios.jsp): " + sql);
				rs = sentencia.executeQuery(sql);
				while (rs.next()) {

					municipio.setNom_poblacion(rs.getString("nom_poblacion"));
					municipio.setNucleos(rs.getString("nucleos"));
					municipio.setBandera(rs.getString("bandera"));
					municipio.setEscudo(rs.getString("escudo"));
					municipio.setHombres(rs.getString("hombres"));
					municipio.setMujeres(rs.getString("mujeres"));
					municipio.setHabitantes(rs.getString("habitantes"));
					municipio.setHombres_agrupados(rs.getString("hombres_agrupados"));
					municipio.setMujeres_agrupadas(rs.getString("mujeres_agrupadas"));
					municipio.setHabitantes_agrupados(rs.getString("habitantes_agrupados"));
					municipio.setFecha_padron(rs.getString("fecha_padron"));
					municipio.setLatitud(rs.getString("latitud"));
					municipio.setLongitud(rs.getString("longitud"));
					municipio.setNivel_zoom(rs.getString("nivel_zoom"));
					municipio.setLimites(rs.getString("limites"));
					municipio.setDistancia_capital(rs.getString("distancia_capital"));
					municipio.setIdGaleria(rs.getString("idGaleria"));
					municipio.setCorporacion_municipal(rs.getString("corporacion_municipal"));
					municipio.setEnlaces_interes(rs.getString("enlaces_interes"));
					municipio.setTexto(rs.getString("texto"));
					municipio.setAyto_direccion(rs.getString("ayto_direccion"));
					municipio.setAyto_cpostal(rs.getString("ayto_cpostal"));
					municipio.setAyto_telefono(rs.getString("ayto_telefono"));
					municipio.setAyto_fax(rs.getString("ayto_fax"));
					municipio.setAyto_email(rs.getString("ayto_email"));
					municipio.setAyto_web(rs.getString("ayto_web"));
					municipio.setCodigo_ine(rs.getString("codigo_ine"));
					municipio.setColor_fondo(rs.getString("color_fondo"));
					municipio.setColor_texto(rs.getString("color_texto"));
					municipio.setAlineacion(rs.getString("alineacion"));

				}
			} catch (Exception er) {
				LOG.error("ERROR realizando consulta (f-municipios.jsp): " + er);
				er.printStackTrace();
				return;
			}
			//LOG.debug("Objeto MUNICIPIO (f-municipios.jsp):" + municipio);
			
			// Si todo ha ido bien leemos el CSV del municipio
			final String csvStr = new String(cmsObjectAdmin.readFile("/shared/CSV/datosMuni2.csv").getContents(), "Cp1252");
			String[] ar = csvStr.split("\r\n");
			String[] muniInfo = new String[0];
			String[] muniInfoCabecera = ar[0].split(";");
			boolean tenemosCsv = false;
			for (int i = 1; i < ar.length; i++) {
				String[] arL = ar[i].split(";");
				// El segundo elemento es el codigo INE
				if (arL[1].equalsIgnoreCase(municipio.getCodigo_ine())) {
					LOG.debug("El Municipio es: " + arL[2]);
					muniInfo = arL;
					tenemosCsv = true;
					i = ar.length;
				}
			}
			//En el String[] muniInfo tenemos la linea con la información del municipio
			pageContext.setAttribute("tenemosCsv", tenemosCsv);
			pageContext.setAttribute("muniInfo", muniInfo);
			pageContext.setAttribute("muniInfoCabecera", muniInfoCabecera);
		    conexion.close();
		}
		//pageContext.setAttribute("municipio", municipio);
		pageContext.setAttribute("nombre", municipio.getNom_poblacion());
		pageContext.setAttribute("escudo", municipio.getEscudo());
		pageContext.setAttribute("bandera", municipio.getBandera());
		pageContext.setAttribute("texto", municipio.getTexto());
		pageContext.setAttribute("corporacionMunicipal", municipio.getCorporacion_municipal());
		pageContext.setAttribute("enlacesInteres", municipio.getEnlaces_interes());
		pageContext.setAttribute("direccion", municipio.getAyto_direccion());
		pageContext.setAttribute("cPostal", municipio.getAyto_cpostal());
		pageContext.setAttribute("telef", municipio.getAyto_telefono());
		pageContext.setAttribute("fax", municipio.getAyto_fax());
		pageContext.setAttribute("email", municipio.getAyto_email());
		pageContext.setAttribute("web", municipio.getAyto_web());
		pageContext.setAttribute("estadistica", "");
		pageContext.setAttribute("lat", municipio.getLatitud());
		pageContext.setAttribute("lng", municipio.getLongitud());
		pageContext.setAttribute("zoom", municipio.getNivel_zoom());
		pageContext.setAttribute("limites", municipio.getLimites());
	%>
			<header class="headline clearfix">
				<h1 class="title pull-left">
					<c:out value="${nombre}" escapeXml="false" />
				</h1>
				<div class="pull-right text-right">
					<img src="http://w2.diphuelva.es/portalweb/municipios/Imagenes/<c:out value="${escudo}" escapeXml="false" />"
						height="60" />
					<img
						src="http://w2.diphuelva.es/portalweb/municipios/Imagenes/<c:out value="${bandera}" escapeXml="false" />"
						height="60" class="ml-15" style="border: solid 1px black;" />
				</div>
			</header>
			<div class="row tab-v1">
				<div class="col-xxs-12">
					<ul class="nav nav-tabs nav-justified mb-50" role="tablist">
						<c:if test="${not empty texto}">
							<li class="active" role="presentation"><a href="#general" role="tab" data-toggle="tab" id="tab-general">Datos Generales</a></li>
						</c:if>
						<c:if test="${not empty tenemosCsv}">
							<li role="presentation"><a href="#estadistica" role="tab" data-toggle="tab" id="tab-estadistica">Datos Estadísticos</a></li>
						</c:if>
						<c:if test="${not empty corporacionMunicipal}">
							<li role="presentation"><a href="#corporacion" role="tab" data-toggle="tab" id="tab-corporacion">Corporación Municipal</a></li>
						</c:if>
						<c:if test="${not empty enlacesInteres}">
							<li role="presentation"><a href="#enlaces" role="tab" data-toggle="tab" id="tab-enlaces">Enlaces de Interés</a></li>
						</c:if>
						<c:if test="${not empty direccion || not empty cPostal || not empty telef || not empty fax || not empty email || not empty web}">
							<li role="presentation"><a href="#contacto" role="tab" data-toggle="tab" id="tab-contacto">Datos de Contacto</a></li>
						</c:if>
					</ul>
				</div>
				<div class="col-sm-4 col-xxs-12">
					<div id="muniMap"></div>
				</div>
				<div class="col-sm-8 col-xxs-12" role="tabpanel">
					<div class="tab-content">
						<c:if test="${not empty texto}">
							<div id="general" role="tabpanel" class="tab-pane fade active in">
								<c:out value="${texto}" escapeXml="false" />
							</div>
						</c:if>
						<c:if test="${not empty tenemosCsv}">
							<div id="estadistica" role="tabpanel" class="tab-pane fade">
								<h3 style="font-family:arial;font-size:15pt;">Municipio</h3>
								<p>${muniInfoCabecera[3]}: ${muniInfo[3]} km²</p>
								<p>${muniInfoCabecera[4]}: ${muniInfo[4]} m</p>
								<p>${muniInfoCabecera[5]}: ${muniInfo[5]}</p>
								<h3 style="font-family:arial;font-size:15pt;">Población</h3>
								<p>${muniInfoCabecera[6]}: ${muniInfo[6]} hab.</p>
								<p>${muniInfoCabecera[13]}: ${muniInfo[13]} %.</p>
								<p>${muniInfoCabecera[14]}: ${muniInfo[14]} hab.</p>
								<p>
									<span>
										<fmt:parseNumber var = "menores20" type="number" value = "${muniInfo[11]}" />
										<fmt:parseNumber var = "mayores65" type="number" value = "${muniInfo[12]}" />
										<c:set var="pob_20_65" value="${100 - menores20 - mayores65}" />
										<img src="http://w2.diphuelva.es/comun/imagenes/graficos/graficoPoblacion.php?datosPob=${muniInfo[6]}@${muniInfo[7]}@${muniInfo[8]}@${muniInfo[9]}@${muniInfo[10]}@${muniInfo[11]}@${muniInfo[12]}@${pob_20_65}" alt="Gráfico de Población" border="0"/>
									</span>
								</p>
								<h3 style="font-family:arial;font-size:15pt;">Sociedad</h3>
								<p>${muniInfoCabecera[23]}: ${muniInfo[23]}</p>
								<p>${muniInfoCabecera[24]}: ${muniInfo[24]}</p>
								<p>${muniInfoCabecera[25]}: ${muniInfo[25]}</p>
								<c:if test="${muniInfo[22] + muniInfo[32] + muniInfo[33] + muniInfo[34] + muniInfo[35] > 0}">
									<p>
										<span>
											<img src="http://w2.diphuelva.es/comun/imagenes/graficos/graficoSociedad.php?datosSoc=${muniInfo[22]}@${muniInfo[32]}@${muniInfo[33]}@${muniInfo[34]}@${muniInfo[35]}" alt="Gráfico de Sociedad" border="0"/>
										</span>
									</p>
								</c:if>
								<c:if test="${muniInfo[22] + muniInfo[32] + muniInfo[33] + muniInfo[34] + muniInfo[35] == 0}">
									<p>Centros Educativos: 0</p>
								</c:if>
								<h3 style="font-family:arial;font-size:15pt;">Economía</h3>
								<p>${muniInfoCabecera[38]}: ${muniInfo[38]}</p>
								<p>${muniInfoCabecera[43]}: ${muniInfo[43]}</p>
								<p>
									<span>
										<img src="http://w2.diphuelva.es/comun/imagenes/graficos/graficoEconomia.php?datosEco=${muniInfo[38]}@${muniInfo[39]}@${muniInfo[40]}@${muniInfo[41]}@${muniInfo[42]}@${muniInfo[43]}@${muniInfo[44]}@${muniInfo[45]}@${muniInfo[46]}@${muniInfo[47]}" alt="Gráfico de Economía" border="0"/>
									</span>
								</p>
								<p>
									Datos obtenidos y más información en:<br />
									<a href="http://www.juntadeandalucia.es/institutodeestadisticaycartografia/sima/htm/sm${muniInfo[1]}.htm" target="_blank">Junta de Andalucía, Consejería de Economía, Innovación, Ciencia y Empleo.</a>
								</p>
							</div>
						</c:if>
						<c:if test="${not empty corporacionMunicipal}">
							<div id="corporacion" role="tabpanel" class="tab-pane fade">
								<c:out value="${corporacionMunicipal}" escapeXml="false" />
							</div>
						</c:if>
						<c:if test="${not empty enlacesInteres}">
							<div id="enlaces" role="tabpanel" class="tab-pane fade">
								<c:out value="${enlacesInteres}" escapeXml="false" />
							</div>
						</c:if>
						<c:if test="${not empty direccion || not empty cPostal || not empty telef || not empty fax || not empty email || not empty web}">
							<div id="contacto" role="tabpanel" class="tab-pane fade">
								<ul>
									<c:if test="${not empty direccion}">
										<li>Dirección: <c:out value="${direccion}" escapeXml="false" /></li>
									</c:if>
									<c:if test="${not empty cPostal}">
										<li>Código Postal: <c:out value="${cPostal}" escapeXml="false" /></li>
									</c:if>
									<c:if test="${not empty telef}">
										<li>Teléfono: <c:out value="${telef}" escapeXml="false" /></li>
									</c:if>
									<c:if test="${not empty fax}">
										<li>Fax: <c:out value="${fax}" escapeXml="false" /></li>
									</c:if>
									<c:if test="${not empty email}">
										<li>EMail: <c:out value="${email}" escapeXml="false" /></li>
									</c:if>
									<c:if test="${not empty web}">
										<li>Web: <a href="http://<c:out value="${web}" escapeXml="false" />" target="_blanck"><c:out
													value="${web}" escapeXml="false" /></a></li>
									</c:if>
								</ul>
							</div>
						</c:if>
					</div>
				</div>
			</div>
		
		<script>
			var map;
			function initMap() {
				console.log('Iniciando mapa: lat(${lat}) lng(${lng}) KML(${nombre}.kml) zoom: (${zoom})');
				
				var muni = new google.maps.LatLng(${lat}, ${lng});
				var map = new google.maps.Map(document.getElementById('muniMap'), {
					center: muni,
					mapTypeId: google.maps.MapTypeId.HYBRID,
					mapTypeControl: true,
				    mapTypeControlOptions: {
				        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
				        position: google.maps.ControlPosition.TOP_LEFT
				    },
				    zoomControl: true,
				    zoomControlOptions: {
				        position: google.maps.ControlPosition.RIGHT_BOTTOM
				    },
				    scaleControl: true,
				    
				    streetViewControl: true,
				    streetViewControlOptions: {
				        position: google.maps.ControlPosition.LEFT_BOTTOM
				    },
				    fullscreenControl: true,
					zoom: ${zoom}
				});
				
				var coordInfoWindow = new google.maps.InfoWindow();
				coordInfoWindow.setContent('<b>${nombre}</b>');
				coordInfoWindow.setPosition(muni);
				coordInfoWindow.open(map);
				
				var kml		= new google.maps.KmlLayer("http://m.diphuelva.es/kml/${limites}",{preserveViewport:true});
				kml.setMap(map);
				
				console.log('Mapa iniciado');
			}
		</script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAyy5eN3odXrWIdTDMahgtb7FgNjLc8I4A&callback=initMap&language=es&region=ES" async defer></script>
		<style type="text/css">
			/* Always set the map height explicitly to define the size of the div element that contains the map. */
			#muniMap {
				height: 30em;
			}
		</style>
<%
	}else{
	    //Mostramos el listado de municipios
	    sql = 
             "SELECT to_number(poblacion,'999999') as codpoblacion, "
              + "nom_poblacion, "
              + "1 as anchura, "
              + "1 as altura, "
              + "f3.path as fPath, "
              + "f3.fichero as fFichero, "
              + "escudo AS fichero_teselapropia "
              + "FROM v_poblaciones p "
              + "LEFT OUTER JOIN fichero f3 "
              + "ON p.idfichero = f3.idfichero "
              + "WHERE p.provincia = 21 AND "
              + "p.tipo <> 'N' "
              + "order by nom_poblacion "
              ;
	    try {
			LOG.debug("Realizamos la consulta (f-municipios.jsp): " + sql);
			rs = sentencia.executeQuery(sql);
%>
<div class="element parent dph-servicio dph-servicio-corporacion">
	<div class="sg-tabs sg-tabs-estandard">
		<div class="tab-v1" role="tabpanel">
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane fade active in">
					<div class="h3 no-margin mb-30 mt-10 text-center">Listado de Municipios</div>
					<ul class="list-unstyled main-ul row gutter-sm fade in" style="display: flex; flex-wrap: wrap;">
<%
			while (rs.next()) {
			    String codpoblacion = rs.getString("codpoblacion");
			    String nom_poblacion = rs.getString("nom_poblacion");
			    //String anchura = rs.getString("anchura");
			    //String altura = rs.getString("altura");
			    //String fPath = rs.getString("fPath");
			    String fFichero = rs.getString("fFichero");
			    //String fichero_teselapropia = rs.getString("fichero_teselapropia");

			    LOG.debug("-------------------------------------------------------");
			    LOG.debug("codpoblacion ::" + codpoblacion + "::");
			    LOG.debug("nom_poblacion ::" + nom_poblacion + "::");
			    //LOG.debug("anchura ::" + anchura + "::");
			    //LOG.debug("altura ::" + altura + "::");
			    //LOG.debug("fPath ::" + fPath + "::");
			    LOG.debug("fFichero ::" + fFichero + "::");
			    //LOG.debug("fichero_teselapropia ::" + fichero_teselapropia + "::");
			    
			    pageContext.setAttribute("nom_poblacion", nom_poblacion);
			    pageContext.setAttribute("fFichero", fFichero);
			    pageContext.setAttribute("codpoblacion", codpoblacion);
			    
			    if(StringUtils.isNotEmpty(fFichero)){
			        pageContext.setAttribute("fichero", fFichero.replaceAll("ñ", "").replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("á", "").replaceAll("é", "").replaceAll("í", "").replaceAll("ó", "").replaceAll("ú", "") );
			    	//out.println("<p><a href='/servicios/municipios?idMuni="+codpoblacion+"'>"+nom_poblacion+"<br><img src='http://w2.diphuelva.es/portalweb/municipios/Imagenes/" + fFichero +"' /></a></p>");
%>
					<li class="corporacion-item col-xxs-6 col-xs-4 col-sm-3 col-lg-2 mb-30">
						<div class="corporacion-item-wrapper box">
							<div class="element complete-mode with-media overlayed">
								<a href="/servicios/municipios?idMuni=${codpoblacion}" title="Ver detalle de ${nom_poblacion}" class="data-corporacion-loader overlayer" >
									<span class="hover-overlay" aria-hidden="true"/>
									<span class="sr-only">Ver detalle de ${nom_poblacion}</span>
								</a>
								<div class="img-wrapper overlayed">
									<div class="img-wrapper-scale">
										<cms:img src="/.galleries/img-listado-municipios/${fichero}" alt="${nom_poblacion}" width="480" cssclass="img-responsive" />
										<%-- <img src="http://w2.diphuelva.es/portalweb/municipios/Imagenes/${fFichero}" alt="${nom_poblacion}" class="img-responsive" width="480" height="180"/> --%>
									</div>
								</div>
								<div class="text-overlay equalheight-corporacion-text-overlay" style="height: 73px;">
									<div class="wrapper text-center">
										<h2 style="color: #FFFFFF" class="title-item regular-text no-margin no-padding">${nom_poblacion}</h2>
									</div>
								</div>
							</div>
						</div>
					</li>
<%
			    }
			}
%>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<%
		} catch (Exception er) {
			LOG.error("ERROR realizando consulta (f-municipios.jsp): " + er);
			er.printStackTrace();
			return;
		}
	}
%>
</article>
</cms:bundle>
<%
    LOG.debug("---Fin f-municipios.jsp");
%>
<%!public class Municipio {
        private String nom_poblacion;
        private String nucleos;
        private String bandera;
        private String escudo;
        private String hombres;
        private String mujeres;
        private String habitantes;
        private String hombres_agrupados;
        private String mujeres_agrupadas;
        private String habitantes_agrupados;
        private String fecha_padron;
        private String latitud;
        private String longitud;
        private String nivel_zoom;
        private String limites;
        private String distancia_capital;
        private String idGaleria;
        private String corporacion_municipal;
        private String enlaces_interes;
        private String texto;
        private String ayto_direccion;
        private String ayto_cpostal;
        private String ayto_telefono;
        private String ayto_fax;
        private String ayto_email;
        private String ayto_web;
        private String codigo_ine;
        private String color_fondo;
        private String color_texto;
        private String alineacion;

        public Municipio() {
            super();
        }

        public Municipio(String nom_poblacion, String nucleos, String bandera, String escudo, String hombres, String mujeres, String habitantes,
                        String hombres_agrupados, String mujeres_agrupadas, String habitantes_agrupados, String fecha_padron, String latitud,
                        String longitud, String nivel_zoom, String limites, String distancia_capital, String idGaleria, String corporacion_municipal,
                        String enlaces_interes, String texto, String ayto_direccion, String ayto_cpostal, String ayto_telefono, String ayto_fax,
                        String ayto_email, String ayto_web, String codigo_ine, String color_fondo, String color_texto, String alineacion) {
            super();
            this.nom_poblacion = nom_poblacion;
            this.nucleos = nucleos;
            this.bandera = bandera;
            this.escudo = escudo;
            this.hombres = hombres;
            this.mujeres = mujeres;
            this.habitantes = habitantes;
            this.hombres_agrupados = hombres_agrupados;
            this.mujeres_agrupadas = mujeres_agrupadas;
            this.habitantes_agrupados = habitantes_agrupados;
            this.fecha_padron = fecha_padron;
            this.latitud = latitud;
            this.longitud = longitud;
            this.nivel_zoom = nivel_zoom;
            this.limites = limites;
            this.distancia_capital = distancia_capital;
            this.idGaleria = idGaleria;
            this.corporacion_municipal = corporacion_municipal;
            this.enlaces_interes = enlaces_interes;
            this.texto = texto;
            this.ayto_direccion = ayto_direccion;
            this.ayto_cpostal = ayto_cpostal;
            this.ayto_telefono = ayto_telefono;
            this.ayto_fax = ayto_fax;
            this.ayto_email = ayto_email;
            this.ayto_web = ayto_web;
            this.codigo_ine = codigo_ine;
            this.color_fondo = color_fondo;
            this.color_texto = color_texto;
            this.alineacion = alineacion;
        }

        protected String getNom_poblacion() {
            return nom_poblacion;
        }

        protected void setNom_poblacion(String nom_poblacion) {
            this.nom_poblacion = nom_poblacion;
        }

        protected String getNucleos() {
            return nucleos;
        }

        protected void setNucleos(String nucleos) {
            this.nucleos = nucleos;
        }

        protected String getBandera() {
            return bandera;
        }

        protected void setBandera(String bandera) {
            this.bandera = bandera;
        }

        protected String getEscudo() {
            return escudo;
        }

        protected void setEscudo(String escudo) {
            this.escudo = escudo;
        }

        protected String getHombres() {
            return hombres;
        }

        protected void setHombres(String hombres) {
            this.hombres = hombres;
        }

        protected String getMujeres() {
            return mujeres;
        }

        protected void setMujeres(String mujeres) {
            this.mujeres = mujeres;
        }

        protected String getHabitantes() {
            return habitantes;
        }

        protected void setHabitantes(String habitantes) {
            this.habitantes = habitantes;
        }

        protected String getHombres_agrupados() {
            return hombres_agrupados;
        }

        protected void setHombres_agrupados(String hombres_agrupados) {
            this.hombres_agrupados = hombres_agrupados;
        }

        protected String getMujeres_agrupadas() {
            return mujeres_agrupadas;
        }

        protected void setMujeres_agrupadas(String mujeres_agrupadas) {
            this.mujeres_agrupadas = mujeres_agrupadas;
        }

        protected String getHabitantes_agrupados() {
            return habitantes_agrupados;
        }

        protected void setHabitantes_agrupados(String habitantes_agrupados) {
            this.habitantes_agrupados = habitantes_agrupados;
        }

        protected String getFecha_padron() {
            return fecha_padron;
        }

        protected void setFecha_padron(String fecha_padron) {
            this.fecha_padron = fecha_padron;
        }

        protected String getLatitud() {
            return latitud;
        }

        protected void setLatitud(String latitud) {
            this.latitud = latitud;
        }

        protected String getLongitud() {
            return longitud;
        }

        protected void setLongitud(String longitud) {
            this.longitud = longitud;
        }

        protected String getNivel_zoom() {
            return nivel_zoom;
        }

        protected void setNivel_zoom(String nivel_zoom) {
            this.nivel_zoom = nivel_zoom;
        }

        protected String getLimites() {
            return limites;
        }

        protected void setLimites(String limites) {
            this.limites = limites;
        }

        protected String getDistancia_capital() {
            return distancia_capital;
        }

        protected void setDistancia_capital(String distancia_capital) {
            this.distancia_capital = distancia_capital;
        }

        protected String getIdGaleria() {
            return idGaleria;
        }

        protected void setIdGaleria(String idGaleria) {
            this.idGaleria = idGaleria;
        }

        protected String getCorporacion_municipal() {
            return corporacion_municipal;
        }

        protected void setCorporacion_municipal(String corporacion_municipal) {
            this.corporacion_municipal = corporacion_municipal;
        }

        protected String getEnlaces_interes() {
            return enlaces_interes;
        }

        protected void setEnlaces_interes(String enlaces_interes) {
            this.enlaces_interes = enlaces_interes;
        }

        protected String getTexto() {
            return texto;
        }

        protected void setTexto(String texto) {
            this.texto = texto;
        }

        protected String getAyto_direccion() {
            return ayto_direccion;
        }

        protected void setAyto_direccion(String ayto_direccion) {
            this.ayto_direccion = ayto_direccion;
        }

        protected String getAyto_cpostal() {
            return ayto_cpostal;
        }

        protected void setAyto_cpostal(String ayto_cpostal) {
            this.ayto_cpostal = ayto_cpostal;
        }

        protected String getAyto_telefono() {
            return ayto_telefono;
        }

        protected void setAyto_telefono(String ayto_telefono) {
            this.ayto_telefono = ayto_telefono;
        }

        protected String getAyto_fax() {
            return ayto_fax;
        }

        protected void setAyto_fax(String ayto_fax) {
            this.ayto_fax = ayto_fax;
        }

        protected String getAyto_email() {
            return ayto_email;
        }

        protected void setAyto_email(String ayto_email) {
            this.ayto_email = ayto_email;
        }

        protected String getAyto_web() {
            return ayto_web;
        }

        protected void setAyto_web(String ayto_web) {
            this.ayto_web = ayto_web;
        }

        protected String getCodigo_ine() {
            return codigo_ine;
        }

        protected void setCodigo_ine(String codigo_ine) {
            this.codigo_ine = codigo_ine;
        }

        protected String getColor_fondo() {
            return color_fondo;
        }

        protected void setColor_fondo(String color_fondo) {
            this.color_fondo = color_fondo;
        }

        protected String getColor_texto() {
            return color_texto;
        }

        protected void setColor_texto(String color_texto) {
            this.color_texto = color_texto;
        }

        protected String getAlineacion() {
            return alineacion;
        }

        protected void setAlineacion(String alineacion) {
            this.alineacion = alineacion;
        }

        public String toString() {
            return "Nom_poblacion: " + getNom_poblacion() + ", " + "Nucleos: " + getNucleos() + ", " + "Bandera: " + getBandera() + ", " + "Escudo: "
                            + getEscudo() + ", " + "Hombres: " + getHombres() + ", " + "Mujeres: " + getMujeres() + ", " + "Habitantes: "
                            + getHabitantes() + ", " + "Hombres_agrupados: " + getHombres_agrupados() + ", " + "Mujeres_agrupadas: "
                            + getMujeres_agrupadas() + ", " + "Habitantes_agrupados: " + getHabitantes_agrupados() + ", " + "Fecha_padron: "
                            + getFecha_padron() + ", " + "Latitud: " + getLatitud() + ", " + "Longitud: " + getLongitud() + ", " + "Nivel_zoom: "
                            + getNivel_zoom() + ", " + "Limites: " + getLimites() + ", " + "Distancia_capital: " + getDistancia_capital() + ", "
                            + "IdGaleria: " + getIdGaleria() + ", " + "Corporacion_municipal: " + getCorporacion_municipal() + ", "
                            + "Enlaces_interes: " + getEnlaces_interes() + ", " + "Texto: " + getTexto() + ", " + "Ayto_direccion: "
                            + getAyto_direccion() + ", " + "Ayto_cpostal: " + getAyto_cpostal() + ", " + "Ayto_telefono: " + getAyto_telefono() + ", "
                            + "Ayto_fax: " + getAyto_fax() + ", " + "Ayto_email: " + getAyto_email() + ", " + "Ayto_web: " + getAyto_web() + ", "
                            + "Codigo_ine: " + getCodigo_ine() + ", " + "Color_fondo: " + getColor_fondo() + ", " + "Color_texto: " + getColor_texto()
                            + ", " + "Alineacion: " + getAlineacion() + ".";
        }

    }%>