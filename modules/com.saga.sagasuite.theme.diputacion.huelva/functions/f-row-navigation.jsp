<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<fmt:setLocale value="${cms.locale}"/>
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
    <div>
    <c:set var="showportadamenu"></c:set>
    <c:set var="showportadamenu">
        <cms:property name="dph.portadashowmenu" file="search" />
    </c:set>
    <c:if test="${empty showportadamenu or showportadamenu == 'true'}">
        <c:if test="${not empty param.linktexts}">
            <c:set var="linktexts">${param.linktexts}</c:set>
        </c:if>
        <c:if test="${not empty param.linkanchors}">
            <c:set var="linkanchors">${param.linkanchors}</c:set>
        </c:if>
        <c:if test="${not empty cms.element.settings.settinglinktexts}">
            <c:set var="linktexts">${cms.element.settings.settinglinktexts}</c:set>
        </c:if>
        <c:if test="${not empty cms.element.settings.settinglinkanchors}">
            <c:set var="linkanchors">${cms.element.settings.settinglinkanchors}</c:set>
        </c:if>
        <c:if test="${not empty linktexts}">
            <c:set var="linktextsarray" value="${fn:split(linktexts,',' )}" />
        </c:if>
        <c:if test="${not empty linkanchors}">
            <c:set var="linkanchorsarray" value="${fn:split(linkanchors,',' )}" />
        </c:if>
        <div class="row-navigation">
            <div class="navbar parent menu-wrapper">
                <div class="wrapper">
                    <nav class="element menu menu-horizontal">
                        <ul class="nav nav-pills">
                            <c:forEach var="linkitem" items="${linktextsarray}" varStatus="itemstatus">
                                <c:set var="totalitems">${itemstatus.count}</c:set>
                            </c:forEach>
                            <c:forEach var="linktext" items="${linktextsarray}" varStatus="linkstatus">
                                <li class="items-${totalitems}">
                                    <a href="${linkanchorsarray[linkstatus.count - 1]}">${linktext}</a>
                                </li>
                            </c:forEach>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </c:if>
    </div>
</cms:bundle>
