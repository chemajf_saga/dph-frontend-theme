<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<fmt:setLocale value="${cms.locale}"/>
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
    <div class="row layout-4-col ">
        <%-- columna 1 --%>
        <div class="col-xs-12 col-sm-3">
            ${themeConfiguration.CustomFieldKeyValue.footercol1}
        </div>
        <%-- columna 2 --%>
        <div class="col-xs-12 col-sm-3">
            ${themeConfiguration.CustomFieldKeyValue.footercol2}
        </div>
        <%-- columna 3 --%>
        <div class="col-xs-12 col-sm-3">
            ${themeConfiguration.CustomFieldKeyValue.footercol3}
        </div>
        <%-- columna 4 --%>
        <div class="col-xs-12 col-sm-3">
            ${themeConfiguration.CustomFieldKeyValue.footercol4}
        </div>
    </div>
</cms:bundle>