<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" buffer="none" session="false"
	trimDirectiveWhitespaces="true"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
    //System.out.println("---Inicio municipios-listado-db.jsp");

    List<Map<String, String>> res = new LinkedList<Map<String, String>>();
    String sql = "SELECT poblacion, nom_poblacion FROM v_poblaciones_nomat WHERE provincia=21 AND tipo='M' ORDER BY tipo DESC, nom_poblacion";

    String p = request.getAttribute("p") != null ? request.getAttribute("p").toString() : "";
    String e = request.getAttribute("e") != null ? request.getAttribute("e").toString() : "";

    int pageN = 1;
    int eltos = 9999;
    try {
        pageN = Integer.parseInt(p);
        eltos = Integer.parseInt(e);
    } catch (Exception ex) {
        //System.out.println("ERROR (municipios-listado-db.jsp): Obteniendo parametros.");
    }

    Connection conexion = null;
    Statement sentencia = null;
    ResultSet rs = null;

    try {
        Class.forName("org.postgresql.Driver");
        //System.out.println("Puente JDBC-ODBC cargado (municipios-listado-db.jsp)!");
    } catch (Exception ex) {
        //System.out.println("ERROR (municipios-listado-db.jsp): No se pudo cargar el puente JDBC-ODBC.");
        ex.printStackTrace();
        return;
    }

    try {
        // DESA conexion = DriverManager.getConnection("jdbc:postgresql://192.168.131.30:5432/portalweb", "ocmsdph", "ocmsdph");
        /*PROD*/conexion = DriverManager.getConnection("jdbc:postgresql://192.168.136.33:5432/portalweb", "ocmsdph", "ocmsdph");
        //System.out.println("Conexion realizada (municipios-listado-db.jsp)");
    } catch (Exception er) {
        //System.out.println("ERROR obteniendo conexion (municipios-listado-db.jsp): " + er);
        er.printStackTrace();
        return;
    }

    try {
        sentencia = conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        //System.out.println("Sentencia creada (municipios-listado-db.jsp)");
    } catch (Exception er) {
        //System.out.println("ERROR obteniendo sentencia (municipios-listado-db.jsp): " + er);
        er.printStackTrace();
        return;
    }

    int i = 0;
    try {
        //System.out.println("Realizamos la consulta (municipios-listado-db.jsp): " + sql);
        rs = sentencia.executeQuery(sql);
        while (rs.next()) {

            Map<String, String> aux = new HashMap<String, String>();

            aux.put("poblacion", rs.getString("poblacion"));
            aux.put("nom_poblacion", rs.getString("nom_poblacion"));

            res.add(i, aux);
            i++;
        }
    } catch (Exception er) {
        //System.out.println("ERROR realizando consulta (municipios-listado-db.jsp): " + er);
        er.printStackTrace();
        return;
    }

    //System.out.println("Resltado de la consulta (municipios-listado-db.jsp): [" + i + "]");

    // Creamos el HTML
    int init = 0;
    int end = res.size() - 1;
    if (res.size() > eltos) {
        init = eltos * (pageN - 1);
        end = init + eltos - 1;
        if (end >= res.size()) {
            end = res.size() - 1;
        }
    }
    //System.out.println("Paginamos desde " + init + " hasta " + end + "");

    String html = "";
    html += "<ul class=\"nav ul-munis\">";
    int j = 0;
    for (int index = init; index <= end; index++) {
        html += "<li><a class=\"a-munis\" href=\"/servicios/municipios?idMuni=" + res.get(index).get("poblacion") + "\">"
                        + res.get(index).get("nom_poblacion") + "</a></li>";
        j++;
    }
    html += "</ul>";
    pageContext.setAttribute("html", html);

    request.setAttribute("p", null);
    request.setAttribute("e", null);
    conexion.close();
%>
<c:out value="${html}" escapeXml="false" />
<style type="text/css" >
ul.ul-munis{
	
}
ul.ul-munis li{
	
}
a.a-munis{
  padding: 4px 0px !important;
}
</style>
<%
    //System.out.println("---FIN municipios-listado-db.jsp");
%>