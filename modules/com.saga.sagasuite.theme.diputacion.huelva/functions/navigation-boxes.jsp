<%@ page import="org.opencms.file.CmsResource" %>
<%@ page import="org.apache.http.HttpRequest" %>
<%@ page import="org.opencms.jsp.util.CmsJspStandardContextBean" %>
<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%!
    private String getUriParentFolder(HttpServletRequest req) {
        String uri = CmsJspStandardContextBean.getInstance(req).getVfs().getRequestContext().getUri();
        return CmsResource.getParentFolder(uri);
    }
%>

<fmt:setLocale value="${cms.locale}"/>
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">

    <c:if test="${cms.element.setting.marginbottom.value != '0'}">
        <c:set var="marginClass">margin-bottom-${cms.element.setting.marginbottom.value}</c:set>
    </c:if>
    <c:set var="columnclass">col-sm-6 col-md-4</c:set>
    <c:if test="${not empty cms.element.settings.columnclass}">
        <c:set var="columnclass">${cms.element.setting.columnclass}</c:set>
    </c:if>
    <c:set var="startfolder"><%=getUriParentFolder(request)%></c:set>
    <c:if test="${not empty cms.element.settings.navstartfolder}">
        <c:set var="startfolder">${cms.element.setting.navstartfolder}</c:set>
    </c:if>
        <nav class="element">
            <cms:navigation type="forSite" resource="${startfolder}" startLevel="0" endLevel="0" var="nav"/>
            <ul class="row list-unstyled">
                <c:set var="counter" value="0"/>
                <c:forEach items="${nav.items}" var="elem" varStatus="status">
                    <c:set var="navdescription">
                        ${elem.description}
                    </c:set>
                    <c:set var="navtitle">
                        ${elem.navText}
                    </c:set>
                    <c:set var="imagePath">
                        ${elem.navImage}
                    </c:set>
                    <li class="mb-40 ${columnclass}">
                        <div class="element parent sg-blog sg-microcontent sg-microcontent-panel box navigation-box default-box">

                            <div class="element complete-mode with-media no-spacing-row" style="border: solid 1px #ddd">
                                <div class="row object-aside">
                                    <c:if test="${not empty imagePath}">
                                        <div class="overlayed container-fluid equalheightarticlever" style="background-image: url('<cms:link>${imagePath}</cms:link>');background-position: center center;background-color: transparent;background-size: cover;color:inherit;min-height:100px">
                                            <a class="overlayer" href="<cms:link>${elem.resourceName}</cms:link>" title="<fmt:message key="key.go.to" />${elem.navText}">
                                                <span class="sr-only">${elem.navText}</span>
                                            </a>
                                        </div>
                                    </c:if>
                                    <div class="container-fluid media no-margin equalheightarticlever">
                                        <div class="media-body-wrapper" style="padding: 30px">
                                            <header>
                                                <h2 class="title h3">
                                                    <a href="<cms:link>${elem.resourceName}</cms:link>">
                                                        ${elem.navText}
                                                    </a>
                                                </h2>
                                            </header>
                                            <c:if test="${not empty navdescription}">
                                                <div class="description">
                                                    ${navdescription}
                                                </div>
                                            </c:if>
                                            <div class="action-btn clearfix text-right">
                                                <a class="btn btn-specific-main btn-block" href="<cms:link>${elem.resourceName}</cms:link>" title="<fmt:message key="key.go.to" />${elem.navText}">
                                                    <span aria-hidden="true" class="fa fa-plus"></span>
                                                    <fmt:message key="key.know.more" />
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </li>

                    <script type="text/javascript">
                        (function(){
                            jQuery.fn.equalHeights = function() {
                                var maxHeight = 0;
                                // get the maximum height
                                this.each(function(){
                                    var $this = jQuery(this);
                                    if ($this.height() > maxHeight) { maxHeight = $this.height(); }
                                });
                                // set the elements height
                                this.each(function(){
                                    var $this = jQuery(this);
                                    $this.height(maxHeight);
                                });
                            };

                            jQuery(document).ready(function() {
                                var classes = ["equalheightarticlever"];
                                for (var i = 0, j = classes.length; i < j; i++) {
                                    jQuery('.' + classes[i]).equalHeights();
                                }
                            });
                        })();
                    </script>
                    <c:set var="counter" value="${status.count + 1}" />
                </c:forEach>
            </ul>
            <c:if test="${counter == '0' and !cms.isOnlineProject}">
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="no-margin">Navegación en formato caja</h4>
                    <h5 class="no-margin"><strong>No existen secciones hijas para mostrar</strong></h5>
                    Este recurso lee las secciones hijas de la sección actual. Cuando cree secciones hijas se mostrarán aquí en formato caja.
                </div>
            </c:if>
        </nav>
</cms:bundle>
