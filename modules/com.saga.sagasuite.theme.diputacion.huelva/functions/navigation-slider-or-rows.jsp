<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<fmt:setLocale value="${cms.locale}"/>
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
    <c:if test="${cms.element.setting.marginbottom.value != '0'}">
        <c:set var="marginClass">margin-bottom-${cms.element.setting.marginbottom.value}</c:set>
    </c:if>
    <c:set var="format">slider</c:set>
    <c:if test="${not empty cms.element.settings.format}">
        <c:set var="format">${cms.element.setting.format}</c:set>
    </c:if>
    <c:if test="${format == 'slider'}">
        <script type="text/javascript" src="<cms:link>/system/modules/com.saga.sagasuite.core.script/resources/bx-slider/jquery.bxslider.min.js</cms:link>"></script>
    </c:if>
    <c:if test="${not empty cms.element.settings.classmainbox }">
        <c:set var="classmainbox">${cms.element.settings.classmainbox}</c:set>
    </c:if>
    <cms:device type="mobile">
        <c:set var="numSlides">1</c:set>
        <c:if test="${not empty cms.element.settings.NumSlidesMobile }">
            <c:set var="numSlides">${cms.element.settings.NumSlidesMobile }</c:set>
        </c:if>
    </cms:device>
    <cms:device type="desktop,console">
        <c:set var="numSlides">6</c:set>
        <c:if test="${not empty cms.element.settings.NumSlidesPC }">
            <c:set var="numSlides">${cms.element.settings.NumSlidesPC }</c:set>
        </c:if>
    </cms:device>
    <cms:device type="tablet">
        <c:set var="numSlides">3</c:set>
        <c:if test="${not empty cms.element.settings.NumSlidesTablet }">
            <c:set var="numSlides">${cms.element.settings.NumSlidesTablet }</c:set>
        </c:if>
    </cms:device>

    <c:set var="id">navigationslider</c:set>
    <c:if test="${not empty param.id}">
        <c:set var="id">${param.id}</c:set>
    </c:if>
    <%-- formato slider --%>
    <c:if test="${format == 'slider'}">
        <nav class="element parent sg-microcontent navigation-slider-rows sg-slider sg-slider-horizontal<c:out value=' ${marginClass} ${classmainbox}' />">
            <cms:navigation type="forSite" resource="${param.navStartFolder}" startLevel="0" endLevel="0" var="nav"/>
            <ul class="bxslider slider" id="${id}">
                <c:forEach items="${nav.items}" var="elem" varStatus="status">
                    <li class="slide">
                        <a class="equalheightslides table-block" href="<cms:link>${elem.resourceName}</cms:link>">
                            <span class="table-cell">
                                ${elem.navText}
                            </span>
                        </a>
                    </li>
                </c:forEach>
            </ul>
        </nav>
    </c:if>
    <%-- formato rows --%>
    <c:if test="${format == 'rows'}">
        <c:set var="columnpcclass">col-md-3 col-lg-2</c:set>
        <c:set var="columntabletclass">col-xs-6 col-sm-4</c:set>
        <c:set var="columnmobileclass">col-xxs-12</c:set>

        <c:if test="${not empty cms.element.settings.NumSlidesMobile }">
            <fmt:formatNumber var="columnmobile" value="${12 / cms.element.settings.NumSlidesMobile }" maxFractionDigits="0" />
            <c:set var="columnmobileclass">col-xxs-${columnmobile}</c:set>
        </c:if>
        <c:if test="${not empty cms.element.settings.NumSlidesTablet }">
            <fmt:formatNumber var="columntablet" value="${12 / cms.element.settings.NumSlidesTablet }" maxFractionDigits="0" />
            <c:set var="columntabletclass">col-xs-${columntablet}</c:set>
        </c:if>
        <c:if test="${not empty cms.element.settings.NumSlidesPC }">
            <fmt:formatNumber var="columnpc" value="${12 / cms.element.settings.NumSlidesPC }" maxFractionDigits="0" />
            <c:set var="columnpcclass">col-md-${columnpc}</c:set>
        </c:if>
        <nav class="element parent sg-microcontent pr-20 pl-20 navigation-slider-rows<c:out value=' ${marginClass} ${classmainbox}' />">
            <cms:navigation type="forSite" resource="${param.navStartFolder}" startLevel="0" endLevel="0" var="nav"/>
            <ul class="row list-unstyled" id="${id}">
                <c:forEach items="${nav.items}" var="elem" varStatus="status">
                    <li class="mb-20<c:out value=' ${columnmobileclass} ${columntabletclass} ${columnpcclass}' />">
                        <a class="equalheightslides table-block" href="<cms:link>${elem.resourceName}</cms:link>">
                            <span class="table-cell">
                                ${elem.navText}
                            </span>
                        </a>
                    </li>
                </c:forEach>
            </ul>
        </nav>
    </c:if>
    <script type="text/javascript">
        <c:if test="${format == 'slider'}">
            $(document).ready(function(){
                $('#${id}').bxSlider({
                    minSlides: ${numSlides},
                    <c:if test="${numSlides!=1}">
                    maxSlides: 10,
                    slideMargin: 20,
                    </c:if>
                    <c:if test="${numSlides==1}">
                    slideMargin:0,
                    </c:if>
                    moveSlides: 0,
                    slideWidth: 5000,
                    //pager: false,
                    //controls: false,
                    nextText: '<span class="pe-7s-angle-right" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.function.slider.next" /></span> ',
                    prevText: '<span class="pe-7s-angle-left" aria-hidden="true"></span><span class="sr-only"><fmt:message key="key.function.slider.previous" /></span>',
                    auto: true,
                    pause: '5000',
                    autoHover: true,
                    speed: 500
                });
            });
        </c:if>
        (function(){
            jQuery.fn.equalHeights = function() {
                var maxHeight = 0;
                // get the maximum height
                this.each(function(){
                    var $this = jQuery(this);
                    if ($this.height() > maxHeight) { maxHeight = $this.height(); }
                });
                // set the elements height
                this.each(function(){
                    var $this = jQuery(this);
                    $this.height(maxHeight);
                });
            };

            jQuery(window).load(function() {
                var classes = ["equalheightslides","equalheightslides-two"];
                for (var i = 0, j = classes.length; i < j; i++) {
                    jQuery('.' + classes[i]).equalHeights();
                }
            });
        })();
    </script>
</cms:bundle>
