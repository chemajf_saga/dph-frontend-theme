<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<fmt:setLocale value="${cms.locale}"/>
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">

    <c:if test="${cms.element.setting.paddingbottom.value != '0'}">
        <c:set var="stylepb">padding-bottom:${cms.element.setting.paddingbottom.value}px;</c:set>
    </c:if>
    <c:if test="${cms.element.setting.paddingtop.value != '0'}">
        <c:set var="stylept">padding-top:${cms.element.setting.paddingtop.value}px;</c:set>
    </c:if>
    <c:if test="${not empty cms.element.settings.classmainbox }">
        <c:set var="classmainbox">${cms.element.settings.classmainbox}</c:set>
    </c:if>
    <c:if test="${not empty cms.element.settings.centered and cms.element.settings.centered=='true'}">
        <c:set var="centered">centered</c:set>
    </c:if>
    <c:if test="${not empty cms.element.settings.titletext }">
        <c:set var="titletext">${cms.element.settings.titletext}</c:set>
    </c:if>
    <c:if test="${not empty cms.element.settings.subtitletext }">
        <c:set var="subtitletext">${cms.element.settings.subtitletext}</c:set>
    </c:if>
    <c:if test="${not empty cms.element.settings.link }">
        <c:set var="link">${cms.element.settings.link}</c:set>
    </c:if>
    <c:if test="${not empty cms.element.settings.linktext }">
        <c:set var="linktext">${cms.element.settings.linktext}</c:set>
    </c:if>
    <c:if test="${cms.element.settings.nomarginhead == 'true' }">
        <c:set var="nomarginhead">no-margin-head</c:set>
    </c:if>
    <c:if test="${not empty cms.element.settings.icon }">
        <c:set var="icon">${cms.element.settings.icon}</c:set>
    </c:if>

    <c:set var="titletag">h2</c:set>
    <c:if test="${not empty cms.element.settings.titletag }">
        <c:set var="titletag">${cms.element.settings.titletag }</c:set>
        <c:if test="${titletag == 'h1'}">
            <c:set var="subtitletag">h2</c:set>
        </c:if>
        <c:if test="${titletag == 'h2'}">
            <c:set var="subtitletag">h3</c:set>
        </c:if>
        <c:if test="${titletag == 'h3'}">
            <c:set var="subtitletag">h4</c:set>
        </c:if>
        <c:if test="${titletag == 'h4'}">
            <c:set var="subtitletag">h5</c:set>
        </c:if>
        <c:if test="${titletag == 'h5'}">
            <c:set var="subtitletag">div</c:set>
        </c:if>
        <c:if test="${titletag == 'div'}">
            <c:set var="subtitletag">div</c:set>
        </c:if>
    </c:if>

    <c:if test="${not empty cms.element.settings.titlesize }">
        <c:set var="titlesize">${cms.element.settings.titlesize }</c:set>
        <c:if test="${titlesize == 'h1'}">
            <c:set var="subtitlesize">h2</c:set>
        </c:if>
        <c:if test="${titlesize == 'h2'}">
            <c:set var="subtitlesize">h3</c:set>
        </c:if>
        <c:if test="${titlesize == 'h3'}">
            <c:set var="subtitlesize">h4</c:set>
        </c:if>
        <c:if test="${titlesize == 'h4'}">
            <c:set var="subtitlesize">h5</c:set>
        </c:if>
        <c:if test="${titlesize == 'h5'}">
            <c:set var="subtitlesize">size-default</c:set>
        </c:if>
        <c:if test="${titlesize == 'size-default'}">
            <c:set var="subtitlesize">size-default</c:set>
        </c:if>
    </c:if>
    <c:if test="${empty centered}">
        <div class="element parent section-title not-centered <c:out value='${classmainbox} ${nomarginhead}' />" style="${stylept}${stylepb}">
            <header class="headline">
                <${titletag} class="title v-align-m ${titlesize}">
                    <c:if test="${not empty icon}">
                        <span class="${icon} v-align-m inline-b mr-10" aria-hidden="true"></span>
                    </c:if>
                    ${titletext}
                </${titletag}>
                <c:if test="${not empty subtitletext}">
                    <${subtitletag} class="subtitle v-align-m">
                        ${subtitletext}
                    </${subtitletag}>
                </c:if>
                <c:if test="${not empty link}">
                    <a class="v-align-m inline-b" href="<cms:link>${link}</cms:link>">
                        <c:if test="${not empty linktext}">
                            <span class="pe-7s-link v-align-m fs20 pr-10 inline-b" aria-hidden="true"></span>
                            <span class="v-align-m fs16 inline-b">${linktext}</span>
                        </c:if>
                    </a>
                </c:if>
            </header>
        </div>
    </c:if>
    <c:if test="${not empty centered}">
        <div class="element parent section-title centered <c:out value='${classmainbox} ${nomarginhead}' />" style="${stylept}${stylepb}">
            <header class="headline">
                <${titletag} class="title ${titlesize}">
                    <c:if test="${not empty icon}">
                        <span class="${icon} v-align-m inline-b mr-10" aria-hidden="true"></span>
                    </c:if>
                    <span class="v-align-m inline-b">${titletext}</span>
                </${titletag}>
                <c:if test="${not empty subtitletext}">
                    <${subtitletag} class="subtitle ${subtitlesize}">
                        ${subtitletext}
                    </${subtitletag}>
                </c:if>
                <c:if test="${not empty link}">
                    <a class="mt-5 disp-block" href="<cms:link>${link}</cms:link>">
                        <span class="pe-7s-link v-align-m fs20 pr-10 inline-b" aria-hidden="true"></span>
                        <span class="v-align-m fs16 inline-b">${linktext}</span>
                    </a>
                </c:if>
            </header>
        </div>
    </c:if>
</cms:bundle>
