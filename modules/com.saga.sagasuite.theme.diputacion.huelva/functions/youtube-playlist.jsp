<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<fmt:setLocale value="${cms.locale}"/>
<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">
    <div class="element parent function-youtube-playlist">
        <c:if test="${not empty cms.element.settings.marginbottom}">
            <c:set var="marginClass">margin-bottom-${cms.element.settings.marginbottom}</c:set>
        </c:if>
        <c:if test="${not empty cms.element.settings.classmainbox}">
            <c:set var="classmainbox">${cms.element.setting.classmainbox}</c:set>
        </c:if>
        <c:set var="url">${param.url}</c:set>
        <c:set var="videotitle">${param.videotitle}</c:set>
        <c:if test="${not empty url}">
            <c:if test="${fn:contains(url,'user/')}">
                <c:set var="user">${fn:substringAfter(url,'user/')}</c:set>
            </c:if>
            <c:if test="${fn:contains(url,'channel/')}">
                <c:set var="channel">PL${fn:substringAfter(url,'channel/')}</c:set>
            </c:if>
            <c:if test="${fn:contains(url,'/watch?v')}">
                <c:set var="video">${fn:substringAfter(url,'/watch?v=')}</c:set>
            </c:if>
            <c:if test="${fn:contains(url,'youtu.be/')}">
                <c:set var="video">${fn:substringAfter(url,'youtu.be/')}</c:set>
            </c:if>
        </c:if>
        <c:if test="${not empty user or not empty channel or not empty video}">
            <div class="embed-responsive embed-responsive-16by9<c:out value=" ${marginClass} ${classmainbox}" />">
                <c:if test="${not empty user}">
                    <iframe class="embed-responsive-item" src="http://www.youtube.com/embed?max-results=1&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;listType=user_uploads&amp;list=${user}" frameborder="0" allowfullscreen="" title="${videotitle}"></iframe>
                </c:if>
                <c:if test="${not empty channel}">
                    <iframe class="embed-responsive-item" src="http://www.youtube.com/embed?max-results=10&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;listType=playlist&amp;list=${channel}" frameborder="0" allowfullscreen="" title="${videotitle}"></iframe>
                </c:if>
                <c:if test="${not empty video}">
                    <iframe width="1280" height="720" src="https://www.youtube.com/embed/${video}" frameborder="0" allowfullscreen></iframe>
                </c:if>
            </div>
        </c:if>
    </div>
</cms:bundle>
