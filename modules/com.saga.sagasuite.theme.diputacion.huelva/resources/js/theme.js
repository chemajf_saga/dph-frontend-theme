/* ===========================================================
 * Demo js
 * ========================================================== */


$(document).ready(function () {

// Magnific popup Lightbox
	// TRANSLATION - Add it after jquery.magnific-popup.js and before first initialization code

	var curLang = $('html').attr('lang');

	switch (curLang) {
		case 'de':
			$.extend(true, $.magnificPopup.defaults, {
				tClose: 'Schließen',
				tLoading: 'Wird geladen...',
				gallery: {
					tPrev: 'Zurück',
					tNext: 'Weiter',
					tCounter: '%curr% von %total%'
				},
				image: {
					tError: 'Das <a href="%url%">Bild</a> konnte nicht geladen werden.'
				},
				ajax: {
					tError: 'Der <a href="%url%">Inhalt</a> konnte nicht geladen werden.'
				}
			});
			break;
		case 'es':
			$.extend(true, $.magnificPopup.defaults, {
				tClose: 'Cerrar (Tecla Esc)', // Alt text on close button
				tLoading: 'Cargando...', // Text that is displayed during loading. Can contain %curr% and %total% keys
				gallery: {
					tPrev: 'Imagen Anterior de la Galería (Tecla de flecha a la izquierda)', // Alt text on left arrow
					tNext: 'Imagen Siguiente de la Galería (Tecla de flecha a la derecha)', // Alt text on right arrow
					tCounter: '%curr% de %total%' // Markup for "1 of 7" counter
				},
				image: {
					tError: '<a href="%url%">La imagen</a> no puede cargarse.' // Error message when image could not be loaded
				},
				ajax: {
					tError: '<a href="%url%">El contenido</a> no se puede cargar.' // Error message when ajax request failed
				}
			});
			break;
		case 'en':
			$.extend(true, $.magnificPopup.defaults, {
				tClose: 'Close (Esc)', // Alt text on close button
				tLoading: 'Loading...', // Text that is displayed during loading. Can contain %curr% and %total% keys
				gallery: {
					tPrev: 'Previous (Left arrow key)', // Alt text on left arrow
					tNext: 'Next (Right arrow key)', // Alt text on right arrow
					tCounter: '%curr% of %total%' // Markup for "1 of 7" counter
				},
				image: {
					tError: '<a href="%url%">The image</a> could not be loaded.' // Error message when image could not be loaded
				},
				ajax: {
					tError: '<a href="%url%">The content</a> could not be loaded.' // Error message when ajax request failed
				}
			});
			break;
		case 'ca':
			$.extend(true, $.magnificPopup.defaults, {
				tClose: 'Tancar (Tecla Esc)', // Alt text on close button
				tLoading: 'Carregant...', // Text that is displayed during loading. Can contain %curr% and %total% keys
				gallery: {
					tPrev: 'Imatge Anterior de la Galeria (Tecla de fletxa ala esquerra)', // Alt text on left arrow
					tNext: 'Imatge Següent de la Galeria (Tecla de fletxa ala dreta)', // Alt text on right arrow
					tCounter: '%curr% de %total%' // Markup for "1 of 7" counter
				},
				image: {
					tError: '<a href="%url%">La imatge</a> no pot carregar-se.' // Error message when image could not be loaded
				},
				ajax: {
					tError: '<a href="%url%">El contingut</a> no pot carregar-se.' // Error message when ajax request failed
				}
			});
			break;
		default:
			$.extend(true, $.magnificPopup.defaults, {
				tClose: 'Close (Esc)', // Alt text on close button
				tLoading: 'Loading...', // Text that is displayed during loading. Can contain %curr% and %total% keys
				gallery: {
					tPrev: 'Previous (Left arrow key)', // Alt text on left arrow
					tNext: 'Next (Right arrow key)', // Alt text on right arrow
					tCounter: '%curr% of %total%' // Markup for "1 of 7" counter
				},
				image: {
					tError: '<a href="%url%">The image</a> could not be loaded.' // Error message when image could not be loaded
				},
				ajax: {
					tError: '<a href="%url%">The content</a> could not be loaded.' // Error message when ajax request failed
				}
			});
	}

	try{
		$('.zoom-filter').magnificPopup({
			type:'image',
			image: {
				titleSrc: 'data-title'
			},
			/*closeMarkup: '<button title="tClose" type="button" class="mfp-close"><span aria-hidden="true">&#215;</span></button>',*/
			gallery:{
				enabled:true
			}
		});
	}catch(e){}

	// Magnific popup Lightbox
	try{
		$('.gallery').each(function() { // the containers for all your galleries
			$(this).magnificPopup({
				delegate: '.zoom-filter', // the selector for gallery item
				type: 'image',
				gallery: {
					enabled:true
				}
			});
		});
	}catch(e){}

	// perfect scrollbar
	$('.ps-container').perfectScrollbar();

	$('.ps-container-y').perfectScrollbar({
		suppressScrollX: true
	});
	$('.ps-container-x').perfectScrollbar({
		suppressScrollY: true
	});

	if($('.ps-active-y').length) {
		$('.ps-active-y').parent().addClass('ps-active-y-parent');
	}
	if($('.ps-active-x').length) {
		$('.ps-active-x').parent().addClass('ps-active-x-parent');
	}




	//Implementa tooltip en los elementos con la clase selector y ademas le aniadimos las clases asociadas al enlace al contenedor del tooltip

	$('[data-toggle="tooltip"]').tooltip();

	$( ".hastooltip" ).tooltip();

	$( ".tooltip-left" ).tooltip({
		placement: 'left'
	});
	$( ".tooltip-right" ).tooltip({
		placement: 'right'
	});
	$( ".tooltip-bottom" ).tooltip({
		placement: 'bottom'
	});

	// Implementa Popup para mostrar el detalle de recursos
	// say your selector and click handler looks something like this...


	$( "<div class='dialogo'></div>" ).appendTo( "body" );
	$(".popup a").click(abreDialogo);

	// in your function, just grab the event object and go crazy...

	function abreDialogo(event){
		$(".dialogo").dialog({
			modal: true,
			title: $(this).attr('title'),
			show: {
				effect: "drop",
				duration: 500,
				direction: 'up'
			},
			hide: {
				effect: "drop",
				duration: 500,
				direction: 'down'
			},
			position: 'center',
			draggable: false,
			height: 600,
			width: 650,
			closeOnEscape: true

		});

		var elem = $(this);
		var miRuta = elem.attr('href');
		$(".ui-dialog-content").load(miRuta);


		return false;
	}

	// para ocultar la caja de compartir cuando se hace click fuera
	if($('.sg-share').length) {
		$('body').click(function(){
			$('#sharebox ').collapse('hide')
		});
	}

	// funcion para obtener parametros de la url
	function GetURLParameter(sParam){
		var sPageURL = window.location.search.substring(1);
		var sURLVariables = sPageURL.split('&');
		for (var i = 0; i < sURLVariables.length; i++) {
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam) {
				return sParameterName[1];
			}
		}
	}

	// para poder añadir atributo href real a los enlaces de evento collapse por si no hay javascript que se pueda navegar

	$("a[data-toggle]").on("click" ,function (e) {
		e.preventDefault();
	});

	// definimos las variables necesarias para controlar las animaciones con scroll y que tengan en cuenta si hay sticky header o no
	var widthwindow = $(window).width();

	// si estamos en offline contamos el alto del toolbar del ADE
	var toolbarHeight = 0;
	var stickyHeight = 0;
	var headerHeight = 0;
	if($('.toolbar-offline-space').length){
		var toolbarHeight = 52;
	}
	if(widthwindow > 1200) {
		var stickyHeight = 80;
	}
	var headerHeight = toolbarHeight + stickyHeight;
	// para que los enlaces con ancla especiales nos lleven con animacion al bloque correspondiente de la página

	$("[data-link]").on("click" ,function (e) {
		e.preventDefault();
		if($('.dph-portada').length){
			var dynamicMarginPortada = Number(($('.dph-portada #wrapper-container > .template-page-block-main > .wrapper').prop('style')['margin-top']).replace("px",""));
		}
		var datalinkhash = $(this).attr("data-link");
		$('html, body').animate({scrollTop: $(datalinkhash).offset().top - dynamicMarginPortada - headerHeight}, 1000);
		if(history.pushState) {
			history.pushState(null, null, datalinkhash);
		}
		else {
			location.hash = datalinkhash;
		}
	});
	$("a[href^='#']").not('[class*="data-media-loader"],[data-toggle],[data-loading]').on("click" ,function (e) {
		e.preventDefault();
		if($('.dph-portada').length){
			var dynamicMarginPortada = Number(($('.dph-portada #wrapper-container > .template-page-block-main > .wrapper').prop('style')['margin-top']).replace("px",""));
		}
		var linkhash = $(this).attr("href");
		var linkhashname = linkhash.replace('#', '');
		if($('[name^='+linkhashname+']').length){
			$('html, body').animate({scrollTop: $('[name^='+linkhashname+']').offset().top - dynamicMarginPortada - headerHeight}, 1000);
			if(history.pushState) {
				history.pushState(null, null, linkhash);
			}
			else {
				location.hash = linkhash;
			}
		}else{
			$('html, body').animate({scrollTop: $(linkhash).offset().top - dynamicMarginPortada - headerHeight}, 1000);
			if(history.pushState) {
				history.pushState(null, null, linkhash);
			}
			else {
				location.hash = linkhash;
			}
		}
	});

	//Si al cargar viene un hash en la url, debemos abrir dicho tab
	var hash = document.location.hash;

	try{
		var Saga = window.Saga || {};
		if ((Saga.sgtablinks === undefined || Saga.sgtablinks === null || !Saga.sgtablinks) && (hash) && (hash != '#beginlist')) {
			$('html, body').animate({scrollTop: $(hash).offset().top - headerHeight}, 2000);
		}
		if(hash == '#beginlist') {
			$('html, body').animate({scrollTop: $(hash).offset().top - headerHeight}, 2000);
		}
		else {}
	}catch(e){}

	// PARA QUE EN MOVIL AL HACER CLICK EN ENLACES DE CALENDARIO HAGA SCROLL AUTOMATICO HASTA LA LISTA

	if(widthwindow < 480) {
		$(document).on('click', '.eventsCalendar-day a, .monthTitle', function (e) {
			var scrollCalendarTo = $(e.target).parents('.tscalendar').find('.eventsCalendar-list-wrapper');
			console.log(scrollCalendarTo);
			$('html, body').animate({scrollTop: $(scrollCalendarTo).offset().top - 60}, 800);
		});
	}


	// BOX CON IMAGEN ANIMADA

	$( ".animated-img-box a" ).mouseover(function() {
		$(this).parents('.animated-box').addClass('animation-on');
	}).mouseout(function() {
		$(this).parents('.animated-box').removeClass('animation-on');
	});

	// PARA MOVER LA CAJA DE INDICADORES RELACIONADOS AL FINAL DEL BUSCADOR DE CONTENIDO RELACIONADO

	$('.sg-indicador-trans .categories-list.well.well-sm').insertAfter( $( ".sg-search-transparencia" ) );

	// INCLUIR ICONO DE FLECHA CUANDO EL MENU HORIZONTAL ES MAS PEQUEÑO QUE EL DIV PADRE

	if ($(".row-navigation .navbar .nav").prop('scrollWidth') > $(".row-navigation .navbar .nav").width() ) {
		$(".row-navigation .navbar .nav").parents('.navbar').addClass('overflowing');
	}else {}

// para que en el menu responsive si haces click en una seccion por debajo de otra desplegada muy extensa haga scrolltop hasta la que has hecho click	
// para cargar el overlay de fondo en el menu responsive

	$('.menu.mobile').on('shown.bs.collapse', function (e) {

		var clickedHeader = $(this).find('.panel > .in').closest('.panel').find('.bullet');
		var offset = clickedHeader.offset();
		var top = $(window).scrollTop();
		if(offset) {
			var topOfHeader = offset.top;
			if(topOfHeader < top) {
				$('html,body').animate({ scrollTop: topOfHeader}, 100, 'swing');
			}
		}
	});

	$('#header .mega-menu .navbar-nav').on('shown.bs.collapse', function (e) {
		console.log(e.target);
		var clickedHeader = $(e.target).parents('.dropdown-mega:not(.collapsed)').closest('.dropdown-mega').find('.dropdown-toggle');
		var offset = clickedHeader.offset();
		var top = $(window).scrollTop();
		if(offset) {
			var topOfHeader = offset.top;
			if(topOfHeader < top) {
				$('html,body').animate({ scrollTop: topOfHeader}, 400, 'swing');
			}
		}
	});


	// PARA ALINEAR VERTICALMENTE LA CAJA DE TEXTO EN EL CARRUSEL

	// SI ES CON VIDEO

	// asignar alto del div padre al hijo en el elemento super destacado del listado de la home

	var parentcentradoVertical = $('.list-element-super-dest > div');
	if(parentcentradoVertical.length > 0) {
		var soncentradoVertical = $('.list-element-super-dest > div > .wrapper-content');
		var heightcentrado = parentcentradoVertical.outerHeight();
		soncentradoVertical.height(heightcentrado);
		parentcentradoVertical.height(heightcentrado);
	}else{}



	// PARA MOSTRAR LA PANTALLA COMPLETA CON EL BUSCADOR

	var buscadorWrapper = $('.overlay-search');
	var buscadorTrigger = $('a[href*=#search]');
	buscadorTrigger.click(function () {
		$('html').addClass('no-scroll-y');
		$(this).removeClass('active');
		buscadorWrapper.addClass('active');
		buscadorWrapper.find('.form-control').focus();
	});

	var buscadorWrapperClose = $('.overlay-search button.close');

	buscadorWrapperClose.click(function () {
		buscadorWrapper.removeClass('active');
		$('html').removeClass('no-scroll-y');
	});

// PARA MOSTRAR TEMATICAS A PANTALLA COMPLETA

	var tematicasWrapper = $('#tematicaspopup');
	var tematicasTrigger = $('[data-target*=tematicaspopup]');
	tematicasTrigger.click(function (e) {
		e.preventDefault();
		$('html').addClass('no-scroll-y');
		$(this).removeClass('active');
		tematicasWrapper.addClass('active');
	});

	var tematicasWrapperClose = $('#tematicaspopup button.close');
	tematicasWrapperClose.click(function () {
		tematicasWrapper.removeClass('active');
		$('html').removeClass('no-scroll-y');
	});

	// para cerrarlo con tecla esc

	document.onkeydown = function(evt) {
		evt = evt || window.event;
		var isEscape = false;
		if ("key" in evt) {
			isEscape = (evt.key == "Escape" || evt.key == "Esc");
		} else {
			isEscape = (evt.keyCode == 27);
		}
		if (isEscape && $('#tematicaspopup.active').length) {
			tematicasWrapperClose.click();
		}
		if (isEscape && $('.overlay-search.active').length) {
			buscadorWrapperClose.click();
		}
	};

	//Boton de volver arriba

	var offset = 840;
	var duration = 500;
	$( "body" ).append( "<div class='back-to-top' style='display: none;'><a title='Top' class='btn pt-10 pb-10 btn-specific-main'><span class='pe-7s-angle-up fs30' aria-hidden='true'></span></a></div>" );
	$( ".back-to-top > a" ).tooltip( {placement: 'left'} );
	jQuery(window).scroll(function() {

		if (jQuery(this).scrollTop() > offset) {
			jQuery('.back-to-top').fadeIn(duration);
		} else {
			jQuery('.back-to-top').fadeOut(duration);
		}
	});

	jQuery('.back-to-top > a').click(function(event) {
		event.preventDefault();
		jQuery('html, body').animate({scrollTop: 0}, duration);
		return false;
	});

	// para efecto desplazamiento en contenido secciones con menu de indice

	$('.articulo .nav-stacked li a[href*="#"],.articulo .tab-content .clearfix .pull-right a[href*="#"]').on("click" ,function (e) {
		e.preventDefault();
		var hash = $(this).attr("href");
		var widthwindow = $(window).width();
		if(widthwindow > 1200) {
			var offsetheader = 110;
		}else{
			var offsetheader = 30;
		}
		$('html, body').animate({scrollTop: $(hash).offset().top - offsetheader}, 1000);
		if (history.pushState) {
			history.pushState(null, null, hash);
		}
		else {
			location.hash = hash;
		}
	});
	// PARA EVITAR EL CIERRE AUTOMATICO DEL DROPDOWN-MENU DEL MEGA MENU CUANDO HACES CLICK DENTRO DEL MISMO

	$(document).on('click', '.main-navigation-collapse .nav-justified .dropdown-menu', function (e) {
		e.stopPropagation();
	});

	// btn volver atrás

	$('.btn-back').on('click', function (e) {
		window.history.back();
	});

	// PARA CAMBIAR EL ICONO DE DESPLIEGUE DE MENU Y ANADIR EL OVERLAY DE FONDO

	$('body').append('<div class="overlay-menu-responsive" style="display: none;"></div>');
	$('body').append('<div class="overlay-column-responsive" style="display: none;"></div>');

	$('body .main-content.aside-right').append('<button type="button" class="btn visible-sm visible-xs visible-xxs btn-specific-secondary column-btn display-to-left"><span class="fa fa-chevron-left" aria-hidden="true"></span></button>');
	$('body .main-content.aside-left').append('<button type="button" class="btn visible-sm visible-xs visible-xxs btn-specific-secondary column-btn display-to-right"><span class="fa fa-chevron-right" aria-hidden="true"></span></button>');

	$('.main-content.aside-right .column-btn').on('click', function (e) {
		if (!$(e.currentTarget).hasClass('shown')) {
			$(e.currentTarget).addClass('shown');
			$('.main-content.aside-right > .wrapper > .row > [class*="col-"]:last-child').addClass('displayed');
			$('.overlay-column-responsive').fadeIn();
			$(e.currentTarget).find('.fa').removeClass('fa-chevron-left').addClass('fa-chevron-right');
			$('html').addClass('no-scroll');
		}
		else {
			$(e.currentTarget).removeClass('shown');
			$('.main-content.aside-right > .wrapper > .row > [class*="col-"]:last-child').removeClass('displayed');
			$('.overlay-column-responsive').fadeOut();
			$(e.currentTarget).find('.fa').removeClass('fa-chevron-right').addClass('fa-chevron-left');
			$('html').removeClass('no-scroll');
		}
	});

	$('.main-content.aside-left .column-btn').on('click', function (e) {
		if (!$(e.currentTarget).hasClass('shown')) {
			$(e.currentTarget).addClass('shown');
			$('.main-content.aside-left > .wrapper > .row > [class*="col-"]:first-child').addClass('displayed');
			$('.overlay-column-responsive').fadeIn();
			$(e.currentTarget).find('.fa').removeClass('fa-chevron-right').addClass('fa-chevron-left');
			$('html').addClass('no-scroll');
		}
		else {
			$(e.currentTarget).removeClass('shown');
			$('.main-content.aside-left > .wrapper > .row > [class*="col-"]:first-child').removeClass('displayed');
			$('.overlay-column-responsive').fadeOut();
			$(e.currentTarget).find('.fa').removeClass('fa-chevron-left').addClass('fa-chevron-right');
			$('html').removeClass('no-scroll');
		}
	});

	// PARA DESPLEGAR COLUMNAS AL DESLIZAR DEDO POR PANTALLA A LA IZQUIERDA Y VICEVERSA

	// DESPLIEGUE DE COLUMNA DERECHA

	$("body.responsive-device").on("swipeleft",function(){
		$.event.special.swipe.horizontalDistanceThreshold = 100;
		var clase = $('.column-btn.display-to-left').attr('class');
		if (!$('.column-btn').hasClass('shown')) {
			$('.column-btn').click();
		}
	});
	$("body.responsive-device").on("swiperight",function(){
		$.event.special.swipe.horizontalDistanceThreshold = 100;
		var clase = $('.column-btn.display-to-left').attr('class');
		if ($('.column-btn').hasClass('shown')) {
			$('.column-btn').click();
		}
	});

	// DESPLIEGUE DE COLUMNA IZQUIERDA

	$("body.responsive-device").on("swiperight",function(){
		$.event.special.swipe.horizontalDistanceThreshold = 100;
		var clase = $('.column-btn.display-to-right').attr('class');
		if (!$('.column-btn').hasClass('shown')) {
			$('.column-btn').click();
		}
	});
	$("body.responsive-device").on("swipeleft",function(){
		$.event.special.swipe.horizontalDistanceThreshold = 100;
		var clase = $('.column-btn.display-to-right').attr('class');
		if ($('.column-btn').hasClass('shown')) {
			$('.column-btn').click();
		}
	});

	$('.header .navbar-toggle').on('click', function (e) {
		var expanded = $(this).attr('aria-expanded');
		if (expanded == 'false'){
			$(this).find('.nat-icon').removeClass('menu-icon').addClass('close-icon');
			$('.overlay-menu-responsive').fadeIn();
		}
		if (expanded == 'true'){
			$(this).find('.nat-icon').removeClass('close-icon').addClass('menu-icon');
			$('.overlay-menu-responsive').fadeOut();
			$('.main-navigation-collapse').find('a[aria-expanded="true"]').click();
		}
	});
	// cuando hacemos click en la capa de overlay lo plegamos todo
	$(document).on('click', '.overlay-menu-responsive', function (e) {
		$('.header .navbar-toggle').attr({'class': 'navbar-toggle collapsed','aria-expanded': 'false'});
		$('.header .navbar-toggle').find('.nat-icon').removeClass('icon-close').addClass('menu-icon');
		$('.main-navigation-collapse').collapse('hide');
		$('.overlay-menu-responsive').hide();
		$('html,body').animate({ scrollTop: '#header'}, 1500, 'swing');
	});

	$(document).on('click', '.overlay-column-responsive', function (e) {
		$('.overlay-column-responsive').hide();
		$('.column-btn').removeClass('shown');
		$('.main-content.aside-right .column-btn .fa').removeClass('fa-chevron-right').addClass('fa-chevron-left');
		$('.main-content.aside-left .column-btn .fa').removeClass('fa-chevron-left').addClass('fa-chevron-right');
		$('.main-content.aside-right > .wrapper > .row > [class*="col-"]:last-child.displayed').removeClass('displayed');
		$('.main-content.aside-left > .wrapper > .row > [class*="col-"]:first-child.displayed').removeClass('displayed');
		$('html').removeClass('no-scroll');
	});


	// COMPORTAMIENTO DE LOS DESPLEGABLES DEL MENU PRINCIPAL


	$('.header .main-navigation-collapse > .navbar-nav').on('show.bs.collapse', function () {
		$(this).find('.collapse[aria-expanded="true"]').collapse('hide');
	});
	$('.header .main-navigation-collapse > .navbar-nav > li > .dropdown-toggle').on('show.bs.collapse', function () {
		$(this).find('.collapse[aria-expanded="true"]').collapse('hide');
	});


	$(document).on('click', function (e) {
		var isnotheadercollapse = $(e.target).parents('.header .main-navigation-collapse > .navbar-nav').length == 0;
		if(isnotheadercollapse ){
			$('.header .main-navigation-collapse > .navbar-nav .collapse').collapse('hide');
		}
	});

	$('.header .main-navigation-collapse > .navbar-nav .btn-close').on('click', function () {
		$(this).parents('.header .main-navigation-collapse > .navbar-nav').find('.collapse[aria-expanded="true"]').collapse('hide');
	});

	// para que cambie la orientacion de la flecha de espliegue

	$('.header .dropdown-menu-collapse').on('show.bs.collapse', function (e) {
		$(e.target).parents('.dropdown-mega').find('.dropdown-toggle .fa').removeClass('fa-angle-down').addClass('fa-angle-up');
	});

	$('.header .dropdown-menu-collapse').on('hide.bs.collapse', function (e) {
		$(e.target).parents('.dropdown-mega').find('.dropdown-toggle .fa').removeClass('fa-angle-up').addClass('fa-angle-down');
	});

	// PARA QUE EN LA HOME LA CAJA DE LA DERECHA DEL CARRUSEL COJA EL MISMO ALTO QUE EL CARRUSEL
/*	if($('.carousel .carousel-inner .item img').length) {
		var heightwindowforcarousel = $('.carousel .carousel-inner .item img').height();
		$('.carousel .carousel-inner .item .container-caption').height(heightwindowforcarousel);
		console.log(heightwindowforcarousel);
	}*/
	if($('.tr-carousel-access-home').length) {
		if(widthwindow > 990) {
			var imgs = $('.tr-carousel-access-home > .wrapper > .row > [class*="col-"]:last-child img');
			var imgsLength = imgs.length;
			var imgsCont = 0;
			imgs.each(function () {
				var img = new Image();
				img.onload = function () {
					imgsCont++;
					if (imgsCont === imgsLength && $(this).naturalHeight !== 0) {
						var heightwindowforcarousel = $('.tr-carousel-access-home > .wrapper > .row > [class*="col-"]:last-child > *').height();
						$('.tr-carousel-access-home .carousel-bg .carousel-inner').height(heightwindowforcarousel);
						$('.tr-carousel-access-home .carousel-bg .carousel-inner .item').height(heightwindowforcarousel);
						$('.tr-carousel-access-home .carousel-bg .carousel-inner .item .responsive-video').height(heightwindowforcarousel);
						$('.tr-carousel-access-home .carousel-bg .carousel-inner .item .container-caption').height(heightwindowforcarousel);
						if($('.tr-carousel-access-home .carousel-bg .item .responsive-video').length) {
							var widthwindowforvideo = $('.tr-carousel-access-home .carousel-bg .carousel-inner .item').width();
							var heightwindowforvideo = heightwindowforcarousel;
							var ratio = widthwindowforvideo / heightwindowforvideo;
							console.log('ratio theme: '+ratio);
							if(ratio > 1.77){
								var rationec = 1.77;//ratio de formato 16:9
								var necheight = widthwindowforvideo / rationec;
								var difheight = heightwindowforvideo - necheight;
								var modiftop = difheight / 2;
								if($('.tr-carousel-access-home .carousel-bg .item .responsive-video .embed-responsive iframe').length) {
									$('.tr-carousel-access-home .carousel-bg .item .responsive-video .embed-responsive').css({
										'top' : modiftop+'px',
										'height': necheight+'px'
									});
								}
								if($('.tr-carousel-access-home .carousel-bg .item .responsive-video video').length) {
									$('.tr-carousel-access-home .carousel-bg .item .responsive-video video').css({
										'top' : modiftop+'px'
									});
								}
							}else {
								var rationec = 1.77;//ratio de formato 16:9
								var necwidth = heightwindowforvideo * rationec;
								var difwidth = widthwindowforvideo - necwidth;
								var modifleft = difwidth / 2;
								$('.tr-carousel-access-home .carousel-bg .item .responsive-video .embed-responsive').css({
									'left' : modifleft+'px',
									'width': necwidth+'px',
									'top': 'inherit',
									'height': 'inherit'
								});
								if($('.tr-carousel-access-home .carousel-bg .item .responsive-video video').length) {
									$('.tr-carousel-access-home .carousel-bg .item .responsive-video video').css({
										'left' : modifleft+'px',
										'width': necwidth+'px',
										'top': 'inherit',
										'height': 'inherit'

									});
								}
							}
						}
					}
					console.log($(this).attr('src') + ' - done!');
				}
				img.src = $(this).attr('src');
			});
		}
	}



	// para que la caja overlay display table coja el mismo alto que la imagen que contiene
	$( '.overlayed-table > img' ).each(function( index ) {
		var boxheight = $(this).height();
		$(this).parent().find('.overlayer').css('height',boxheight+'px');
	});

});
/* control de comportamiento de contenido principal con respecto al subheader*/
var widthwindow = $(window).width();
if((widthwindow > 1200) && ($('.subheader').length)) {
	var varcontrollerheight = 380;//altura de la caja de subheader padre
	var breakpointsc = 1;
	var beginningmargin = -(varcontrollerheight / 2);
	if($('.row-navigation').length) {
		var beginningmargin = (-varcontrollerheight + 145) / 2;
	}
	if($('.dph-portada .breadcrumbs').length) {
		var varcontrollerheight = 500;//altura de la caja de subheader padre cuando es portada de zona
		var beginningmargin = -(varcontrollerheight / 2);
		if($('.row-navigation').length) {
			var beginningmargin = (-varcontrollerheight + 145) / 2;
		}
	}
	if($('.dph-portada .breadcrumbs.portada-no-menu').length) {
		var varcontrollerheight = 640;//altura de la caja de subheader hija cuando es portada de zona
		var beginningmargin = -(varcontrollerheight / 3.5);
	}
	$(window).load(function() {
		$('.parallax-mirror:not(.parallax-subheader)').css('margin-top',(varcontrollerheight / 2) + 'px');
	});
	$('#content-interior .template-page-block-main .container').css('margin-top', beginningmargin + 'px');
	$(window).scroll(function() {
		var sc = window.pageYOffset;
		var schalf = window.pageYOffset / 2;
		var scopacity = 1 - (sc / (varcontrollerheight - 50));
		var margintopcss = beginningmargin + schalf;
		if(margintopcss < breakpointsc ) {
			$('#content-interior .template-page-block-main .container').css('margin-top', margintopcss + 'px');
			$('.subheader .breadcrumbs .table-block-wrapper .table-block').css('height', (varcontrollerheight / 2) + (sc * 1.4) + 'px');
			$('.subheader .breadcrumbs .table-block-wrapper .table-block').css('opacity', scopacity);
		}
		else {
			$('#content-interior .template-page-block-main .container').css('margin-top',breakpointsc+'px');
			$('.subheader .breadcrumbs .table-block-wrapper .table-block').css('height', varcontrollerheight +'px');
			$('.subheader .breadcrumbs .table-block-wrapper .table-block').css('opacity', '0');
		}
	});
}
$(window).scroll(function() {
	if (($(window).scrollTop()>225) && ($(window).width()>1200) ){
		$(".btn-edit-area").css("top","116px");
		$("#header .navbar").addClass("sticky");
		$('#header .navbar .navbar-nav .collapse').collapse('hide');
	}
	else {
		$(".btn-edit-area").css("top","152px");
		$("#header .navbar").removeClass("sticky");
	}
});

$(window).load(function() {

	// definimos las variables necesarias para controlar las animaciones con scroll y que tengan en cuenat si hay sticky header o no

	// PARA DETENER EL CARRUSEL CUANDO HACES CLICK DENTRO DE UN ITEM (POR EJEMPLO EN LOS VIDEOS)
	var carruselvideo = $('.carousel-inner .item .responsive-video');
	if(carruselvideo.length > 0){
		$('.carousel-inner .item .responsive-video').prepend('<div class="responsive-video-cover" style="position: absolute;left:0;right:0;top:0;bottom:0">');
		$('.carousel-inner .item .responsive-video .responsive-video-cover').on('click', function (e) {
			var carrusel = $(this).parents('.carousel');
			$(carrusel).carousel('pause');
			$(this).hide();
		});
	}

	// PARA MENSAJES DE CONFIRMACION Y DE ERROR DE LOS FORMULARIOS

	if ( $("#successform").length ) {
		$('html, body').animate({scrollTop: $('#successform').offset().top - headerHeight}, 1000);
	}
	if ( $("#errorform").length ) {
		$('html, body').animate({scrollTop: $('#errorform').offset().top - headerHeight}, 1000);
	}
	if ( $(".webform .alert-danger.alert-dismissible").length ) {
		$('html, body').animate({scrollTop: $('.webform .alert-danger.alert-dismissible').offset().top - headerHeight}, 1000);
	}

	//cambiar estilos en iframes de servicios

/*	if($('#iframeservicios').length) {
		var $iframehead = $('#iframeservicios').contents().find('head');
		$iframehead.append('<style type="text/css">#cabecera,#pie {display: none !important}</style>');
	}*/

});




