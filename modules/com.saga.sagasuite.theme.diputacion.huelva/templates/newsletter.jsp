<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.newsletter.messages">
	<!DOCTYPE html>
	<html>
	<head>
		<!-- Controlamos que la pagina sea editable o no a traves de la property de editable -->
		<c:if test="${empty edit || edit=='true'}">
			<cms:enable-ade/>
		</c:if>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title><cms:info property="opencms.title" /></title>

		<style>
			@import url(http://fonts.googleapis.com/css?family=Open+Sans:400,300);/*font-family: 'Open Sans', sans-serif;*/
			@import url('https://fonts.googleapis.com/css?family=Montserrat:700');
			body,
			table tr td,
			p,
			div {
				font-family: "Open Sans","Helvetica","Arial",sans-serif !important;
			}
			h1,h2,h3,h4,h5 {
				font-family: "Montserrat","Helvetica","Arial",sans-serif !important;
			}
			a {
				text-decoration:underline;
			}

		</style>
	</head>
		<%-- cargamos en una variable la ruta de la pagina donde esta el boletin --%>

	<c:set var="rutaboletin"><cms:link>${cms.requestContext.uri }</cms:link></c:set>

	<body bgcolor="#E0E0E0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0">

	<!--TABLA FONDO Y CENTRADO 3 COLUMNAS-->

	<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td valign="top">
				<table border="0" cellspacing="0" cellpadding="0" align="center" width="800">
					<tr>
						<td>
							<p style="color: #eee;text-align: center;padding: 7px 0;margin: 0; background-color: #333;">
								<span style="font-size: 11px; color: #eee; font-family: Arial,Helvetica,sans-serif;">
									<fmt:message key="key.newsletter.ifyoudontsee" />&nbsp;<a href="${rutaboletin }"><span style="color: #fff;text-decoration: underline"><fmt:message key="key.newsletter.clickhere" /></span></a>
								</span>
							</p>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td valign="top">
				<table border="0" cellspacing="0" cellpadding="0" align="center" width="800">
					<tr>
						<td>
							<!--HEADER-->
							<cms:container name="boletin-container-header" type="newslettercontent" width="1200" maxElements="20">
								<div style="padding: 30px;background: #e4f5fc;font-size: 18px; color: #0C417A; font-family: Arial,Helvetica,sans-serif;">
									<div><strong>Zona de header</strong> a la que arrastrar el recurso de newsletter box que sirva como cabecera</div>
								</div>
							</cms:container>
							<!--HEADER-->
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="0" align="center" width="800" bgcolor="#fff">
					<tr>
						<td>
							<!--TABLA NEWSLETTER 1 COLUMNA-->
							<table border="0" cellspacing="0" cellpadding="0" width="100%">
								<!--Noticias y revistas -->
								<tr>
									<td>
										<cms:container name="boletin-container-main" type="layoutrowsonly" width="1200" maxElements="20">
											<div style="padding: 60px 30px 60px 30px;background: #fff;font-size: 18px; color: #666; font-family: Arial,Helvetica,sans-serif;">
												<div><strong>Zona de contenido</strong> a la que arrastrar los Newsletter Layout Rows</div>
											</div>
										</cms:container>
									</td>
								</tr>
								<!--fin Noticias -->

								<tr>
								</tr>
								<!--fin filas de noticias-->
							</table>
							<!--FIN TABLA NEWSLETTER-->
						</td>
					</tr>
				</table>
				<!--FIN TABLA FONDO Y CENTRADO-->
			</td>
		</tr>
	</table>
	<table border="0" cellspacing="0" cellpadding="0" width="100%">
		<tr>
			<td>
				<table border="0" cellspacing="0" cellpadding="0" align="center" width="800">
					<tr>
						<td>
							<!--footer-->
							<cms:container name="boletin-container-footer" type="newslettercontent" width="1200" maxElements="20">
								<div style="padding: 30px;background: #fefcea;font-size: 18px; color: #7A734B; font-family: Arial,Helvetica,sans-serif;">
									<div><strong>Zona de footer</strong> a la que arrastrar el recurso de newsletter box que sirva como pie</div>
								</div>
							</cms:container>
							<!--footer-->
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	</body>
	</html>
</cms:bundle>