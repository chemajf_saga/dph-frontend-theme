<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>

<!DOCTYPE html>
<html lang="${cms.locale}">
<head>
  <title>
    <cms:info property="opencms.title" />
  </title>
  <meta charset="${cms.requestContext.encoding}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <%-- Las versiones recientes de IE al no leer los condicionales cogen tambien esta css--%>
  <!--[if !IE]><!-->
  <c:if test="${empty skin }">
    <link rel="stylesheet" type="text/css" href="<cms:link>/system/modules/${theme}/skins/skin-default/css/screen.css</cms:link>" id="style-skin" />
  </c:if>
  <c:if test="${not empty skin}">
    <link rel="stylesheet" type="text/css" href="<cms:link>/system/modules/${theme}/skins/${skin}/css/screen.css</cms:link>" id="style-skin" />
  </c:if>
  <!--<![endif]-->

  <%-- Especifico para versiones antiguas de IE que leen los condicionales --%>
  <!--[if lte IE 9]>
  <c:if test="${empty skin }">
    <link rel="stylesheet" type="text/css" href="<cms:link>/system/modules/${theme}/skins/skin-default/css/screen-bootstrap-for-ie.css</cms:link>" />
    <link rel="stylesheet" type="text/css" href="<cms:link>/system/modules/${theme}/skins/skin-default/css/screen-saga-for-ie.css</cms:link>" />
  </c:if>
  <c:if test="${not empty skin }">
    <link rel="stylesheet" type="text/css" href="<cms:link>/system/modules/${theme}/skins/${skin}/css/screen-bootstrap-for-ie.css</cms:link>" />
    <link rel="stylesheet" type="text/css" href="<cms:link>/system/modules/${theme}/skins/${skin}/css/screen-saga-for-ie.css</cms:link>" />
  </c:if>
  <![endif]-->
  <%-- Cargamos la css custom definida por properties --%>
  <c:if test="${not empty csscustom}">
    <link rel="stylesheet" type="text/css" href="<cms:link>${csscustom}</cms:link>" />
  </c:if>

  <cms:headincludes type="javascript" defaults="%(link.weak:/system/modules/com.saga.sagasuite.core.script/resources/jquery/1.10.2/jquery-1.10.2.min.js:927c45ea-7f61-11e3-9005-f18cf451b707)
	|%(link.weak:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/resources/js/bootstrap.min.js:960b8ed1-e555-11e7-a388-63f9a58dae09)
	|%(link.weak:/system/modules/com.saga.sagasuite.core.script/resources/jquery.validate/1.13.1/jquery.validate-1.13.1.min.js:97ebf687-c8ca-11e4-b15d-01e4df46f753)"/>

  <cms:enable-ade/>
</head>
<body>
<div style="padding: 0.5em;">
<c:if test="${themeconfig.value.Logo.isSet}">
  <div class="margin-bottom-10 text-center">
    <img src="<cms:link>${themeconfig.value.Logo}</cms:link>" width="110">
  </div>
</c:if>
<div>
  <cms:container name="container" type="content" width="1200" maxElements="1" editableby="ROLE.DEVELOPER"/>
</div>

</div>
</body>
</html>