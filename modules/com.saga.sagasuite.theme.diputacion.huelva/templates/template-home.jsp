<%@page buffer="none" session="false" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<fmt:setLocale value="${cms.locale}" />

<cms:bundle basename="com.saga.sagasuite.theme.diputacion.huelva.messages">


<!DOCTYPE html>
<%-- 
El atributo 'googleShareHead' incluye el espacio de nombres de shcema.org para declarar el tipo de pagina.
Lo usa Google+ para determinar el tipo de publicacion. Este atributos se carga en 
/system/modules/com.saga.sagasuite.core/templates/load-themeconfig.jsp (plantilla por defecto) con el tipo 'Article' 
en caso de vistas de detalle y 'Organization' en cualquier otra pagina
--%>
<html lang="${cms.locale}" ${googleShareHead}>
<head>

<title><cms:info property="opencms.title" /></title>	
<meta charset="${cms.requestContext.encoding}">
<meta http-equiv="X-UA-Compatible" content="IE=edge"><%-- Activamos para que Internet explorer muestra la página sin modo de compatibilidad en la versión exacta del navegador --%>
<meta name="viewport" content="width=device-width, initial-scale=1.0"> <%-- Mantener proporciones partiendo del ancho del dispositivo donde se reproduce --%>
<meta name="description" content="${descriptionpage }">
<meta name="keywords" content="${keywordspage }">
<meta name="robots" content="index, follow">
<meta name="revisit-after" content="7 days">

<%-- Incluimos el script de los Metas desde el loadconfig del sagasuite core o mediante property configurable en la pagina --%>
	${metas}
<%-- fin de metas --%>	
<%-- Incluimos metas de todas las redes sociales (OpenGraph, Facebook, Twitter, Google+)--%>
${socialMetas}
<%-- fin de metas redes sociales --%>

	<%-- Inlcuimos script definido en la propiedad sagasuite.scripttag.afterMeta --%>
	<c:out value="${scriptTagAfterMeta}" escapeXml="false"/>

<!-- BLOQUE: Stylesheets --------------------------------------------------------------------------->

	<cms:headincludes type="css" closetags="false" defaults="%(link.weak:/system/modules/com.saga.sagasuite.core.script/resources/bx-slider/jquery.bxslider.min.css:51ec326f-1520-11e7-88c4-7fb253176922)" />

		<%-- Las versiones recientes de IE al no leer los condicionales cogen tambien esta css--%>
	<!--[if !IE]><!-->
	<c:if test="${empty skin }">
		<c:set var="defaultrutacss" value="/system/modules/${theme}/skins/skin-default/css/screen.css" />
		<link rel="stylesheet" type="text/css" href="<cms:link>/system/modules/${theme}/skins/skin-default/css/screen.css</cms:link>?v=${cms.vfs.resource[defaultrutacss].version}" />
	</c:if>
	<c:if test="${not empty skin}">
		<c:set var="skintrutacss" value="/system/modules/${theme}/skins/${skin}/css/screen.css" />
		<link rel="stylesheet" type="text/css" href="<cms:link>/system/modules/${theme}/skins/${skin}/css/screen.css</cms:link>?v=${cms.vfs.resource[skintrutacss].version}" />
	</c:if>
	<!--<![endif]-->

		<%-- Especifico para versiones antiguas de IE que leen los condicionales --%>
	<!--[if lte IE 9]>
	<c:if test="${empty skin }">
		<c:set var="iedefaultrutabscss" value="/system/modules/${theme}/skins/skin-default/css/screen-bootstrap-for-ie.css" />
		<c:set var="iedefaultrutasagacss" value="/system/modules/${theme}/skins/skin-default/css/screen-saga-for-ie.css" />
		<link rel="stylesheet" type="text/css" href="<cms:link>/system/modules/${theme}/skins/skin-default/css/screen-bootstrap-for-ie.css</cms:link>?v=${cms.vfs.resource[iedefaultrutabscss].version}" />
		<link rel="stylesheet" type="text/css" href="<cms:link>/system/modules/${theme}/skins/skin-default/css/screen-saga-for-ie.css</cms:link>?v=${cms.vfs.resource[iedefaultrutasagacss].version}" />
	</c:if>
	<c:if test="${not empty skin }">
		<c:set var="ieskinrutabscss" value="/system/modules/${theme}/skins/${skin}/css/screen-bootstrap-for-ie.css" />
		<c:set var="ieskinrutasagacss" value="/system/modules/${theme}/skins/${skin}/css/screen-saga-for-ie.css" />
		<link rel="stylesheet" type="text/css" href="<cms:link>/system/modules/${theme}/skins/${skin}/css/screen-bootstrap-for-ie.css</cms:link>?v=${cms.vfs.resource[ieskinrutabscss].version}" />
		<link rel="stylesheet" type="text/css" href="<cms:link>/system/modules/${theme}/skins/${skin}/css/screen-saga-for-ie.css</cms:link>?v=${cms.vfs.resource[ieskinrutasagacss].version}" />
	</c:if>
	<![endif]-->

<%-- Cargamos la css custom definida por properties --%>

<c:if test="${not empty csscustom}">
	<link rel="stylesheet" type="text/css" href="<cms:link>${csscustom}</cms:link>" />
</c:if>


	<!-- FIN BLOQUE: Stylesheets -->


<!-- BLOQUE: JavaScript --------------------------------------------------------------------------------------------------->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>

	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

	<![endif]-->

	<script src="https://www.google.com/recaptcha/api.js" async defer></script>


<%-- Incluimos el script de analytics desde el loadconfig del sagasuite core o mediante property configurable en la pagina --%>
	
	<c:if test="${not empty analytics}">
		${analytics}
	</c:if>

<%-- fin de analytics --%>		
	
<!-- FIN BLOQUE: JavaScript -->

	
<!-- Controlamos que la pagina sea editable o no a traves de la property de editable -->
<c:if test="${empty edit || edit=='true'}">
	<cms:enable-ade/>
</c:if>

<!-- fin -->

<!-- Fav and touch icons ----------------------------------------------------------------------------------------------------->

	<link rel="apple-touch-icon" sizes="180x180" href="<cms:link>/apple-touch-icon.png</cms:link>">
	<link rel="icon" type="image/png" sizes="32x32" href="<cms:link>/favicon-32x32.png</cms:link>">
	<link rel="icon" type="image/png" sizes="16x16" href="<cms:link>/favicon-16x16.png</cms:link>">
	<link rel="manifest" href="<cms:link>/manifest.json</cms:link>">
	<link rel="mask-icon" href="<cms:link>/safari-pinned-tab.svg</cms:link>" color="#71950a">
	<meta name="theme-color" content="#ffffff">

<!-- fin -->

	<%-- Inlcuimos script definido en la propiedad sagasuite.scripttag.endHead --%>
	<c:out value="${scriptTagEndHead}" escapeXml="false"/>
	
</head>

<!-- FIN HEAD -->

<body class="home ${pagecssclass }<cms:device type="desktop"> desktop-device</cms:device><cms:device type="tablet"> responsive-device tablet-device</cms:device><cms:device type="mobile"> responsive-device mobile-device</cms:device>">

<%-- Inlcuimos script definido en la propiedad sagasuite.scripttag.startBody --%>
<c:out value="${scriptTagStartBody}" escapeXml="false"/>

<cms:include file="/system/modules/com.saga.sagasuite.share/elements/sgfacebook-init-script.jsp"/>
<!-- Accesibilidad -->
<h1 class="sr-only"><cms:info property="opencms.title" /></h1>
<a href="#content-home" class="sr-only" accesskey="s"><fmt:message key="label.access.jump.content"/></a>

<c:if test="${not cms.requestContext.currentProject.onlineProject}">
	<!--=== Placeholder for OpenCms toolbar in the offline project ===-->
	<div style="background: lightgray; height: 35px" class="toolbar-offline-space">&nbsp;</div>
</c:if>

<div id="page" class="page-home">

	<!-- Header  -->
	<div id="header">
		<div class="wrapper">
			<cms:container name="header-container" type="page" width="1200"  maxElements="2" editableby="ROLE.DEVELOPER">
				<div class="alert alert-info lead">
					<strong>Container de Cabecera</strong> Arrastre aquí los recursos de tipo "Cabecera".
				</div>
			</cms:container>
		</div>	
	</div>
	<!-- End Main Header -->
	<%-- tematicas --%>
	<c:set var="themesonfixedbutton">${themeConfiguration.CustomFieldKeyValue.themesonfixedbutton}</c:set>
	<c:if test="${themesonfixedbutton == 'true' }">
		<%-- boton tematicas fijo en lateral de pagina --%>
		<button class="btn btn-specific-third btn-tematicas" data-target="tematicaspopup"><fmt:message key="key.template.element.tematicas" /><span class="pe-7s-more fs20 v-align-m pl-20" aria-hidden="true"></span></button>
	</c:if>
	<div id="tematicaspopup" class="template-container subheader-tematicas subheader-tematicas-popup">
		<div class="wrapper">
			<div class="container">
				<cms:include file="%(link.weak:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/e-temas-popup.jsp:92e7b9ab-e555-11e7-a388-63f9a58dae09)" />
			</div>
		</div>
		<button class="close"><span class="icon-close">&times;</span></button>
	</div>
	<%-- si se ha marcado el campo en el .themeconfig se muestra la caja de compartir en redes sociales --%>
	<c:if test="${themeConfiguration.CustomFieldKeyValue.socialshare == 'true'}">
		<%-- bloque de compartir en rede sociales --%>
		<cms:include file="%(link.weak:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/e-share.jsp:92c73955-e555-11e7-a388-63f9a58dae09)" />
		<%-- fin bloque redes sociales --%>
	</c:if>
<!-- Main Page Content and Sidebar -->
<div id="content-home" class="content content-home">
	<div class="main-content">
		<div class="wrapper">
			<div class="detalle">
					<cms:container name="wrapper-container" type="page" width="1200"  maxElements="10" editableby="ROLE.DEVELOPER">
						<div class="alert alert-info lead">
							<strong>Container de Contenido principal</strong> Arrastre aquí los recursos de tipo "Template row".
						</div>
					</cms:container>
			</div>
        </div>   
	</div>	
</div>

	<!-- footer -->
	<footer id="footer">
			<%-- footer superior --%>
		<div class="template-container footer">
			<div class="wrapper">
				<div class="container">
					<div class="row layout-4-col">
							<%-- columna 1 --%>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<cms:container name="footer-col1-container" type="content" width="300"  maxElements="10" editableby="ROLE.DEVELOPER">
								<div class="alert alert-info lead">
									<strong>Container de Footer - columna 1</strong> Arrastre aquí los recursos.
								</div>
							</cms:container>
						</div>
							<%-- columna 2 --%>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<cms:container name="footer-col2-container" type="content" width="300"  maxElements="10" editableby="ROLE.DEVELOPER">
								<div class="alert alert-info lead">
									<strong>Container de Footer - columna 2</strong> Arrastre aquí los recursos.
								</div>
							</cms:container>
						</div>
							<%-- columna 3 --%>
						<div class="col-xs-12 col-sm-12 col-md-4">
							<cms:container name="footer-col4-container" type="content" width="300"  maxElements="10" editableby="ROLE.DEVELOPER">
								<div class="alert alert-info lead">
									<strong>Container de Footer - columna 4</strong> Arrastre aquí los recursos.
								</div>
							</cms:container>
						</div>
						<%-- columna 4 ancho completo--%>
<%--						<div class="col-xxs-12 mt-30">
							<cms:container name="footer-col3-container" type="content" width="1200"  maxElements="10" editableby="ROLE.DEVELOPER">
								<div class="alert alert-info lead">
									<strong>Container de Footer - columna 4</strong> Arrastre aquí los recursos.
								</div>
							</cms:container>
						</div>--%>
					</div>
				</div>
			</div>
		</div>
			<%-- copyright --%>
		<div class="template-container copyright">
			<div class="wrapper">
				<div class="container">
					<cms:include file="%(link.weak:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/elements/e-copyright.jsp:925eda7b-e555-11e7-a388-63f9a58dae09)" />
				</div>
			</div>
		</div>
	</footer>


</div>

<%-- JAVASCRIPT --%>

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

	<%-- Incluimos scripts definidos en la configurión de formatters --%>

<cms:headincludes type="javascript" defaults="%(link.weak:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/resources/js/smooth-scroll.js:96405a7d-e555-11e7-a388-63f9a58dae09)
	|%(link.weak:/system/modules/com.saga.sagasuite.core.script/resources/magnific-popup/1.1.0/js/jquery.magnific-popup.min.js:af29c0df-06f2-11e6-8cce-7fb253176922)
	|%(link.weak:/system/modules/com.saga.sagasuite.core.script/resources/sagasuite/equal-height-boxes-dynamic.js:b50ade9f-cb9a-11e7-918b-7fb253176922)
	|%(link.weak:/system/modules/com.saga.sagasuite.core.script/resources/perfect-scrollbar/0.6.7/js/min/perfect-scrollbar.jquery.min.js:7f231a74-9358-11e5-8845-7fb253176922)
	|%(link.weak:/system/modules/com.saga.sagasuite.core.script/resources/holder/2.3.1/holder.min.js:13e5e548-fc55-11e3-a209-f18cf451b707)
	|%(link.weak:/system/modules/com.saga.sagasuite.share/resources/js/sgshare-facebook.js:91e154a6-5ac8-11e4-8e60-f18cf451b707)
	|%(link.weak:/system/modules/com.saga.sagasuite.core.script/resources/jquery.validate/1.13.1/jquery.validate-1.13.1.min.js:97ebf687-c8ca-11e4-b15d-01e4df46f753)
	|%(link.weak:/system/modules/com.saga.sagasuite.core.script/resources/bx-slider/jquery.bxslider.min.js:76b982b9-3e37-11e4-87b7-51ecafae78b6)
	|%(link.weak:/system/modules/com.saga.sagasuite.theme.diputacion.huelva/resources/js/theme.js:964cb68f-e555-11e7-a388-63f9a58dae09)" />

<c:if test="${cms.requestContext.currentProject.onlineProject}">
	<script src="<cms:link>/system/modules/com.saga.sagasuite.core.script/resources/tinymce/4.1.7/tinymce.min.js</cms:link>" type="text/javascript"></script>
</c:if>

	<%-- Cargamos el js custom definida por properties --%>
<c:if test="${not empty jscustom}">
	<script src="<cms:link>${jscustom}</cms:link>" type="text/javascript" ></script>
</c:if>

<%-- Inlcuimos script definido en la propiedad sagasuite.scripttag.endBody --%>
<c:out value="${scriptTagEndBody}" escapeXml="false"/>
	<%-- boton de acceso al area de edicion definida la url en el .themeconfig solo visible en proyecto offline--%>
	<c:if test="${not cms.requestContext.currentProject.onlineProject}">
		<c:if test="${not empty themeConfiguration.CustomFieldKeyValue.EditAreaPage}">
			<a href="<cms:link>${themeConfiguration.CustomFieldKeyValue.EditAreaPage}</cms:link>" class="visible-lg btn btn-info btn-edit-area tooltip-left" title="Ir al &Aacute;rea de edici&oacute;n de recursos din&aacute;micos: noticias, eventos, anuncios... etc." style="position:fixed;right:0;top:152px;z-index:10000">
				<span class="fa fa-pencil v-align-m pr-10" aria-hidden="true"></span>
				&Aacute;rea de edici&oacute;n
			</a>
		</c:if>
	</c:if>
</body>

</html>

</cms:bundle>