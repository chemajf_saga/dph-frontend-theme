<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" buffer="none" session="false"
	trimDirectiveWhitespaces="true"%>
<%@ page import="java.util.*"%>
<%@ page import="java.sql.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
    System.out.println("---Inicio test-db.jsp");

    List<Map<String, String>> res = new LinkedList<Map<String, String>>();

    /*String idMun = "25510";
    String sql = 
    "SELECT  p1.nom_poblacion, "
    + "lista_de(p2.nom_poblacion) as nucleos, "
    + "p1.bandera,  "
    + "p1.escudo,  "
    + "p1.hombres,  "
    + "p1.mujeres,  "
    + "p1.habitantes,  "
    + "p1.hombres_agrupados,  "
    + "p1.mujeres_agrupadas,  "
    + "p1.habitantes_agrupados,  "
    + "p1.fecha_padron,  "
    + "p1.latitud,  "
    + "p1.longitud,  "
    + "p1.nivel_zoom,  "
    + "p1.limites,  "
    + "p1.distancia_capital,  "
    + "p1.contenido as idGaleria,  "
    + "p1.corporacion_municipal,  "
    + "p1.enlaces_interes,  "
    + "p1.texto,  "
    + "p1.ayto_direccion,  "
    + "p1.ayto_cpostal,  "
    + "p1.ayto_telefono,  "
    + "p1.ayto_fax,  "
    + "p1.ayto_email,  "
    + "p1.ayto_web, "
    + "SUBSTR(p1.codigo_ine::TEXT,0,6) as codigo_ine, "
    + "p1.color_fondo, "
    + "p1.color_texto, "
    + "p1.alineacion "
    + "FROM v_poblaciones p1 "
    + "LEFT OUTER JOIN v_poblaciones p2 "
    + "ON p1.poblacion = p2.poblacion_sup "
    + "WHERE p1.poblacion = '" + idMun + "' "
    + "GROUP BY 1,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30"; */

    //String sql = "SELECT poblacion, nom_poblacion FROM v_poblaciones_nomat WHERE provincia=21 AND tipo='M' ORDER BY tipo DESC, nom_poblacion";
    //String sql = "SELECT * FROM v_poblaciones";

    //String SIGLAS = "C\\'S";
    String sql = "SELECT * FROM public.v_diputados ORDER BY partido";
    // WHERE siglas_partido = '" + SIGLAS + "' ORDER BY orden_partido";

    Connection conexion = null;
    Statement sentencia = null;
    ResultSet rs = null;

    try {
        Class.forName("org.postgresql.Driver");
        System.out.println("Puente JDBC-ODBC cargado (test-db.jsp)!");
    } catch (Exception e) {
        System.out.println("ERROR (test-db.jsp): No se pudo cargar el puente JDBC-ODBC.");
        e.printStackTrace();
        return;
    }

    try {
	    /*DESA*/conexion = DriverManager.getConnection("jdbc:postgresql://192.168.131.30:5432/portalweb", "ocmsdph", "ocmsdph");
	    //PROD  conexion = DriverManager.getConnection("jdbc:postgresql://192.168.136.33:5432/portalweb", "ocmsdph", "ocmsdph");
        System.out.println("Conexion realizada (test-db.jsp)");
    } catch (Exception er) {
        System.out.println("ERROR obteniendo conexion (test-db.jsp): " + er);
        er.printStackTrace();
        return;
    }

    try {
        sentencia = conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        System.out.println("Sentencia creada (test-db.jsp)");
    } catch (Exception er) {
        System.out.println("ERROR obteniendo sentencia (test-db.jsp): " + er);
        er.printStackTrace();
        return;
    }

    int i = 0;
    try {
        System.out.println("Realizamos la consulta (test-db.jsp): " + sql);
        rs = sentencia.executeQuery(sql);

        // OBTENER NOMBRE DE COLUMNAS
        ResultSetMetaData metaData = rs.getMetaData();
        int rowCount = metaData.getColumnCount();
        String[] names = new String[rowCount];
        System.out.println("Table Name : " + metaData.getTableName(2));
        System.out.println("Field  \tDataType");
        for (int j = 0; j < rowCount; j++) {
            names[j] = metaData.getColumnName(j + 1);
            System.out.print(metaData.getColumnName(j + 1) + "  \t");
            System.out.println(metaData.getColumnTypeName(j + 1));
        }

        while (rs.next()) {

            Map<String, String> aux = new HashMap<String, String>();
            for(String name : names){
	            aux.put(name, rs.getString(name));
            }

            res.add(i, aux);
            i++;
        }

    } catch (Exception er) {
        System.out.println("ERROR realizando consulta (test-db.jsp): " + er);
        er.printStackTrace();
        return;
    }

    System.out.println("Resltado de la consulta (test-db.jsp): [" + sql + "]");
    out.println("<h1>Resltado de la consulta (test-db.jsp): [" + sql + "]</h1>");
    for (Map<String, String> m : res) {
        Set<String> keys = m.keySet();
        for (String key : keys) {
            System.out.print("\t" + key + "::" + m.get(key) + "");
            out.print("<p>::" + key + "::" + m.get(key) + "::</p>");
        }
        System.out.println("\t");
    out.println("<hr/>");
    }
    out.println("<hr/>");
    out.println("<hr/>");

    conexion.close();
    System.out.println("---FIN test-db.jsp");
%>
<h2>TABLA: - v_poblaciones</h2>
<p>provincia - numeric</p>
<p>poblacion - bpchar</p>
<p>poblacion_sup - bpchar</p>
<p>codigo_ine - numeric</p>
<p>tipo - bpchar</p>
<p>nom_poblacion - varchar</p>
<p>bandera - varchar</p>
<p>escudo - varchar</p>
<p>hombres - int4</p>
<p>mujeres - int4</p>
<p>habitantes - int4</p>
<p>hombres_agrupados - int4</p>
<p>mujeres_agrupadas - int4</p>
<p>habitantes_agrupados - int4</p>
<p>fecha_padron - date</p>
<p>latitud - numeric</p>
<p>longitud - numeric</p>
<p>nivel_zoom - numeric</p>
<p>limites - varchar</p>
<p>distancia_capital - int2</p>
<p>contenido - int4</p>
<p>corporacion_municipal - text</p>
<p>enlaces_interes - text</p>
<p>texto - text</p>
<p>ayto_direccion - varchar</p>
<p>ayto_cpostal - numeric</p>
<p>ayto_telefono - varchar</p>
<p>ayto_fax - varchar</p>
<p>ayto_email - varchar</p>
<p>ayto_web - varchar</p>
<p>color_fondo - varchar</p>
<p>color_texto - varchar</p>
<p>alineacion - bpchar</p>
<p>idfichero - int4</p>
<p>---------------------------</p>
<h2>TABLA: - public.v_diputados</h2>
<p>nombre - varchar</p>
<p>apellido1 - varchar</p>
<p>apellido2 - varchar</p>
<p>persona - bpchar</p>
<p>genero - bpchar</p>
<p>tipo_documento - bpchar</p>
<p>documento - varchar</p>
<p>curriculum - text</p>
<p>competencias - varchar</p>
<p>posicion_pleno - int4</p>
<p>cargo_municipio - varchar</p>
<p>poblacion - bpchar</p>
<p>nom_poblacion - varchar</p>
<p>codpartido - int4</p>
<p>siglas_partido - varchar</p>
<p>orden_partido - int4</p>
<p>codjudicial - int4</p>
<p>partidojudicial - varchar</p>
<p>equipogobierno - int4</p>
<p>coddepartamento - int4</p>
<p>departamento - varchar</p>
<p>imagen_ficha - varchar</p>
<p>descripcion_imagen_ficha - varchar</p>
<p>imagen_tesela - varchar</p>
<p>descripcion_imagen_tesela - varchar</p>
<p>idgaleria - int4</p>
