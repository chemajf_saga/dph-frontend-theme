<%@page buffer="none" session="false" taglibs="c,cms,fmt"%>
<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.header.workplace">

<%-- MODELO CABECERA DESKTOP --%>

<cms:device type="desktop">

<div id="headerblock">

	<div id="${content.value.Id }-header07" class="header07">
	
		<div id="header-container">
			<div class="header-top">
				<div class="navbar navbar-default">
					<div class="container">
						<div class="navbar-header">
							<button data-target=".navbar-top-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
			                    <span class="icon-bar"></span>
			                    <span class="icon-bar"></span>
			                    <span class="icon-bar"></span>
		              		</button>						
						</div>					
						<div class="navbar-top-collapse navbar-collapse collapse">
							<c:if test="${content.value.Block1.exists }">
								<c:set var="elements" value="${content.subValueList['Block1'] }" scope="request"/>
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
							</c:if>
							<div class="pull-right">
								<c:if test="${content.value.Block2.exists }">
									<c:set var="elements" value="${content.subValueList['Block2'] }" scope="request"/>
									<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
								</c:if>
							</div>
						</div>					
					</div>
				</div>			
			</div>
		</div>
		<div class="container">
			<div id="header-inf">
				<div class="row">
					<div class="col-sm-4 logo">
						<c:if test="${content.value.Block3.exists }">
							<c:set var="elements" value="${content.subValueList['Block3'] }" scope="request"/>
							<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
						</c:if>
					</div>
					<div class="col-sm-8 lema">
						<div class="pull-right">
							<c:if test="${content.value.Block4.exists }">
								<c:set var="elements" value="${content.subValueList['Block4'] }" scope="request"/>
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
							</c:if>	
						</div>					
					</div>
				</div>	
			</div>			
			<div id="navigation">
				<div class="navbar navbar-default" role="navigation">
						<div class="navbar-header">
							<button data-target=".navbar-navigation-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
	                   			<span class="sr-only"><fmt:message key="label.header.menu.secciones"/></span>
			                    <span class="icon-bar"></span>
			                    <span class="icon-bar"></span>
			                    <span class="icon-bar"></span>
	               			</button>
						</div>
						<div class="navbar-navigation-collapse navbar-collapse collapse">
							<c:if test="${content.value.Block5.exists }">
								<c:set var="elements" value="${content.subValueList['Block5'] }" scope="request"/>
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
							</c:if>						
						</div>
				</div>
			</div>		
		</div>
	</div>

</div>

</cms:device>
<%-- FIN DESKTOP --%>

<%-- MODELO CABECERA TABLET Y MOVIL --%>

<cms:device type="tablet, mobile">

<div id="${content.value.Id }-header07" class="header07 responsive">


	<div id="header-container">
		<div class="header-top">
			<div class="navbar navbar-default">
				<div class="container">
					<div class="navbar-header">
						<button data-target=".navbar-top-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
	              		</button>						
					</div>					
					<div class="navbar-top-collapse navbar-collapse collapse">
						<c:if test="${content.value.Block2.exists }">
							<c:set var="elements" value="${content.subValueList['Block2'] }" scope="request"/>
							<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
						</c:if>
						<div class="pull-right">
							<c:if test="${content.value.Block3.exists }">
								<c:set var="elements" value="${content.subValueList['Block3'] }" scope="request"/>
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
							</c:if>
						</div>
					</div>					
				</div>
			</div>			
		</div>
	</div>
	<div class="container">
		<div id="header-inf">
			<div class="header">
				<div class="navbar navbar-default" role="navigation">
					<div class="container">
						<div class="navbar-header">
							<button data-target=".navbar-responsive-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    			<span class="sr-only">menu-title: ${menuConfig.Title} | navigation: <cms:property name="Title" file="${navStartFolder }"/></span>
			                    <span class="icon-bar"></span>
			                    <span class="icon-bar"></span>
			                    <span class="icon-bar"></span>
                			</button>
							<c:if test="${content.value.Block4.exists }">
								<c:set var="elements" value="${content.subValueList['Block4'] }" scope="request"/>
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
							</c:if>
						</div>
						<div class="navbar-responsive-collapse navbar-collapse collapse">
							<c:if test="${content.value.Block5.exists }">
								<c:set var="elements" value="${content.subValueList['Block5'] }" scope="request"/>
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
							</c:if>						
						</div>
					</div>
				</div>
			</div>
			<div class="navigation">
				<div class="navbar navbar-default" role="navigation">
						<div class="navbar-header">
							<button data-target=".navbar-navigation-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    			<span class="sr-only">Toggle navigation</span>
			                    <span class="icon-bar"></span>
			                    <span class="icon-bar"></span>
			                    <span class="icon-bar"></span>
                			</button>
						</div>
						<div class="navbar-navigation-collapse navbar-collapse collapse">
							<c:if test="${content.value.Block6.exists }">
								<c:set var="elements" value="${content.subValueList['Block6'] }" scope="request"/>
								<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.header/elements/e-printblock.jsp:3b86b999-63f1-11e3-b1ce-f18cf451b707)"/>
							</c:if>						
						</div>
				</div>
			</div>		
		</div>
	</div>
</div>
</cms:device>
<%-- FIN TABLET Y MOVIL --%>


</cms:bundle>
