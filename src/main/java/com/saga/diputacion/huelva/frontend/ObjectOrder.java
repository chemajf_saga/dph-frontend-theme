/**
 * Package com.saga.diputacion.huelva.frontend
 * File ObjectOrder.java
 * Created at 2 nov. 2018
 * Created by chema.jimenez
 */
package com.saga.diputacion.huelva.frontend;

import java.util.Collections;
import java.util.Date;

/**
 * @author chema.jimenez
 * @since 2 nov. 2018
 *
 *        <br/><br/>
 *
 *        <div><h5>Descripci&oacute;n:</h5>Objeto para ordenar objetos que no implementan {@link Comparable}. Tiene tres
 *        atributos: <ul> <li><b>order</b></li> De tipo {@link Object}, es el objetopor el cual ordenaremos y puede ser
 *        de tipo {@link Date}, {@link String} o {@link Integer} <li><b>object</b></li> De tipo {@link Object}, es el
 *        objeto que no implementa {@link Comparable} y que queremos ordenar <li><b>validarte</b></li> De tipo
 *        {@link Boolean}, indica si el objeto es válido y el objeto es válido solo si <i><b>order</b></i> es de tipo
 *        {@link Date}, {@link String} o {@link Integer} </ul></div> <br/> <div><h5>Modo de empleo:</h5>Esta clase
 *        <i>{@link ObjectOrder}</i> implementa la interfaz {@link Comparable}, con lo que son objetos que se pueden
 *        ordenar untilizando {@link Collections}.<br/> </div> <br/> <div> <h5>Ejemplo:</h5> Supongamos que tenemos un
 *        lista de objetos de tipo <i>NotOrdenableObject</i> que <b>no implementan</b> {@link Comparable} y que tienen
 *        un atributo (por ejemplo de tipo {@link Date}) por el que queremos ordenar.<br/> Tenemos que crear un listado
 *        de objetos {@link ObjectOrder} donde el atributo <b>order</b> sea el objeto de tipo Date (de
 *        NotOrdenableObject) y <b>object</b> sea el propio objeto NotOrdenableObject.<br/> Esta lista se podrá ordenar
 *        utilizando {@link Collections}, recorrerla de forma ordenada y a traves del método getObject() obtener el
 *        objeto de tipo <i>NotOrdenableObject</i>.Códifo de ejemplo:
 *
 *        <pre>
 *List<ObjectOrder> listaOrdenada = new ArrayList<ObjectOrder>();
 *int i = 0;
 *List<NotOrdenableObject> elementos = [...];
 *for (NotOrdenableObject elemento : elementos) {
 *    ObjectOrder auxOrder = (elemento.getDate(), elemento);
 *    listaOrdenada.add(i, auxOrder);
 *    i++;
 *}
 *Collections.sor(listaOrdenada); //OR Collections.reverse(listaOrdenada);
 *        </pre>
 *
 *        </div>
 */
public class ObjectOrder implements Comparable<ObjectOrder> {

    /**
     * Objeto por el cual ordenaremos. Tiene que ser de tipo {@link Date}, {@link String} o {@link Integer}
     */
    private Object order;
    /**
     * Objeto que no implementa {@link Comparable} y que queremos ordenar.
     */
    private Object object;
    /**
     * Indica si el objeto es válido y el objeto es válido <b>solo si</b> es de tipo {@link Date}, {@link String} o
     * {@link Integer}.
     */
    private boolean validate = false;

    /**
     *
     */
    public ObjectOrder() {
    }

    /**
     * @param order
     * @param object
     */
    public ObjectOrder(Object order, Object object) {
        super();
        if (this.validateOrdenacion(order)) {
            this.order = order;
            this.object = object;
        }
    }

    /**
     * valida que el atributo <b>order</b> es de tipo {@link Date}, {@link String} o {@link Integer}
     *
     * @param order Objeto por el que se ordea
     * @return <b>True</b> si el atributo es de tipo {@link Date}, {@link String} o {@link Integer} o <b>false</b> en
     *         caso contrario
     */
    private boolean validateOrdenacion(Object order) {
        this.validate = true;
        return (order instanceof Date || order instanceof String || order instanceof Integer);
    }

    /**
     * @return order
     */
    public Object getOrder() {
        return this.order;
    }

    /**
     *
     * @param order the order to set
     */
    public void setOrder(Object order) {
        this.order = order;
    }

    /**
     * @return object
     */
    public Object getObject() {
        return this.object;
    }

    /**
     * @param object the object to set
     */
    public void setObject(Object object) {
        this.object = object;
    }

    /**
     * @return validate
     */
    public boolean isValidate() {
        return this.validate;
    }

    /**
     * @param validate the validate to set
     */
    public void setValidate(boolean validate) {
        this.validate = validate;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(ObjectOrder o) {
        if (o.order instanceof Date && this.order instanceof Date) {
            return ((Date) this.order).compareTo((Date) o.order);
        } else {
            if (o.order instanceof String && this.order instanceof String) {
                return ((String) this.order).compareTo((String) o.order);
            } else {
                if (o.order instanceof Integer && this.order instanceof Integer) {
                    return ((Integer) this.order).compareTo((Integer) o.order);
                } else {
                    return 0;
                }
            }
        }
    }

}
